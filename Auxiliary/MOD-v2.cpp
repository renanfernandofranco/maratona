/**
 * multi-MOD struct
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
using ll = long long;

const int MODS [] = {1000'000'007, 1000'000'009, 1'000'000'207};
const int SZMOD = 3;

struct MINT{
  int v[SZMOD];

  MINT(ll val = 0){
    FOR(k, SZMOD){
      v[k] = (-MODS[k] <= val && val < MODS[k]) ? val : val % MODS[k];
      if(v[k] < 0) v[k] += MODS[k];
    }
  }

  MINT& operator *= (const MINT& m2){ return (*this) = this->operator*(m2); }
  MINT& operator += (const MINT& m2){ return (*this) = this->operator+(m2); }
  MINT& operator -= (const MINT& m2){ return (*this) = this->operator-(m2); }
  bool operator == (const MINT& m2) const { return equal(v, v + SZMOD, m2.v); }
  bool operator != (const MINT& m2) const { return !equal(v, v + SZMOD, m2.v); }
  bool operator < (const MINT& m2) const {
    return lexicographical_compare(v, v + SZMOD, m2.v, m2.v + SZMOD);
  }
  friend istream& operator >> (istream &is, MINT &a) { ll t; is >> t; a = MINT(t); return is; }

  MINT operator * (const MINT& m2) const{
    MINT ret;
    FOR(k, SZMOD)
      ret.v[k] = (v[k] * 1LL * m2.v[k]) % MODS[k];
    return ret;
  }

  MINT operator + (const MINT& m2) const{
    MINT ret; int tmp;
    FOR(k, SZMOD)
      ret.v[k] = (tmp = v[k] + m2.v[k]) >= MODS[k] ? tmp - MODS[k] : tmp;
    return ret;
  }

  MINT operator - (const MINT& m2) const{
    MINT ret; int tmp;
    FOR(k, SZMOD)
      ret.v[k] = (tmp = v[k] - m2.v[k]) < 0 ? tmp + MODS[k] : tmp;
    return ret;
  }

  friend MINT pw(MINT a, ll b){
    MINT ret = 1;
    while(b){
      if(b & 1) ret *= a;
      b >>= 1; a *= a;
    }
    return ret;
  }
};
