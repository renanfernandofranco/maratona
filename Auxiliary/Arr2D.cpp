/**
 * Array 2D - Access the field of a Matrix with a pair
 */

#include <bits/stdc++.h>

using namespace std;
using ii = pair<int, int>;
#define NONE make_pair(-1, -1)
#define FOR(i, b) for(int i = 0; i < (b); i++)

template<class T>
struct arr2D{
   vector<vector<T>> v;
   int x, y;
   arr2D(int x, int y):x(x), y(y), v(x, vector<T>(y)){}
   T& operator [] (ii p){ return v[p.first][p.second];}
   void set(T val){ FOR(i, x) FOR(j, y) v[i][j] = val; }
};
