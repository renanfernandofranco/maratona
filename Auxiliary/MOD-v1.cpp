/**
 * MOD struct
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;

template <int MOD>
struct MINT{
  int v;

  MINT(ll val = 0){
    v = (-MOD <= val && val < MOD) ? val : val % MOD;
    if(v < 0) v += MOD;
  }

  MINT operator - () const { return MINT(-v); }
  MINT operator * (const MINT& m2) const { return v * 1LL * m2.v % MOD; }
  MINT operator / (const MINT& m2) const { return (*this) * inv(m2); }
  MINT& operator *= (const MINT& m2){ return (*this) = (*this) * m2; }
  MINT& operator /= (const MINT& m2){ return (*this) = (*this) / m2; }
  MINT& operator += (const MINT& m2){ return (*this) = (*this) + m2; }
  MINT& operator -= (const MINT& m2){ return (*this) = (*this) - m2; }
  bool operator == (const MINT& m2) const { return v == m2.v; }
  bool operator != (const MINT& m2) const { return v != m2.v; }
  bool operator < (const MINT& m2) const { return v < m2.v; }
  friend istream& operator >> (istream &is, MINT &a) { ll t; is >> t; a = MINT(t); return is; }
  friend ostream& operator << (ostream &os, const MINT &a) { return os << a.v; }
  friend MINT inv(const MINT& a) { return pw(a, MOD - 2); } // only if MOD is prime

  MINT operator + (const MINT& m2) const{
    int tmp = v + m2.v;
    return tmp >= MOD ? tmp - MOD : tmp;
  }

  MINT operator - (const MINT& m2) const{
    int tmp = v - m2.v;
    return tmp < 0 ? tmp + MOD : tmp;
  }

  friend MINT pw(MINT a, ll b){
    MINT ret = 1;
    while(b){
      if(b & 1) ret *= a;
      b >>= 1; a *= a;
    }
    return ret;
  }
};

using mint = MINT<998'244'353>;