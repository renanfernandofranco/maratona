/**
 * Greatest and Smallest Shift Cyclic
 * Greatest and Smallest Suffix
 *
 * Complexity:
 *  - Time: O(n)
 *
 * Status: weakly tested
 *
 */

#include <bits/stdc++.h>

using namespace std;

string minCyclicString(string s) {
  s += s;
  int n = s.size();
  int i = 0, ans = 0, j, k;
  while (i < n / 2) {
    k = ans = i;
    j = i + 1;
    while (j < n && s[k] <= s[j]) {
      if (s[k] < s[j])
        k = i;
      else
        k++;
      j++;
    }
    while (i <= k)
      i += j - k;
  }
  return s.substr(ans, n / 2);
}

string maxCyclicString(string s) {
  s += s;
  int n = s.size();
  int i = 0, ans = 0, j, k;
  while (i < n / 2) {
    k = ans = i;
    j = i + 1;
    while (j < n && s[k] >= s[j]) {
      if (s[k] > s[j])
        k = i;
      else
        k++;
      j++;
    }
    while (i <= k)
      i += j - k;
  }
  return s.substr(ans, n / 2);
}

string greatestSuffix(string s) {
  int n = s.size();
  int i = 0, j = 0, ans = 0, k;
  while (i < n && j < n) {
    k = ans = i;
    j = i + 1;
    while (j < n && s[k] >= s[j]) {
      if (s[k] > s[j])
        k = i;
      else
        k++;
      j++;
    }
    while (i <= k)
      i += j - k;
  }
  return s.substr(ans);
}

// null character is greater than any other
string smallestSuffix(string s) {
  int n = s.size();
  int i = 0, j = 0, ans = 0, k;
  while (i < n && j < n) {
    k = ans = i;
    j = i + 1;
    while (j < n && s[k] <= s[j]) {
      if (s[k] < s[j])
        k = i;
      else
        k++;
      j++;
    }
    while (i <= k)
      i += j - k;
  }
  return s.substr(ans);
}
