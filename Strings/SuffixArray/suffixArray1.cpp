/**
 * Suffix Array
 * ============
 *
 * Complexity to build:
 *  - Time: O(n * (lg n)^2) - use radix sort to improve the complexity
 *  - Space: O(n)
 *
 * Operations:
 *  - LCP Array in O(n)
 *  - Check if a string P is a substring of T in O(|P| * lg |T|)
 *  - Count number of occurrences of the string P in string T in O(|P| * lg |T|)
 *  - Count number of different substrings in O(n)
 *  - Find the indices [l, r] of the k-smallest distinct substring in O(lg n) by query
 *
 * Status: slightly tested
 */

#include <bits/stdc++.h>
using namespace std;
using ll = long long;
using ii = pair<int, int>;

struct SuffixArray{
  struct Entry{
    int ft, sc, idx;
    bool operator < (const Entry& e2) const{
      return ft != e2.ft ? ft < e2.ft : (sc != e2.sc ? sc < e2.sc : idx < e2.idx);
    }
  };

  string T;
  int n, i, cnt, sz;
  vector<Entry> L;
  vector<int> rank, sa;

  SuffixArray(string& T): T(T){
    n = T.length();
    rank.resize(n);
    L.resize(n);
    sa.resize(n); // suffix array
    build();
  }

  void build(){
    for (i = 0; i < n; i ++)
      rank[i] = T[i];

    // if using in cyclic string, then change the stop criteria to "cnt != n - 1 && sz <= n"
    for (cnt = -1, sz = 1; cnt != n - 1; sz <<= 1) {
      for (i = 0; i < n; i ++){
        L[i].ft = rank[i];
        L[i].sc = i + sz < n ? rank[i + sz] : -1; // for cyclic string use L[i].sc = rank[(i + sz) % n];
        L[i].idx = i;
      }
      sort(L.begin(), L.end());
      for (cnt = -1, i = 0; i < n; i++)
        rank[L[i].idx] = (i > 0 && L[i].ft == L[i - 1].ft && L[i].sc == L[i - 1].sc) ? cnt : ++cnt;
    }

    for (i = 0; i < n; i++)
      sa[i] = L[i].idx;
  }

  bool find(string& pat){
    int m, ret, l = 0, r = n - 1;

    while (l <= r){
      m = l + r >> 1;
      ret = T.compare(sa[m], pat.length(), pat);
      if(ret == 0)
        return true;
      if(ret < 0)
        l = m + 1;
      else
        r = m - 1;
    }
    return false;
  }

  int count(string& pat){
    int m, ret, l = 0, r = n;

    while (l < r){
      m = l + r >> 1;
      ret = T.compare(sa[m], pat.length(), pat);
      if(ret < 0)
        l = m + 1;
      else
        r = m;
    }
    int lb = r;

    l = 0, r = n;
    while (l < r){
      m = l + r >> 1;
      ret = T.compare(sa[m], pat.length(), pat);
      if(ret <= 0)
        l = m + 1;
      else
        r = m;
    }

    return r - lb;
  }

  // Kasai Algorithm
  vector<int> lcpArray(){
    vector<int> lcp(n, 0);

    for (int k = 0, i = 0; i < n; i++, k ? k--: 0){
      if(rank[i] == n - 1){
        k = 0;
        continue;
      }
      int j = sa[rank[i] + 1];
      while(i + k < n && j + k < n && T[i + k] == T[j + k])
        k++;
      lcp[rank[i] + 1] = k; // Use lcp[rank[i]] = k to not skip the lcp[0]
    }
    return lcp;
  }
};

ll numberDifferSubstr(string s){
  int n = s.length();
  SuffixArray sa(s);
  vector<int> lcp = sa.lcpArray();
  ll res = 0;

  for (int i = 0; i < n; i++)
    res += n - sa.sa[i] - lcp[i];

  return res;
}

vector<ii> kSmallestSubstr(string s, vector<ll> querys){
  int n = s.length();
  SuffixArray sa(s);
  vector<ll> pref(n);
  vector<int> lcp = sa.lcpArray();

  ll acum = n - sa.sa[0];
  pref[0] = 0;

  for (int i = 1; i < n; i++){
    pref[i] = acum;
    acum += n - sa.sa[i] - lcp[i];
  }

  vector<ii> ans;
  for (ll q : querys){
    assert(q <= acum);

    int m = lower_bound(pref.begin(), pref.end(), q) - pref.begin() - 1;

    int leftIdx = sa.sa[m];
    int rightIdx = sa.sa[m] + lcp[m] + q - pref[m] - 1;
    ans.push_back({leftIdx, rightIdx});
  }

  return ans;
}
