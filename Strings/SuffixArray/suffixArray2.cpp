/**
 * Suffix Array
 * ============
 *
 * Complexity to build:
 *  - Time: O(n * (lg n)^2) - use radix sort to improve the complexity
 *  - Space: O(n * lg n)
 *
 * Operations:
 *  - Find LCP between two suffixs in O(lg n)
 *
 * Status: slightly tested
 */

#include <bits/stdc++.h>
using namespace std;
using ll = long long;

struct SuffixArray{
  struct Entry{
    int ft, sc, idx;
    bool operator < (const Entry& e2) const{
      return ft != e2.ft ? ft < e2.ft : (sc != e2.sc ? sc < e2.sc : idx < e2.idx);
    }
  };

  string T;
  int n, i, lvl, cnt, sz;
  vector<vector<int>> rank;
  vector<Entry> L;
  vector<int> sa;

  SuffixArray(string& T): T(T){
    n = T.length();
    int maxLog = ceil(log2(n)) + 1;
    rank.assign(maxLog, vector<int> (n));
    L.resize(n);
    sa.resize(n); // suffix array
    build();
  }

  void build(){
    for (i = 0; i < n; i ++)
      rank[0][i] = T[i];

    // if using in cyclic string, then change the stop criteria to "cnt != n - 1 && sz <= n"
    for (cnt = -1, lvl = 1, sz = 1; cnt != n - 1; lvl++, sz <<= 1) {
      for (i = 0; i < n; i ++){
        L[i].ft = rank[lvl - 1][i];
        L[i].sc = i + sz < n ? rank[lvl - 1][i + sz] : -1; // for cyclic string use L[i].sc = rank[lvl - 1][(i + sz) % n];
        L[i].idx = i;
      }
      sort(L.begin(), L.end());
      for (cnt = -1, i = 0; i < n; i++)
        rank[lvl][L[i].idx] = (i > 0 && L[i].ft == L[i - 1].ft && L[i].sc == L[i - 1].sc) ? cnt : ++cnt;
    }

    for (i = 0; i < n ; i++)
      sa[i] = L[i].idx;
  }

  int lcp(int x, int y){
    int k, ret = 0;

    if (x == y)
      return n - x;

    for (k = lvl - 1; k >= 0 && x < n && y < n; k --)
      if (rank[k][x] == rank[k][y]){
        x += 1 << k;
        y += 1 << k;
        ret += 1 << k;
      }

    return ret;
  }
};
