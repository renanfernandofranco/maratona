/**
 * Suffix Array
 * ============
 *
 * Complexity to build:
 *  - Time: O(n * lg n)
 *  - Space: O(n * lg n)
 *
 * Operations:
 *  - Find LCP between two suffixes in O(1)
 *
 * Status: slightly tested
 */

#include <bits/stdc++.h>
using namespace std;
using ll = long long;

struct SuffixArray{
  struct Entry{
    int ft, sc, idx;
  };

  #define GET(e, i) ((i) == 0 ? e.ft + 1 : e.sc + 1)
  string T;
  int n, i, maxLog, cnt, sz;
  vector<vector<int>> st;
  vector<Entry> L, tmp;
  vector<int> rank, sa, lg2, freq;

  SuffixArray(string& T): T(T){
    n = T.length();
    maxLog = ceil(log2(n)) + 1;
    st.assign(maxLog, vector<int> (n));
    lg2.resize(n + 1);
    rank.resize(n);
    L.resize(n);
    sa.resize(n); // suffix array

    // 128 is the size of the alphabet
    freq.resize(max(n , 128) + 1);
    tmp.resize(n);

    build();
    makeLcp();
  }

  void build(){
    for (i = 0; i < n; i ++)
      rank[i] = T[i];

    for (cnt = -1, sz = 1; cnt != n - 1; sz <<= 1) {
      for (i = 0; i < n; i ++){
        L[i].ft = rank[i];
        L[i].sc = i + sz < n ? rank[i + sz] : -1;
        L[i].idx = i;
      }
      radixSort();
      for (cnt = -1, i = 0; i < n; i++)
        rank[L[i].idx] = (i > 0 && L[i].ft == L[i - 1].ft && L[i].sc == L[i - 1].sc) ? cnt : ++cnt;
    }

    for (i = 0; i < n ; i++)
      sa[i] = L[i].idx;
  }

  void radixSort(){
    for (int k = 1; k >= 0; k --){
      fill(freq.begin(), freq.end(), 0);

      for (i = 0; i < n; i++)
        freq[GET(L[i], k)]++;

      for (i = 1; i < freq.size(); i++)
        freq[i] += freq[i - 1];

      for (i = n - 1; i >= 0; i--)
        tmp[--freq[GET(L[i], k)]] = L[i];
      tmp.swap(L);
    }
  }

  void makeLcp(){
    int i, j, k;
    lg2[1] = 0;
    for (i = 2; i <= n; i++)
      lg2[i] = lg2[i/2] + 1;

    for (i = 0; i < n; i++)
      st[0][i] = n - sa[i];

    if(maxLog == 1) return;

    // st[1] is the LCP Array - Using Kasai Algorithm
    k = st[1][n - 1] = 0;
    for (i = 0; i < n; i++, k ? k--: 0){
      if(rank[i] == n - 1){
        k = 0;
        continue;
      }
      int j = sa[rank[i]+1];
      while(i + k < n && j + k < n && T[i + k] == T[j + k])
        k++;
      st[1][rank[i]] = k;
    }

    for (j = 2; j < maxLog; j++)
      for (i = 0; i + (1 << j) <= n; i++)
        st[j][i] = min(st[j - 1][i], min(st[j - 1][i + 1], st[j - 1][i + (1 << (j - 1))]));
  }

  int lcp(int x, int y){
    x = rank[x], y = rank[y];
    if(x > y)
      swap(x, y);
    int j = lg2[y - x + 1];
    return min(st[j][x], st[j][y - (1 << j) + 1]);
  }
};
