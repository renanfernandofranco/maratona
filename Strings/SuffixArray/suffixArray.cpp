/**
 * Suffix Array - lean version
 * ===========================
 *
 * Complexity to build:
 *  - Time: O(n * lg n)
 *  - Space: O(n)
 *
 * Status: slightly tested
 */

#include <bits/stdc++.h>
using namespace std;
using ll = long long;

struct SuffixArray{
  struct Entry{
    int ft, sc, idx;
  };

  #define GET(e, i) ((i) == 0 ? e.ft + 1 : e.sc + 1)
  string T;
  int n, i, sz, cnt;
  vector<Entry> L, tmp;
  vector<int> rank, sa, freq;

  SuffixArray(string& T): T(T){
    n = T.length();
    rank.resize(n);
    L.resize(n);
    sa.resize(n); // suffix array

    // 128 is the size of the alphabet
    freq.resize(max(n , 128) + 1);
    tmp.resize(n);

    build();
  }

  void build(){
    for (i = 0; i < n; i ++)
      rank[i] = T[i];

    // if using in cyclic string, then change the stop criteria to "cnt != n - 1 && sz <= n"
    for (cnt = -1, sz = 1; cnt != n - 1; sz <<= 1) {
      for (i = 0; i < n; i ++){
        L[i].ft = rank[i];
        L[i].sc = i + sz < n ? rank[i + sz] : -1; // for cyclic string use L[i].sc = rank[(i + sz) % n];
        L[i].idx = i;
      }
      radixSort();
      for (cnt = -1, i = 0; i < n; i++)
        rank[L[i].idx] = (i > 0 && L[i].ft == L[i - 1].ft && L[i].sc == L[i - 1].sc) ? cnt : ++cnt;
    }

    for (i = 0; i < n; i++)
      sa[i] = L[i].idx;
  }

  void radixSort(){
    for (int k = 1; k >= 0; k--){
      fill(freq.begin(), freq.end(), 0);

      for (i = 0; i < n; i++)
        freq[GET(L[i], k)]++;

      for (i = 1; i < freq.size(); i++)
        freq[i] += freq[i - 1];

      for (i = n - 1; i >= 0; i--)
        tmp[--freq[GET(L[i], k)]] = L[i];
      tmp.swap(L);
    }
  }
};
