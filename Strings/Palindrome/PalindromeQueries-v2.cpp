/**
 * Implicit Treaps - Palindrome
 *
 * Operations:
 *  - Check if a substring [l, r] is a palindrome
 *  - Erase the substring [l, r]
 *  - Insert a caracter C at position P
 *
 * Complexity:
 *  - Time: O(lg n) per operation (probabilistic)
 *  - Space: O(n)
 *
 * Strategy: The hashing of a subTree is maintained, being the
 *  same calculated in both directions, and if both are equals,
 *  then the string stored in subTree is a palindrome
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
using vi = vector<int>;
using ll = long long;

#define MAXN 300'005
#define MAXQ 300'005
const ll MODS[] = {1'000'000'007, 1'000'000'009};
const int SZMD = 2;
const ll BASE = 31;
ll pot[MAXN + MAXQ][SZMD];

struct Node{
  ll data, pref[SZMD], suff[SZMD];
  int prior, sz;
  Node *l, *r;
  Node (ll data) : data(data), prior(rand()), l(NULL), r(NULL) { }
};

using Treap = Node*;

int sz (Treap t) {
  return t ? t->sz : 0;
}

void update (Treap t) {
  if (t){
    t->sz = 1 + sz(t->l) + sz(t->r);

    #define UnionHash(bef, pw, i, aft) bef = (bef + pot[pw][i] * aft) % MODS[i]
    #define getPref(t, i) (t ? t->pref[i] : 0)
    #define getSuff(t, i) (t ? t->suff[i] : 0)

    FOR(i, SZMD){
      t->pref[i] = getPref(t->l, i);
      UnionHash(t->pref[i], sz(t->l), i, t->data);
      UnionHash(t->pref[i], sz(t->l) + 1, i, getPref(t->r, i));

      t->suff[i] = getSuff(t->r, i);
      UnionHash(t->suff[i], sz(t->r), i, t->data);
      UnionHash(t->suff[i], sz(t->r) + 1, i, getSuff(t->l, i));
    }
  }
}

void merge (Treap & t, Treap l, Treap r) {
  if (!l || !r)
    t = l ? l : r;
  else if (l->prior > r->prior)
    merge (l->r, l->r, r),  t = l;
  else
    merge (r->l, l, r->l),  t = r;
  update (t);
}

void split (Treap t, Treap & l, Treap & r, int key, int add = 0) {
  if (!t)
    return void( l = r = NULL );
  int cur_key = add + sz(t->l); //implicit key
  if (key <= cur_key)
    split (t->l, l, t->l, key, add),  r = t;
  else
    split (t->r, t->r, r, key, add + 1 + sz(t->l)),  l = t;
  update (t);
}

Treap build(vi& a){
  Treap t = NULL;
  FOR(i, (int)a.size())
    merge(t, t, new Node(a[i]));
  return t;
}

void addAtPos(Treap& t, int pos, ll data){
  Treap r;

  split(t, t, r, pos);
  merge(t, t, new Node(data));
  merge(t, t, r);
}

void eraseRange(Treap& t, int l, int r){
  Treap between, after;
  split(t, t, after, r + 1);
  split(t, t, between, l);
  merge(t, t, after);
}

bool isPalindrome(Treap& t, int l, int r){
  if(l >= r)
    return true;

  Treap between, after;

  split(t, t, after, r + 1);
  split(t, t, between, l);

  bool ret = true;
  FOR(i, SZMD)
    ret &= between->pref[i] == between->suff[i];

  merge(t, t, between);
  merge(t, t, after);

  return ret;
}

void buildPot(){
  FOR(i, SZMD)
    pot[0][i] = 1;

  for(int i = 1; i < MAXN + MAXQ; i++)
    FOR(j, SZMD)
      pot[i][j] = (pot[i - 1][j] * BASE) % MODS[j];
}
