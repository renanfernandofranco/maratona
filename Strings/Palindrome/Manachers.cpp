/**
 * Find Longest Palindrome using Manacher's Algorithm
 *
 * Complexity:
 *  - Time: O(n)
 *  - Space: O(n)
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
using vi = vector<int>;

struct Manacher{
  string s;
  int n;
  vi d1, d2;

  Manacher (string s) : s(s), n(s.length()), d1(n), d2(n){
    for (int i = 0, l = 0, r = -1; i < n; i++) {
      int k = (i > r) ? 1 : min(d1[l + r - i], r - i + 1);
      while (0 <= i - k && i + k < n && s[i - k] == s[i + k])
        k++;
      d1[i] = k--;
      if (i + k > r) {
        l = i - k;
        r = i + k;
      }
    }
    for (int i = 0, l = 0, r = -1; i < n; i++) {
      int k = (i > r) ? 0 : min(d2[l + r - i + 1], r - i + 1);
      while (0 <= i - k - 1 && i + k < n && s[i - k - 1] == s[i + k])
        k++;
      d2[i] = k--;
      if (i + k > r) {
        l = i - k - 1;
        r = i + k ;
      }
    }
  }

  string Longest(){
    string ret;
    int mx = 0;
    FOR(i, n)
      if(d1[mx] < d1[i])
        mx = i;

    ret = s.substr(mx - d1[mx] + 1, 2 * d1[mx] - 1);
    mx = 0;
    FOR(i, n)
      if(d2[mx] < d2[i])
        mx = i;

    if(2 * d2[mx] > ret.size())
      ret =  s.substr(mx - d2[mx], 2 * d2[mx]);
    return ret;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  string s;
  cin >> s;
  Manacher mn(s);
  cout << mn.Longest() << endl;
}
