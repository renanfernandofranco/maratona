/**
 * Segment Tree - Palindrome
 *
 * Operations:
 *  - Check if a substring [l, r] is a palindrome
 *  - Change caracter in position P
 *
 * Complexity:
 *  - Time: O(lg n) per operation
 *  - Space: O(n)
 *
 * Strategy: The hashing of the interval is maintained, being the
 *  same calculated in both directions, and if both are equals,
 *  then the string stored in the interval is a palindrome
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
using vi = vector<int>;
using ll = long long;

const ll MODS[] = {1'000'000'007, 1'000'000'009};
const int SZMD = 2;
const ll BASE = 31;

vector<ll> pot[SZMD];

template<class T>
struct Data{
  T pref[SZMD], suff[SZMD];
  int sz;
  bool good;
  Data(): sz(0), good(true){
    FOR(i, SZMD)
      pref[i] = suff[i] = 0;
  }

  Data(T val): sz(1), good(true){
    FOR(i, SZMD)
      pref[i] = suff[i] = val;
  }
};

template <class T>
struct Combine{
  Data<T> operator() (Data<T> l, Data<T> r){
    Data<T> m;
    #define UnionHash(bef, pw, i, aft) bef = (bef + pot[i][pw] * aft) % MODS[i]

    FOR(i, SZMD){
      m.pref[i] = UnionHash(l.pref[i], l.sz, i, r.pref[i]);
      m.suff[i] = UnionHash(r.suff[i], r.sz, i, l.suff[i]);
      m.good &=  m.pref[i] ==  m.suff[i];
    }

    m.sz = l.sz + r.sz;
    return m;
  }
};

template <class T, class OP>
struct SegmentTree{
  int n;
  vector<T> t;
  OP op;

  SegmentTree(int n): n(n), t(n << 1){}

  void Set(int i, T val){
    t[i + n] = val;
  }

  void Build() {  // Build the tree
    for (int i = n - 1; i > 0; --i)
      t[i] = op(t[i << 1], t[i << 1 | 1]);
  }

  void Update(int p, T value) { // Set value at position p
    for (t[p += n] = value; p >>= 1; )
      t[p] = op(t[p << 1], t[p << 1 | 1]);
  }

  T Query(int l, int r) { // Composite of the interval [l, r)
    T resl, resr;
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
      if (l & 1) resl = op(resl, t[l++]);
      if (r & 1) resr = op(t[--r], resr);
    }
    return op(resl, resr);
  }
};

void buildPot(int n){
  FOR(i, SZMD)
    pot[i].assign(n, 1);

  FOR(i, SZMD)
    for(int j = 1; j < n; j++)
      pot[i][j] = (pot[i][j - 1] * BASE) % MODS[i];
}

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q;
  string s;

  cin >> n >> q >> s;

  SegmentTree<Data<int>, Combine<int>> seg(n);
  FOR(i, n)
    seg.Set(i, s[i] - 'a' + 1);

  buildPot(n + 1);
  seg.Build();

  FOR(i, q){
    int tp, l, r, p;
    char c;
    cin >> tp;
    if(tp == 1){
      cin >> p >> c;
      seg.Update(p - 1, c - 'a' + 1);
    }else{
      cin >> l >> r;
      cout << (seg.Query(l - 1, r).good ? "YES" : "NO") << endl;
    }
  }
}
