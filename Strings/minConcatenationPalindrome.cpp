/**
 * Given a set of string, where each one has a cost, this algorithm find
 * o cost of the concatenation of the sames (possibly using one string more
 * than once) that is a palindrome and has cost minimum
 * 
 * The algorithm maintains as vertices the remaining of string that did not
 * match yet and the side of this remaining. And in one interaction of
 * Dijkstra is do an attempt to insert a string on the opposite side and
 * then is calculated which is the new unmatch remaining.
 * 
 * Complexity:
 *          - Time : O(n ^ 2 * L * (L + lg (n * L)) ), where L is the size
 *               of the greater string of the input
 * 
 * Problem: https://atcoder.jp/contests/abc175/tasks/abc175_f
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
using is = pair<int, string>;
using lis = pair<ll, is>;

#define LEFT 1

map<is, ll> cost;
vector<is> strings;

bool isPalindrome(string& s, int ini, int fim){
    return ini > fim ? true : (s[ini] == s[fim] && isPalindrome(s, ini + 1, fim - 1));
}

ll dijkstra(){
    set<lis> q;
    string sFrom, sLeft, sRight;
    q.insert({0, {LEFT, ""}});
    int i, j;

    while(!q.empty()){
        ll dist = q.begin()->first;
        sFrom = q.begin()->second.second;
        int side = q.begin()->second.first;
        q.erase(q.begin());

        (side == LEFT ? sLeft : sRight) = sFrom;

        for(is p: strings){
            (side == LEFT ? sRight : sLeft) = p.second;

            bool good = true;
            for(i = 0, j = sRight.length() - 1; i < sLeft.size() && j >= 0; i++, j--)
                good &= sRight[j] == sLeft[i];

            if(good){
                string nxt;
                if (i < sLeft.size())
                    nxt = sLeft.substr(i);
                if (j >= 0)
                    nxt = sRight.substr(0, j + 1);

                ll dist2 = dist + p.first;
                int sideNxt = i < sLeft.size() || nxt.empty();

                is pNxt = {sideNxt, nxt};
                if (!cost.count(pNxt) || dist2 < cost[pNxt]){
                    q.erase({cost[pNxt], pNxt});
                    cost[pNxt] = dist2;
                    q.insert({dist2, pNxt});
                }
            }
        }
    }

    ll ans = LONG_LONG_MAX;
    for(auto p: cost){
        string s = p.first.second;
        if (isPalindrome(s, 0, (int)s.length() - 1))
            ans = min(p.second, ans);
    }
    return (ans == LONG_LONG_MAX ? -1 : ans);
}

int main(){
    cin.tie(0)->sync_with_stdio(0);
    
    int n;
    string s;
    ll c;
    
    cin >> n;
    strings.resize(n);

    for(int i = 0; i < n; i++){
        cin >> s >> c;
        strings[i] = {c, s};
    }

    cout << dijkstra() << endl;
}
