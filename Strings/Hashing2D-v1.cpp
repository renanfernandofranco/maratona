/**
 * String Matcher 2D using Polynomial Hashing
 * 
 * Count occurences of P (a matrix 2D) in T(a matrix 2D)
 * 
 * Complexity:
 *  - Build Time: O(|T|)  
 *  - Query Time: O(|T|)
 *  - Space: O(|T| + |P|)
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define endl '\n'
using ll = long long;
using vi = vector<int>;
using vvi = vector<vi>;

const ll BASE = 5; // Use a prime greater that the size of the alphabet
const ll MODS [] = {1000'000'007, 1000'000'009};
const int SZMOD = 2;

template <class T = int>
struct MOD{
   T v[SZMOD];

   MOD(T vl = 0){ this->operator=(vl); }

   void operator *= (const MOD& m2){ (*this) = this->operator*(m2); }

   void operator += (const MOD& m2){ (*this) = this->operator+(m2); }
   
   void operator -= (const MOD& m2){ (*this) = this->operator-(m2); }

   void operator = (T vl){ FOR(k, SZMOD) v[k] = vl; }

   bool operator == (const MOD& m2){ return equal(v, v + SZMOD, m2.v); }

   MOD operator * (const MOD& m2){
      MOD ret;
      FOR(k, SZMOD)
         ret.v[k] = (v[k] * 1LL * m2.v[k]) % MODS[k];
      return ret;
   }

   MOD operator + (const MOD& m2){
      MOD ret; T tmp;
      FOR(k, SZMOD)
         ret.v[k] = (tmp = v[k] + m2.v[k]) >= MODS[k] ? tmp - MODS[k] : tmp;
      return ret;
   }

   MOD operator - (const MOD& m2){
      MOD ret; T tmp;
      FOR(k, SZMOD)
         ret.v[k] = (tmp = v[k] - m2.v[k]) < 0 ? tmp + MODS[k] : tmp;
      return ret;
   }
};

template <class T>
struct Hash2D{
   int n, m;
   vector<vector<MOD<T>>> col; // prefix sum of hashing in a col of T
   vector<MOD<T>> pot;
   #define CHECK(v, i, j) (i >= 0 && j >= 0 ? v[i][j] : 0)

   template <class U>
   Hash2D(vector<vector<U>>& t): n(t.size()), m(t[0].size()),
      col(n, vector<MOD<T>>(m)), pot(n * m, 1){
         
      FOR(i, pot.size() - 1)
         pot[i + 1] = pot[i] * BASE;
      
      FOR(i, n)
         FOR(j, m)
            col[i][j] = CHECK(col, i - 1, j) + pot[i * m + j] * t[i][j];
   }

   MOD<T> range(int i, int j, int sz){
      return col[i + sz - 1][j] - CHECK(col, i - 1, j);
   }

   template <class U>
   int count(vector<vector<U>>& p){
      int ans = 0;
      int np = p.size(), mp = p[0].size();

      if(np > n || mp > m)
         return 0;

      MOD<T> hashP;
      FOR(i, np)
         FOR(j, mp)
            hashP += pot[i * m + j] * p[i][j];

      FOR(i, n - np + 1){
         MOD<T> hashCur;
         FOR(j, mp - 1)
            hashCur += range(i, j, np);
      
         for(int j = mp - 1; j < m; j++){
            hashCur += range(i, j, np);
            ans += hashP == hashCur;
            hashP *= BASE;
            hashCur -= range(i, j - (mp - 1), np);
         }
         hashP *= pot[mp - 1];
      }

      return ans;
   }
};

int main(){
   cin.tie(0)->sync_with_stdio(0);
   int np, mp, n, m;
   
   while(cin >> np >> mp >> n >> m){
      vvi p(np, vi(mp)), t(n, vi(m));
      string s;
      
      FOR(i, np){
         cin >> s;
         FOR(j, mp)
            p[i][j] = (s[j] == 'o') + 1;
      }
      
      FOR(i, n){
         cin >> s;
         FOR(j, m)
            t[i][j] = (s[j] == 'o') + 1;
      }
      
      Hash2D<int> hash2D(t);
      cout << hash2D.count(p) << endl;
   }
}