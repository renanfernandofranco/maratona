# String Algorithms

This is a collection of algorithms used for or inspired by string processing.

## Knuth-Moris-Pratt (KMP) Algorithm

This is a very popular algorithm for pattern matching, and the most efficient for single pattern matching. A brief explanation of the algorithm can be found on [Geeks for Geeks post][1] or the [Wikipedia][2], or many other resources on the web. For a more strict analysis, see Section 32.4 of the Introduction to Algorithms [CLR09].

This repository KMP algorithm implementation is based on the Halim *et al* implementation [HHS+13]. 

# References

[CLR09] Cormen, Thomas H., Charles E. Leiserson, Ronald L. Rivest, and Clifford Stein. **Introduction to algorithms**. MIT press, 2009.
[HHS+13] Halim, Steven, Felix Halim, Steven S. Skiena, and Miguel A. Revilla. **Competitive Programming 3**. Lulu Independent Publish, 2013.

[1]: https://www.geeksforgeeks.org/kmp-algorithm-for-pattern-searching/
[2]: https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm
