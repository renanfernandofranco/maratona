/**
 * Knuth-Moris-Pratt Algorithm
 * ===========================
 * Finds the match of a pattern P in a given text T.
 *
 * Complexity:
 *  - Build:
 *    - Time: O(|P|)
 *    - Memory: O(|P| + |T|)
 *  - Query:
 *    - Time: O(|P| + |T|)
 *
 *  Usage:
 *    This implementation abstracts the data type. Thus, the templated
 *    class declaration format is required (see main() for an example).
 *
 *    A text T and a pattern P of an indexed object (e.g. string, vector)
 *    must be given beforehand to the constructor for the preprocessing
 *    step. After that, a query can be performed, optionally giving a
 *    start index for the search. As the return a boolean value indicates
 *    whether the pattern was found.
 *
 *  Notes:
 *    A slightly modification can be done in order to return the first
 *    occurence index, or possibly accumate all these indices.
 *
 * Problem: https://codeforces.com/problemset/problem/1200/E
 *
 */

#include <bits/stdc++.h>
using namespace std;

template <typename Type>
struct KMP {
  Type &T, &P;
  int n, m;
  vector<int> back;

  KMP(Type &t, Type &p): T(t), P(p), n(t.size()), m(p.size()), back(m + 1) {
    build_lps();
  }

  void build_lps() {
    int i = 0, j = -1;
    back[0] = -1;
    while (i < m) {
      while (j >= 0 && P[i] != P[j])
        j = back[j];
      i++, j++;
      back[i] = j;
    }
  }

  bool match(int i = 0) {
    int j = 0;
    while (i < n) {
      while (j >= 0 && T[i] != P[j])
        j = back[j];
      i++, j++;
      if (j == m)
        return true;
    }
    return false;
  }

  // length of the longest suffix of the text that is the prefix of the pattern
  int suffixPrefix() {
    int i = max(0, n - m);
    int j = 0;
    while (i < n) {
      while (j >= 0 && T[i] != P[j])
        j = back[j];
      i++, j++;
    }
    return j;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);
  int n;
  string T, P;
  cin >> n >> T;
  for(int i = 1; i < n; i++){
    cin >> P;
    KMP<string> matcher(T, P);
    int len = matcher.suffixPrefix();
    T.append(P.begin() + len, P.end());
  }
  cout << T << endl;
}
