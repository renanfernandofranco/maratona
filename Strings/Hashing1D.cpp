/**
 * String Matcher using Polynomial Hashing
 *
 * Return occurences of P in T
 *
 * Complexity:
 *  - Build Time: O(|T|)
 *  - Query Time: O(|T| + |P|)
 *  - Space: O(|T| + |P|)
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define FORI(i, b) for(int i = 1; i <= (int)(b); i++)
#define endl '\n'
using ll = long long;
using vi = vector<int>;
using vvi = vector<vi>;

const int BASE = 31; // Use a prime greater that the size of the alphabet
const int MODS [] = {1000'000'007, 1000'000'009, 1'000'000'207};
const int SZMOD = 3;

template <class T = int>
struct MOD{
  T v[SZMOD];

  MOD(T vl = 0){ this->operator=(vl); }

  void operator *= (const MOD& m2){ (*this) = this->operator*(m2); }

  void operator += (const MOD& m2){ (*this) = this->operator+(m2); }

  void operator -= (const MOD& m2){ (*this) = this->operator-(m2); }

  void operator = (T vl){ FOR(k, SZMOD) v[k] = vl; }

  bool operator == (const MOD& m2){ return equal(v, v + SZMOD, m2.v); }

  MOD operator * (const MOD& m2){
    MOD ret;
    FOR(k, SZMOD)
      ret.v[k] = (v[k] * 1LL * m2.v[k]) % MODS[k];
    return ret;
  }

  MOD operator + (const MOD& m2){
    MOD ret; T tmp;
    FOR(k, SZMOD)
      ret.v[k] = (tmp = v[k] + m2.v[k]) >= MODS[k] ? tmp - MODS[k] : tmp;
    return ret;
  }

  MOD operator - (const MOD& m2){
    MOD ret; T tmp;
    FOR(k, SZMOD)
      ret.v[k] = (tmp = v[k] - m2.v[k]) < 0 ? tmp + MODS[k] : tmp;
    return ret;
  }
};

template<class U>
struct Matcher{
  int n;
  vector<MOD<U>> pot, pref;
  const U R = 'a' - 1;

  template<class V>
  Matcher(V& T): n(T.size()), pot(n + 1, 1), pref(n + 1, 0){
    FORI(i, n)
      pot[i] = pot[i - 1] * BASE;
    FORI(i, n)
      pref[i] = pot[i] * (T[i] - R) + pref[i - 1];
  }

  template<class V>
  vi Solve(V& P){
    int k = P.size();

    MOD<U> hashP;
    FOR(i, k)
      hashP += pot[i] * (P[i] - R);

    vi occurs;
    FOR(i, n - k + 1){
      MOD<U> hashT = pref[i + k] - pref[i];
      if(hashP * pot[i + 1] == hashT)
        occurs.push_back(i);
    }
    
    return occurs;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  string p, t;
  int n;
  while(cin >> n){
    cin >> p >> t;
    Matcher<int> matcher(t);
    vi occurs = matcher.Solve(p);
    for(int i : occurs)
      cout << i + 1 << endl;
    if(occurs.empty())
      cout << endl;
  }
}
