/**
 * Suffix Tree - Multiple Strings
 * 
 * Complexity:
 *  - Time: O(n)
 *  - Space: O(n)
 * 
 * Operations:
 *  - Find the amount of the substring that belongs to one unique string of the set in O(n)
 *  - Find the length of the longest substring that belongs to all strings in O(n)
 * 
 * Attention - Currently, the maximum size of the set of the strings is 31, and probably can be
 * extends to 64 changing the mask to unsigned long long (and any bit shift involving the same)
 * 
 */

#include <bits/stdc++.h>

#define endSpec '@' // use the characters in range '2' to '@' as the end of the string 
using namespace std;
using ll = long long;

string s;
int sz, cn, cd;

struct node {
  int l, r, p, suf, mask;
  map<char, int> f;

  node (int l = 0, int r = -1, int p = -1): l(l), r(r), p(p), suf(-1){}
  inline int len() { return r - l + 1;}
  inline char operator[] (int i) { return s[l + i];}
};

vector<node> t;

inline int new_node(int l, int r, int p){
  t[sz] = node(l, r, p);
  return sz++;
}

int dfs(int v){
  t[v].mask = 0;
  if (t[v].f.empty())
    t[v].mask |= 1 << (endSpec - s[t[v].r]);

  for (auto p: t[v].f)
    t[v].mask |= dfs(p.second);
  return t[v].mask;
}

void build(vector<string>& ss){
  int k, i, j, g, mid, n, ns;
  
  int szTotal = 0;
  for (k = 0; k < ss.size(); k++)
    szTotal += ss[k].length() + 1;

  sz = 1;
  s.clear();
  s.reserve(szTotal);
  t.assign(szTotal << 1, node());

  for(k = 0; k < ss.size(); k++){
    j = i = s.length();
    
    s.append(ss[k]);

    char endc = endSpec - k;
    s.push_back(endc);

    cn = cd = ns = 0;
    n = s.length();

    for (; j < n; j++){
      for (; i <= j; i++){
        if (cd == t[cn].len() ? t[cn].f.count(s[j]) : t[cn][cd] == s[j]){
          if (cd == t[cn].len()){
            cn = t[cn].f[s[j]];
            cd = 0;
          } 
          cd++;
          break;
        }else if (cd == t[cn].len()){
          t[cn].f[s[j]] = new_node(j, n - 1, cn);
          if (cn){
            cn = t[cn].suf;
            cd = t[cn].len();
          }
        }else if(cd < t[cn].len()){
          mid = new_node(t[cn].l, t[cn].l + cd - 1, t[cn].p);
          t[t[mid].p].f[t[mid][0]] = mid;
          t[mid].f[s[j]] = new_node(j, n - 1, mid);
          t[mid].f[t[cn][cd]] = cn;
          t[cn].p = mid;
          t[cn].l += cd;
          if (ns)
            t[ns].suf = mid;

          cn = t[mid].p;
          if (cn){
            cn = t[cn].suf;
            g = j - cd;
          }else
            g = i + 1;
          while(g < j && g + t[t[cn].f[s[g]]].len() <= j){
            cn = t[cn].f[s[g]];
            g += t[cn].len();
          }
          if (g == j){
            t[mid].suf = cn;
            cd = t[cn].len();
            ns = 0;
          }else{
            ns = mid;
            cn = t[cn].f[s[g]];
            cd = j - g;
          }
        }
      }
    }
    t[0].f.erase(endc);
  }
  dfs(0);
}

void printSuffixes(int v = 0, int len = 0){
  len += t[v].len();
  for (auto p : t[v].f)
    printSuffixes(p.second, len);
  if(t[v].f.empty())
    cout << s.substr(t[v].r - len + 1, len) << endl;
}

// find the amount of the substring that belongs to one unique string of the set
ll differSymetric(int v = 0){
  ll ret = 0;
  if (t[v].f.empty())
    ret += t[v].len() - 1;
  else if(!(t[v].mask & (t[v].mask - 1))) // if the mask has only one bit on
    ret += t[v].len();
  for(auto p: t[v].f)
    ret += differSymetric(p.second);
  return ret;
}

// find the length of the longest substring that belongs to all strings
ll longestCommonSubstring(int v, int len, int szSet){
  len += t[v].len();
  ll ret = __builtin_popcount(t[v].mask) == szSet ? len : 0;
  for(auto p: t[v].f)
    ret = max(ret, longestCommonSubstring(p.second, len, szSet));
  return ret;
}
