/**
 * Suffix Tree
 * 
 * Complexity:
 *  - Time: O(n)
 *  - Space: O(n)
 * 
 * Operations:
 *  - Find(or count number of occurrences) of a string P in string T in O(|P|)
 *  - Get the greatest suffix in O(n)
 *  - Find the kth substring in lexicographical order (including repetition)
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;

struct SuffixTree{
  inline static string s;
  int sz;

  struct Node {
    int l, r, p, suf, leaf;
    map<char, int> f;

    Node (int l = 0, int r = -1, int p = -1):
      l(l), r(r), p(p), suf(-1), leaf(0){}
    inline int len() { return r - l + 1;}
    inline char operator[] (int i) { return s[l + i];}
  };

  vector<Node> t;

  inline int getNode(int l, int r, int p){
    t[sz] = Node(l, r, p);
    return sz++;
  }

  int dfs(int v){
    for (auto p : t[v].f)
      t[v].leaf += dfs(p.second);
    return t[v].leaf += t[v].f.size() == 0;
  }

  SuffixTree(string s_) : sz(1) {
    int i, j, g, mid, n, ns, cn, cd;
    s = s_ + "$";
    n = s.length();
    t.assign(n << 1, Node());
    i = j = cn = cd = ns = 0;

    for (; j < n; j++){
      for (; i <= j; i++){
        if (cd == t[cn].len() ? t[cn].f.count(s[j]) : t[cn][cd] == s[j]){
          if (cd == t[cn].len()){
            cn = t[cn].f[s[j]];
            cd = 0;
          } 
          cd++;
          break;
        }else if (cd == t[cn].len()){
          t[cn].f[s[j]] = getNode(j, n - 1, cn);
          if (cn){
            cn = t[cn].suf;
            cd = t[cn].len();
          }
        }else if(cd < t[cn].len()){
          mid = getNode(t[cn].l, t[cn].l + cd - 1, t[cn].p);
          t[t[mid].p].f[t[mid][0]] = mid;
          t[mid].f[s[j]] = getNode(j, n - 1, mid);
          t[mid].f[t[cn][cd]] = cn;
          t[cn].p = mid;
          t[cn].l += cd;
          if (ns)
            t[ns].suf = mid;

          cn = t[mid].p;
          if (cn){
            cn = t[cn].suf;
            g = j - cd;
          }else
            g = i + 1;
          while(g < j && g + t[t[cn].f[s[g]]].len() <= j){
            cn = t[cn].f[s[g]];
            g += t[cn].len();
          }
          if (g == j){
            t[mid].suf = cn;
            cd = t[cn].len();
            ns = 0;
          }else{
            ns = mid;
            cn = t[cn].f[s[g]];
            cd = j - g;
          }
        }
      }
    }
    t[0].f.erase('$');
    dfs(0);
  }

  bool Find(string& pat){
    int cn = 0, cd = 0;
    for(char c: pat){
      if (cd == t[cn].len() ? t[cn].f.count(c) : t[cn][cd] == c){
        if (cd == t[cn].len()){
          cn = t[cn].f[c];
          cd = 0;
        }
        cd++;
      }else
        return false;
    }
    return true;
  }

  int Count(string& pat){
    int cn = 0, cd = 0;
    for(char c: pat){
      if (cd == t[cn].len() ? t[cn].f.count(c) : t[cn][cd] == c){
        if (cd == t[cn].len()){
          cn = t[cn].f[c];
          cd = 0;
        }
        cd++;
      }else
        return 0;
    }
    return t[cn].leaf;
  }

  string GreatestSuffix(){
    string ret;
    int cn = 0;
    
    do{
      cn = t[cn].f.rbegin()->second;
      ret.append(s.substr(t[cn].l, t[cn].len()));
    }while(t[cn].f.size());

    ret.pop_back();
    return ret;
  }

  void kSmallest(int v, ll& rem, string& rev){
    ll len = t[v].len() - t[v].f.empty();
    if(len * t[v].leaf < rem){
      rem -= len * t[v].leaf;
      for(auto p : t[v].f){
        kSmallest(p.second, rem, rev);
        if(rem == 0){
          string temp = s.substr(t[v].l, len);
          rev.insert(rev.end(), temp.rbegin(), temp.rend());
          return;
        }
      }
    }else{
      int sz = ceil((.0 + rem) / t[v].leaf);
      string temp = s.substr(t[v].l, sz);
      rev.insert(rev.end(), temp.rbegin(), temp.rend());
      rem = 0;
    }
  }

  string KSmallest(ll k){
    string rev;
    ll rem = k;
    kSmallest(0, rem, rev);
    return string(rev.rbegin(), rev.rend());
  }
};