/**
 * Polynomial Hashing 2D
 * 
 * Precomputes the hash for k x l-submatrices of a 2D matrix M.
 * 
 * Complexity:
 *  - Build Time: O(|M|)
 *  - Query Time: O(1)
 *  - Space: O(|M|)
 * 
 * Usage example: fixed-size 2D string matching in O(|T| + |P|) time
 * and memory complexities.
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)

using ll = long long;
using vi = vector<int>;
using vvi = vector<vi>;

const int BASE = 5; // Use a prime greater than the size of the alphabet
const int MODS [] = {1'000'000'007, 1'000'000'009};
const int SZMOD = 2;

template <class T = int>
struct MOD {
   T v[SZMOD];

   MOD(T vl = 0){ this->operator=(vl); }

   void operator *= (const MOD& m2){ (*this) = this->operator*(m2); }

   void operator += (const MOD& m2){ (*this) = this->operator+(m2); }
   
   void operator -= (const MOD& m2){ (*this) = this->operator-(m2); }

   void operator = (T vl){ FOR(k, SZMOD) v[k] = vl; }

   bool operator == (const MOD& m2){ return equal(v, v + SZMOD, m2.v); }

   MOD operator * (const MOD& m2){
      MOD ret;
      FOR(k, SZMOD)
         ret.v[k] = (v[k] * 1LL * m2.v[k]) % MODS[k];
      return ret;
   }

   MOD operator + (const MOD& m2){
      MOD ret; T tmp;
      FOR(k, SZMOD)
         ret.v[k] = (tmp = v[k] + m2.v[k]) >= MODS[k] ? tmp - MODS[k] : tmp;
      return ret;
   }

   MOD operator - (const MOD& m2){
      MOD ret; T tmp;
      FOR(k, SZMOD)
         ret.v[k] = (tmp = v[k] - m2.v[k]) < 0 ? tmp + MODS[k] : tmp;
      return ret;
   }
};

template<class T>
struct Hash2D {
   int n, m;
   vector<vector<MOD<T>>> _hash;

   template<class U>
   Hash2D(vector<vector<U>>& mat, int k, int l): 
      n(mat.size()), m(mat[0].size()), _hash(n, vector<MOD<T>>(m)){

      vector<vector<MOD<T>>> row(n, vector<MOD<T>>(m));

      MOD<T> base2l(1);
      FOR(i, l) base2l *= BASE;

      FOR(i, n) {
         MOD<T> cur;
         for (int j = m - 1; j >= 0; j--) {
               cur *= BASE, cur += mat[i][j];
               if (j + l < m)
                  cur -= base2l * mat[i][j + l];
               row[i][j] = cur;
         }
      }

      MOD<T> base2lk(1);
      FOR(i, k) base2lk *= base2l;

      FOR(j, m) {
         MOD<T> cur;
         for (int i = n - 1; i >= 0; i--) {
               cur *= base2l, cur += row[i][j];
               if (i + k < n)
                  cur -= base2lk * row[i + k][j];
               _hash[i][j] = cur;
         }
      }
   }

   /**
    * @returns the hash code for the k x l-submatrix
    * starting at the (i,j)-cell.
    */
   MOD<T> hash(int i, int j) {
      return _hash[i][j];
   }
};

int main() {
   cin.tie(0)->sync_with_stdio(0);
   int np, mp, n, m;

   while (cin >> np >> mp >> n >> m) {
      vvi p(np, vi(mp)), t(n, vi(m));
      string s;
      
      FOR(i, np) {
         cin >> s;
         FOR(j, mp)
            p[i][j] = (s[j] == 'x') + 1;
      }
      
      FOR(i, n) {
         cin >> s;
         FOR(j, m)
            t[i][j] = (s[j] == 'x') + 1;
      }

      Hash2D<int> hashT(t, np, mp), hashP(p, np, mp);

      int ans = 0;
      FOR(i, n - np + 1)
         FOR(j, m - mp + 1)
            if (hashP.hash(0, 0) == hashT.hash(i, j))
               ans++;

      cout << ans << '\n';
   }
}
