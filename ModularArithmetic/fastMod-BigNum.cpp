/**
 * Algorithm for fast mod of a big integer by a small integer
 * 
 * Complexity:
 *      - Time: O(n), where n is the amount of digits of the Big integer
 *      - Space: O(1)
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;

int main(){
    cin.sync_with_stdio(0);
    cin.tie(0);
    string a;
    ll b, ans = 0;
    cin >> a >> b;

    for(char dig: a)
        ans = (ans * 10LL + (ll)(dig - '0')) % b;
    
    cout << ans << endl;
}