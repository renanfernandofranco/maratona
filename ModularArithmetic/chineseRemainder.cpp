/**
 * Given:
 *  - a number P
 *  - a list of numbers p = p1, p2, ..., pn
 *
 * This algorithm check if is possible discover x mod P for all integer x
 * using only operations of type x mod p[i] with i in the range [i, n]
 *
 * This is a generalization of the Chinese Remainder Theorem, in which
 * the numbers in the list not need to be pairwise relatively prime
 *
 * The naive approach builds the LCM of numbers in the list and check
 * if P divides this number, but this leads to big numbers.
 *
 * This algorithm solves the problem storing in LCM only the factors
 * that are factors of P, and at the end check if LCM is equal P
 *
 * Complexity:
 *  - Time: O(n * lg m), where n is the size of the list p and m
 *                       is the greater value among the numbers
 *                       in list p and the number P
 *  - Space: O(1)
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;

ll __lcm(ll n1, ll n2){
  return n1 * n2 / __gcd(n1, n2);
}

int main(){
  cin.sync_with_stdio(0);
  cin.tie(0);
  ll n, P, lcm;
  cin >> n >> P;

  vector<ll> p(n);
  for(ll i = 0; i < n; i++)
    cin >> p[i];

  lcm = 1;
  for(ll i = 0; i < n; i++)
    lcm = __lcm(__gcd(P, p[i]), lcm);

  cout << (lcm == P? "Yes\n": "No\n");
}
