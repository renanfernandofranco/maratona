#include <bits/stdc++.h>

/**
 * Floyd's Cycle-Finding Algorithm
 * ===============================
 * 
 * Given a function f : S -> S, where S is a finite set, and a start
 *    value `x0`, the sequence of values created using f will eventually
 *    cycle.
 * Let `mu` be the cycle first ocorrence, and `lambda` the shortest
 *    cycle length. Then Floyd's algorithm allows us to compute both these
 *    values efficiently.
 *
 * Complexity:
 *    - Time: O(mu + lambda)
 *    - Space: O(1)
 */

using namespace std;
using ii = pair<int, int>;

template<typename T>
ii find_cycle(T x0, T (*f)(T)) {
  T tortoise = f(x0), hare = f(tortoise);

  while (tortoise != hare)
    tortoise = f(tortoise), hare = f(f(hare));

  int mu = 0; hare = x0;
  while (tortoise != hare)
    tortoise = f(tortoise), hare = f(hare), mu++;

  int lambda = 1; hare = f(tortoise);
  while (tortoise != hare)
    hare = f(hare), lambda++;

  return ii(mu, lambda);
}

// Problem defined function
int g(int x) {
  return (x * 7 + 987654) % 100000007;
}

int main() {
  int x0 = 1;

  ii cycle = find_cycle<int>(x0, g);

  cout << "Start: " << cycle.first << endl;
  cout << "Length: " <<  cycle.second << endl;

  return 0;
}
