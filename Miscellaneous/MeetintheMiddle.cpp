/**
 * Meet in the Middle - Subset Sum
 *
 * Find how many ways can you choose a subset of
 * the numbers with sum x
 *
 * Complexity:
 *  - Time: O(n * 2^(n/2))
 *  - Space: O(2^(n/2))
 *
 */


#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'
#define all(a) a.begin(), a.end()

using ll = long long;
using vll = vector<ll>;
#define isOn(st, i) ((st) & (1 << (i)))

void brute(vll& v, vll& ans){
  int n = v.size();
  ans.resize(1 << n);
  FOR(st, (1 << n)){
    ll sum = 0;
    FOR(i, n){
      if(isOn(st, i))
        sum += v[i];
    }
    ans[st] = sum;
  }
}

ll MM(vll v, ll x){
  int n = v.size();
  vll v1(v.begin(), v.begin() + n / 2), sums1;
  vll v2(v.begin() + n / 2, v.end()), sums2;

  brute(v1, sums1);
  brute(v2, sums2);

  sort(all(sums2));
  ll ret = 0;
  for(ll val : sums1)
    ret += upper_bound(all(sums2), x - val) - lower_bound(all(sums2), x - val);
  return ret;
}

int main(){
  cin.tie(0)->sync_with_stdio(0);

  ll n, x;
  cin >> n >> x;
  vll v(n);
  FOR(i, n)
    cin >> v[i];

  cout << MM(v, x) << endl;
}
