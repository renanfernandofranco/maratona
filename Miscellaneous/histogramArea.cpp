#include <bits/stdc++.h>

/**
 * Largest Rectangular Area under a Histogram
 * ==========================================
 *
 * Complexity:
 *  - Time: O(n)
 *  - Space: O(n)
 *
 * URI: 1683
 * 
 * Attention: be careful of integer overflow.
 */

using namespace std;

int maximal_area(vector<int> &hist) {
  int result = 0, n = hist.size();
  stack<int> s;
  
  int i = 0;
  while (i < n) {
    if (s.empty() || hist[s.top()] < hist[i])
      s.push(i++);
    else {
      int tp = s.top(); s.pop();

      // consider only the bars in the range (s.top, i),
      // where hist[tp] is the minimum.
      int width = s.empty() ? i : i - (s.top() + 1);

      result = max(result, width * hist[tp]);
    }
  }

  // compute the rectangles that extend to the end of the histogram,
  // i.e. that no smaller bar is found ahead of top of the stack.
  while (!s.empty()) {
    int tp = s.top(); s.pop();

    int width = s.empty() ? n : n - (s.top() + 1);

    result = max(result, width * hist[tp]);
  }

  return result;
}
