# Makefile Tester

Here we describe the way the [Makefile](Makefile) is configured to enable automatic
testing for predefined tests.

## Usage

First, we will show the most basic workflow to have a problem compiled, and tested.
Then, we will show some tricks that can be done in order to speed up its usage, and
allow some level of customization without changing the `Makefile` code.

### Basic workflow

Let's consider we are solvign the problem D in a contest. Then, we will probably be
coding in a `d.cpp` file. Usually, in order to compile the program we would run
something like:
```bash
$ make d
```

This would produce the compiled `d` file.

#### Compiling with the tester

Using this test engine, a similar compiling workflow can be used. Here, we can define
which problem we want to run. For instance,
```bash
$ make PROBLEM=d
```

For short, we can use the `P` variable, reducing it to:
```bash
$ make P=d
```

Note that the capital letter is important here. `make p=d` won't led to the same result.
Despite that, for compilation only, there's no need to bother with this different sintax.
We can simply use the very same command as before, _i.e._ `make d`, and the same result
will be achieved.

### Other features

However, with the tester `Makefile`, we can do a couple of things more. Let's see some
tasks currently defined in the `Makefile`:

- _create_. Used to prepare the environment to the testing system;
- _optim_. Compiles the code with the optimization flag;
- _test_. Runs the test engine;
- _clean_. Clears the environment.

#### Test preparation

In order to run the tester system, first we will have to place the files in a convenient
form on the folder. This is what the `create` command is for. Currently, it just creates
the test folder with the correct naming.

Thus, returning to our example, before even compiling, we should run the `create` command
for problem D.
```bash
$ make create P=d
```

This will create the test folder for problem D, where you should place the test files.
A convention for the test file names must be followed, which is: input files must have
the `.in` extension, while answer files must have the same name as the input but with
the `.ans` extension.

#### Testing the program

After adding all the tests to the created folder, the following command can be used
to run the tests.
```bash
$ make test P=d
```

This will automatically compile the file, if it has been updated, and then run the tests.
Each test is run with the `time` utilitary, which outputs the time taken to run the test.
Moreover, a simple _AC_ or _WA_ veredict is given for each test, besides having a memory
check using the `valgrind` utilitary.

#### Cleaning the files

When no further tests are needed, the simple `make clean P=d` command can clear the folder
mess create for testing. It will be asked whether you are sure you want to delete the test
files, to prevent accidental file deletion.

### Tricks

All Makefile variables can be set to use the environment variable if it is defined. This
is the case for the variable `P`. Thus, we can take advantage of that, and avoid typing
every time the `P=<problem>` argument.

This can be achieved simply `export`ing the variable in the environment. Therefore, for
our example, we could have typed the following commands:

```bash
$ export P=d
$ make create
# ... put the files on the folder, and...
$ make test
```
This makes it easier to quickly compile the files and run the tests. It is important,
though, not to forget to re-export the environment variable when another problem is
going to be tested.

A mix of the two approaches is also possible. When an argument is given for `make`, it
ignores the environment variable.

#### Customization

Checking a bit closer the `Makefile` variables, some of them have been defined to fetch
the environment variable (the ones using the `?=` assignmnet). Therefore, we can `export`
custom values for them. For instance, we could change to all problems use the same test
folder by using the following command:
```bash
$ export TEST_DIR=my-tests
```
