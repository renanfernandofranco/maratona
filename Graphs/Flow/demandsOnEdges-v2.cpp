/**
 * Flow with Demands on Edges
 * 
 * The flow at each edge must be in the range:
 * lb[i] <= f[i] <= up[i]
 * 
 * I have no idea if the simplex's code is correct
 * 
 * Complexity:
 *  - Time: O(|V|^2 * |E|^2) with faith
 *  - Space : O(|V|^2 * |E|)
 * 
 * Note: if necessary use long double
 * 
 * Problem B of https://codeforces.com/gym/100199
 * 
 */

#include <bits/stdc++.h>
using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
#define all(a) a.begin(), a.end()
#define SZ(a) ((int)a.size())
using ld = double;
using vd = vector<ld>;
using vvd = vector<vd>;
using vi = vector<int>;
using ii = pair<int, int>;
 
const ld EPS = 1e-9;
#define diffZero(x) ((x > EPS) || (x < -EPS))

inline bool lessPair(ld d1, int i1, ld d2, int i2){
  return abs(d1 - d2) > EPS ? d1 < d2 : i1 < i2;
}

struct LPSolver {
  int m, n;
  vi B, N;
  vvd D;
 
  LPSolver(const vvd &A, const vd &b, const vd &c) :
   m(SZ(b)), n(SZ(c)), B(m + 1), N(n + 1), D(m + 1, vd(n + 1, 0)) {
    FOR(i, m){
      FOR(j, n)
        D[i + 1][j] = A[i][j];
      D[i + 1][n] = b[i];
    }
    FOR(i, n)
      D[0][i] = -c[i];
    FOR(i, m + 1)
      B[i] = -i;
    FOR(j, n + 1)
      N[j] = j;
  }
 
  void Pivot(int r, int s) {
    FOR(i, m + 1) 
      if (i != r && diffZero(D[i][s]))
        FOR(j, n + 1) 
          if (j != s)
            D[i][j] -= D[r][j] * D[i][s] / D[r][s];
    FOR(j, n + 1)
      if (j != s) 
        D[r][j] /= D[r][s];
    FOR(i, m + 1)
      if (i != r) 
        D[i][s] /= -D[r][s];
    D[r][s] = 1.0 / D[r][s];
    swap(B[r], N[s]);
  }
 
  ld Solve(vector<ld>& x){
    while(true){
      int ii = 1, jj = 0;
      FORI(i, m)
        if (lessPair(D[i][n], B[i], D[ii][n], B[ii])) 
          ii = i;
      if (D[ii][n] > -EPS)
        break;
      FOR(j, n)
        if (D[ii][j] < D[ii][jj])
          jj = j;
      if (D[ii][jj] > -EPS)
        return -numeric_limits<ld>::infinity(); //Infeasible
      Pivot(ii, jj);
    }
 
    while(true){
      int ii = 1, jj = 0;
      FOR(j, n)
        if (lessPair(D[0][j], N[j], D[0][jj], N[jj]))
          jj = j;
      if (D[0][jj] > -EPS)
        break;
      FORI(i, m)
        if (D[i][jj] > EPS && (D[ii][jj] < EPS ||
          lessPair(D[i][n] / D[i][jj], B[i], D[ii][n] / D[ii][jj], B[ii])))
            ii = i;
      if (D[ii][jj] < EPS)
        return numeric_limits<ld>::infinity(); //Unbounded
      Pivot(ii, jj);
    }
 
    x.assign(n, 0);
    FORI(i, m)
      if (B[i] >= 0) 
        x[B[i]] = D[i][n];
    return D[0][n];
  }
};

int main() {
  #ifdef ONLINE_JUDGE
  freopen("cooling.in", "r", stdin); 
  freopen("cooling.out", "w", stdout);
  #endif
  cin.tie(0)->sync_with_stdio(0);
  int n, m, u, v, lb2, up2;
  cin >> n >> m;

  vector<vi> up(n, vi(n, 0)), lb(n, vi(n, 0));

  vector<ii> edges;

  FOR(i, m){
    cin >> u >> v >> lb2 >> up2;
    u--; v--;
    lb[u][v] = lb2;
    up[u][v] = up2;
    edges.push_back({u, v});
  }
  
  vvd A;
  vd B, C(n * n);

  FOR(i, n){
    vd l1(n * n), l2(n * n);
    FOR(j, n){
      if(up[i][j]){
        l1[i * n + j] = -1;
        l2[i * n + j] = 1;
      }if(up[j][i]){
        l1[j * n + i] = 1;
        l2[j * n + i] = -1;
      }
    }
    // input flow at a vertex is equal to output flow
    A.push_back(l1);
    A.push_back(l2);
    B.push_back(0);
    B.push_back(0);
  }

  for(ii p : edges){
    int v = p.first, u = p.second;
    vd l1(n * n), l2(n * n);
    l1[v * n + u] = 1;
    l2[v * n + u] = -1;
    A.push_back(l1);
    B.push_back(up[v][u]); // f[v][u] <= up[v][u]
    A.push_back(l2);
    B.push_back(-lb[v][u]); // -f[v][u] <= -lb[v][u]
  }
  LPSolver LPS(A, B, C);
  vd X;
  int ans = round(-LPS.Solve(X));

  if(SZ(X) < n)
    return (cout << "NO\n", 0);
  cout << "YES\n";
  
  for(ii p : edges){
    int v = p.first, u = p.second;
    cout << (int) round(X[v * n + u]) << '\n';
  }
  return 0;
}
