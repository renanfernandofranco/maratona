/**
 * Min cost Max Flow
 * 
 * Works only with non-negative weights.
 * 
 * Can works in graphs without negative cycles if another shortest path
 * algorithm is used to fix the Dijkstra potential in the initialization
 * 
 * Complexity:
 *  - Time: O(|E|^2 * log |E| * log U), where U is the max capacity
 *  - Space: O(|V| + |E|)
 * 
 * Problem: https://loj.ac/p/102
 * Paper: https://web.stanford.edu/class/cs361b/files/cs361b-notes.pdf
 * 
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)
#define all(a) a.begin(), a.end()

using vi = vector<int>;
using vvi = vector<vi>;
using ll = long long;
using pli =  pair<ll, int>;

struct MinCostMaxFlow{
  const ll INF = 1e18;
  const ll LARGE = 3e13;

  struct FlowEdge{
    int from, to;
    ll cap, flow, cost;
  };

  int n, m, M;
  vvi adj;
  vector<FlowEdge> edges;
  vi dad;
  vector<char> seen;
  vector<ll> pot, dist;
  priority_queue<pli, vector<pli>, greater<pli>> q;

  MinCostMaxFlow(int n, int m): n(n), m(m), M(0), adj(n + 1),
     dad(n + 1), seen(n + 1), pot(n + 1), dist(n + 1) {}

  ll len(int id) {
    return edges[id].cost + pot[edges[id].from] - pot[edges[id].to];
  }

  void AddEdge(int u, int v, ll cap, ll cost) {
    edges.push_back({u, v, cap, 0, cost});
    edges.push_back({v, u, 0, 0, -cost});
    adj[u].push_back(M);
    adj[v].push_back(M + 1);
    M += 2;
  }

  void dijkstra(int s) {
    fill(all(dist), INF);
    fill(all(dad), -1);
    dist[s] = 0;
    q.push({0, s});

    while (!q.empty()) {
      int u = q.top().second;
      ll d = q.top().first;
      q.pop();

      if(dist[u] < d)
        continue;

      for(int id : adj[u])
        if (edges[id].flow){
          ll w = len(id);
          assert(w >= 0);

          int v = edges[id].to;
          if (dist[v] > d + w) {
            dist[v] = d + w;
            dad[v] = id;
            q.push({dist[v], v});
          }
        }
    }
  }

  void init(){
    AddEdge(n - 1, 0, LARGE, -LARGE);
    pot[n] = INF;
    FOR(i, n){
      AddEdge(n, i, 0, 0);
      edges[M - 2].flow = 1;
    }
  }

  void addOneFlow(int id) {
    if (edges[id].flow)
      return void(edges[id].flow++);

    int u = edges[id].from;
    int v = edges[id].to;
    dijkstra(v);

    if (dist[u] < INF && dist[u] + len(id) < 0) {
      edges[id ^ 1].flow++;

      while (u != v) {
        int id2 = dad[u];
        edges[id2].flow--;
        edges[id2 ^ 1].flow++;
        u = edges[id2].from;
      }
    }else 
      edges[id].flow++;

    ll maxDist = 0;
    FOR(i, n)
      if (dist[i] < INF)
        maxDist = max(maxDist, dist[i]);

    ll w = abs(len(id));
    FOR(i, n)
      pot[i] += dist[i] < INF ? dist[i] : maxDist + w;

    dijkstra(n);

    FOR(i, n)
      pot[i] += dist[i] - pot[n];
  }

  pair<ll, ll> Solve(){
    init();
    REV(i, 41){
      FOR(j, 2 * m + 2)
        edges[j].flow <<= 1;

      FOR(j, m + 1)
        if ((edges[j << 1].cap >> i) & 1)
          addOneFlow(j << 1);
    }

    ll minCost = 0;
    FOR(i, m)
      minCost += edges[i << 1 | 1].flow * edges[i << 1].cost;

    return {edges[m << 1 | 1].flow, minCost};
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);
  int n, m;
  cin >> n >> m;

  MinCostMaxFlow G(n, m);

  FOR(i, m){
    int u, v, f, w;
    cin >> u >> v >> f >> w;
    u--; v--;
    G.AddEdge(u, v, f, w);
  }

  auto ret = G.Solve();
  cout << ret.first << ' ' << ret.second << endl;

  return 0;
}
