/**
 * Maximum Flow - Edmonds-Karp
 * 
 * Complexity:
 *  - Time: O(|V| * |E|^2)
 *  - Space: O(|V|^2)
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using ii = pair<int, int>;
using vi = vector<int>;

#define MAX 1101
int cap[MAX][MAX]; // cap[x][y] is the flow capacity of the x to y
vector<vi> adj;
vi dad;
int flow;

void augument(int u){
  if(dad[u] != u){
    flow = min(cap[dad[u]][u], flow);
    augument(dad[u]);
    cap[dad[u]][u] -= flow;
    cap[u][dad[u]] += flow;
  }
}

int maxFlow(int s, int t, int n){
  bool find = true;
  int ans = 0;

  while(find){
    find = false;

    queue<int> q;
    q.push(s);

    dad.assign(n, -1);
    dad[s] = s;

    while(!q.empty() && !find){
      int v = q.front(); q.pop();
      for(int u : adj[v]){
        if (dad[u] == -1 && cap[v][u] > 0){
          dad[u] = v;
          q.push(u);
          find |= u == t;
        }
      }
    }

    if(find){
      flow = INT_MAX;
      augument(t);
      ans += flow;
    }
  }
  return ans;
}
