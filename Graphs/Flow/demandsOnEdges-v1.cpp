/**
 * Flow with Demands on Edges
 * 
 * The flow at each edge must be in the range:
 * lb[i] <= f[i] <= up[i]
 * 
 * Network without source and sink
 * 
 * Time Complexity:
 *  - Non-scaling: O(|V|^2 * |E|)
 *  - Scaling: O(|V| * |E| * lg U) with higher constant
 * 
 * Space Complexity: O(|V| + |E|)
 * 
 * Problem B of https://codeforces.com/gym/100199
 * 
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using vi = vector<int>;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()

template <class T>
struct Dinic {
  struct FlowEdge {
    int from, to;
    T cap, flow = 0;
    FlowEdge(int from, int to, T cap) : from(from), to(to), cap(cap) {}
  };

  const static bool SCALING = false;
  const T INF = numeric_limits<T>::max();
  vector<FlowEdge> edges;
  vector<vi> adj;
  vi level, ptr;
  queue<int> q;
  int n, s, t, lim, m;

  Dinic(int n, int s, int t): 
    adj(n), level(n), ptr(n), n(n), s(s), t(t), m(0) {}

  void AddEdge(int v, int u, T cap, bool isDirected = true) {
    edges.emplace_back(v, u, cap);
    edges.emplace_back(u, v, isDirected ? 0 : cap);
    adj[v].push_back(m);
    adj[u].push_back(m + 1);
    m += 2;
  }

  bool bfs() {
    fill(all(level), -1);
    level[s] = 0;
    q.push(s);

    while (!q.empty()) {
      int v = q.front(); q.pop();
      for (int id : adj[v]) {
        auto& e = edges[id];
        if (level[e.to] == -1 && e.flow < e.cap && (!SCALING || e.cap - e.flow >= lim)){
          level[e.to] = level[v] + 1;
          q.push(e.to);
        }
      }
    }
    return level[t] != -1;
  }

  T dfs(int v, T pushed) {
    if (v == t || !pushed)
      return pushed;
    for (int& cid = ptr[v]; cid < (int)adj[v].size(); cid++) {
      int id = adj[v][cid];
      auto& e = edges[id];
      if (level[v] + 1 != level[e.to])
        continue;
      if (T tr = dfs(e.to, min(pushed, e.cap - e.flow))){
        e.flow += tr;
        edges[id ^ 1].flow -= tr;
        return tr;
      }
    }
    return 0;
  }

  ll Solve() {
    ll f = 0;
    for (lim = SCALING ? (1 << 30) : 1; lim > 0; lim >>= 1) {
      while (bfs()) {
        fill(all(ptr), 0);
        while (T pushed = dfs(s, INF))
          f += pushed;
      }
    }
    return f;
  }
};

template <class T>
struct FlowWithDemands{
  struct Edge{
    int from, to;
    T lb, up;
  };
  
  int n, s, t;
  Dinic<T> dinic;
  vector<Edge> edges;

  FlowWithDemands(int n): n(n), s(n), t(n + 1),
     dinic(n + 2, s, t) {}

  void AddEdge(int from, int to, T lb, T up){
    edges.push_back({from, to, lb, up});
  }

  T Build(){
    vector<T> din(n, 0), dout(n, 0);
    for(auto e: edges){
      dout[e.from] += e.lb;
      din[e.to] += e.lb;
      dinic.AddEdge(e.from, e.to, e.up - e.lb);
    }
    FOR(i, n){
      dinic.AddEdge(s, i, din[i]);
      dinic.AddEdge(i, t, dout[i]);
    }
    return accumulate(all(dout), (T)0);
  }

  bool Solve(vector<T>& flows){
    T obj = Build();
    
    // the flow circulation within each vertex must be
    // equal to the sum of the lower bound of the edges
    bool ans = obj == dinic.Solve();
    
    int m = edges.size();
    flows.resize(m);
    FOR(i, m)
      flows[i] = dinic.edges[i << 1].flow + edges[i].lb;
    return ans;
  }
};

int main() {
  #ifdef ONLINE_JUDGE
  freopen("cooling.in", "r", stdin); 
  freopen("cooling.out", "w", stdout);
  #endif
  cin.tie(0)->sync_with_stdio(0);
  int n, m, u, v, lb, up;
  cin >> n >> m;

  FlowWithDemands<int> G(n);
  FOR(i, m){
    cin >> u >> v >> lb >> up;
    u--; v--;
    G.AddEdge(u, v, lb, up);
  }

  vector<int> flows;
  if(!G.Solve(flows))
    return (cout << "NO\n", 0);

  cout << "YES\n";
  FOR(i, m)
    cout << flows[i] << '\n';
  return 0;
}
