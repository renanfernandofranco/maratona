/**
 * Flow with demands on vertices and with restriction
 * of the flow circulation distance equal to 1
 * 
 * Using Edmonds Karp, but it's easy to switch to Dinic
 * 
 * Complexity:
 *  - Time: O(|V| * |E| ^ 2)
 *  - Space: O(|V| + |E|)
 * 
 * Problem: https://codeforces.com/contest/546/problem/E
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()

template<class T>
struct Edmonds{
  static const T INF = numeric_limits<T>::max() / 2;
  
  struct FlowEdge {
    int v, u;
    T cap, flow;
    FlowEdge(int v, int u, T cap): v(v), u(u), cap(cap), flow(0) {}
  };

  vector<FlowEdge> edges;
  vector<vi> adj;
  vi ptr;
  int s, t, m;
  T ret, flow;

  Edmonds(int n) : adj(n), ptr(n), m(0), ret(0) {}

  void AddEdge(int v, int u, T dirCap, bool isDir = true){
    edges.emplace_back(v, u, dirCap);
    edges.emplace_back(u, v, isDir ? 0 : dirCap);
    adj[v].push_back(m);
    adj[u].push_back(m + 1);
    m += 2;
  }

  void augument(int u){
    if(u != s){
      int id = ptr[u];
      flow = min(edges[id].cap - edges[id].flow, flow);
      augument(edges[id].v);
      edges[id].flow += flow;
      edges[id ^ 1].flow -= flow;
    }
  }

  int MaxFlow(int ss, int tt){
    s = ss; t = tt;
    bool find = true;

    while(find){
      find = false;

      stack<int> q;
      q.push(s);
      
      fill(all(ptr), -1);
      ptr[s] = -2;

      while(!q.empty() && !find){
        int v = q.top(); q.pop();
        for(int id : adj[v]){
          auto e = edges[id];
          if (ptr[e.u] == -1 && e.cap - e.flow > 0){
            ptr[e.u] = id;
            q.push(e.u);
            find |= e.u == t;
          }
        }
      }

      if(find){
        flow = INF;
        augument(t);
        ret += flow;
      }
    }
    return ret;
  }
};

template<class T>
void splitAndSet(Edmonds<T>& ed, vector<T>& a, vector<T>& demands,
 vector<vi>& adj, int n, int s, int t){
  #define SIB(u) (u + n)
  FOR(i, n){
    int v = i, v2 = SIB(v);

    if(adj[v].size()){
      for(auto u: adj[v])
        ed.AddEdge(v, SIB(u), Edmonds<T>::INF); //original edge

      // internal edges of each vertex. Uses the edge capability
      // to restrict the circulation distance of the flow
      ed.AddEdge(v, v2, a[i] + max(0, demands[i]), false);
    }
    if(demands[i] > 0)
      ed.AddEdge(v, t, demands[i]);
    else if(demands[i] < 0)
      ed.AddEdge(s, v2, -demands[i]);
  }
}

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, m, u, v;
  cin >> n >> m;

  vi a(n), b(n), demands(n);
  FOR(i, n)
    cin >> a[i];
  FOR(i, n)
    cin >> b[i];

  int N = 2 * n + 2;
  vector<vi> adj(N);
  FOR(i, m){
    cin >> u >> v;
    u--; v--;
    adj[u].push_back(v);
    adj[v].push_back(u);
  }
  
  FOR(i, n)
    demands[i] = b[i] - a[i];

  if(accumulate(all(demands), 0) != 0)
    return (cout << "NO\n", 0);

  Edmonds<int> ed(N);
  int s = N - 2, t = N - 1;
  splitAndSet(ed, a, demands, adj, n, s, t);

  ed.MaxFlow(s, t);
  bool ans = true;
  for(int id : ed.adj[s]){
    auto e = ed.edges[id];
    ans &= e.cap - e.flow == 0;
  }

  if(!ans)
    return (cout << "NO\n", 0);
  cout << "YES\n";

  vector<vi> mat(n, vi(n));
  FOR(i, n)
    mat[i][i] = a[i];

  FOR(i, n)
    for(int id : ed.adj[i]){
      auto e = ed.edges[id];
      int u = e.u - n;
      if(u >= n || u == i)
        continue;
      mat[i][u] = max(0, e.flow);
      mat[i][i] -= e.flow;
    }

  FOR(i, n)
    FOR(j, n)
      cout << mat[i][j] << "\n "[j < n - 1];
}
