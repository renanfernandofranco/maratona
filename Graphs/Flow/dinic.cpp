/**
 * Dinic's Algorithm for Maximum Flow
 * 
 * Time Complexity:
 *  - Non-scaling: O(|V|^2 * |E|)
 *  - Scaling: O(|V| * |E| * lg U) with higher constant
 * 
 * Space Complexity: O(|V| + |E|)
 * 
 * Problem: https://codeforces.com/group/3qadGzUdR4/contest/101712/problem/G
 * 
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using vi = vector<int>;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()

template <class T>
struct Dinic {
  struct FlowEdge {
    int from, to;
    T cap, flow = 0;
    FlowEdge(int from, int to, T cap) : from(from), to(to), cap(cap) {}
  };

  const static bool SCALING = false;
  const T INF = numeric_limits<T>::max();
  vector<FlowEdge> edges;
  vector<vi> adj;
  vi level, ptr;
  queue<int> q;
  int n, s, t, lim, m;

  Dinic(int n, int s, int t): 
    adj(n), level(n), ptr(n), n(n), s(s), t(t), m(0) {}

  void AddEdge(int v, int u, T cap, bool isDirected = true) {
    edges.emplace_back(v, u, cap);
    edges.emplace_back(u, v, isDirected ? 0 : cap);
    adj[v].push_back(m);
    adj[u].push_back(m + 1);
    m += 2;
  }

  bool bfs() {
    fill(all(level), -1);
    level[s] = 0;
    q.push(s);

    while (!q.empty()) {
      int v = q.front(); q.pop();
      for (int id : adj[v]) {
        auto& e = edges[id];
        if (level[e.to] == -1 && e.flow < e.cap && (!SCALING || e.cap - e.flow >= lim)){
          level[e.to] = level[v] + 1;
          q.push(e.to);
        }
      }
    }
    return level[t] != -1;
  }

  T dfs(int v, T pushed) {
    if (v == t || !pushed)
      return pushed;
    for (int& cid = ptr[v]; cid < (int)adj[v].size(); cid++) {
      int id = adj[v][cid];
      auto& e = edges[id];
      if (level[v] + 1 != level[e.to])
        continue;
      if (T tr = dfs(e.to, min(pushed, e.cap - e.flow))){
        e.flow += tr;
        edges[id ^ 1].flow -= tr;
        return tr;
      }
    }
    return 0;
  }

  ll Solve() {
    ll f = 0;
    for (lim = SCALING ? (1 << 30) : 1; lim > 0; lim >>= 1) {
      while (bfs()) {
        fill(all(ptr), 0);
        while (T pushed = dfs(s, INF))
          f += pushed;
      }
    }
    return f;
  }
};

int main(){
  cin.sync_with_stdio(0);
  cin.tie(0);
  int n, m, u, v, w;
  cin >> n >> m;
  Dinic<int> dinic(n, 0, n - 1);

  FOR(i, m){
    cin >> u >> v >> w;
    dinic.AddEdge(u - 1, v - 1, w);
  }

  cout << dinic.Solve() << endl;
  FOR(i, m)
    cout << dinic.edges[2 * i].flow << endl;
  
}
