/**
 * Min cost Max Flow
 * 
 * Works with negative weights, but not negative cycles
 * 
 * Complexity:
 *  - Time: O(|f| * |V| * |E|), where f is the max flow
 *  - Space: O(|V| + |E|)
 * 
 * Problem: https://codeforces.com/group/Ohoz9kAFjS/contest/266572/problem/G
 * 
 */

#include <bits/stdc++.h>
using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()
using vi = vector<int>;
using ll = long long;

template<class T, class U>
struct MCMF{
  const T INFT = numeric_limits<T>::max();
  const U INFU = numeric_limits<U>::max();
  
  struct FlowEdge {
    int from, to;
    T cap, flow;
    U cost;
    FlowEdge(int from, int to, T cap, U cost): from(from),
      to(to), cap(cap), flow(0), cost(cost){}
  };

  int n, s, t, m;
  vector<vi> adj;
  vi p;
  vector<U> d;
  vector<FlowEdge> edges;
  T push;
  ll flow;
  U cost;

  MCMF(int n) : n(n), m(0), adj(n), p(n), d(n){}

  void AddEdge(int from, int to, T dirCap, U cost, bool isDir = true){
    edges.emplace_back(FlowEdge(from, to, dirCap, cost));
    edges.emplace_back(FlowEdge(to, from, isDir ? 0 : dirCap, -cost));
    adj[from].push_back(m);
    adj[to].push_back(m + 1);
    m += 2;
  }

  void SPFA() {
    fill(all(d), INFU);
    fill(all(p), -1);
    d[s] = 0;
    vector<char> inq(n, false);
    queue<int> q;
    q.push(s);

    while (!q.empty()) {
      int u = q.front();
      q.pop();
      inq[u] = false;
      for (int id : adj[u]) {
        auto e = edges[id];
        auto v = e.to;
        if (e.cap - e.flow > 0 && d[v] > d[u] + e.cost) {
          d[v] = d[u] + e.cost;
          p[v] = id;
          if (!inq[v]) {
            inq[v] = true;
            q.push(v);
          }
        }
      }
    }
  } 

  void augument(int cur){
    if(cur != s){
      int id = p[cur];
      push = min(edges[id].cap - edges[id].flow, push);
      augument(edges[id].from);
      edges[id].flow += push;
      edges[id ^ 1].flow -= push;
    }
  }

  pair<ll, U> Solve(int ss, int tt){
    s = ss; t = tt;
    cost = flow = 0;
    while(SPFA(), d[t] != INFU){
      push = INFT;
      augument(t);
      flow += push;
      cost += d[t] * push;
    }
    return {flow, cost};
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, m;
  cin >> n >> m;

  MCMF<int, long long> ed(n);
  FOR(i, m){
    int u, v, cap, cost;
    cin >> u >> v >> cap >> cost;
    u--, v--;
    ed.AddEdge(u, v, cap, cost);
  }
  
  cout << ed.Solve(0, n - 1).second << endl;
}
