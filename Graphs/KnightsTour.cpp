/**
 * Open Knight Tour - Heuristc Approach
 *
 * Las Vegas algorithm - Probabilistically Fast
 *
 * Complexity:
 *  - Time : Exponential ???
 *  - Space : O(n * m)
 *
 * Problem: https://cses.fi/problemset/task/1689
 *
 */

#include <bits/stdc++.h>

using namespace std;
using ii = pair<int, int>;
using vi = vector<int>;
using vvi = vector<vi>;
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define all(a) a.begin(), a.end()
#define SZ(v) ((int)v.size())
#define isValid(Y, X) ((Y) >= 0 && (Y) < n && (X) >= 0 && (X) < m)

struct KnightTour{
  int n, m;
  vvi lvl;
  int dy[8] = {-2, -2, -1, -1, 1, 1, 2, 2};
  int dx[8] = {-1, 1, -2, 2, -2, 2, -1, 1};

  KnightTour(int n, int m): n(n), m(m), lvl(n, vi(m, 0)){}

  bool Solve(int y, int x, int dep = 1){
    lvl[y][x] = dep;
    if(dep == n * m)
      return true;
    vector<ii> movs;
    FOR(k, 8){
      int y2 = y + dy[k], x2 = x + dx[k];
      if(isValid(y2, x2) && !lvl[y2][x2]){
        int cnt = 0;
        FOR(l, 8){
          int x3 = x2 + dx[l], y3 = y2 + dy[l];
          cnt += isValid(y3, x3) && !lvl[y3][x3];
        }
        movs.push_back({cnt, k});
      }
    }
    sort(all(movs));
    for(auto p : movs)
      if(Solve(y + dy[p.second], x + dx[p.second], dep + 1))
        return true;
    lvl[y][x] = 0;
    return false;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);
  int n = 121, m = 100, x, y;
  cin >> x >> y;

  KnightTour kt(n, m);
  kt.Solve(y - 1, x - 1);
  FOR(i, n)
    FOR(j, m)
      cout << kt.lvl[i][j] << " \n"[j + 1 == m];
}
