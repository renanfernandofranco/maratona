/**
 * Kernelization of the Vertex Cover
 *
 * Find Vertex Cover when same is less equal to k
 *
 * Time Complexity:
 *  - Time: O((|V| + |E|) * lg |V| + k^3 + 1.3803^2k)
 *  - Space: O(|V| + |E|)
 *
 * Problem: https://codeforces.com/gym/331461/problem/D
 *
 */

#include <bits/stdc++.h>
using namespace std;

#define NIL 0
#define INF (1<<28)
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)
#define SZ(a) (int(a.size()))
#define all(a) a.begin(), a.end()
using vi = vector<int>;
using ii = pair<int, int>;
using vvi = vector<vi>;

struct MinCoverBip {
  int n, m, nm;
  vvi G;
  vi match, dist;
  // n: number of nodes on left side, nodes are numbered 1 fun n
  // m: number of nodes on right side, nodes are numbered n+1 fun n+m
  // G = NIL[0] ∪ G1[G[1---n]] ∪ G2[G[n+1---n+m]]
  MinCoverBip(int n, int m):
    n(n), m(m), nm(n + m + 1), G(nm), match(nm), dist(nm){}

  void AddEdge(int vl, int vr){
    G[vl].push_back(vr + n);
    G[vr + n].push_back(vl);
  }

  bool bfs() {
    queue<int> q;
    FORI(i, n)
      if(match[i] == NIL) {
        dist[i] = 0;
        q.push(i);
      }
      else
        dist[i] = INF;

    dist[NIL] = INF;
    while(!q.empty()) {
      int u = q.front(); q.pop();
      if(u != NIL)
        for(int v : G[u])
          if(dist[match[v]] == INF) {
            dist[match[v]] = dist[u] + 1;
            q.push(match[v]);
          }
    }
    return dist[NIL] != INF;
  }

  bool biColor(int u) {
    if(u != NIL) {
      for(int v : G[u])
        if(dist[match[v]] == dist[u] + 1) {
          if(biColor(match[v])) {
            match[v] = u;
            match[u] = v;
            return true;
          }
        }
      dist[u] = INF;
      return false;
    }
    return true;
  }

  int maxMatching() {
    int ret = 0;
    fill(all(match), NIL);
    while(bfs())
      FORI(i, n)
        if(match[i] == NIL && biColor(i))
          ret++;
    return ret;
  }

  vvi solve(){
    maxMatching();

    vvi cover(2, vi());
    vi Z(nm, 0);
    queue<int> q;

    fill(all(dist), INF);
    FORI(u, n)
      if(match[u] == NIL){
        q.push(u);
        Z[u] = true;
        dist[u] = 0;
      }

    while(!q.empty()){
      int u = q.front(); q.pop();
      for(int v : G[u])
        if (dist[v] == INF && (dist[u] ^ (match[v] != u))){
            dist[v] = !dist[u];
            Z[v] = true;
            q.push(v);
          }
      }

    FORI(u, n)
      if(match[u] != NIL && !Z[u])
        cover[0].push_back(u);

    for(int u = n + 1; u < nm; u++)
      if(match[u] != NIL && Z[u])
        cover[1].push_back(u - n);

    return cover;
  }
};

/**
 * Maximum Indepent Set
 * Time complexity: O(1.3803^n)
 */
struct MIS{
  int n;
  map<int, set<int>> adj;
  set<ii, greater<ii>> D;
  vi best, cur;
  set<int> seen;

  // Use n = -1 to ignore isolated vertices;
  MIS(int n = -1): n(n){}

  void AddEdge(int u, int v){
    adj[u].insert(v);
    adj[v].insert(u);
  }

  vi Solve(){
    assert(n == -1 || adj.empty() || (adj.rbegin()->first < n));
    for(auto p : adj)
      D.insert({SZ(adj[p.first]), p.first});
      
    mis();

    FOR(v, n)
      if(!adj.count(v))
        best.push_back(v);
    sort(all(best));

    return best;
  }

  // bicoloration of the component
  void biColor(int v, vi& s1, vi& s2, bool side){
    if(side)
      s2.push_back(v);
    else
      s1.push_back(v);
    seen.insert(v);

    for(int u : adj[v])
      if(!seen.count(u))
        biColor(u, s1, s2, !side);
  }

  vi mis2(){
    vi ret;
    seen.clear();
    for(auto p : D){
      int v = p.second;
      if(!seen.count(v)){
        vi s1, s2;
        biColor(v, s1, s2, false);

        if(SZ(s1) < SZ(s2))
          swap(s1, s2);

        // possible odd cycle
        if(SZ(s1) > SZ(s2)){
          bool find = false;
          for(int v : s1)
            find |= SZ(adj[v]) <= 1; // not is odd cycle
          if(!find)
            swap(s1, s2);
        }
        ret.insert(ret.end(), all(s1));
      }
    }
    return ret;
  }

  void remAdd(int v, bool type){
    for(int u : adj[v]){
      D.erase({SZ(adj[u]), u});
      if(type)
        adj[u].erase(v);
      else
        adj[u].insert(v);
      D.insert({SZ(adj[u]), u});
    }
  }

  void mis(){
    if(D.empty() || D.begin()->first <= 2){
      auto rem = mis2();
      if(SZ(rem) + SZ(cur) > SZ(best)){
        best = cur;
        best.insert(best.end(), all(rem));
      }
      return;
    }

    int v = D.begin()->second;
    D.erase(D.begin());

    for(int u : adj[v]){
      D.erase({SZ(adj[u]), u});
      adj[u].erase(v);
      remAdd(u, true);
    }

    cur.push_back(v);
    mis();
    cur.pop_back();

    for(int u : adj[v]){
      D.insert({SZ(adj[u]), u});
      remAdd(u, false);
    }

    mis();

    D.insert({SZ(adj[v]), v});
    remAdd(v, false);
  }
};

struct VertexCover{
  int n, k;
  vvi adj;
  priority_queue<ii> D;
  vi cover, diff, used;
  set<int> V;

  VertexCover(int n, int k): n(n), k(k), adj(n), diff(n, 0), used(n, 0){
    FOR(i, n)
      V.insert(i);
  }

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void addInCover(int v){
    for(int u : adj[v])
      diff[u]++;

    used[v] = true;
    V.erase(v);
    cover.push_back(v);
    k--;
  }

  void clearGraph(){
    for(auto it = V.begin(); it != V.end();){
      int v = *it;
      if(adj[v].empty())
        it = V.erase(it);
      else{
        it++;
        REV(i, SZ(adj[v])){
          if(used[adj[v][i]]){
            swap(adj[v][i], adj[v].back());
            adj[v].pop_back();
          }
        }
      }
    }
  }

  bool reduce(){
    FOR(i, n)
      D.push({SZ(adj[i]), i});

    while(!D.empty() && D.top().first > k){
      ii top = D.top();
      D.pop();
      int v = top.second;
      int curVal = top.first - diff[v];

      if(curVal <= k){
        D.push({curVal, v});
        diff[v] = 0;
        continue;
      }

      addInCover(v);
      if(k < 0)
        return false;
    }
    clearGraph();

    return true;
  }

  vi twoApproximation(){
    if(V.empty())
      return vi();

    int id = 0;
    vi fun(n), inv(n);

    for(int v : V){
      fun[v] = id;
      inv[id] = v;
      id++;
    }

    MinCoverBip minCoverBip(id, id);

    for(int v : V){
      for(int u : adj[v]){
        minCoverBip.AddEdge(fun[v] + 1, fun[u] + 1);
        minCoverBip.AddEdge(fun[u] + 1, fun[v] + 1);
      }
    }

    vvi approx = minCoverBip.solve();
    vi cnt(n, 0);
    for(vi& side : approx)
      for(int v : side)
        cnt[inv[v - 1]]++;

    vi ans;
    FOR(v, n){
      if(cnt[v] == 2)
        addInCover(v);
      else
        if(cnt[v] == 1)
          ans.push_back(v);
    }
    clearGraph();

    return ans;
  }

  bool coverByApproximation(vi& approx){
    MIS mis;

    for(int v : approx)
      for(int u : adj[v])
        mis.AddEdge(v, u);

    vi complement = mis.Solve();
    vector<char> used(n, false);

    for(int v: complement)
      used[v] = true;

    for(int v : approx)
      if(!used[v]){
        cover.push_back(v);
        k--;
      }

    return k >= 0;
  }

  bool Solve(){
    if(!reduce())
      return false;

    // Now graph has at most k^2 vertices and k^2 + k edges
    vi approx = twoApproximation();
    if(k < 0 || SZ(approx) > 2 * k)
      return false;

    return coverByApproximation(approx);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, m;
  cin >> n >> m;

  VertexCover vc(n, 28);

  FOR(i, m){
    int u, v;
    cin >> u >> v;
    vc.AddEdge(u, v);
  }

  auto good = vc.Solve();
  if(good){
    cout << n - SZ(vc.cover) << '\n';
  }else{
    cout << "-1" << '\n';
  }
}
