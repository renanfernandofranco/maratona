/**
 * Hopcroft–Karp Algorithm for Minimum Vertex Cover in Bipartite Graph
 *
 * Time Complexity:
 *  - Time: O(|V|^0.5 * |E|)
 *  - Space: O(|V| + |E|)
 *
 * Problem: https://codeforces.com/group/3qadGzUdR4/contest/101712/problem/A
 *
 */
#include <bits/stdc++.h>

using namespace std;
#define NIL 0
#define INF (1<<28)
#define FORI(i, b) for(int i = 1; i <= b; i++)
#define all(v) v.begin(), v.end()
using vi = vector<int>;
using vvi = vector<vi>;

struct MinCoverBip {
  int n, m, nm;
  vvi G;
  vi match, dist;
  // n: number of nodes on left side, nodes are numbered 1 fun n
  // m: number of nodes on right side, nodes are numbered n+1 fun n+m
  // G = NIL[0] ∪ G1[G[1---n]] ∪ G2[G[n+1---n+m]]
  MinCoverBip(int n, int m):
    n(n), m(m), nm(n + m + 1), G(nm), match(nm), dist(nm){}

  void AddEdge(int vl, int vr){
    G[vl].push_back(vr + n);
    G[vr + n].push_back(vl);
  }

  bool bfs() {
    queue<int> q;
    FORI(i, n)
      if(match[i] == NIL) {
        dist[i] = 0;
        q.push(i);
      }
      else
        dist[i] = INF;

    dist[NIL] = INF;
    while(!q.empty()) {
      int u = q.front(); q.pop();
      if(u != NIL)
        for(int v : G[u])
          if(dist[match[v]] == INF) {
            dist[match[v]] = dist[u] + 1;
            q.push(match[v]);
          }
    }
    return dist[NIL] != INF;
  }

  bool biColor(int u) {
    if(u != NIL) {
      for(int v : G[u])
        if(dist[match[v]] == dist[u] + 1) {
          if(biColor(match[v])) {
            match[v] = u;
            match[u] = v;
            return true;
          }
        }
      dist[u] = INF;
      return false;
    }
    return true;
  }

  int maxMatching() {
    int ret = 0;
    fill(all(match), NIL);
    while(bfs())
      FORI(i, n)
        if(match[i] == NIL && biColor(i))
          ret++;
    return ret;
  }

  vvi solve(){
    maxMatching();

    vvi cover(2, vi());
    vi Z(nm, 0);
    queue<int> q;

    fill(all(dist), INF);
    FORI(u, n)
      if(match[u] == NIL){
        q.push(u);
        Z[u] = true;
        dist[u] = 0;
      }

    while(!q.empty()){
      int u = q.front(); q.pop();
      for(int v : G[u])
        if (dist[v] == INF && (dist[u] ^ (match[v] != u))){
            dist[v] = !dist[u];
            Z[v] = true;
            q.push(v);
          }
      }

    FORI(u, n)
      if(match[u] != NIL && !Z[u])
        cover[0].push_back(u);

    for(int u = n + 1; u < nm; u++)
      if(match[u] != NIL && Z[u])
        cover[1].push_back(u - n);

    return cover;
  }
};
