/**
 * Minimum Edge Cover for Bipartite Graphs
 * 
 * How minimum edge cover is equal to maximum matching
 * extended it greedily so that all vertices are covered,
 * then is possible obtained an algorithm for general 
 * graphs replacing the matching algorithm
 * 
 * Complexity:
 *    - Time: matching complexity + O(V + E)
 *    - Space: O(V + E)
 * 
 * In this case matching complexity is O(VE)
 * 
 * Attention: This algorithm says that graphs with isolated
 * vertices has an edge cover!!!
 * 
 */

#include <bits/stdc++.h>

using namespace std;

using ii = pair<int, int>;
using vi = vector<int>;
#define FOR(i, b) for(int i = 0; i < (b); i++)

struct BipMatcher {
   int n, m;
   vector<vi> adj;
   vector<char> seen;
   vi matchR;

   BipMatcher(int n, int m):n(n), m(m), adj(n), seen(m), matchR(m, -1){}

   void AddEdge(int u, int v){
      adj[u].push_back(v);
   }

   bool bpm(int u){ 
      for (int v: adj[u]){
         if (!seen[v]){
            seen[v] = true;  
            if (matchR[v] == -1 || bpm(matchR[v])){ 
               matchR[v] = u; 
               return true; 
            }
         }
      }
      return false; 
   }   

   int Solve(){
      int result = 0;

      FOR(u, n){
         seen.assign(m, false);
         if (bpm(u))
            result++; 
      }

      return result; 
   }
};

struct MinEdgeCover{
   int n, m;
   BipMatcher matcher;

   MinEdgeCover(int n, int m): n(n), m(m), matcher(n, m){}

   void AddEdge(int u, int v){
      matcher.AddEdge(u, v);
   }
   
   vector<ii> Solve(){
      matcher.Solve();

      vector<ii> ret;
      vector<char> seenL(n, false), seenR(m, false);
      
      FOR(v, m){
         int u = matcher.matchR[v]; 
         if(u != -1){
            seenL[u] = seenR[v] = true;
            ret.emplace_back(u, v);
         }
      }

      FOR(u, n)
         for(int v : matcher.adj[u])
            if(!seenL[u] || !seenR[v]){
               seenL[u] = seenR[v] = true;
               ret.emplace_back(u, v);
            }
         
      
      // The existence of isolated vertex can be checked here

      return ret;
   }
};

int main(){ 
   cin.sync_with_stdio(0);
   cin.tie(0);
   int n, m, v;
   cin >> n >> m;
   
   MinEdgeCover edgeCover(n, m);

   FOR(u, n)
      while(cin >> v, v)
         edgeCover.AddEdge(u, v - 1);

   auto cover = edgeCover.Solve();

   cout << cover.size() << endl;
   for(ii p : cover)
      cout << p.first + 1 << ' ' << p.second + 1 << endl;

   return 0; 
}
