/**
 * Link-Cut Tree - Vertex Set, Path Composite
 * 
 * Each vertex store a linear Function, and the Link-Cut store
 * the path composite of the prefix and of the suffix, and
 * can answer the queries of the type f_u(f_...(f_v(x))) mod M
 * 
 * Complexity:
 *  - Time: O(lg n) amortized per operation
 *  - Space: O(n)
 * 
 * https://judge.yosupo.jp/problem/dynamic_tree_vertex_set_path_composite
 * 
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < (b); i++)

const int MD = 998'244'353;

template <class T = int>
struct MOD{
   T v;

   MOD(T vl = 0){ this->operator=(vl); }

   void operator *= (const MOD& m2){ (*this) = this->operator*(m2); }

   void operator += (const MOD& m2){ (*this) = this->operator+(m2); }
   
   void operator -= (const MOD& m2){ (*this) = this->operator-(m2); }

   void operator = (T vl){ v = vl; }

   bool operator == (const MOD& m2){ return v == m2.v; }

   MOD operator * (const MOD& m2) const{
      return v * 1LL * m2.v % MD;
   }

   MOD operator + (const MOD& m2) const{
      T tmp = v + m2.v;
      return tmp >= MD ? tmp - MD : tmp;
   }

   MOD operator - (const MOD& m2) const{
      T tmp = v - m2.v;
      return tmp < 0 ? tmp + MD : tmp;
   }
};

struct Function{
   MOD<int> a = 1, b = 0;
   Function operator +(const Function& f2) const{
      return Function{a * f2.a, a * f2.b + b};
   }

   int operator ()(int x){
      return (a * x + b).v; 
   }
};

template <class U>
struct LinkCut {
   struct Node {
      int p = 0, c[2] = {0, 0}, pp = 0;
      bool flip = 0;
      U val, dp[2]; // dp[0] = prefix composite, dp[1] = suffix composite
   };
   int n;
   vector<Node> T;
   
   LinkCut(int n) : n (n + 1), T(n + 1){}
   
   // SPLAY TREE OPERATIONS START

   int dir(int x, int y) { return T[x].c[1] == y; }
   
   void set(int x, int d, int y) {
      if (x) T[x].c[d] = y, pull(x);
      if (y) T[y].p = x;
   }
   
   void pull(int x) {
      if (!x) return;
      int f = T[x].flip, l = T[x].c[f], r = T[x].c[!f];
      int fl = T[l].flip, fr = T[r].flip;
      T[x].dp[f] = T[l].dp[fl] + T[x].val + T[r].dp[fr];
      T[x].dp[!f] = T[r].dp[!fr] + T[x].val + T[l].dp[!fl];
   }
   
   void push(int x) {
      if (!x || !T[x].flip) return;
      int &l = T[x].c[0], &r = T[x].c[1];
      swap(l, r); T[l].flip ^= 1; T[r].flip ^= 1;
      T[x].flip = 0;
   }
   
   void rotate(int x, int d) { 
      int y = T[x].p, z = T[y].p, w = T[x].c[d];
      swap(T[x].pp, T[y].pp);
      set(y, !d, w);
      set(x, d, y);
      set(z, dir(z, y), x);
   }
   
   void splay(int x) { 
      for (push(x); T[x].p;) {
         int y = T[x].p, z = T[y].p;
         push(z); push(y); push(x);
         int dx = dir(y, x), dy = dir(z, y);
         if (!z) 
         rotate(x, !dx); 
         else if (dx == dy) 
         rotate(y, !dx), rotate(x, !dx); 
         else
         rotate(x, dy), rotate(x, dx);
      }
   }
   
   // SPLAY TREE OPERATIONS END
   
   void MakeRoot(int u) {
      Access(u);
      int l = T[u].c[0];
      T[l].flip ^= 1;
      swap(T[l].p, T[l].pp);
      set(u, 0, 0);
   }
   
   void Access(int _u) {
      for (int v = 0, u = _u; u; u = T[v = u].pp) {
         splay(u); splay(v);
         int r = T[u].c[1];
         T[v].pp = 0;
         swap(T[r].p, T[r].pp);
         set(u, 1, v);
      }
      splay(_u);
   }

   void Link(int u, int v) { 
      assert(!Connected(u, v));
      MakeRoot(v);
      T[v].pp = u;
   }

   void Cut(int u, int v) {
      MakeRoot(u); Access(u); splay(v);
      assert(T[v].pp == u);
      T[v].pp = 0;
   }

   bool Connected(int u, int v) {
      if (u == v) return true;
      MakeRoot(u); Access(v); splay(u);
      return T[v].p == u || T[T[v].p].p == u;
   }

   // return the function f_u(f_...(f_v(x)))
   U PathComposite(int u, int v) {
      MakeRoot(u); Access(v);
      int l = T[v].c[0];
      return T[l].dp[T[l].flip] + T[v].val;
   }

   void Set(int u, U val){
      T[u].val = val;
      Access(u);
   }
};

int main(){
   cin.sync_with_stdio(0);
   cin.tie(0);
   int n, q;
   cin >> n >> q;
   LinkCut<Function> lc(n);

   FOR(i, n){
      int a, b;
      cin >> a >> b;
      lc.Set(i + 1, {a, b});
   }

   FOR(i, n - 1){
      int u, v;
      cin >> u >> v;
      lc.Link(u + 1, v + 1);
   }

   FOR(i, q){
      int tp, u, v, p, w, x, a, b;
      cin >> tp;
      switch(tp){
         case 0:
            cin >> u >> v >> w >> x;
            lc.Cut(u + 1, v + 1);
            lc.Link(w + 1, x + 1);
            break;
         case 1:
            cin >> p >> a >> b;
            lc.Set(p + 1, {a, b});
            break;
         case 2:
            cin >> u >> v >> x;
            cout << lc.PathComposite(v + 1, u + 1)(x) << endl;
            break;            
      }
   }
}
