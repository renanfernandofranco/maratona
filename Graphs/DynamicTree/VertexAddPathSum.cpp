/**
 * Link-Cut Tree - Vertex Add, Path Sum
 * 
 * Complexity:
 *  - Time: O(lg n) amortized per operation
 *  - Space: O(n)
 * 
 * https://judge.yosupo.jp/problem/dynamic_tree_vertex_add_path_sum
 * 
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < (b); i++)

template <class U>
struct LinkCut {
   struct Node {
      int p = 0, c[2] = {0, 0}, pp = 0;
      bool flip = 0;
      U val, dp = 0;
   };
   int n;
   vector<Node> T;
   
   LinkCut(int n) : n (n + 1), T(n + 1){}
   
   // SPLAY TREE OPERATIONS START

   int dir(int x, int y) { return T[x].c[1] == y; }
   
   void set(int x, int d, int y) {
      if (x) T[x].c[d] = y, pull(x);
      if (y) T[y].p = x;
   }
   
   void pull(int x) {
      if (!x) return;
      int &l = T[x].c[0], &r = T[x].c[1];
      T[x].dp = T[x].val + T[l].dp + T[r].dp;
   }
   
   void push(int x) {
      if (!x || !T[x].flip) return;
      int &l = T[x].c[0], &r = T[x].c[1];
      swap(l, r); T[l].flip ^= 1; T[r].flip ^= 1;
      T[x].flip = 0;
   }
   
   void rotate(int x, int d) { 
      int y = T[x].p, z = T[y].p, w = T[x].c[d];
      swap(T[x].pp, T[y].pp);
      set(y, !d, w);
      set(x, d, y);
      set(z, dir(z, y), x);
   }
   
   void splay(int x) { 
      for (push(x); T[x].p;) {
         int y = T[x].p, z = T[y].p;
         push(z); push(y); push(x);
         int dx = dir(y, x), dy = dir(z, y);
         if (!z) 
         rotate(x, !dx); 
         else if (dx == dy) 
         rotate(y, !dx), rotate(x, !dx); 
         else
         rotate(x, dy), rotate(x, dx);
      }
   }
   
   // SPLAY TREE OPERATIONS END
   
   void MakeRoot(int u) {
      Access(u);
      int l = T[u].c[0];
      T[l].flip ^= 1;
      swap(T[l].p, T[l].pp);
      set(u, 0, 0);
   }
   
   void Access(int _u) {
      for (int v = 0, u = _u; u; u = T[v = u].pp) {
         splay(u); splay(v);
         int r = T[u].c[1];
         T[v].pp = 0;
         swap(T[r].p, T[r].pp);
         set(u, 1, v);
      }
      splay(_u);
   }

   void Link(int u, int v) { 
      assert(!Connected(u, v));
      MakeRoot(v);
      T[v].pp = u;
   }

   void Cut(int u, int v) {
      MakeRoot(u); Access(u); splay(v);
      assert(T[v].pp == u);
      T[v].pp = 0;
   }

   bool Connected(int u, int v) {
      if (u == v) return true;
      MakeRoot(u); Access(v); splay(u);
      return T[v].p == u || T[T[v].p].p == u;
   }

   U PathSum(int u, int v) {
      MakeRoot(u); Access(v);
      return T[v].val + T[T[v].c[0]].dp;
   }

   void Add(int u, U val){
      T[u].val += val;
      Access(u);
   }
};

int main(){
   cin.sync_with_stdio(0);
   cin.tie(0);
   int n, q;
   cin >> n >> q;
   LinkCut<long long> lc(n);

   FOR(i, n){
      int vl;
      cin >> vl;
      lc.Add(i + 1, vl);
   }

   FOR(i, n - 1){
      int u, v;
      cin >> u >> v;
      lc.Link(u + 1, v + 1);
   }

   FOR(i, q){
      int tp, u, v, p, w, x;
      cin >> tp;
      switch(tp){
         case 0:
            cin >> u >> v >> w >> x;
            lc.Cut(u + 1, v + 1);
            lc.Link(w + 1, x + 1);
            break;
         case 1:
            cin >> p >> x;
            lc.Add(p + 1, x);
            break;
         case 2:
            cin >> u >> v;
            cout << lc.PathSum(u + 1, v + 1) << endl;
            break;            
      }
   }
}
