/**
 * Euler Tour Tree - SubTree Add, SubTreeSum - Using Splay Tree
 * 
 * Complexity:
 *  - Time: O(lg n) amortized per operation
 *  - Space: O(n)
 * 
 * https://judge.yosupo.jp/problem/dynamic_tree_vertex_add_subtree_sum
 * 
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'
using ll = long long;
using ii = pair<int, int>;

template <class U>
struct EulerTree{
   struct Node {
      int p = 0, c[2] = {0, 0}, sz = 1;
      U val, dp = 0;
   };
   int n;
   vector<Node> T;
   vector<map<int, ii>> edges;

   EulerTree(int n) : T(n), edges(n){T[0].sz = 0;}
   
   // SPLAY TREE OPERATIONS START

   int dir(int x, int y) { return T[x].c[1] == y; }
   
   void set(int x, int d, int y) {
      if (x) T[x].c[d] = y, pull(x);
      if (y) T[y].p = x;
   }
   
   void pull(int x) {
      if (!x) return;
      int l = T[x].c[0], r = T[x].c[1];
      T[x].dp = T[x].val + T[l].dp + T[r].dp;
      T[x].sz = 1 + T[l].sz + T[r].sz;
   }
   
   void rotate(int x, int d) { 
      int y = T[x].p, z = T[y].p, w = T[x].c[d];
      set(y, !d, w);
      set(x, d, y);
      set(z, dir(z, y), x);
   }
   
   void splay(int x) { 
      for (; T[x].p;) {
         int y = T[x].p, z = T[y].p;
         int dx = dir(y, x), dy = dir(z, y);
         if (!z) 
         rotate(x, !dx); 
         else if (dx == dy) 
         rotate(y, !dx), rotate(x, !dx); 
         else
         rotate(x, dy), rotate(x, dx);
      }
   }

   int root(int v){
      while(T[v].p)
         v = T[v].p;
      return v;
   }

   int merge(int v, int u){
      if(u != 0){
         u = root(u);
         while(T[u].c[0])
            u = T[u].c[0];
         splay(u);
         set(u, 0, root(v));
      }
      return v;
   }

   template<typename First, typename... Rest>
   int merge(First s, Rest... t){
      return merge(s, merge(t...));
   }

   int cutEdge(int v, int dir){
      int c = T[v].c[dir];
      set(v, dir, 0);
      T[c].p = 0;
      return c;
   }

   int split(int v, int dir){
      splay(v);
      return cutEdge(v, dir);
   }

   int makeRoot(int v){
      return merge(v, split(v, 0));
   }
   
   // SPLAY TREE OPERATIONS END

   int getNode(){
      T.push_back(Node());
      return T.size() - 1;
   }

   int getPos(int v){
      int pos = T[T[v].c[0]].sz;
      while(T[v].p){
         bool isRight = dir(T[v].p, v);
         v = T[v].p;
         if(isRight)
            pos += 1 + T[T[v].c[0]].sz;
      }
      return pos;
   }

   void Set(int v, U val){
      T[v].val = T[v].dp = val;
   }

   void Add(int v, U val){
      splay(v);
      T[v].val += val;
      T[v].dp += val;
   }

   U Sum(int v){
      // splay(v);
      return T[root(v)].dp;
   }

   void Link(int v, int u){
      if(v > u) swap(v, u);
      int e1 = getNode(), e2 = getNode();
      edges[v][u] = {e1, e2};
      merge(makeRoot(v), e1, makeRoot(u), e2);
   }

   void Cut(int v, int u){
      if(v > u) swap(v, u);
      assert(edges[v].count(u));
      auto edge = edges[v][u];
      edges[v].erase(u);

      int e1 = edge.first;
      int e2 = edge.second;
      int pos1 = getPos(e1);
      int pos2 = getPos(e2);
      if(pos1 > pos2) swap(e1, e2);

      int r = split(e2, 1);
      cutEdge(e2, 0);
      split(e1, 1);
      int l = cutEdge(e1, 0);
      merge(l, r);
   }
};

int main(){
   cin.tie(0)->sync_with_stdio(0);
   int n, q, u, v, val, tp, w, x, p;
   cin >> n >> q;
   EulerTree<long long> ETT(n + 1);

   FOR(i, n){
      cin >> val;
      ETT.Set(i + 1, val);
   }

   FOR(i, n - 1){
      cin >> u >> v;
      ETT.Link(u + 1, v + 1);
   }

   FOR(i, q){
      cin >> tp;
      switch (tp) {
         case 0:
            cin >> u >> v >> w >> x;
            ETT.Cut(u + 1, v + 1);
            ETT.Link(w + 1, x + 1);
            break;
         case 1:
            cin >> p >> val;
            ETT.Add(p + 1, val);
            break;
         case 2:
            cin >> v >> p;
            ETT.Cut(v + 1, p + 1);
            cout << ETT.Sum(v + 1) << endl;
            ETT.Link(v + 1, p + 1);
      }
   }
}
