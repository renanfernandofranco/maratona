/**
 * Euler Tour Tree - SubTree Add, SubTreeSum - Using Treap
 * 
 * Complexity:
 *  - Time: O(lg n) probabilistic
 *  - Space: O(n)
 * 
 * https://judge.yosupo.jp/problem/dynamic_tree_vertex_add_subtree_sum
 * 
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'
using ll = long long;

template <class U>
struct EulerTree{
  struct Node{
    U val, dp;
    int sz, prior;
    Node *l = NULL, *r = NULL, *p = NULL;
    Node (U val): val(val), dp(val), sz(1), prior(rand()){}
  };

  using Treap = Node*;
  vector<Treap> T;
  vector<map<int, pair<Treap, Treap>>> edges;

  EulerTree(int n) : T(n), edges(n){}

  int sz (Treap t) {
    return t ? t->sz : 0;
  }

  void pull(Treap t){
    t->dp = t->val + (t->l ? t->l->dp : 0) + (t->r ? t->r->dp : 0);
  }

  void update (Treap t) {
    if (t){
      pull(t);
      t->sz = 1 + sz(t->l) + sz(t->r);
      t->p = NULL;
      if(t->l)
        t->l->p = t;
      if(t->r)
        t->r->p = t;
    }
  }

  void merge (Treap & t, Treap l, Treap r) {
    if (!l || !r)
      t = l ? l : r;
    else if (l->prior > r->prior)
      merge (l->r, l->r, r),  t = l;
    else
      merge (r->l, l, r->l),  t = r;
    update (t);
  }

  void split (Treap t, Treap & l, Treap & r, int key, int add = 0) {
    if (!t)
      return void( l = r = NULL );
    int cur_key = add + sz(t->l); //implicit key
    if (key <= cur_key)
      split (t->l, l, t->l, key, add),  r = t;
    else
      split (t->r, t->r, r, key, add + 1 + sz(t->l)),  l = t;
    update(t);
  }

  pair<Treap, int> getPos(Treap t){
    int pos = sz(t->l);

    while(t->p){
      bool isRight = t->p->r == t;
      t = t->p;
      if(isRight)
        pos += 1 + sz(t->l);
    }
    return {t, pos};
  }

  Treap makeRoot(int v){
    Treap t;
    auto pr = getPos(T[v]);
    split(pr.first, pr.first, t, pr.second);
    merge(t, t, pr.first);
    return t;
  }

  void Set(int v, U val){
    T[v] = new Node(val);
  }

  void Add(int v, U val){
    T[v]->val += val;
    for(Treap t = T[v]; t; t = t->p)
      pull(t);
  }

  U Sum(int v){
    return getPos(T[v]).first->dp;
  }

  void Link(int v, int u){
    if(v > u) swap(v, u);
    Treap tv = makeRoot(v);
    Treap tu = makeRoot(u);
    Treap e1 = new Node(0);
    Treap e2 = new Node(0);
    edges[v][u] = {e1, e2};
    merge(tv, tv, e1);
    merge(tv, tv, tu);
    merge(tv, tv, e2);
  }

  void Cut(int v, int u){
    if(v > u) swap(v, u);
    assert(edges[v].count(u));
    auto edge = edges[v][u];
    edges[v].erase(u);

    auto prv = getPos(edge.first);
    auto pru = getPos(edge.second);
    Treap t = prv.first, after, tmp;
    int pos1 = prv.second;
    int pos2 = pru.second;
    if(pos1 > pos2) swap(pos1, pos2);

    split(t, t, after, pos2 + 1);
    split(t, t, tmp, pos2);
    split(t, t, tmp, pos1 + 1);
    split(t, t, tmp, pos1);
    merge(t, t, after);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q, u, v, val, tp, w, x, p;
  cin >> n >> q;
  EulerTree<long long> ETT(n);

  FOR(i, n){
    cin >> val;
    ETT.Set(i, val);
  }

  FOR(i, n - 1){
    cin >> u >> v;
    ETT.Link(u, v);
  }

  FOR(i, q){
    cin >> tp;
    switch (tp) {
      case 0:
        cin >> u >> v >> w >> x;
        ETT.Cut(u, v);
        ETT.Link(w, x);
        break;
      case 1:
        cin >> p >> val;
        ETT.Add(p, val);
        break;
      case 2:
        cin >> v >> p;
        ETT.Cut(v, p);
        cout << ETT.Sum(v) << endl;
        ETT.Link(v, p);
    }
  }
}
