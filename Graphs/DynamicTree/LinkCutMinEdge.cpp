/**
 * Link-Cut Tree - Min Edge
 *
 * Performs the following operations in O(lg n) amortized:
 *  - Connect two vertices
 *  - Separete two vertices
 *  - Make a vertex the root of your tree
 *  - Check if two vertices are connected
 *  - Find the edge with the minimum cost between two vertices
 *
 * Problem G of https://codeforces.com/gym/101564
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'

struct Edge{
  int u, v, c;
  bool operator < (const Edge&  e2)const{
    return c != e2.c ? (c < e2.c) : (u != e2.u ? u < e2.u : v < e2.v);
  }
  Edge(int u = 0, int v = 0, int c = INT_MAX): u(u), v(v), c(c){}
};

struct LinkCut {
  struct Node {
    int p = 0, c[2] = {0, 0}, pp = 0;
    bool flip = 0;
    Edge val = Edge(), dp = Edge();
  };
  int n, ids;
  map<Edge, int> mapEdges;
  vector<Node> T;

  LinkCut(int n, int q) : T(n + 1 + q), n (n + 1), ids (n + 1){}

  // SPLAY TREE OPERATIONS START

  int dir(int x, int y) { return T[x].c[1] == y; }

  void set(int x, int d, int y) {
    if (x) T[x].c[d] = y, pull(x);
    if (y) T[y].p = x;
  }

  void pull(int x) {
    if (!x) return;
    int &l = T[x].c[0], &r = T[x].c[1];
    T[x].dp = min({T[x].val, T[l].dp, T[r].dp});
  }

  void push(int x) {
    if (!x || !T[x].flip) return;
    int &l = T[x].c[0], &r = T[x].c[1];
    swap(l, r); T[l].flip ^= 1; T[r].flip ^= 1;
    T[x].flip = 0;
  }

  void rotate(int x, int d) {
    int y = T[x].p, z = T[y].p, w = T[x].c[d];
    swap(T[x].pp, T[y].pp);
    set(y, !d, w);
    set(x, d, y);
    set(z, dir(z, y), x);
  }

  void splay(int x) {
    for (push(x); T[x].p;) {
      int y = T[x].p, z = T[y].p;
      push(z); push(y); push(x);
      int dx = dir(y, x), dy = dir(z, y);
      if (!z)
        rotate(x, !dx);
      else if (dx == dy)
        rotate(y, !dx), rotate(x, !dx);
      else
        rotate(x, dy), rotate(x, dx);
    }
  }

  // SPLAY TREE OPERATIONS END

  void MakeRoot(int u) {
    Access(u);
    int l = T[u].c[0];
    T[l].flip ^= 1;
    swap(T[l].p, T[l].pp);
    set(u, 0, 0);
  }

  void Access(int _u) {
    for (int v = 0, u = _u; u; u = T[v = u].pp) {
      splay(u); splay(v);
      int r = T[u].c[1];
      T[v].pp = 0;
      swap(T[r].p, T[r].pp);
      set(u, 1, v);
    }
    splay(_u);
  }

  void _Link(int u, int v) {
    assert(!Connected(u, v));
    MakeRoot(v);
    T[v].pp = u;
  }

  void _Cut(int u, int v) {
    MakeRoot(u); Access(u); splay(v);
    assert(T[v].pp == u);
    T[v].pp = 0;
  }

  void Cut(Edge edge){
    int id = mapEdges[edge];
    _Cut(edge.u, id);
    _Cut(id, edge.v);
    mapEdges.erase(edge);
  }

  void Link(int u, int v, Edge edge){
    int id = ids++;
    T[id].dp = T[id].val = edge;
    mapEdges[edge] = id;
    _Link(u, id);
    _Link(id, v);
  }

  bool Connected(int u, int v) {
    if (u == v) return true;
    MakeRoot(u); Access(v); splay(u);
    return T[v].p == u || T[T[v].p].p == u;
  }

  Edge GetMinEdge(int u, int v) {
    MakeRoot(u); Access(v);
    assert(T[v].dp.c != INT_MAX);
    return T[v].dp;
  }
};

int solve(vector<Edge>& edges, int n, int m){
  LinkCut lk(n, m);
  set<Edge> onTree;
  int res = INT_MAX;

  for(auto e : edges){
    int u = e.u, v = e.v, c = e.c;

    if (lk.Connected(u, v)){
      Edge edge = lk.GetMinEdge(u, v);
      lk.Cut(edge);
      onTree.erase(edge);
    }

    lk.Link(u, v, e);
    onTree.insert(e);

    if (onTree.size() == n - 1)
      res = min(res, onTree.rbegin()->c - onTree.begin()->c);
  }
  return res;
}

int main(){
  cin.sync_with_stdio(0);
  cin.tie(0);
  int n, m, cnt = 1;
  while(cin >> n, n){
    cin >> m;
    vector<Edge> edges(m);
    for(auto& e: edges){
      cin >> e.u >> e.v >> e.c;
      e.u++; e.v++;
    }
    sort(edges.begin(), edges.end());
    cout << solve(edges, n, m) << endl;
  }
}
