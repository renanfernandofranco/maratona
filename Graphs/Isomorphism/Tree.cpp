/**
 * Tree Isomorphism
 *
 * Given two rooted trees, check if they are isomorphic
 *
 * Complexity:
 *  - Time: O(n * lg n)
 *  - Space: O(n)
 *
 * Problem: https://cses.fi/problemset/task/1700
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using vi = vector<int>;
using vvi = vector<vi>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define REV(i, b) for(int i = b - 1; i >= 0; i--)
#define FORI(i, b)  for(int i = 1; i <= (int)(b); i++)
#define all(a) a.begin(), a.end()

template <int MOD>
struct MINT{
  int v;

  MINT(ll val = 0){
    v = (-MOD <= val && val < MOD) ? val : val % MOD;
    if(v < 0) v += MOD;
  }

  MINT operator - () const { return MINT(-v); }
  MINT operator * (const MINT& m2) const { return v * 1LL * m2.v % MOD; }
  MINT operator / (const MINT& m2) const { return (*this) * inv(m2); }
  MINT& operator *= (const MINT& m2){ return (*this) = (*this) * m2; }
  MINT& operator /= (const MINT& m2){ return (*this) = (*this) / m2; }
  MINT& operator += (const MINT& m2){ return (*this) = (*this) + m2; }
  MINT& operator -= (const MINT& m2){ return (*this) = (*this) - m2; }
  bool operator == (const MINT& m2) const { return v == m2.v; }
  bool operator != (const MINT& m2) const { return v != m2.v; }
  bool operator < (const MINT& m2) const { return v < m2.v; }
  friend istream& operator >> (istream &is, MINT &a) { ll t; is >> t; a = MINT(t); return is; }
  friend ostream& operator << (ostream &os, const MINT &a) { return os << a.v; }
  friend MINT inv(const MINT& a) { return pw(a, MOD - 2); } // only if MOD is prime

  MINT operator + (const MINT& m2) const{
    int tmp = v + m2.v;
    return tmp >= MOD ? tmp - MOD : tmp;
  }

  MINT operator - (const MINT& m2) const{
    int tmp = v - m2.v;
    return tmp < 0 ? tmp + MOD : tmp;
  }

  friend MINT pw(MINT a, ll b){
    MINT ret = 1;
    while(b){
      if(b & 1) ret *= a;
      b >>= 1; a *= a;
    }
    return ret;
  }
};

using mint = MINT<1'000'000'007>;
const mint BASE = 31;

struct Tree{
  int n;
  vvi adj;
  vector<mint> hs, pot;
  vi sz;

  Tree(int n): n(n), adj(n), hs(n), pot(n, 1), sz(n, 1){
    FOR(i, n - 1)
      pot[i + 1] = pot[i] * BASE;
  }

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void dfs(int v, int h = 0){
    for (int u : adj[v]){
      adj[u].erase(find(all(adj[u]), v));
      dfs(u, h + 1);
    }

    sort(all(adj[v]), [&](int u, int v){
      return hs[u] != hs[v] ? hs[u] < hs[v] : sz[u] < sz[v];
    });

    hs[v] = h;
    for (int u : adj[v]){
      hs[v] += hs[u] * pot[sz[v]];
      sz[v] += sz[u];
    }
  }
};

bool isIsomorphic(Tree& A, Tree& B, int va, int vb){
  if(A.hs[va] != B.hs[vb] || A.adj[va].size() != B.adj[vb].size())
    return false;

  int n = A.adj[va].size();
  FOR(i, n)
    if(!isIsomorphic(A, B, A.adj[va][i], B.adj[vb][i]))
      return false;
  return true;
}

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int t;

  cin >> t;

  while(t--){
    int n;

    cin >> n;
    Tree A(n), B(n);

    FOR(i,  n - 1){
      int u, v;
      cin >> u >> v; u--; v--;
      A.AddEdge(u, v);
    }

    FOR(i,  n - 1){
      int u, v;
      cin >> u >> v; u--; v--;
      B.AddEdge(u, v);
    }

    A.dfs(0);
    B.dfs(0);
    cout << (isIsomorphic(A, B, 0, 0) ? "YES" : "NO") << endl;
  }
}
