/**
 * Dijkstra for sparse graphs with STL heap
 *
 * Complexity:
 *   - Time: O((n + m) lg n)
 *   - Space: O(n + m)
 *
 * Problem: https://judge.yosupo.jp/problem/shortest_path
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ii = pair<int, int>;
using vi = vector<int>;
using vll = vector<ll>;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'

template<class U>
struct Graph{
  struct Edge{
    int to;
    U w;
    bool operator > (const Edge& e2) const { return w > e2.w; };
  };
  const U INF = numeric_limits<U>::max();

  int n;
  vector<vector<Edge>> adj;

  Graph(int n): n(n), adj(n){}

  void AddEdge(int from, int to, U w, bool dir = true){
    adj[from].push_back({to, w});
    if(!dir) adj[to].push_back({from, w});
  }

  void Dijkstra(int s, vector<U>& d, vector<int>& p){
    d.assign(n, INF);
    p.assign(n, -1);

    d[s] = 0;
    p[s] = s;
    priority_queue<Edge, vector<Edge>, greater<Edge>> q;
    q.push({s, d[s]});

    while (!q.empty()) {
      int v = q.top().to;
      U d_v = q.top().w;
      q.pop();

      if(d[v] != d_v)
        continue;

      for (auto edge : adj[v]) {
        U len = edge.w;
        int to = edge.to;

        if (d[v] + len < d[to]) {
          d[to] = d[v] + len;
          p[to] = v;
          q.push({to, d[to]});
        }
      }
    }
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, s, t, v, u, w;
  cin >> n >> m >> s >> t;

  Graph<ll> G(n);
  FOR(i, m){
    cin >> v >> u >> w;
    G.AddEdge(v, u, w);
  }

  vll d;
  vi p;
  G.Dijkstra(s, d, p);

  if(p[t] == -1){
    cout << -1 << endl;
  }else{
    ll dist = d[t];
    list<ii> path;
    for( ; p[t] != t; t = p[t])
      path.push_front({p[t], t});

    cout << dist << ' ' << path.size() << endl;
    for(ii pr : path)
      cout << pr.first << ' ' << pr.second << endl;
  }
}
