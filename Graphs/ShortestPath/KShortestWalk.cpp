/**
 * K-Shortest Walk
 *
 * Given a source s, find the first k shortest paths of
 * s for all vertices
 *
 * Complexity:
 *   - Time: O(k(n + m) lg n)
 *   - Space: O(kn + m)
 *
 * Problem: https://cses.fi/problemset/task/1196/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ii = pair<int, int>;
using vi = vector<int>;
using vll = vector<ll>;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'

template<class U>
struct Graph{
  struct Edge{
    int to;
    U w;
    bool operator > (const Edge& e2) const { return w > e2.w; };
  };
  const U INF = numeric_limits<U>::max();

  int n;
  vector<vector<Edge>> adj;

  Graph(int n): n(n), adj(n){}

  void AddEdge(int from, int to, U w, bool dir = true){
    adj[from].push_back({to, w});
    if(!dir) adj[to].push_back({from, w});
  }

  void KShortestWalk(int s, vector<vector<U>>& d, int k){
    d.resize(n);

    d[s].push_back(0);
    priority_queue<Edge, vector<Edge>, greater<Edge>> q;
    q.push({s, d[s].back()});

    while (!q.empty()) {
      int v = q.top().to;
      U d_v = q.top().w;
      q.pop();

      if (d[v].size() > k)
        continue;

      d[v].push_back(d_v);
      for (auto edge : adj[v])
        if(d[edge.to].size() < k)
          q.push({edge.to, d_v + edge.w});
    }
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, k;
  cin >> n >> m >> k;

  int s = 0, t = n - 1;
  Graph<ll> G(n);
  FOR(i, m){
    int v, u, w;
    cin >> v >> u >> w;
    v--; u--;
    G.AddEdge(v, u, w);
  }

  vector<vll> d;
  G.KShortestWalk(s, d, k);

  FOR(i, k)
    cout << d[t][i] << " \n"[i == k - 1];
}
