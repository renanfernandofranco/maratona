/**
 * This algorithm solves the Chinese Postman Problem with a bit modification, instead
 * of finding the cost of Eulerian Circuit, the same finds the cost of Eulerian Path.
 * But is possible solve the classic version with easy changes.
 *
 * Given a graph, with |V| = n and k odd-degree vertices, this algorithm runs the
 * FloydWarshall to find all distances between each vertex, and check at this moment
 * if the graph is connected, since it if is not, so the problem has no solution.
 *
 * After that, it is verified if k <= 2, if positive the graph is already an Eulerian Path.
 *
 * Otherwise, it runs an superpolynomial algorithm to find a matching of size (k / 2 - 1)
 * with minimal cost that will be added to the graph to make it Eulerian.
 *
 * Lastly, is calculated the sum of all edges in the graph
 *
 * Complexity:
 *  - Time: O(k!! + n^3) with constant ~60, where "!!" is the double factorial operation
 *  - Space: O(n^2)
 *
 * Apparently is possible solve this problem in polynomial time using another algorithm
 * for found the pairs of edges, but the only way that I found is using the Blossom
 * Algorithm for Weighted Graphs and the same is very hard to be implemented.
 *
 * Uri: 1053
 *
 */

#include <bits/stdc++.h>

#define SZ(v) ((int)v.size())
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORS(i, b, s) for(int i = 0; i < (b); i += (s))
#define all(a) a.begin(), a.end()
#define endl '\n'

using namespace std;
using ii = pair<int, int>;
using vi = vector<int>;

template<class BidirIt>
bool next_pair_permutation(BidirIt first, BidirIt last){
  if (first == last) return false;
  BidirIt cur = last;
  if (first == --cur) return false;
  if(distance(first, last) % 2 != 0) return false;

  BidirIt nxt, i, j;
  while (true) {
    nxt = cur;
    cur = prev(cur, 2);
    if (*cur < *nxt) {
      for(i = next(cur); i != last; i = next(i, 2))
        if(*cur < *i)
          break;
      if(i == last)
        for(i = prev(last); i != cur; i = prev(i, 2))
          if(*cur < *i)
            break;
      std::iter_swap(cur, i);

      sort(next(cur), last);

      return true;
    }
    if (cur == next(first)) {
      sort(first, last);
      return false;
    }
  }
}

template<class T>
struct Graph{
  struct Edge{
    int to;
    T w;
  };
  const T INF = numeric_limits<T>::max()/2;

  int n;
  vector<vector<Edge>> adj;
  vector<vector<T>> d;

  Graph(int n): n(n), adj(n), d(n, vector<T> (n, INF)){}

  void AddEdge(int u, int v, T w, bool directed = false){
    adj[u].push_back({v, w});
    if(!directed) adj[v].push_back({u, w});
  }

  bool IsConnected(){
    FOR(i, n){
      for(Edge e : adj[i])
        d[i][e.to] = e.w;
      d[i][i] = 0;
    }

    FOR(k, n)
      FOR(i, n)
        FOR(j, n)
          d[i][j] = min(d[i][j] , d[i][k] + d[k][j]);

    FOR(i, n)
      FOR(j, n)
        if(d[i][j] == INF)
          return false;
    return true;
  }

  T sumEdges(){
    T res = 0;
    for(auto& l : adj)
      for(Edge e : l)
        res += e.w;
    return res / 2;
  }

  void createEulerianPath(vi odd){
    vector<ii> toAdd(SZ(odd) / 2 - 1);

    T best = INF;
    do{
      T cur = 0, maxEdg = 0;
      FORS(i, SZ(odd), 2){
        int u = odd[i], v = odd[i + 1];
        cur += d[u][v];
        maxEdg = max(maxEdg, d[u][v]);
      }
      if(cur - maxEdg < best){
        best = cur - maxEdg;
        bool skip = false;
        FORS(i, SZ(odd), 2){
          int u = odd[i], v = odd[i + 1];
          if(!skip && d[u][v] == maxEdg)
            skip = true;
          else
            toAdd[i / 2 - skip] = {u, v};
        }
      }
    }while(next_pair_permutation<vi::iterator>(all(odd)));

    for(auto p : toAdd)
      AddEdge(p.first, p.second, d[p.first][p.second]);
  }

  T Solve(){
    vi odd;
    FOR(i, SZ(adj))
      if(SZ(adj[i]) & 1)
        odd.push_back(i);

    if(SZ(odd) > 2)
      createEulerianPath(odd);

    return sumEdges();
  }
};

Graph<double> readInput();

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int t;
  cin >> t;
  for(int h = 1; h <= t; h++){
    Graph<double> G = readInput();

    if(!G.IsConnected())
      cout << "Case " << h << ": ~x(" << endl;
    else
      cout << "Case " << h << ": " << setprecision(2) << fixed << G.Solve() << endl;
  }
  return 0;
}

Graph<double> readInput(){
  int n;
  cin >> n;

  string d1, d2, da;
  set<pair<string, string>> Edges;
  FOR(i, n){
    cin >> d1 >> d2;
    if(d1 > d2)
      swap(d1, d2);
    da = d1;
    if(d1[0] == d2[0])
      for(d1[1]++; d1 <= d2; da = d1, d1[1]++)
        Edges.insert({da, d1});
    else if(d1[1] == d2[1])
      for(d1[0]++; d1 <= d2; da = d1, d1[0]++)
        Edges.insert({da, d1});
    else if(d1[0] - d2[0] == d1[1] - d2[1])
      for(d1[0]++, d1[1] ++; d1 <= d2; da = d1, d1[0]++, d1[1]++)
        Edges.insert({da, d1});
    else if(d1[0] - d2[0] == d2[1] - d1[1])
      for(d1[0]++, d1[1] --; d1 <= d2; da = d1, d1[0]++, d1[1]--)
        Edges.insert({da, d1});
    else
      Edges.insert({d1, d2});
  }

  int id = 0;
  unordered_map<string, int> mp;
  for(auto e : Edges){
    string u = e.first, v = e.second;
    if(!mp.count(v))
      mp[v] = id++;
    if(!mp.count(u))
      mp[u] = id++;
  }

  Graph<double> G(id);
  for(auto e : Edges){
    string u = e.first, v = e.second;
    G.AddEdge(mp[u], mp[v], hypot(v[0] - u[0], v[1] - u[1]));
  }
  return G;
}
