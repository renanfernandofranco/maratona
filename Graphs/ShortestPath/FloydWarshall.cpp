/**
 * Floyd Warshall - All Shortest Paths
 *
 * Complexity:
 *   - Time: O(n^3)
 *   - Space: O(n^2)
 *
 * Problem: https://cses.fi/problemset/task/1672/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'

template<class U>
struct Graph{
  const U INF = numeric_limits<U>::max() / 2;

  int n;
  vector<vector<U>> d;

  Graph(int n): n(n), d(n, vector<U>(n, INF)){
    FOR(i, n)
      d[i][i] = 0;
  }

  void AddEdge(int from, int to, U w, bool dir = true){
    d[from][to] = min(d[from][to], w);
    if(!dir) d[to][from] = min(d[to][from], w);
  }

  vector<vector<U>> FloydWarshall(){
    FOR(k, n)
      FOR(i, n)
        FOR(j, n)
          d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
    return d;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, v, u, w, q;
  cin >> n >> m >> q;

  Graph<ll> G(n);
  FOR(i, m){
    cin >> v >> u >> w;
    v--; u--;
    G.AddEdge(v, u, w, false);
  }

  auto d = G.FloydWarshall();

  FOR(i, q){
    cin >> v >> u; v--; u--;
    cout << (d[v][u] < G.INF ? d[v][u] : -1) << endl;
  }
}
