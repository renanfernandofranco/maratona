/**
 * Shortest Path Faster Algorithm (SPFA)
 *
 * Attention!!!
 * This algorithm not ignores cycles that are not between S to T
 *
 * Complexity:
 *  - Time: O(nm) in worst case, but O(m) in average case
 *  - Space: O(n + m)
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ii = pair<int, int>;
using vi = vector<int>;
using vll = vector<ll>;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'

template<class U>
struct Graph{
  using Edge = pair<int, U>;
  const U INF = numeric_limits<U>::max();

  int n;
  vector<vector<Edge>> adj;

  Graph(int n): n(n), adj(n){}

  void AddEdge(int from, int to, U w, bool dir = true){
    adj[from].push_back({to, w});
    if(!dir) adj[to].push_back({from, w});
  }

  bool SPFA(int s, vector<U>& d, vector<int>& p) {
    d.assign(n, INF);
    p.assign(n, -1);
    vector<int> cnt(n, 0);
    vector<char> inqueue(n, false);
    queue<int> q;

    d[s] = 0;
    p[s] = s;
    q.push(s);
    inqueue[s] = true;
    while (!q.empty()) {
      int v = q.front();
      q.pop();
      inqueue[v] = false;

      for (auto edge : adj[v]) {
        int to = edge.first;
        U len = edge.second;

        if (d[v] + len < d[to]) {
          d[to] = d[v] + len;
          p[to] = v;
          if (!inqueue[to]) {
            q.push(to);
            inqueue[to] = true;
            cnt[to]++;
            if (cnt[to] > n)
              return false;  // negative cycle
          }
        }
      }
    }
    return true;
  }
};


int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, s, t, v, u, w;
  cin >> n >> m >> s >> t;

  Graph<ll> G(n);
  FOR(i, m){
    cin >> v >> u >> w;
    G.AddEdge(v, u, w);
  }

  vll d;
  vi p;
  G.SPFA(s, d, p);

  if(p[t] == -1){
    cout << -1 << endl;
  }else{
    ll dist = d[t];
    list<ii> path;
    for( ; p[t] != t; t = p[t])
      path.push_front({p[t], t});

    cout << dist << ' ' << path.size() << endl;
    for(ii pr : path)
      cout << pr.first << ' ' << pr.second << endl;
  }
}
