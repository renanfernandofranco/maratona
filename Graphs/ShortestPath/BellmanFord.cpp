/**
 * Bellman Ford - Find shortest path of S to T
 *
 * This algorithm ignores cycles that are not
 * between S to T
 *
 * Complexity:
 *  - Time: O(|V| * |E|)
 *  - Space: O(|V| + |E|)
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using vi = vector<int>;
using vvi = vector<vi>;
using vll = vector<ll>;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'

template<class T>
struct BellmanFord{
  struct Edge{
    int v, u;
    T cost;
  };
  T INF = numeric_limits<T>::max() / 2 - 1;

  int n;
  vector<T> d;
  vector<Edge> edges;
  vvi G, Gt;
  vector<char> fromS, toT;
  BellmanFord(int n): n(n), d(n, 0), G(n), Gt(n),
    fromS(n, false), toT(n, false){}

  void AddEdge(int v, int u, T cost){
    G[v].push_back(u);
    Gt[u].push_back(v);
    edges.push_back({v, u, cost});
  }

  void dfs(int v, vvi& adj, vector<char>& seen){
    seen[v] = true;
    for(int u : adj[v])
      if(!seen[u])
        dfs(u, adj, seen);
  }

  bool Solve(int s, int t, vector<T>& d, vi& p){
    dfs(s, G, fromS);
    dfs(t, Gt, toT);

    d.assign(n, INF);
    p.assign(n, -1);
    d[s] = 0;
    p[s] = s;

    FOR(i, n)
      for(auto e : edges){
        int v = e.v, u = e.u;
        if(d[v] + e.cost < d[u]){
          p[u] = v;
          d[u] = max (-INF, d[v] + e.cost);
          if(i == n - 1 && fromS[u] && toT[u])
            return false; // negative cycle of s to t
        }
      }
    return true;
  }
};


int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, v, u, w;
  cin >> n >> m;

  BellmanFord<ll> G(n);
  FOR(i, m){
    cin >> v >> u >> w;
    v--; u--;
    G.AddEdge(v, u, -w);
  }

  vll d;
  vi p;
  if(G.Solve(0, n - 1, d, p)){
    cout << -d[n - 1] << endl;
  }else{
    cout << "-1\n";
  }
}
