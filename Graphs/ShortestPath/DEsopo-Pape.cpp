/**
 * D´Esopo-Pape algorithm
 * 
 * Complexity:
 *  - Time: Exponential, but frequently will work faster than Dijkstra
 *  - Space: O(n + m) 
 * 
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ii = pair<int, int>;
using vi = vector<int>;
using vll = vector<ll>;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'

template<class U>
struct Graph{
  const U INF = numeric_limits<U>::max();

  struct Edge {
    int to;
    U w;
  };

  int n;
  vector<vector<Edge>> adj;

  Graph(int n): n(n), adj(n){}

  void AddEdge(int from, int to, U w, bool dir = true){
    adj[from].push_back({to, w});
    if(!dir) adj[to].push_back({from, w});
  }

  void Solve(int s, vector<U>& d, vector<int>& p) {
    d.assign(n, INF);
    p.assign(n, -1);
    d[s] = 0;
    p[s] = s;
    vector<int> m(n, 2);
    deque<int> q;
    q.push_back(s);

    while (!q.empty()) {
      int u = q.front();
      q.pop_front();
      m[u] = 0;
      for (Edge e : adj[u]) {
        if (d[e.to] > d[u] + e.w) {
          d[e.to] = d[u] + e.w;
          p[e.to] = u;
          if (m[e.to] == 2) {
            m[e.to] = 1;
            q.push_back(e.to);
          } else if (m[e.to] == 0) {
            m[e.to] = 1;
            q.push_front(e.to);
          }
        }
      }
    }
  }
};


int main(){
  cin.tie(0)->sync_with_stdio(0);
  
  int n, m, s, t, v, u, w;
  cin >> n >> m >> s >> t;

  Graph<ll> G(n);
  FOR(i, m){
    cin >> v >> u >> w;
    G.AddEdge(v, u, w);
  }

  vll d;
  vi p;
  G.Solve(s, d, p);

  if(p[t] == -1){
    cout << -1 << endl;
  }else{
    ll dist = d[t];
    list<ii> path;
    for( ; p[t] != t; t = p[t])
      path.push_front({p[t], t});

    cout << dist << ' ' << path.size() << endl;
    for(ii pr : path)
      cout << pr.first << ' ' << pr.second << endl;
  }
}
