/**
 * Dijkstra for sparse graphs with PB_DS heap
 * 
 * Complexity:
 *   - Time: O((n + m) lg n)
 *   - Space: O(n + m) 
 * 
 * Also can use thin_heap_tag for time complexity
 * O(n lg n + m) with a greater constant
 * 
 */

#include <bits/stdc++.h>
#include <ext/pb_ds/priority_queue.hpp>

using namespace std;
using namespace __gnu_pbds;

template<class T>
using Heap = __gnu_pbds::priority_queue<T, greater<T>, pairing_heap_tag>;
using ll = long long;
using ii = pair<int, int>;
using vi = vector<int>;
using vll = vector<ll>;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'

struct Graph{
  #define U ll // for a strange reason, not works as template
  using Edge = pair<U, int>;
  const U INF = numeric_limits<U>::max();
  
  int n;
  vector<vector<Edge>> adj;

  Graph(int n): n(n), adj(n){}

  void AddEdge(int from, int to, U w, bool dir = true){
    adj[from].push_back({w, to});
    if(!dir) adj[to].push_back({w, from});
  }

  void Dijkstra(int s, vector<U>& d, vi& p){
    d.assign(n, INF);
    p.assign(n, -1);

    d[s] = 0;
    p[s] = s;
    Heap<Edge> q;
    vector<Heap<Edge>::point_iterator> its(n);
    its[s] = q.push({0, s});

    while (!q.empty()) {
      int v = q.top().second;
      U d_v = q.top().first;
      q.pop();

      for (auto edge : adj[v]) {
        U len = edge.first;
        int to = edge.second;

        if (d[v] + len < d[to]) {
          d[to] = d[v] + len;
          if(p[to] == -1)
            its[to] = q.push({d[to], to});
          else
            q.modify(its[to], {d[to], to});
          p[to] = v;
        }
      }
    }
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  
  int n, m, s, t, v, u, w;
  cin >> n >> m >> s >> t;

  Graph G(n);
  FOR(i, m){
    cin >> v >> u >> w;
    G.AddEdge(v, u, w);
  }

  vll d;
  vi p;
  G.Dijkstra(s, d, p);

  if(p[t] == -1){
    cout << -1 << endl;
  }else{
    ll dist = d[t];
    list<ii> path;
    for( ; p[t] != t; t = p[t])
      path.push_front({p[t], t});

    cout << dist << ' ' << path.size() << endl;
    for(ii pr : path)
      cout << pr.first << ' ' << pr.second << endl;
  }
}
