/**
 * LCA with Dynamic Root - Binary Lifting
 *
 * Given x, y and r, find the lca between x and y
 * when r is the root
 *
 * Complexity:
 *  - Build Time: O(n * lg n)
 *  - Query Time: O(lg n)
 *  - Space: O(n * lg n)
 *
 * Problem: https://www.codechef.com/problems/TALCA
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ii = pair<int, int>;
using vi = vector<int>;
using vvi = vector<vi>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define REV(i, b) for(int i = b - 1; i >= 0; i--)
#define FORI(i, b)  for(int i = 1; i <= (int)(b); i++)

struct Tree{
  int n, timer, maxLg;
  vvi adj, up;
  vi tin, tout;

  Tree(int n): n(n), timer(0), maxLg(ceil(log2(n))),
   adj(n), up(maxLg + 1, vi(n, 0)), tin(n), tout(n){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void dfs(int v, int p){
    tin[v] = timer++;
    for (int u : adj[v])
      if (u != p)
        dfs(u, up[0][u] = v);
    tout[v] = timer++;
  }

  bool isAncestor(int u, int v) {
    return tin[u] <= tin[v] && tout[u] >= tout[v];
  }

  void Build(int root = 0) {
    up[0][root] = root;
    dfs(root, root);

    FORI(i, maxLg)
      FOR(v, n)
        up[i][v] = up[i - 1][up[i - 1][v]];
  }

  int lca(int u, int v){
    if (isAncestor(u, v))
      return u;
    if (isAncestor(v, u))
      return v;
    REV(i, maxLg + 1)
      if (!isAncestor(up[i][u], v))
        u = up[i][u];
    return up[0][u];
  }

  // lca between x and y when r is the root
  int LCA(int x, int y, int r){
    int v[] = {lca(x, r), lca(y, r), lca(x, y)};
    sort(v, v + 3);
    return v[v[0] == v[1] ? 2 : 0];
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q;
  cin >> n;

  Tree G(n);

  FOR(i, n - 1){
    int u, v;
    cin >> u >> v; u--; v--;
    G.AddEdge(u, v);
  }

  G.Build();

  cin >> q;

  FOR(i, q){
    int x, y, r;
    cin >> x >> y >> r;
    x--; y--; r--;
    cout << G.LCA(x, y, r) + 1 << endl;
  }
}
