/**
 * Lowest Common Ancestor - Binary Lifting - Path sum
 *
 * Complexity:
 *  - Time: O((n + Q) * lg n + m)
 *  - Space: O((n + Q) * lg n + m)
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'

using ll = long long;
using vi = vector<int>;
using vvi = vector<vi>;
using vll = vector<ll>;

struct Tree{
  struct Edge{
    int to;
    ll w;
  };

  int n, maxLg;
  vector<vector<Edge>> adj;
  vvi up;
  vector<vll> toUp;
  vi depth;

  Tree(int n): n(n), adj(n), depth(n){
    maxLg = ceil(log2(n));
    up.assign(maxLg + 1, vi(n, 0));
    toUp.assign(maxLg + 1, vll(n, 0));
  }

  void AddEdge(int u, int v, ll w){
    adj[u].push_back({v, w});
    adj[v].push_back({u, w});
  }

  void dfs(int v, int p){
    for (Edge e : adj[v])
      if (e.to != p){
        up[0][e.to] = v;
        toUp[0][e.to] = e.w;
        depth[e.to] = depth[v] + 1;
        dfs(e.to, v);
      }
  }

  void Build(int root = 0) {
    depth[root] = 0;
    dfs(root, root);

    for (int i = 1; i <= maxLg; i++)
      for(int v = 0; v < n; v++){
        int p = up[i - 1][v];
        toUp[i][v] = toUp[i - 1][p] + toUp[i - 1][v];
        up[i][v] = up[i - 1][p];
      }
  }

  ll GetCost(int x, int y){

    if(depth[y] < depth[x])
      swap(x, y);

    ll acum = 0;

    for(int i = maxLg; i >= 0; i--){
      if(depth[up[i][y]] >= depth[x]){
        acum += toUp[i][y];
        y = up[i][y];
      }
    }

    if(x != y){
      for(int i = maxLg; i >= 0; i--){
        if(up[i][y] != up[i][x]){
          acum += toUp[i][y];
          acum += toUp[i][x];

          y = up[i][y];
          x = up[i][x];
        }
      }

      acum += toUp[0][y];
      acum += toUp[0][x];
    }

    return acum;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int t, n, q, u, v, w;

  cin >> t;

  while(t--){
    cin >> n;
    Tree tree(n);

    FOR(i, n - 1){
      cin >> u >> v >> w;
      //u--; v--;
      tree.AddEdge(u, v, w);
    }

    tree.Build();

    cin >> q;
    FOR(i, q){
      cin >> u >> v;
      //u--; v--;
      cout << tree.GetCost(u, v) << endl;
    }
  }
}
