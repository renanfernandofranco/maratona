/**
 * Lowest Common Ancestor - Tarjan's Offline Algorithm
 *
 * Complexity:
 *  - Time: O(n + m + Q)
 *  - Space: O(n + m + Q)
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'
using vi = vector<int>;
using vvi = vector<vi>;

struct UnionFind{
  vi dad, sz;

  UnionFind(int n): dad(n), sz(n, 1){
    FOR(i, n) dad[i] = i;
  }

  int Find(int v){
    return (v == dad[v] ? v : v = Find(dad[v]));
  }

  int Union(int v, int u){
    v = Find(v);
    u = Find(u);
    if(u != v){
      if(sz[v] < sz[u]) swap(u, v);
      sz[v] += sz[u];
      dad[u] = v;
    }
    return v;
  }
};

struct LCA{
  vector<vector<pair<int, int*>>> Q;
  vi anc;
  vector<char> seen;
  UnionFind uf;

  LCA(int n): Q(n), anc(n), seen(n), uf(n){}

  void AddQuery(int u, int v, int* ans){
    Q[u].push_back({v, ans});
    Q[v].push_back({u, ans});
  }

  void dfs(vvi& G, int v){
    seen[v] = true;
    anc[v] = v;
    for (int u : G[v])
      if (!seen[u]) {
        dfs(G, u);
        anc[uf.Union(v, u)] = v;
      }
    for (auto pr : Q[v])
      if (seen[pr.first])
        *pr.second = anc[uf.Find(pr.first)];
  }

  void Solve(vvi& G, int root = 0){
    dfs(G, root);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, u, v, p;

  cin >> n >> q;
  vvi G(n);

  FOR(i, n - 1){
    cin >> p;
    G[p].push_back(i + 1);
    G[i + 1].push_back(p);
  }

  LCA lca(n);
  vi ans(q);

  FOR(i, q){
    cin >> u >> v;
    lca.AddQuery(u, v, &ans[i]);
  }

  lca.Solve(G);

  FOR(i, q)
    cout << ans[i] << endl;
}
