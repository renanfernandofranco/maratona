/**
 * Lowest Common Ancestor - Binary Lifting - Kth Ancestor
 *
 * Complexity:
 *  - Time: O((n + Q) * lg n + m)
 *  - Space: O((n + Q) * lg n + m)
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
#define endl '\n'

using vi = vector<int>;
using vvi = vector<vi>;

struct Tree{

  int n, maxLg;
  vvi adj, up;
  vi depth;

  Tree(int n): n(n), adj(n), depth(n){
    maxLg = ceil(log2(n));
    up.assign(maxLg + 1, vi(n, 0));
  }

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void dfs(int v, int p){
    for (int u : adj[v])
      if (u != p){
        up[0][u] = v;
        depth[u] = depth[v] + 1;
        dfs(u, v);
      }
  }

  void Build(int root = 0) {
    depth[root] = 0;
    dfs(root, root);

    FORI(i, maxLg)
      FOR(v, n){
        int p = up[i - 1][v];
        up[i][v] = up[i - 1][p];
      }
  }

  int Kth(int x, int k){
    REV(i, maxLg + 1)
      if(depth[x] - depth[up[i][x]] < k){
        k -= 1 << i;
        x = up[i][x];
      }
    return k == 1 ? up[0][x] : -1;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, u, k;

  cin >> n >> q;
  Tree tree(n);

  FORI(i, n - 1){
    cin >> u;
    tree.AddEdge(i, u - 1);
  }

  tree.Build();

  FOR(i, q){
    cin >> u >> k;
    int ans = tree.Kth(u - 1, k);
    cout << (ans + (ans != -1)) << endl;
  }
}
