/**
 * Fast Kuhn Algorithm for Maximum Bipartite Matching (max BPM)
 *
 * Given a bipartite graph G = (V, E) with V = (L, R), and the vertices
 * in L and R, respectively, in the range [0,|L|) and [0,|R|), this
 * algorithm finds the maximum matching in G.
 *
 * This algorithm only needs the outgoing edges of L. More specifically,
 * it needs adj[] such that adj[u], with u in L, stores the vertices in R
 * that share edges with u.
 *
 * Time Complexity:
 *  - Time: O(|L| * |E|), but with faith, it can run faster than O(|L|^0.5 *|E|)
 *  - Space: O(|V| + |E|)
 *
 * Problem: https://codeforces.com/group/3qadGzUdR4/contest/101712/problem/A
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
#define FOR(i, b) for(int i = 0; i < (b); i++)

struct BipMatcher {
  int n, m, result;
  vector<vi> adj;
  vector<char> seenR;
  vi matchL, matchR;

  BipMatcher(int n, int m):n(n), m(m), adj(n), seenR(m), matchL(n, -1), matchR(m, -1) {}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
  }

  bool bpm(int u) {
    for (int v: adj[u])
      if (!seenR[v]) {
        seenR[v] = true;
        if (matchR[v] == -1 || bpm(matchR[v])) {
          matchL[u] = v;
          matchR[v] = u;
          return true;
        }
      }

    return false;
  }

  // Finds a reasonable (simple) matching to start with.
  void init() {
    FOR(u, n)
      for (int v : adj[u])
        if (matchR[v] == -1) {
          matchL[u] = v;
          matchR[v] = u;
          result++;
          break;
        }
  }

  /**
   * Computes the maximum matching. The first call must be
   * using `from = 0` (default) to correctly start the matching.
   *
   * @param from - Index for a vertex in L to start running
   * the matching algorithm from and up to the (n-1)-th vertex.
   */
  int Solve(int from = 0) {
    if (!from) {
      result = 0;
      init(); // optional
    }

    for (int run = 1; run; ) {
      run = 0, seenR.assign(m, false);
      for (int u = from; u < n; u++)
        if (matchL[u] == -1 && bpm(u))
          result++, run = 1;
    }

    return result;
  }
};

int main() {
  cin.sync_with_stdio(0);
  cin.tie(0);
  int n, m, u, v;
  cin >> n >> m;

  BipMatcher matcher(n, m);

  FOR(u, n)
    while(cin >> v, v)
      matcher.AddEdge(u, v - 1);

  cout << matcher.Solve() << endl;

  FOR(v, m)
    if(matcher.matchR[v] != -1)
      cout << matcher.matchR[v] + 1 << ' ' << v + 1 << endl;

  return 0;
}
