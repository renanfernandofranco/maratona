/**
 * Maximum Independent Set
 *
 * Time Complexity:
 *  - Time: O(1.3803^|V|)
 *  - Space: O(|V| + |E|)
 *
 * Problem: https://judge.yosupo.jp/problem/maximum_independent_set
 *
 */

#include <bits/stdc++.h>
using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)
#define SZ(a) (int(a.size()))
#define all(a) a.begin(), a.end()
using vi = vector<int>;
using ii = pair<int, int>;
using vvi = vector<vi>;

/**
 * Maximum Indepent Set
 * Time complexity: O(1.3803^n)
 */
struct MIS{
  int n;
  map<int, set<int>> adj;
  set<ii, greater<ii>> D;
  vi best, cur;
  set<int> seen;

  // Use n = -1 to ignore isolated vertices;
  MIS(int n = -1): n(n){}

  void AddEdge(int u, int v){
    adj[u].insert(v);
    adj[v].insert(u);
  }

  vi Solve(){
    assert(n == -1 || adj.empty() || (adj.rbegin()->first < n));
    for(auto p : adj)
      D.insert({SZ(adj[p.first]), p.first});
      
    mis();

    FOR(v, n)
      if(!adj.count(v))
        best.push_back(v);
    sort(all(best));

    return best;
  }

  // bicoloration of the component
  void biColor(int v, vi& s1, vi& s2, bool side){
    if(side)
      s2.push_back(v);
    else
      s1.push_back(v);
    seen.insert(v);

    for(int u : adj[v])
      if(!seen.count(u))
        biColor(u, s1, s2, !side);
  }

  vi mis2(){
    vi ret;
    seen.clear();
    for(auto p : D){
      int v = p.second;
      if(!seen.count(v)){
        vi s1, s2;
        biColor(v, s1, s2, false);

        if(SZ(s1) < SZ(s2))
          swap(s1, s2);

        // possible odd cycle
        if(SZ(s1) > SZ(s2)){
          bool find = false;
          for(int v : s1)
            find |= SZ(adj[v]) <= 1; // not is odd cycle
          if(!find)
            swap(s1, s2);
        }
        ret.insert(ret.end(), all(s1));
      }
    }
    return ret;
  }

  void remAdd(int v, bool type){
    for(int u : adj[v]){
      D.erase({SZ(adj[u]), u});
      if(type)
        adj[u].erase(v);
      else
        adj[u].insert(v);
      D.insert({SZ(adj[u]), u});
    }
  }

  void mis(){
    if(D.empty() || D.begin()->first <= 2){
      auto rem = mis2();
      if(SZ(rem) + SZ(cur) > SZ(best)){
        best = cur;
        best.insert(best.end(), all(rem));
      }
      return;
    }

    int v = D.begin()->second;
    D.erase(D.begin());

    for(int u : adj[v]){
      D.erase({SZ(adj[u]), u});
      adj[u].erase(v);
      remAdd(u, true);
    }

    cur.push_back(v);
    mis();
    cur.pop_back();

    for(int u : adj[v]){
      D.insert({SZ(adj[u]), u});
      remAdd(u, false);
    }

    mis();

    D.insert({SZ(adj[v]), v});
    remAdd(v, false);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, m;
  cin >> n >> m;

  MIS mis(n);

  FOR(i, m){
    int u, v;
    cin >> u >> v;
    mis.AddEdge(u, v);
  }

  auto ss = mis.Solve();
  cout << SZ(ss) << endl;
  FOR(i, SZ(ss))
    cout << ss[i] << " \n"[i + 1 == SZ(ss)];
}
