/**
 * Hungarian Algorithm - Assignment Problem
 * 
 * Find the cost of the Perfect Matching with the smallest cost in
 * a Bipartite Graph
 * 
 * Input:
 *  - Matrix n x m with non-negative integers with n <= m
 * 
 * The greatest cost can be obtained by multiplying all values by -1
 * The negative values ​​can be eliminated by adding an integer to all inputs
 * 
 * Complexity:
 *  - Time : O(n^2 * m)
 *  - Space: O(n + m) (excluding the input)
 * 
 * ATTENTION !!! Use n <= m, the opposite not works!
 * 
 * Problem: https://codeforces.com/group/Ohoz9kAFjS/contest/266572/problem/H
 * 
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
#define all(a) a.begin(), a.end()

using vi = vector<int>;
using vvi = vector<vi>;

template<typename T>
T Hungarian(const vector<vector<T>>& cost, vi& ans) {
	const T INF = numeric_limits<T>::max();

	int n = cost.size();
	int m = cost[0].size();

	vector<T> u(n + 1), v(m + 1), dist(m + 1);
	vi p(m + 1), way(m + 1), used(m + 1);

	FORI(i, n) {
		p[0] = i;
		int j0 = 0;
		fill(all(dist), INF);

		do {
			used[j0] = i;
			int i0 = p[j0], j1 = -1;
			T delta = INF;

			FORI(j, m)
				if (used[j] != i) {
					T cur = cost[i0 - 1][j - 1] - u[i0] - v[j];

					if (cur < dist[j]) 
						dist[j] = cur, way[j] = j0;

					if (dist[j] < delta) 
						delta = dist[j], j1 = j;
				}

			FOR(j, m + 1) {
				if (used[j] == i) 
					u[p[j]] += delta, v[j] -= delta;

				else 
					dist[j] -= delta;
			}

			j0 = j1;
		} while (p[j0] != 0);

		for (int j1; j0; j0 = j1)
			p[j0] = p[j1 = way[j0]];
	}
  
  ans.resize(n + 1);
  FOR(i, m)
    ans[p[i + 1]] = i;
	ans.erase(ans.begin());

	return -v[0];
}

int main(){
	cin.tie(0)->sync_with_stdio(0);
	int n;
  cin >> n;

  vvi v(n, vi(n));
  FOR(i, n)
    FOR(j, n)
      cin >> v[i][j];

  vi ans;
  cout << Hungarian<int>(v, ans) << endl;
  FOR(i, n)
    cout << i + 1 << ' ' << ans[i] + 1 << endl;
}
