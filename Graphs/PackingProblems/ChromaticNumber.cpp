/**
 * Chromatic Number
 *
 * Using the include-exclude principle and
 * modular numbers to simulate large numbers
 *
 * Time Complexity:
 *  - Time: O(n * 2^n)
 *  - Space: O(2^n)
 *
 * Problem: https://judge.yosupo.jp/problem/chromatic_number
 *
 */

#include <bits/stdc++.h>
using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
using vi = vector<int>;

const int MODS [] = {1000'000'007, 1000'000'009};
const int SZMOD = 2;

template <class T = int>
struct MOD{
  T v[SZMOD];

  MOD(T vl = 0){ this->operator=(vl); }

  MOD operator *= (const MOD& m2){ return (*this) = this->operator*(m2); }

  MOD operator += (const MOD& m2){ return (*this) = this->operator+(m2); }

  MOD operator -= (const MOD& m2){ return (*this) = this->operator-(m2); }

  void operator = (T vl){ FOR(k, SZMOD) v[k] = vl; }

  bool operator == (const MOD& m2){ return equal(v, v + SZMOD, m2.v); }

  bool operator != (const MOD& m2){ return !equal(v, v + SZMOD, m2.v); }

  MOD operator * (const MOD& m2) const{
    MOD ret;
    FOR(k, SZMOD)
      ret.v[k] = (v[k] * 1LL * m2.v[k]) % MODS[k];
    return ret;
  }

  MOD operator + (const MOD& m2) const{
    MOD ret; T tmp;
    FOR(k, SZMOD)
      ret.v[k] = (tmp = v[k] + m2.v[k]) >= MODS[k] ? tmp - MODS[k] : tmp;
    return ret;
  }

  MOD operator - (const MOD& m2) const{
    MOD ret; T tmp;
    FOR(k, SZMOD)
      ret.v[k] = (tmp = v[k] - m2.v[k]) < 0 ? tmp + MODS[k] : tmp;
    return ret;
  }
};

struct ChromaticNumber{
  int n;
  vi adjBit;

  ChromaticNumber(int n) : n(n), adjBit(n, 0){}

  void AddEdge(int u, int v){
    adjBit[u] |= 1 << v;
    adjBit[v] |= 1 << u;
  }

  int Solve(){
    // I[S] = the number of independent subsets in S
    // I[S] = I[S / {v}] + I[S / N(v)]
    vector<MOD<>> I(1 << n, 0);
    I[0] = 1;
    FORI(S, (1 << n) - 1){
      int v = __builtin_ctz(S);
      I[S] = I[S ^ (1 << v)] + I[(S ^ (1 << v)) & ~adjBit[v]];
    }

    vector<MOD<>> sum(1 << n);
    FOR(S, (1 << n))
      sum[S] = __builtin_parity(S) ? 1 : (MOD<>(0) - MOD<>(1));

    FORI(K, n - 1){
      MOD<> res = 0;
      FOR(S, (1 << n))
        res += sum[S] *= I[S];
      if(res != 0)
        return K;
    }
    return n;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);
  int n, m;
  cin >> n >> m;

  ChromaticNumber cn(n);
  FOR(i, m){
    int u, v;
    cin >> u >> v;
    cn.AddEdge(u, v);
  }

  cout << cn.Solve() << endl;
}
