/**
 * Ford-Fulkerson Algorithm for Maximum Bipartite Matching(maxBPM)
 *  with each vertex being a pair (i, j)
 * 
 * Seen description of the maxBPM-vc.cpp for more details
 * 
 * Time Complexity:
 *     - Time: my reference says that is O( |V| * |E| ), but I believe that is O(|L| * |E|)
 *     - Space: O(|V| + |E|)
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using ii = pair<int, int>;
#define NONE make_pair(-1, -1)
#define FOR(i, b) for(int i = 0; i < (b); i++)

template<class T>
struct arr2D{
   vector<vector<T>> v;
   int x, y;
   arr2D(int x, int y):x(x), y(y), v(x, vector<T>(y)){}
   T& operator [] (ii p){ return v[p.first][p.second];}
   void set(T val){ FOR(i, x) FOR(j, y) v[i][j] = val; }
};

struct BipMatcher{
   int x, y;
   arr2D<vector<ii>> adj;
   arr2D<char> seen;
   arr2D<ii> matchR;
   vector<ii> L;

   BipMatcher(int x, int y): x(x), y(y), adj(x, y), seen(x, y), matchR(x, y){
      matchR.set(NONE);
      seen.set(false);
   }

   void AddEdge(ii u, ii v){
      adj[u].push_back(v);

      if(!seen[u]){
         seen[u] = true;
         L.push_back(u);
      }
   }

   bool bpm(ii u){ 
      for (ii v: adj[u]){
         if (!seen[v]){
            seen[v] = true;  
            if (matchR[v] == NONE || bpm(matchR[v])){ 
               matchR[v] = u; 
               return true; 
            }
         }
      }
      return false; 
   }   

   int Solve(){
      int result = 0;

      for (ii u : L){
         seen.set(false);
         if (bpm(u))
            result++; 
      }

      return result; 
   }
};