/**
 * Edmond's Blossoms algorithm give a maximum matching 
 * in general graphs (non-bipartite)
 * 
 * Complexity:
 *  - Time: O(|V| ^ 3)
 *  - Space: O(|V| + |E|)
 * 
 * Problem : https://judge.yosupo.jp/problem/general_matching
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using ii = pair<int, int>;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()
#define endl '\n'

struct EdmondBlossom{
  int n, l;
  vvi adj;
  vi match, dad, base;
  vector<char> seen, bloss, has;
  queue<int> q;

  EdmondBlossom(int n) : n(n), adj(n), match(n),
     dad(n), base(n), seen(n), bloss(n), has(n) {}

  void AddEdge(int a, int b){
    adj[a].push_back(b);
    adj[b].push_back(a);
  }

  void contract(int u, int v, bool first = true) {
    if (first) {
      fill(all(bloss), false);
      fill(all(has), false);
      int k = u;
      l = v;
      while (true) {
        has[k = base[k]] = true;
        if (match[k] == -1)
          break;
        k = dad[match[k]];
      }
      while (!has[l = base[l]]) 
        l = dad[match[l]];
    }

    while (base[u] != l) {
      bloss[base[u]] = bloss[base[match[u]]] = true;
      dad[u] = v;
      v = match[u];
      u = dad[match[u]];
    }

    if (!first) 
      return;

    contract(v, u, false);
    FOR(i, n)
      if (bloss[base[i]]) {
        base[i] = l;
        if (!seen[i]) 
          q.push(i);
        seen[i] = true;
      }
  }

  int getpath(int s) {
    FOR(i, n) {
      base[i] = i; 
      dad[i] = -1;
      seen[i] = false;
    }

    seen[s] = true; 
    q = queue<int>();
    q.push(s);
    while (q.size()) {
      int u = q.front(); q.pop();

      for (int i : adj[u]) {
        if (base[i] == base[u] || match[u] == i) 
          continue;
        if (i == s || (match[i] != -1 && dad[match[i]] != -1))
          contract(u, i);
        else if (dad[i] == -1) {
          dad[i] = u;
          if (match[i] == -1) 
            return i;
          i = match[i];
          seen[i] = true;
          q.push(i);
        }
      }
    }
    return -1;
  }

  void greedy(){
    FOR(i, n)
      if (match[i] == -1)
        for (int j : adj[i]) 
          if (match[j] == -1) {
            match[i] = j;
            match[j] = i;
            break;
          }
  }

  vector<ii> Solve(){
    fill(all(match), -1);
    greedy();

    FOR(i, n)
      if (match[i] == -1) {
        int j = getpath(i);
        while (j != -1) {
          int p = dad[j], pp = match[p];
          match[p] = j;
          match[j] = p;
          j = pp;
        }
      }

    vector<ii> ans;
    FOR(i, n)
      if(i < match[i])
        ans.push_back({i, match[i]});
    return ans;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  cin >> n >> m;

  EdmondBlossom G(n);
  FOR(i, m){
    int a, b;
    cin >> a >> b;
    G.AddEdge(a, b);
  }

  auto ans = G.Solve();
  cout << ans.size() << endl;
  for(auto p : ans)
    cout << p.first << " " << p.second << endl;

  return 0;
}
