/**
 * Hopcroft–Karp Algorithm for Maximum Bipartite Matching(maxBPM)
 * 
 * Time Complexity:
 *    - Time: O(|V|^0.5 * |E|)
 *    - Space: O(|V| + |E|)
 * 
 * Problem: https://codeforces.com/group/3qadGzUdR4/contest/101712/problem/A
 * 
 */

#include <bits/stdc++.h>

using namespace std;
#define NIL 0
#define INF (1<<28)
#define FORI(i, b) for(int i = 1; i <= b; i++)
#define all(v) v.begin(), v.end()
using vi = vector<int>;

struct BipMatcher {
  vector<vi> G;
  vi match, dist;
  int n, m, nm, ret;
  // n: number of nodes on left side, nodes are numbered 1 to n
  // m: number of nodes on right side, nodes are numbered n+1 to n+m
  // G = NIL[0] ∪ G1[G[1---n]] ∪ G2[G[n+1---n+m]]
  BipMatcher(int n, int m):
    n(n), m(m), G(nm = n + m + 1), match(nm), dist(nm){}

  void AddEdge(int vl, int vr){
    G[vl].push_back(vr + n);
    G[vr + n].push_back(vl);
  }

  bool bfs() {
    queue<int> q;
    FORI(i, n)
      if(match[i] == NIL) {
        dist[i] = 0;
        q.push(i);
      }
      else 
        dist[i] = INF;

    dist[NIL] = INF;
    while(!q.empty()) {
      int u = q.front(); q.pop();
      if(u != NIL)
        for(int v : G[u])
          if(dist[match[v]] == INF) {
            dist[match[v]] = dist[u] + 1;
            q.push(match[v]);
          }
    }
    return dist[NIL] != INF;
  }

  bool dfs(int u) {
    if(u != NIL) {
      for(int v : G[u])
        if(dist[match[v]] == dist[u] + 1) {
          if(dfs(match[v])) {
            match[v] = u;
            match[u] = v;
            return true;
          }
        }
      dist[u] = INF;
      return false;
    }
    return true;
  }

  int Solve(bool reset = true) {
    if (reset) ret = 0, fill(all(match), NIL);

    while(bfs())
      FORI(i, n)
        if(match[i] == NIL && dfs(i))
          ret++;
    return ret;
  }
};

int main(){
  cin.sync_with_stdio(0);
  cin.tie(0);
  int n, m, u, v;
  cin >> n >> m;

  BipMatcher bip(n, m);

  FORI(u, n)
    while(cin >> v, v)
      bip.AddEdge(u, v);

  cout << bip.Solve() << endl;

  FORI(u, n)
    if(bip.match[u] != NIL)
      cout << u << ' ' << bip.match[u] - n << endl;
}
