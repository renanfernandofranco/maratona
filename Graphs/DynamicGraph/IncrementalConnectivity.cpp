/**
 * Incremental Connectivity using Persistent Union Find Offline
 *
 * There are n isolated vertices and m edges are added sequentially.
 *
 * Next, process Q queries, where the answer for each query is the minimum
 * length edge prefix that needs to be added to connect two vertices u and v.
 *
 * Complexity:
 *  - Time: O((n + m + Q) * lg m), assuming DSU with constant time by operation
 *  - Space: O(n + m + Q)
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define SZ(v) ((int) v.size())
#define endl '\n'
#define all(a) a.begin(), a.end()

using ii = pair<int, int>;
using vi = vector<int>;

struct UnionFind{
  vi dad, sizes;

  UnionFind(int n): dad(n), sizes(n, 1){
    FOR(i, n) dad[i] = i;
  }

  int FindSet(int u){
    return u == dad[u] ? u : (dad[u] = FindSet(dad[u]));
  }

  int UnionSet(int u, int v){
    u = FindSet(u);
    v = FindSet(v);
    if (u != v){
      if (sizes[u] < sizes[v])
        swap(u, v);
      sizes[u] += sizes[v];
      dad[v] = dad[u];
    }
    return u;
  }

  bool Connected(int u, int v){
    return FindSet(u) == FindSet(v);
  }
};

struct Graph {
  #define MID(l, r) (((l) + (r) + 1) / 2)
  struct Query{
    int u, v, l, r, id;
    Query(int u, int v, int l, int r, int id) : u(u), v(v),
       l(l), r(r), id(id) {}

    bool operator < (const Query& q2){
      return MID(l, r) < MID(q2.l, q2.r);
    }
  };

  int n, m;
  vector<ii> edges;
  vector<Query> Q;

  Graph(int n, int m) : n(n), m(m) {}

  void AddEdge(int u, int v) {
    edges.push_back({u, v});
  }

  void AddQuery(int u, int v){
    Q.push_back({u, v, -1, m, (int)Q.size()});
  }

  vi Solve(){
    assert(m == (int)edges.size());
    int q = Q.size();

    // Parallel Binary Search
    while(true){
      UnionFind uf(n);
      int tm = 0; // all edges in range[0, tm) are already inserted
      bool any = false;

      sort(all(Q));
      FOR(i, q){
        if(Q[i].l == Q[i].r)
          continue;
        any = true;
        int qm = MID(Q[i].l, Q[i].r);

        while(tm < m && tm < qm){
          uf.UnionSet(edges[tm].first, edges[tm].second);
          tm++;
        }

        if(uf.Connected(Q[i].u, Q[i].v))
          Q[i].r = qm - 1;
        else
          Q[i].l = qm;
      }
      if(!any)
        break;
    }
    vi ans(q);
    FOR(i, q) // the answer is -1 if it doesn't have the path from u to v
      ans[Q[i].id] = (Q[i].l == m ? -1 : Q[i].l + 1);
    return ans;
  }
};


int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, m, q;
  cin >> n >> m >> q;

  Graph G(n, m);

  FOR(i, m){
    int u, v;
    cin >> u >> v;
    G.AddEdge(u - 1, v - 1);
  }

  FOR(i, q){
    int u, v;
    cin >> u >> v;
    G.AddQuery(u - 1, v - 1);
  }

  vi ans = G.Solve();

  FOR(i, q)
    cout << ans[i] << endl;
}
