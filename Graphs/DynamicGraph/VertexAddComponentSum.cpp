/**
 * Offline Dynamic Connectivity - Vertex Add, Component Sum
 *
 * Complexity:
 *  - Time: O(N + Q lg Q)
 *  - Space: O(N + Q)
 *
 * Based in: https://cp-algorithms.com/data_structures/deleting_in_log_n.html
 *
 * https://judge.yosupo.jp/problem/dynamic_graph_vertex_add_component_sum
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)

template<class T>
struct DsuWithRollbacks {
  struct DsuSave {
    int v, u, rInc;
    T vl;
  };

  vector<int> p, rnk;
  vector<T> vls;
  stack<DsuSave> op;

  DsuWithRollbacks(int n) : p(n), rnk(n, 0), vls(n, 0) {
    FOR(i, n) p[i] = i;
  }

  int Find(int v) {
    return (v == p[v]) ? v : Find(p[v]);
  }

  bool Union(int v, int u) {
    v = Find(v);
    u = Find(u);
    if (v == u)
      return false;

    if (rnk[v] < rnk[u]) swap(v, u);
    op.push({v, u, rnk[v] == rnk[u], vls[u]});
    vls[v] += vls[u];
    rnk[v] += rnk[v] == rnk[u];
    p[u] = v;
    return true;
  }

  void rollback() {
    assert(!op.empty());
    DsuSave s = op.top();
    op.pop();
    vls[s.v] -= s.vl;
    rnk[s.v] -= s.rInc;
    p[s.u] = s.u;
  }
};

template<class T>
struct OfflineDynamicConnectivity {
  struct Update {
    int v, u;
    bool united;
    Update(int v, int u) : v(v), u(u){}
  };

  int N;
  vector<vector<Update>> t;
  vector<int> Q;
  DsuWithRollbacks<T> dsu;
  vector<map<int, int>> starts;

  OfflineDynamicConnectivity(int nV, int nEvents): N(1 << (int)ceil(log2(nEvents))),
    t(2 * N), Q(2 * N, -1), dsu(nV), starts(nV) {}

  void addUpdate(int l, int r, Update q) {
    for (l += N, r += N + 1; l < r; l >>= 1, r >>= 1) {
      if (l & 1) t[l++].push_back(q);
      if (r & 1) t[--r].push_back(q);
    }
  }

  void Connect(int v, int u, int time){
    assert(starts[min(v, u)].insert({max(v, u), time}).second);
  }

  void Disconnect(int v, int u, int time){
    if(v > u) swap(v, u);
    assert(starts[v].count(u));
    addUpdate(starts[v][u], time, {v, u});
    starts[v].erase(u);
  }

  void Query(int v, int time){
    Q[time + N] = v;
  }

  void dfs(int v, int l, int r, vector<T>& ans) {
    for (Update& q : t[v])
      q.united = dsu.Union(q.v, q.u);

    if (l == r){
      if(Q[v] != -1)
        ans.push_back(dsu.vls[dsu.Find(Q[v])]);
    }
    else {
      int mid = (l + r) / 2;
      dfs(2 * v, l, mid, ans);
      dfs(2 * v + 1, mid + 1, r, ans);
    }
    for (Update q : t[v])
      if (q.united)
        dsu.rollback();
  }

  vector<T> Solve() {
    vector<T> ans;
    FOR(v, starts.size())
      for(auto p : starts[v])
        addUpdate(p.second, N - 1, {v, p.first});
    starts.clear();
    dfs(1, 0, N - 1, ans);
    return ans;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q;
  cin >> n >> q;

  OfflineDynamicConnectivity<ll> DG(n + q, q + 1);

  FOR(i, n)
    cin >> DG.dsu.vls[i];

  FOR(i, q){ // i = current time
    int tp, u, v;
    cin >> tp >> v;
    switch(tp){
      case 0:
        cin >> u;
        DG.Connect(v, u, i);
        break;
      case 1:
        cin >> u;
        DG.Disconnect(v, u, i);
        break;
      case 2: // Vertex Add
        cin >> DG.dsu.vls[n + i];
        DG.Connect(v, n + i, i);
        break;
      case 3:
        DG.Query(v, i);
    }
  }

  auto v = DG.Solve();
  for(auto e : v)
    cout << e << endl;
}
