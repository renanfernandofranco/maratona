/**
 * Offline Dynamic Connectivity - Count components using a range of edges
 *
 * Query: find the number of components of the graph when is used the edges in the range [l, r]
 *
 * Complexity:
 *  - Time: O(((M + Q)lg N + N) * sqrt(M))
 *  - Space: O(N + M + Q)
 *
 * Problem: https://codeforces.com/edu/course/2/lesson/7/3/practice/contest/289392/problem/B
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)

struct DsuWithRollbacks {
  struct DsuSave {
    int v, u, rInc;
  };

  vi p, rnk;
  stack<DsuSave> op;

  DsuWithRollbacks(int n) : p(n), rnk(n, 0){
    FOR(i, n) p[i] = i;
  }

  int Find(int v) {
    return (v == p[v]) ? v : Find(p[v]);
  }

  bool Union(int v, int u) {
    v = Find(v);
    u = Find(u);
    if (v == u)
      return false;

    if (rnk[v] < rnk[u]) swap(v, u);
    op.push({v, u, rnk[v] == rnk[u]});
    rnk[v] += rnk[v] == rnk[u];
    p[u] = v;
    return true;
  }

  int getNumSets(){
    return p.size() - op.size();
  }

  void rollback() {
    assert(!op.empty());
    DsuSave s = op.top();
    op.pop();
    rnk[s.v] -= s.rInc;
    p[s.u] = s.u;
  }
};

struct MO{
  #define BLOCK_SIZE 500
  struct Query {

    int l, r, idx;
    bool operator < (Query other) const {
      return make_pair(l / BLOCK_SIZE, r) <
        make_pair(other.l / BLOCK_SIZE, other.r);
    }
  };

  struct Edge{
    int u, v;
  };

  int n;
  vector<Query> Q;
  vector<Edge> edges;
  DsuWithRollbacks dsu;
  vi ans;
  stack<bool> st;

  MO(int n): n(n), dsu(0) {}

  void AddQuery(int l, int r){
    Q.push_back({l, r, (int)Q.size()});
  }

  void AddEdge(int u, int v){
    edges.push_back({u, v});
  }

  void add(int i){
    st.push(dsu.Union(edges[i].u, edges[i].v));
  }

  void pop(){
    if(st.top())
      dsu.rollback();
    st.pop();
  }

  vi Solve(){
    int lastBlock = -1, curR = -1;

    ans.resize(Q.size());
    sort(Q.begin(), Q.end());

    for(auto q : Q){
      int curBlock = q.l / BLOCK_SIZE, endBlock = (curBlock + 1) * BLOCK_SIZE - 1;

      if(lastBlock < curBlock){
        lastBlock = curBlock;
        curR = endBlock;
        dsu = DsuWithRollbacks(n);
        while(!st.empty()) st.pop();
      }

      while(curR < q.r)
        add(++curR);

      for(int i = q.l; i <= min(q.r, endBlock); i++)
        add(i);

      ans[q.idx] = dsu.getNumSets();

      for(int i = q.l; i <= min(q.r, endBlock); i++)
        pop();
    }

    return ans;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, q;
  cin >> n >> m;

  MO mo(n);

  FOR(i, m){
    int u, v;
    cin >> u >> v;
    mo.AddEdge(u - 1, v - 1);
  }

  cin >> q;

  FOR(i, q){
    int l, r;
    cin >> l >> r;
    mo.AddQuery(l - 1, r - 1);
  }

  vi ans = mo.Solve();

  FOR(i, q)
    cout << ans[i] << endl;
}
