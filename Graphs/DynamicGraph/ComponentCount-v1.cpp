/**
 * Offline Dynamic Connectivity - Count components after updates
 *
 * Complexity:
 *  - Time: O(N + Q * lg N * lg Q)
 *  - Space: O(N + Q)
 *
 * Problem: https://cses.fi/problemset/task/2133/
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)

struct DsuWithRollbacks {
  struct DsuSave {
    int v, u, rInc;
  };

  vector<int> p, rnk;
  stack<DsuSave> op;

  DsuWithRollbacks(int n) : p(n), rnk(n, 0){
    FOR(i, n) p[i] = i;
  }

  int Find(int v) {
    return (v == p[v]) ? v : Find(p[v]);
  }

  bool Union(int v, int u) {
    v = Find(v);
    u = Find(u);
    if (v == u)
      return false;

    if (rnk[v] < rnk[u]) swap(v, u);
    op.push({v, u, rnk[v] == rnk[u]});
    rnk[v] += rnk[v] == rnk[u];
    p[u] = v;
    return true;
  }

  int getNumSets(){
    return p.size() - op.size();
  }

  void rollback() {
    assert(!op.empty());
    DsuSave s = op.top();
    op.pop();
    rnk[s.v] -= s.rInc;
    p[s.u] = s.u;
  }
};

struct OfflineDynamicConnectivity {
  struct Update {
    int v, u;
    bool united;
    Update(int v, int u) : v(v), u(u){}
  };

  int N;
  vector<vector<Update>> t;
  vector<char> Q;
  DsuWithRollbacks dsu;
  vector<map<int, int>> starts;

  OfflineDynamicConnectivity(int nV, int nEvents): N(1 << (int)ceil(log2(nEvents))),
    t(2 * N), Q(2 * N, false), dsu(nV), starts(nV) {}

  void addUpdate(int l, int r, Update q) {
    for (l += N, r += N + 1; l < r; l >>= 1, r >>= 1) {
      if (l & 1) t[l++].push_back(q);
      if (r & 1) t[--r].push_back(q);
    }
  }

  void Connect(int v, int u, int time){
    assert(starts[min(v, u)].insert({max(v, u), time}).second);
  }

  void Disconnect(int v, int u, int time){
    if(v > u) swap(v, u);
    assert(starts[v].count(u));
    addUpdate(starts[v][u], time, {v, u});
    starts[v].erase(u);
  }

  void Query(int time){
    Q[time + N] = true;
  }

  void dfs(int v, int l, int r, vector<int>& ans) {
    for (Update& q : t[v])
      q.united = dsu.Union(q.v, q.u);

    if (l == r){
      if(Q[v])
        ans.push_back(dsu.getNumSets());
    }
    else {
      int mid = (l + r) / 2;
      dfs(2 * v, l, mid, ans);
      dfs(2 * v + 1, mid + 1, r, ans);
    }
    for (Update q : t[v])
      if (q.united)
        dsu.rollback();
  }

  vector<int> Solve() {
    vector<int> ans;
    FOR(v, starts.size())
      for(auto p : starts[v])
        addUpdate(p.second, N - 1, {v, p.first});
    starts.clear();
    dfs(1, 0, N - 1, ans);
    return ans;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, q;
  cin >> n >> m >> q;

  OfflineDynamicConnectivity DG(n, m + 2 * q + 1);

  int times = 0;
  FOR(i, m){
    int u, v;
    cin >> u >> v; u--; v--;
    DG.Connect(u, v, times++);
  }
  DG.Query(times++);

  FOR(i, q){
    int tp, u, v;
    cin >> tp >> u >> v; u--; v--;
    switch(tp){
      case 1:
        DG.Connect(u, v, times++);
        break;
      case 2:
        DG.Disconnect(u, v, times++);
        break;
    }
    DG.Query(times++);
  }

  auto ans = DG.Solve();
  FOR(i, ans.size())
    cout << ans[i] << " \n"[i + 1U == ans.size()];
}
