/**
 * Build a simple graph from the sequence of degrees
 *
 * Attention: The algorithm does not try to include multiple edges
 *
 * Complexity:
 *  - Time : O((n + m) lg m)
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1697/
 *
 */

#include "bits/stdc++.h"

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using ii = pair<int, int>;

#define FOR(i, b) for(int i = 0; i < (int)(b); i++)

vector<ii> Solve(vi deg, bool& fail){
  priority_queue<ii> q;

  FOR(i, deg.size())
    if(deg[i] > 0)
      q.push({deg[i], i});

  vector<ii> edges;
  while(!q.empty()){
    int u = q.top().second, d = q.top().first;
    q.pop();
    deg[u] = 0;

    if((int) q.size() < d){
      fail = true;
      return {};
    }

    vi reAdd;
    FOR(i, d){
      int w = q.top().second; q.pop();
      deg[w]--;
      reAdd.push_back(w);
      edges.push_back(minmax(u, w));
    }

    for(int w : reAdd)
      if(deg[w] > 0)
        q.push({deg[w], w});
  }
  return edges;
}

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n;
  cin >> n;

  vi deg(n);

  FOR(i, n)
    cin >> deg[i];

  bool fail = false;
  auto edges = Solve(deg, fail);

  if(fail){
    cout << "IMPOSSIBLE\n";
    return 0;
  }

  sort(edges.begin(), edges.end());
  cout << edges.size() << endl;
  for(ii e : edges)
    cout << e.first + 1 << ' ' << e.second + 1 << "\n";
}
