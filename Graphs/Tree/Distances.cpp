/**
 * Tree - Maximum distance for each node.
 *
 * Determine for each node the maximum distance to another node.
 *
 * Complexity:
 *  - Time: O(n + m)
 *  - Space: O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1132/
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()
#define allM(a, m) a.begin(), a.begin() + (m), a.end()
#define rall(a) a.rbegin(), a.rend()

using vi = vector<int>;
using ii = pair<int, int>;

struct Tree{
  int n;
  vector<vi> adj;
  vector<vector<ii>> bests;
  vi ans;

  Tree(int n): n(n), adj(n), bests(n), ans(n){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  int dfs(int v, int p){
    bests[v] = {{0, v}, {0, v}};
    for(int u : adj[v])
      if(u != p)
        bests[v].push_back({dfs(u, v) + 1, u});

    partial_sort(allM(bests[v], 2), greater<ii>());
    bests[v].resize(2);
    return bests[v][0].first;
  }

  void reRootChilds(int v, int p){
    ans[v] = bests[v][0].first;
    for(int u : adj[v])
      if(u != p){
        vector<ii> old = bests[v];
        bests[u].push_back({bests[v][bests[v][0].second == u].first + 1, v});
        sort(rall(bests[u]));
        reRootChilds(u, v);
        bests[u] = old;
      }
  }

  vi Solve(){
    dfs(0, -1);
    reRootChilds(0, -1);
    return ans;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n;
  cin >> n;

  Tree tree(n);
  FOR(i, n - 1){
    int u, v;
    cin >> u >> v;
    tree.AddEdge(u - 1, v - 1);
  }

  vi ans = tree.Solve();
  FOR(i, n)
    cout << ans[i] << " \n"[i + 1 == n];
}
