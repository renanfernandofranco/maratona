/**
 * Given a tree with n colored vertices, count for each edge how many
 * paths include it and have both endpoints of the same color.
 * 
 * Example of small-to-large in a tree
 * 
 * Complexity
 *  - Time: O(n lg^2 n)
 *  - Space: O(n lg n)
 * 
 */

#include <bits/stdc++.h>

using namespace std;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
using vi = vector<int>;
using ll = long long;
using vll = vector<ll>;
using ii = pair<int, int>;

vector<vi> adj;
map<ii, ll> ans;
vi C, sz;
map<int, ll> gb;
vector<map<int, ll> *> sub;

void dfs(int v, int p){
    for(auto u : adj[v])
        dfs(u, v);

    int bigChild = adj[v].empty() ? -1 : adj[v][0];

    if(bigChild == -1)
        sub[v] = new map<int, ll> ();
    else
        sub[v] = sub[adj[v][0]];

    // if vertex u is not the big child, we compute the answer for (u, v) in linear time
    for(int u : adj[v])
        if(u != bigChild)
            for(auto pr : *sub[u])
                ans[minmax(u, v)] += (gb[pr.first] - pr.second) * pr.second;

    // if v is the big child of his parent p, we compute the answer for (v, p) indirectly
    if(p != -1 && adj[p][0] == v){
        ll cur;
        if(bigChild != -1){
            cur = ans[minmax(bigChild, v)];
            
            map<int, ll> others;
            others[C[v]]++; // include v in other subtrees
            for(int u : adj[v])
                if(u != bigChild)
                    for(auto pr : *sub[u])
                        others[pr.first] += pr.second;

            for(auto pr : others){
                // exclude paths between bigChild subtree and other subtrees from v
                cur -= (*sub[bigChild])[pr.first] * pr.second;
                // include paths between other subtree nodes and remaining nodes
                cur += (gb[pr.first] - (*sub[bigChild])[pr.first] - pr.second) * pr.second;
            }
        }else{
            cur = gb[C[v]] - 1;
        }
        ans[minmax(p, v)] = cur;
    }

    // compute colors in subtree of v.
    (*sub[v])[C[v]] ++;
    for(auto u : adj[v])
       if(u != bigChild)
           for(auto x : *sub[u])
               (*sub[v])[x.first] += x.second;
}

int pre(int u, int p = -1){
    sz[u] = 1;

    if(p != -1)
        adj[u].erase(find(adj[u].begin(), adj[u].end(), p));

    FOR(i, adj[u].size()){
        sz[u] += pre(adj[u][i], u);
        if(sz[adj[u][i]] > sz[adj[u][0]])
            swap(adj[u][0], adj[u][i]);
    }

    return sz[u];
}

int main(){
    cin.tie(0)->sync_with_stdio(0);
    int n, q;

    cin >> n;

    map<ii, int> ids;

    C.resize(n);
    adj.resize(n);
    sz.resize(n);
    sub.resize(n);
    
    FOR(i, n){
        cin >> C[i];
        C[i]--;
        gb[C[i]]++;
    }

    FOR(i, n - 1){
        int u, v;
        cin >> u >> v;
        u--; v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
        ids[minmax(u, v)] = i;
    }

    pre(0);
    dfs(0, -1);

    vector<int> p(n - 1);

    for(auto pr : ans)
        p[ids[pr.first]] = pr.second;

    FOR(i, n - 1)
        cout << p[i] << ' ';

    cout << endl;
}
