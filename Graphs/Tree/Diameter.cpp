/**
 * Tree - Find Diameter
 *
 * Complexity:
 *  - Time: O(n + m)
 *  - Space: O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1131
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()

using vi = vector<int>;

struct Tree{
  int n;
  vector<vi> adj;
  vi d;

  Tree(int n):n(n), adj(n), d(n, 0){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void dfs(int v, int p, int dc){
    d[v] = dc;
    for(int u : adj[v])
      if(u != p)
        dfs(u, v, dc + 1);
  }

  int Diameter(){
    dfs(0, -1, 0);
    int v = max_element(all(d)) - d.begin();
    dfs(v, -1, 0);
    return *max_element(all(d));
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n;
  cin >> n;
  Tree tree(n);

  FOR(i, n - 1){
    int u, v;
    cin >> u >> v;
    tree.AddEdge(u - 1, v - 1);
  }

  cout << tree.Diameter() << endl;
}
