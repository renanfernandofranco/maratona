/**
 * Vertex Add, SubTreeSum - Static Tree
 *
 * Complexity:
 *  - Time Build: O(n)
 *  - Time Query/Update: O(lg n)
 *  - Space: O(n)
 *
 * https://cses.fi/problemset/task/1137/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using vvi = vector<vi>;
using ll = long long;
using vll = vector<ll>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define FORI(i, b)  for(int i = 1; i <= (int)(b); i++)

template <class T>
struct FT { // Indexed by 1
  int n;
  vector<T> bit;

  FT(int n): n(n), bit(n + 1, 0){}

  void Build(vector<T>& a){ // Build in O(n)
    FORI(i, n){
      bit[i] += a[i];
      int ii = i + (i & -i);
      if(ii <= n)
        bit[ii] += bit[i];
    }
  }

  T sum(int r) {
    T ret = 0;
    for (; r > 0; r -= r & -r)
      ret += bit[r];
    return ret;
  }

  void Add(int i, T delta) {
    for (; i <= n; i += i & -i)
      bit[i] += delta;
  }

  T Sum(int l, int r) {
    return sum(r) - sum(l - 1);
  }
};

template <class T>
struct Tree{
  int n, timer;
  vvi adj;
  vi tin, tout;
  FT<T> bit;

  Tree(int n): n(n), timer(-1), adj(n), tin(n), tout(n), bit(n){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void dfs(int v, int p){
    tin[v] = ++timer;
    for (int u : adj[v])
      if (u != p)
        dfs(u, v);
    tout[v] = timer;
  }

  void Build(int root = 0, vector<T> vals = vector<T>()) {
    dfs(root, root);
    if(!vals.empty()){
      vector<T> a(n);
      FOR(v, n)
        a[tin[v] + 1] = vals[v];
      bit.Build(a);
    }
  }

  void AddNode(int v, T val){
    bit.Add(tin[v] + 1, val);
  }

  T Query(int v){
    return bit.Sum(tin[v] + 1, tout[v] + 1);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, u, v;

  cin >> n >> q;
  vll vals(n);
  Tree<ll> tree(n);

  FOR(i, n)
    cin >> vals[i];

  FOR(i, n - 1){
    cin >> u >> v;
    tree.AddEdge(u - 1, v - 1);
  }

  tree.Build(0, vals);

  FOR(i, q){
    int tp, p, vl;
    cin >> tp;
    switch (tp) {
      case 1: // Set Value
        cin >> p >> vl; p--;
        tree.AddNode(p, vl - vals[p]);
        vals[p] = vl;
        break;
      case 2:
        cin >> v; v--;
        cout << tree.Query(v) << endl;
    }
  }
}
