/**
 * Tree - Sum of distances from all to all
 *
 * Determine for each node the sum of the distances
 * from the node to all other nodes.
 *
 * Complexity:
 *  - Time: O(n + m)
 *  - Space: O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1133/
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)

using vi = vector<int>;
using ll = long long;
using vll = vector<ll>;

struct Tree{
  int n;
  vector<vi> adj;
  vector<ll> part, sz, ans;

  Tree(int n): n(n), adj(n), part(n), sz(n), ans(n){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  int dfs(int v, int p){
    sz[v] = 1;
    for(int u : adj[v])
      if(u != p){
        sz[v] += dfs(u, v);
        part[v] += part[u] + sz[u];
      }
    return sz[v];
  }

  void reRootChilds(int v, int p){
    ans[v] = part[v];
    for(int u : adj[v])
      if(u != p){
        ll partu = part[u], partv = part[v];
        int szv = sz[v], szu = sz[u];
        sz[v] -= szu;
        sz[u] += sz[v];
        part[v] -= partu + szu;
        part[u] += part[v] + sz[v];
        reRootChilds(u, v);
        sz[v] = szv, sz[u] = szu;
        part[v] = partv, part[u] = partu;
      }
  }

  vll Solve(){
    dfs(0, -1);
    reRootChilds(0, -1);
    return ans;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n;
  cin >> n;

  Tree tree(n);
  FOR(i, n - 1){
    int u, v;
    cin >> u >> v;
    tree.AddEdge(u - 1, v - 1);
  }

  vll ans = tree.Solve();
  FOR(i, n)
    cout << ans[i] << " \n"[i + 1 == n];
}
