/**
 * Minimum s-t cut of vertices - Using Dinic
 * 
 * The graph used is the implicit graph of a matrix, with the
 * character equals to '1' equals to source, '2' equals to sink,
 * '.' equals to intermediate vertex and '#' a null position
 *
 * This algorithm find min cut to disconnect all 1's of all 2's
 *
 * Complexity:
 * - Time: O (|V|^0.5 * |E|)
 * - Space O (|V| + |E|)
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using ii = pair<int, int>;
using vi = vector<int>;

#define FIX(w) if (mat[w] == '1') w = s; else if (mat[w] == '2') w = t;
#define isValid(u) (u.first >= 0 && u.first < n && u.second >= 0 && u.second < m && mat[u] != '#')
#define isFree(u) (mat[u] == '.')
#define SIB(u) (make_pair(u.first, u.second + m)) // sibling vertex
#define FOR(i, b) for(int i = 0; i < (b); i++)

template<class T>
struct arr2D{
   vector<vector<T>> v;
   int x, y;
   arr2D(int x, int y):x(x), y(y) {v.assign(x, vector<T>(y));} 
   T& operator [] (ii p){ return v[p.first][p.second];}
   void set(T val){ FOR(i, x) FOR(j, y) v[i][j] = val; }
};

int const INF = 10005;
int n, m;
ii s, t;

struct MinCut{
   struct FlowEdge {
      ii v, u;
      int cap, flow = 0;
      FlowEdge(ii v, ii u, int cap) : v(v), u(u), cap(cap) {}
   };

   struct Dinic {
      vector<FlowEdge> edges;
      arr2D<vector<int>> adj;
      arr2D<int> level, ptr;
      int m = 0;
      queue<ii> q;

      Dinic(int x, int y) : adj(x, y), level(x, y), ptr(x, y){}

      void add_edge(ii v, ii u, int cap) {
         edges.emplace_back(v, u, cap);
         edges.emplace_back(u, v, 0);
         adj[v].push_back(m);
         adj[u].push_back(m + 1);
         m += 2;
      }

      bool bfs() {
         while (!q.empty()) {
            ii v = q.front();
            q.pop();
            for (int id : adj[v]) {
               if (edges[id].cap - edges[id].flow < 1)
                  continue;
               if (level[edges[id].u] != -1)
                  continue;
               level[edges[id].u] = level[v] + 1;
               q.push(edges[id].u);
            }
         }
         return level[t] != -1;
      }

      int dfs(ii v, int pushed) {
         if (pushed == 0)
            return 0;
         if (v == t)
            return pushed;
         for (int& cid = ptr[v]; cid < (int)adj[v].size(); cid++) {
            int id = adj[v][cid];
            ii u = edges[id].u;
            if (level[v] + 1 != level[u] || edges[id].cap - edges[id].flow < 1)
               continue;
            int tr = dfs(u, min(pushed, edges[id].cap - edges[id].flow));
            if (tr == 0)
               continue;
            edges[id].flow += tr;
            edges[id ^ 1].flow -= tr;
            return tr;
         }
         return 0;
      }

      int flow() {
         int f = 0;
         while (true) {
            level.set(-1);
            level[s] = 0;
            q.push(s);
            if (!bfs())
               break;
            ptr.set(0);
            while (int pushed = dfs(s, INF)) {
               f += pushed;
            }
         }
         return f;
      }
   };

   Dinic dinic;

   /** Split each vertex in two and set edge capacity
    *  Each vertex (i, j) is split in (i, j) and (i, j + m)
    *  (i, j) contain all edges that are going out of the original vertex
    *  (i, j + m) contain all edges that are entering of the original vertex
    **/
   void splitAndSet(arr2D<set<ii>>& adj){
      FOR(i, n)
         FOR(j, m){
            ii v = {i, j}, v2 = SIB(v);

            if(adj[v].size()){
               for(auto u: adj[v])
                  if(u != s)
                     dinic.add_edge(v, u != t ? SIB(u) : u, INF);//original edge

               // internal edges of each vertex
               dinic.add_edge(v, v2, 1);
               dinic.add_edge(v2, v, 1);
            }
         }
      for(auto u : adj[s])
         dinic.add_edge(s, SIB(u), INF);
   }

   set<ii> findCut(){
      stack<ii> q;
      set<ii> seen, cut;
      q.push(s);
      seen.insert(s);

      // find vertex set of the side of the source
      while(!q.empty()){
         ii v = q.top(); q.pop();
         for(auto id : dinic.adj[v]){
            auto e = dinic.edges[id];
            if (!seen.count(e.u) && e.cap - e.flow > 0){
               seen.insert(e.u);
               q.push(e.u);
            }
         }
      }

      /**
       * Find cut of vertex
       * See each vertex can includes only the vertex of its inner edge
       */
      for(auto v : seen)
         for(auto id: dinic.adj[v]){
            auto e = dinic.edges[id];
            if(!seen.count(e.u) && e.cap - e.flow == 0 && v == SIB(e.u))
               cut.insert(e.u);
            }

      return cut;
   }

   MinCut(arr2D<set<ii>>& adj):dinic(adj.x , adj.y){
      splitAndSet(adj);
   }

   set<ii> get(){
      dinic.flow();
      return findCut();
   }
};

arr2D<set<ii>> readGraph(arr2D<char>& mat){
   arr2D<set<ii>> adj(mat.x, mat.y);
   int xd[] = {1, 0};
   int yd[] = {0, 1};
   mat[s] = '1';
   mat[t] = '2';

   FOR(i, n)
      FOR(j, m){
         ii v = {i, j};
         if(!isValid(v))
            continue;
         FIX(v)

         for(int k = 0; k < 2; k++){
            int i2 = i + xd[k];
            int j2 = j + yd[k];
            ii u = {i2, j2};
            
            if(isValid(u) && (isFree(u) || mat[v] != mat[u])){
               FIX(u)
               adj[v].insert(u);
               adj[u].insert(v);
            }
         }
      }
   return adj;
}

int main(){
   cin.sync_with_stdio(0);
   cin.tie(0);

   cin >> n >> m;

   arr2D<char> mat(n + 1, 2 * m);
   s = {n, 0};
   t = {n, 1};

   FOR(i, n)
      cin >> &(mat.v[i][0]);

   auto adj = readGraph(mat);
   
   MinCut minCut(adj);
   auto cut = minCut.get();

   cout << cut.size() << endl;
   for(auto u : cut)
      mat[u] = '%';
   
   FOR(i, n)
      cout << &(mat.v[i][0]) << endl;
}
