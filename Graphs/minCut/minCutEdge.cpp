/**
 * Minimum s-t cut of edges - Using Dinic
 *
 * Time Complexity:
 *  - Non-scaling: O(|V|^2 * |E|)
 *  - Scaling: O(|V| * |E| * lg U) with higher constant
 *
 * Space Complexity: O(|V| + |E|)
 *
 * Problem: https://cses.fi/problemset/task/1695
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using ll = long long;
using ii = pair<int, int>;
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define all(a) a.begin(), a.end()

template <class T = int>
struct MinCut{
  const static T INF = numeric_limits<T>::max() / 2;

  struct FlowEdge {
    int v, u;
    T cap, flow = 0;
    FlowEdge(int v, int u, T cap) : v(v), u(u), cap(cap) {}
  };

  struct Dinic {
    const static bool SCALING = false;
    vector<FlowEdge> edges;
    vector<vi> adj;
    vi level, ptr;
    queue<int> q;
    int n, s, t, lim, m;

    Dinic(int n, int s, int t):
      adj(n), level(n), ptr(n), n(n), s(s), t(t), m(0) {}

    void AddEdge(int v, int u, T cap, bool isDirected = true) {
      edges.emplace_back(v, u, cap);
      edges.emplace_back(u, v, isDirected ? 0 : cap);
      adj[v].push_back(m);
      adj[u].push_back(m + 1);
      m += 2;
    }

    bool bfs() {
      fill(all(level), -1);
      level[s] = 0;
      q.push(s);

      while (!q.empty()) {
        int v = q.front(); q.pop();
        for (int id : adj[v]) {
          auto& e = edges[id];
          if (level[e.u] == -1 && e.flow < e.cap && (!SCALING || e.cap - e.flow >= lim)){
            level[e.u] = level[v] + 1;
            q.push(e.u);
          }
        }
      }
      return level[t] != -1;
    }

    T dfs(int v, T pushed) {
      if (v == t || !pushed)
        return pushed;
      for (int& cid = ptr[v]; cid < (int)adj[v].size(); cid++) {
        int id = adj[v][cid];
        auto& e = edges[id];
        if (level[v] + 1 != level[e.u])
          continue;
        if (T tr = dfs(e.u, min(pushed, e.cap - e.flow))){
          e.flow += tr;
          edges[id ^ 1].flow -= tr;
          return tr;
        }
      }
      return 0;
    }

    ll Solve() {
      ll f = 0;
      for (lim = SCALING ? (1 << 30) : 1; lim > 0; lim >>= 1) {
        while (bfs()) {
          fill(all(ptr), 0);
          while (T pushed = dfs(s, INF))
            f += pushed;
        }
      }
      return f;
    }
  };

  Dinic dinic;
  int s;
  ll cutSize;

  MinCut(int n, int s, int t): dinic(n, s, t), s(s) {}

  void AddEdge(int v, int u, T cap, bool isDirected = true) {
    dinic.AddEdge(v, u, cap, isDirected);
  }

  ll Solve(vector<ii>& cut) {
    dinic.Solve();
    stack<int> q;
    set<int> seen;
    q.push(s);
    seen.insert(s);

    // find vertex set of the side of the source
    while(!q.empty()){
      int v = q.top(); q.pop();
      for(auto id : dinic.adj[v]){
        auto e = dinic.edges[id];
        if (!seen.count(e.u) && e.cap - e.flow > 0){
          seen.insert(e.u);
          q.push(e.u);
        }
      }
    }

    // Find cut of vertex
    cutSize = 0;
    for(auto v : seen)
      for(auto id: dinic.adj[v]){
        auto e = dinic.edges[id];
        if(!seen.count(e.u) && e.cap - e.flow == 0){
          cut.push_back({v, e.u});
          cutSize += e.cap;
        }
      }
    return cutSize;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  cin >> n >> m;

  MinCut<int> minCut(n, 0, n - 1);
  FOR(i, m){
    int u, v;
    cin >> u >> v; u--; v--;
    minCut.AddEdge(u, v, 1, false);
  }

  vector<ii> cut;
  cout << minCut.Solve(cut) << '\n';
  FOR(i, cut.size())
    cout << cut[i].first + 1 << ' ' << cut[i].second + 1 << endl;
}
