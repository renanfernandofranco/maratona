/**
 * Minimum s-t cut of vertices
 * 
 * The graph used is the implicit graph of a matrix, with the
 * character equals to '1' equals to source, '2' equals to sink,
 * '.' equals to intermediate vertex and '#' a null position
 *
 * This algorithm find min cut to disconnect all 1's of all 2's
 *
 * Complexity:
 * - Time: O (|V| * |E|)
 * - Space O (|V| + |E|)
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using ii = pair<int, int>;
using vi = vector<int>;

#define FIX(w) if (mat[w] == '1') w = s; else if (mat[w] == '2') w = t;
#define isValid(u) (u.first >= 0 && u.first < n && u.second >= 0 && u.second < m && mat[u] != '#')
#define isFree(u) (mat[u] == '.')
#define SIB(u) (make_pair(u.first, u.second + m)) // sibling vertex
#define FOR(i, b) for(int i = 0; i < (b); i++)

template<class T>
struct arr2D{
   vector<vector<T>> v;
   int x, y;
   arr2D(int x, int y):x(x), y(y), v(x, vector<T>(y)){}
   T& operator [] (ii p){ return v[p.first][p.second];}
   void set(T val){ FOR(i, x) FOR(j, y) v[i][j] = val; }
};

int const INF = 10005;
ii s, t;
int n, m;

struct Edmonds{
   struct FlowEdge {
      ii v, u;
      int cap, flow = 0;
      FlowEdge(ii v, ii u, int cap) : v(v), u(u), cap(cap) {}
   };

   vector<FlowEdge> edges;
   arr2D<vi> adj;
   arr2D<int> ptr;
   int flow, m = 0;

   Edmonds(int mx, int my):adj(mx, my), ptr(mx, my){}

   void AddEdge(ii v, ii u, int dirCap, int revCap = 0){
      edges.emplace_back(v, u, dirCap);
      edges.emplace_back(u, v, revCap);
      adj[v].push_back(m);
      adj[u].push_back(m + 1);
      m += 2;
   }

   void augument(ii u){
      if(u != s){
         int id = ptr[u];
         flow = min(edges[id].cap - edges[id].flow, flow);
         augument(edges[id].v);
         edges[id].flow += flow;
         edges[id ^ 1].flow -= flow;
      }
   }

   int maxFlow(){
      bool find = true;
      int ans = 0;

      while(find){
         find = false;

         stack<ii> q;
         q.push(s);
         
         ptr.set(-1);
         ptr[s] = -2;

         while(!q.empty() && !find){
            ii v = q.top(); q.pop();
            for(int id : adj[v]){
               auto e = edges[id];
               if (ptr[e.u] == -1 && e.cap - e.flow > 0){
                  ptr[e.u] = id;
                  q.push(e.u);
                  find |= e.u == t;
               }
            }
         }

         if(find){
            flow = INT_MAX;
            augument(t);
            ans += flow;
         }
      }
      return ans;
   }
};

struct MinCut{
   Edmonds edm;

   MinCut(arr2D<set<ii>>& adj): edm(adj.x, adj.y){
      splitAndSet(adj);
   }

   /** Split each vertex in two and set edge capacity
    *  Each vertex (i, j) is split in (i, j) and (i, j + m)
    *  (i, j) contain all edges that are going out of the original vertex
    *  (i, j + m) contain all edges that are entering of the original vertex
    **/
   void splitAndSet(arr2D<set<ii>>& adj){
      FOR(i, n)
         FOR(j, m){
            ii v = {i, j};
            if(adj[v].size()){
               for(auto u: adj[v])
                  if(u != s)
                     edm.AddEdge(v, u != t? SIB(u) : u, INF);

               edm.AddEdge(v, SIB(v), 1, 1);
            }
         }
      for(auto u : adj[s])
         edm.AddEdge(s, SIB(u), INF);
   }

   set<ii> FindCut(){
      edm.maxFlow();
      stack<ii> q;
      set<ii> seen, cut;
      q.push(s);
      seen.insert(s);

      // find vertex set of the side of the source
      while(!q.empty()){
         ii v = q.top(); q.pop();
         for(auto id : edm.adj[v]){
            auto e = edm.edges[id];
            if (!seen.count(e.u) && e.cap - e.flow > 0){
               seen.insert(e.u);
               q.push(e.u);
            }
         }
      }

      /**
       * Find cut of vertex
       * See each vertex can includes only the vertex of its inner edge
       */
      for(auto v : seen)
         for(auto id: edm.adj[v]){
            auto e = edm.edges[id];
            if(!seen.count(e.u) && e.cap - e.flow == 0 && v == SIB(e.u))
               cut.insert(e.u);
            }

      return cut;
   }
};

arr2D<set<ii>> readGraph(arr2D<char>& mat){
   arr2D<set<ii>> adj(mat.x, mat.y);
   int xd[] = {1, 0};
   int yd[] = {0, 1};
   mat[s] = '1';
   mat[t] = '2';

   FOR(i, n)
      FOR(j, m){
         ii v = {i, j};
         if(!isValid(v))
            continue;
         FIX(v)

         for(int k = 0; k < 2; k++){
            int i2 = i + xd[k];
            int j2 = j + yd[k];
            ii u = {i2, j2};
            
            if(isValid(u) && (isFree(u) || mat[v] != mat[u])){
               FIX(u)
               adj[v].insert(u);
               adj[u].insert(v);
            }
         }
      }
   return adj;
}

int main(){
   cin.sync_with_stdio(0);
   cin.tie(0);

   cin >> n >> m;

   arr2D<char> mat(n + 1, 2 * m);
   s = {n, 0};
   t = {n, 1};

   FOR(i, n)
      cin >> &(mat.v[i][0]);

   auto adj = readGraph(mat);

   MinCut minCut = MinCut(adj);
   auto cut = minCut.FindCut();

   cout << cut.size() << endl;
   for(auto u : cut)
      mat[u] = '%';
   
   FOR(i, n)
      cout << &(mat.v[i][0]) << endl;
}
