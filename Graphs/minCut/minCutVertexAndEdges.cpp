/**
 * Minimum s-t cut of vertices and edges - Using Dinic
 *
 * In this problem, both vertices and edges are weighted.
 * But we can't remove the source and sink
 *
 * Complexity:
 * - Time: O (|V|^2 * |E|)
 * - Space O (|V| + |E|)
 *
 *  Problem: https://vjudge.net/problem/UVA-11506
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using ii = pair<int, int>;

#define SIB(u) (u + k) // sibling vertex
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define SZ(a) ((int) a.size())
#define all(a) a.begin(), a.end()

template<class T = int>
struct MinCut{
  struct FlowEdge {
    int v, u;
    T cap, flow = 0;
    FlowEdge(int v, int u, T cap) : v(v), u(u), cap(cap) {}
  };

  struct Dinic {
    int n, m, s, t;
    vector<FlowEdge> edges;
    vvi adj;
    vi level, ptr;
    queue<int> q;

    Dinic(int n): n(n), m(0), adj(n), level(n), ptr(n){}

    void AddEdge(int v, int u, T cap) {
      edges.emplace_back(v, u, cap);
      edges.emplace_back(u, v, 0);
      adj[v].push_back(m);
      adj[u].push_back(m + 1);
      m += 2;
    }

    bool bfs() {
      while (!q.empty()) {
        int v = q.front();
        q.pop();
        for (int id : adj[v]) {
          if (edges[id].cap - edges[id].flow < 1)
            continue;
          if (level[edges[id].u] != -1)
            continue;
          level[edges[id].u] = level[v] + 1;
          q.push(edges[id].u);
        }
      }
      return level[t] != -1;
    }

    T dfs(int v, T pushed) {
      if (pushed == 0)
        return 0;
      if (v == t)
        return pushed;
      for (int& cid = ptr[v]; cid < (int)adj[v].size(); cid++) {
        int id = adj[v][cid];
        int u = edges[id].u;
        if (level[v] + 1 != level[u] || edges[id].cap - edges[id].flow < 1)
          continue;
        int tr = dfs(u, min(pushed, edges[id].cap - edges[id].flow));
        if (tr == 0)
          continue;
        edges[id].flow += tr;
        edges[id ^ 1].flow -= tr;
        return tr;
      }
      return 0;
    }

    T flow(int ss, int tt) {
      s = ss, t = tt;
      T f = 0;
      while (true) {
        level.assign(n, -1);
        level[s] = 0;
        q.push(s);
        if (!bfs())
          break;
        ptr.assign(n, 0);
        while (T pushed = dfs(s, INF)) {
          f += pushed;
        }
      }
      return f;
    }
  };

  int k;
  Dinic dinic;
  static const T INF = numeric_limits<T>::max() / 2;

  /** Split each vertex in two and set edge capacity
   *  Each vertex (i) is split in (i) and (i + k)
   *  (i) contain all edges that are going out of the original vertex
   *  (i + k) contain all edges that are entering of the original vertex
   **/
  void splitAndSet(vector<vector<ii>>& adj, vi& weights){
    int s = 0, t = k - 1;
    FOR(v, k){
      int v2 = SIB(v);

      for(ii pr: adj[v]){
        int u = pr.first, w = pr.second;
        if(v != t && u != s)
          dinic.AddEdge(v, SIB(u), w); //original edge
      }

      // internal edges of each vertex
      dinic.AddEdge(v, v2, weights[v]);
      dinic.AddEdge(v2, v, weights[v]);
    }
  }

  T findCut(int s){
    stack<int> q;
    set<int> seen;
    q.push(s);
    seen.insert(s);

    // find vertex set of the side of the source
    while(!q.empty()){
      int v = q.top(); q.pop();
      for(auto id : dinic.adj[v]){
        auto e = dinic.edges[id];
        if (!seen.count(e.u) && e.cap - e.flow > 0){
          seen.insert(e.u);
          q.push(e.u);
        }
      }
    }

    vector<T> vertexCostCut, edgeCostCut;
    for(auto v : seen)
      for(auto id: dinic.adj[v]){
        auto e = dinic.edges[id];
        if(!seen.count(e.u) && e.cap - e.flow == 0){
          if(v == SIB(e.u))
            vertexCostCut.push_back(e.cap);
          else
            edgeCostCut.push_back(e.cap);
          }
        }

    return accumulate(all(vertexCostCut), (T)0) + accumulate(all(edgeCostCut), (T)0);
  }

  MinCut(vector<vector<ii>>& adj, vi& weights): k(SZ(adj)), dinic(2 * k){
    splitAndSet(adj, weights);
  }

  T get(int s, int t){
    dinic.flow(s, t);
    return findCut(s);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  while(cin >> n >> m, (n != 0 || m != 0)){
    vector<vector<ii>> adj(n);

    vi weights(n);
    FOR(i, n - 2){
      int id;
      cin >> id;
      cin >> weights[id - 1];
    }
    weights[0] = weights[n - 1] = INT_MAX / 2;

    FOR(i, m){
      int u, v, w;
      cin >> u >> v >> w;
      u--; v--;
      adj[u].push_back({v, w});
      adj[v].push_back({u, w});
    }

    MinCut<> minCut(adj, weights);
    int cutVal = minCut.get(0, 2 * n - 1);

    cout << cutVal << endl;
  }
}
