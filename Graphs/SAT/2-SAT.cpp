/**
 * 2-SAT
 *
 * Given a set of the doors and switches, where each
 * door is controlled by exactly two switches, and an
 * initial state of each door. It is desired to open
 * all doors (state = 1).
 *
 * Complexity:
 *    - Time: O(n + m)
 *    - Space: O(n + m)
 *
 */
#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)

struct SAT{
  int n;
  vvi g, gt;
  vi order, comp;
  vector<char> used, ans;

  SAT(int _n): n(_n << 1), g(n), gt(n),
    comp(n, - 1), used(n, false), ans(_n){}

  // Add Expr : inv ? !(s1 xor s2) : s1 xor s2
  void Add(int s1, int s2, bool inv){
    s1 = (s1 << 1) ^ inv;
    s2 <<= 1;
    addOr(s1, s2);
    addOr(s1 ^ 1, s2 ^ 1);
  }

  // At least one of the two variables must be true.
  void addOr(int v, int u){
    g[v].push_back(u ^ 1);
    g[u].push_back(v ^ 1);
    gt[u ^ 1].push_back(v);
    gt[v ^ 1].push_back(u);
  }

  void dfs1(int v) {
    used[v] = true;
    for (int u : g[v])
      if (!used[u])
        dfs1(u);
    order.push_back(v);
  }

  void dfs2(int v, int cl) {
    comp[v] = cl;
    for (int u : gt[v])
      if (comp[u] == -1)
        dfs2(u, cl);
  }

  bool Solve() {
    FOR(i, n)
      if (!used[i])
        dfs1(i);

    int cl = 0;
    REV(i, n)
      if (comp[order[i]] == -1)
        dfs2(order[i], cl++);

    for (int i = 0; i < n; i += 2) {
      if (comp[i] == comp[i ^ 1])
        return false;
      ans[i >> 1] = comp[i] > comp[i ^ 1];
    }
    return true;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, m, k, d;
  cin >> n >> m;
  SAT sat(m);
  vi st(n); // initial door state
  FOR(i, n)
    cin >> st[i];

  vvi doors(n);
  FOR(i, m){
    cin >> k;
    FOR(j, k){
      cin >> d;
      doors[d - 1].push_back(i);
    }
  }
  // We want the final state to be equal to 1, so
  // the xor of the keys implies the initial state
  FOR(i, n)
    sat.Add(doors[i][0], doors[i][1], st[i]);

  cout << (sat.Solve() ? "YES" : "NO") << endl;
}
