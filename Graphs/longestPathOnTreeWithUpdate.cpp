#include <bits/stdc++.h>

/**
 * Algorithm to find vertice with longest path leaving of the root.
 * Also is possible increment/decrement the cost of a edge
 * 
 * Complexity:
 *      - Time : O(n) to build and O(lg n) to query/update
 *      - Space: O(n) (21 * n to be more exact)
 * 
 * Obs: see that high constant of space brings to a time complexity of build almost O(n lg n)
 * 
 * Status : slightly tested
 */ 


using namespace std;

#define MD(l, r) (l + r >> 1)
#define CL(v) (v << 1)
#define CR(v) (v << 1 | 1)
using ll = long long;

struct edge{
    int to, cost;
    edge(int to, int cost):to(to),cost(cost){}
};

vector<vector<edge>> adj;
vector<int> tin, tout, cost, vseg, vtime, lazy;
int timer;


void build(int v, int tl, int tr){
    if(tl == tr){
        vseg[v] = cost[vtime[tl]];
    }else{
        int tm = MD(tl, tr);
        build(CL(v), tl, tm);
        build(CR(v), tm + 1, tr);
        vseg[v] = max(vseg[CL(v)], vseg[CR(v)]);
    }
}

void push(int v){
    lazy[CL(v)] += lazy[v];
    lazy[CR(v)] += lazy[v];
    vseg[CL(v)] += lazy[v];
    vseg[CR(v)] += lazy[v];
    lazy[v] = 0; 
}

int query(int v, int tl, int tr){
    if(tl == tr)
        return vtime[tl];
    push(v);
    int tm = MD(tl, tr);
    if(vseg[CL(v)] >= vseg[CR(v)])
        return query(CL(v), tl, tm);
    return query(CR(v), tm + 1, tr);
}

void update(int v, int tl, int tr, int l, int r, int x){
    if(l > r)
        return;
    if(tl == tr){
        vseg[v] += x;
        return;
    }
    if(l == tl && r == tr){
        lazy[v] += x;
        vseg[v] += x;
    }else{
        push(v);
        int tm = MD(tl, tr);
        update(CL(v), tl, tm, l, min(r, tm), x);
        update(CR(v), tm + 1, tr, max(l, tm + 1), r, x);
        vseg[v] = max(vseg[CL(v)], vseg[CR(v)]);
    }
}

void dfs(int v, int p){
    vtime[timer] = v;
    tin[v] = timer++;

    for(edge pu : adj[v]){
        if(pu.to != p){
            cost[pu.to] = cost[v] + pu.cost;
            dfs(pu.to, v);
        }
    }
    vtime[timer] = v;
    tout[v] = timer++;
}

void build(int n, int& szSeg){
    szSeg = 2 * n - 1;
    tin.resize(n);
    tout.resize(n);
    vtime.resize(n << 1);
    vseg.resize(n << 3);
    lazy.resize(n << 3);
    cost.resize(n);
    cost[0];
    timer = 0;
    dfs(0, 0);
    build(1, 0 , szSeg);
}

int main(){
    cin.sync_with_stdio(0);
    cin.tie(0);
    int i, n, w, u, v, szSeg;
    cin >> n;
    adj.resize(n);

    for(i = 0; i < n - 1; i++){
        cin >> u >> v >> w;
        u--;
        v--;
        adj[u].push_back(edge(v, w));
        adj[v].push_back(edge(u, w));
    }

    build(n, szSeg);

    while(true){
        u = query(1, 0, szSeg);
        update(1, 0, szSeg, tin[u], tout[u], -1);
    }
    cout << vseg[1] << endl;

}