# Graphs Algorithms

The following graph algorithms are available in this notebook so far.

## Functional Graph

This is a special type of graph in which each vertex has exactly one outgoing edge, which might be directed or not. In this sense, one might identify this kind of graph by checking either the number of outgoing edges or the total number of vertices and edges, or even by the fact that the graph might be seen as a function mapping to itself.

In this kind of graph, we'll eventually get in cycle from any starting vertex, and finding information about each cycle might be the key idea to solve problems involving it. One algorithm for solving this kind of problems on a directed graph is shown in [fn-graph.cpp](./fn-graph.cpp).

Further information about this type of graph and its properties can be found at [Sousa A. slides][1].

## Maximum Matching, Minimum Vertex Cover and Max Independent Set in Bipartite Graphs

A graph is bipartite if its vertices can be partitioned into two sets such that each edge has one endpoint in each set.

A vertex cover in a graph is a set of vertices that includes at least one endpoint of every edge, and a vertex cover is minimum if no other vertex over has fewer vertices.

A matching in a graph is a set of edges no two of which share an endpoint, and a matching is maximum if no other matching has more edges.

Kőnig's theorem states that, in any bipartite graph, the number of edges in a maximum matching is equal to the number of vertices in a minimum vertex cover.

For graphs that are not bipartite, the maximum matching and minimum vertex cover problems are very different in complexity: maximum matchings can be found in polynomial time for any graph, while minimum vertex cover is NP-complete.

The complement of a vertex cover in any graph is an independent set, so a minimum vertex cover is complementary to a maximum independent set and finding maximum independent sets is another NP-complete problem.

An implementation using the Ford-Fulkerson Algorithm for problem of discovering the number of edges of the Maximum Matching in bipartite graphs can be seen in [bpm-vc.cpp](./bpm-vc.cpp). This code was based on the code available in [Maximum Bipartite Matching][3]

Further information about this problems can be found at [Kőnig's theorem][2].

## The Chinese Postman Problem 

The Chinese postman problem, postman tour or route inspection problem is to find a shortest closed path or circuit that visits every edge of an (connected) undirected graph. 

When the graph has an Eulerian circuit (a closed walk that covers every edge once), that circuit is an optimal solution. Otherwise, the optimization problem is to find the smallest number of graph edges to duplicate (or the subset of edges with the minimum possible total weight) so that the resulting multigraph does have an Eulerian circuit.

It can be solved in polynomial time, but for what I found is necessary The Blossom Algorithm for Weighted Graphs, in which is very hard to be implemented

An implementation using an exponential algorithm instead The Blossom Algorithm can be seen in [chinesePostman.cpp](./chinesePostman.cpp). This code solves the Chinese Postman Problem with a bit modification, instead of finding the cost of Eulerian Circuit, the same finds the cost of Eulerian Path.

Further information about this problems can be found at [Chinese Postman Problem][4].

[1]: http://maratona.ic.unicamp.br/MaratonaVerao2020/lecture-b/20200122.pdf

[2]: https://en.wikipedia.org/wiki/K%C5%91nig%27s_theorem_(graph_theory)

[3]: https://www.geeksforgeeks.org/maximum-bipartite-matching/

[4]: https://en.wikipedia.org/wiki/Route_inspection_problem