/**
 * Heavy-Light Decompostion - Edge Add, Max in the Path
 * 
 * Operations:
 *    - Range Query - O(log² n)
 *    - Point Update - O(log n)
 * 
 * Space Complexity : O(n)
 * 
 * https://www.spoj.com/problems/QTREE/
 * 
 */

#include <bits/stdc++.h>
 
using namespace std;

using ii = pair<int, int>;
using vi = vector<int>;
using vvi = vector<vi>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)
 
template <class T>
struct SegmentTree{
   int n;
   vector<T> t;

   SegmentTree(int n): n(n), t(n << 1){}
 
   void Set(int i, T vl){
      t[i + n] = vl;
   }
 
   void Build() {  // Build the tree
      for (int i = n - 1; i > 0; --i) 
         t[i] = max(t[i << 1], t[i << 1 | 1]);
   }
 
   void Update(int p, T value) {  // Set value at position p
      for (t[p += n] = value; p > 1; p >>= 1) 
         t[p >> 1] = max(t[p], t[p ^ 1]);
   }
 
   T Query(int l, int r) {  // max value on interval [l, r)
      T res = 0;
      for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
         if (l & 1) res = max(res, t[l++]);
         if (r & 1) res = max(res, t[--r]);
      }
      return res;
   }
};
 
template <class T>
struct HeavyLight {
   int n, id;
   vector<int> dad, heavy, depth, root, treePos, edgeDad;
   vvi adj;
   vector<T> vals;
   SegmentTree<T> tree;
 
   template <class G> 
   HeavyLight(const G& graph, int r = 0):n (2 * graph.size() - 1), dad(n), heavy(n, -1),
      depth(n), root(n), treePos(n), edgeDad(n), adj(n), vals(n, 0), tree(n){
 
      dad[r] = -1;
      depth[r] = 0;
      id = graph.size();
      subDivision(graph, r, -1);
      dfs(r);
 
      for (int i = 0, currentPos = 0; i < n; ++i)
         if (dad[i] == -1 || heavy[dad[i]] != i)
            for (int j = i; j != -1; j = heavy[j]) {
               root[j] = i;
               treePos[j] = currentPos++;
            }
 
      FOR(i, n)
         tree.Set(treePos[i], vals[i]);
      tree.Build();
   }
 
   int dfs(int v) {
      int size = 1, maxSubtree = 0;
      for (int u : adj[v]) 
         if (u != dad[v]) {
            dad[u] = v;
            depth[u] = depth[v] + 1;
 
            int subtree = dfs(u);
 
            if (subtree > maxSubtree) 
               heavy[v] = u, maxSubtree = subtree;
            size += subtree;
         }
      
      return size;
   }

   template <class G>
   void subDivision(const G& graph, int v, int p){
      for (auto pr : graph[v]){
         int u = pr.first;
         T vl = pr.second;
         
         if(u != p){
            int edge = id++;
            adj[v].push_back(edge);
            adj[edge].push_back(u);
            edgeDad[u] = edge;
 
            vals[edge] = vl;
            subDivision(graph, u, v);
         }
      }
   }
 
   void UpdateEdge(int u, int v, const T& value) {
      if(depth[u] < depth[v])
         swap(u, v);
      tree.Update(treePos[edgeDad[u]], value);
   }
 
   T QueryPath(int u, int v) {
      T res = numeric_limits<T>::min();
      for (; root[u] != root[v]; v = dad[root[v]]) {
         if (depth[root[u]] > depth[root[v]]) 
            swap(u, v);
         res = max(res, tree.Query(treePos[root[v]], treePos[v] + 1));
      }
 
      if (depth[u] > depth[v]) 
         swap(u, v);
      res = max(res, tree.Query(treePos[u], treePos[v] + 1));
      return res;
   }

};
 
int main(){
   cin.tie(0)->sync_with_stdio(0);
   int t;
   cin >> t;

   while(t--){
      int n, u, v, w;

      cin >> n;
      vector<vector<ii>> adj(n);
      vector<ii> edges(n - 1);

      FOR(i, n - 1){
         cin >> u >> v >> w;
         u--; v--;
         adj[u].push_back({v, w});
         adj[v].push_back({u, w});
         edges[i] = {u, v};
      }

      HeavyLight<int> HLD(adj);
      string s;

      while(cin >> s){
         if(s[0] == 'Q'){
            cin >> u >> v;
            cout << HLD.QueryPath(u - 1, v - 1) << endl;
         }else if(s[0] == 'C'){
            cin >> u >> w;
            HLD.UpdateEdge(edges[u - 1].first, edges[u - 1].second, w);
         }else
            break;
      }
   }
} 
