/**
 * Heavy-Light Decompostion - Vertex Add, Path Sum
 *
 * Operations:
 *  - Range Query - O(log² n)
 *  - Point Update - O(log n)
 *
 * Space Complexity : O(n)
 *
 * https://judge.yosupo.jp/problem/vertex_add_path_sum
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using vvi = vector<vi>;
using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T>
struct SegmentTree{
  int n;
  vector<T> t;
  SegmentTree(int n): n(n), t(n << 1){}

  void Set(int i, T vl){
    t[i + n] = vl;
  }

  void Build() {  // Build the tree
    for (int i = n - 1; i > 0; --i)
      t[i] = t[i << 1] + t[i << 1 | 1];
  }

  void Update(int p, T value) { // Increment value at position p
    for (t[p += n] += value; p > 1; p >>= 1)
      t[p >> 1] = t[p] + t[p ^ 1];
  }

  T Query(int l, int r) { // Sum on interval [l, r)
    T res = 0;
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
      if (l & 1) res += t[l++];
      if (r & 1) res += t[--r];
    }
    return res;
  }
};

template <class T>
struct HeavyLight {
  int n;
  vi dad, heavy, depth, root, treePos;
  vvi adj;
  SegmentTree<T> tree;

  HeavyLight(int n): n(n), dad(n),  heavy(n, -1), depth(n),
    root(n), treePos(n), adj(n), tree(n){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void Build(vector<T>& vals, int r = 0){
    dad[r] = -1;
    depth[r] = 0;
    dfs(r);

    for (int i = 0, currentPos = 0; i < n; ++i)
      if (dad[i] == -1 || heavy[dad[i]] != i)
        for (int j = i; j != -1; j = heavy[j]) {
          root[j] = i;
          treePos[j] = currentPos++;
        }
    FOR(i, n)
      tree.Set(treePos[i], vals[i]);
    tree.Build();
  }

  int dfs(int v) {
    int size = 1, maxSubtree = 0;
    for (int u : adj[v])
      if (u != dad[v]) {
        dad[u] = v;
        depth[u] = depth[v] + 1;

        int subtree = dfs(u);

        if (subtree > maxSubtree)
          heavy[v] = u, maxSubtree = subtree;
        size += subtree;
      }

    return size;
  }

  void UpdateVertex(int v, const T& value) {
    tree.Update(treePos[v], value);
  }

  T QueryPath(int u, int v) {
    T res = 0;
    for (; root[u] != root[v]; v = dad[root[v]]) {
      if (depth[root[u]] > depth[root[v]])
        swap(u, v);
      res += tree.Query(treePos[root[v]], treePos[v] + 1);
    }

    if (depth[u] > depth[v])
      swap(u, v);
    res += tree.Query(treePos[u], treePos[v] + 1);
    return res;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q, u, v, p, x, tp;
  cin >> n >> q;
  vector<ll> vals(n);
  HeavyLight<ll> HLD(n);

  FOR(i, n)
    cin >> vals[i];

  FOR(i, n - 1){
    cin >> u >> v;
    HLD.AddEdge(u, v);
  }

  HLD.Build(vals);

  FOR(i, q){
    cin >> tp;
    switch(tp){
      case 0:
        cin >> p >> x;
        HLD.UpdateVertex(p, x);
        break;
      case 1:
        cin >> u >> v;
        cout << HLD.QueryPath(u, v) << endl;
    }
  }
}
