/**
 * Heavy-Light Decompostion - Vertex Set, Path Composite
 *  
 * Each vertex stores a linear function, and given two 
 * vertices u and v, this algorithm can answer queries of
 * type f_u (f _... (f_v (x))) mod M. Since this operation
 * is non-commutative, two SegTree are used, one with the
 * suffix and one with the prefix
 * 
 * Operations:
 *    - Range Query - O(log² n)
 *    - Point Update - O(log n)
 * 
 * Space Complexity : O(n)
 * 
 * https://judge.yosupo.jp/problem/vertex_set_path_composite
 * 
 */

#include <bits/stdc++.h>
 
using namespace std;

using vi = vector<int>;
using vvi = vector<vi>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

const int MD = 998'244'353;

template <class T = int>
struct MOD{
   T v;

   MOD(T vl = 0){ this->operator=(vl); }

   void operator *= (const MOD& m2){ (*this) = this->operator*(m2); }

   void operator += (const MOD& m2){ (*this) = this->operator+(m2); }
   
   void operator -= (const MOD& m2){ (*this) = this->operator-(m2); }

   void operator = (T vl){ v = vl; }

   bool operator == (const MOD& m2){ return v == m2.v; }

   MOD operator * (const MOD& m2) const{
      return v * 1LL * m2.v % MD;
   }

   MOD operator + (const MOD& m2) const{
      T tmp = v + m2.v;
      return tmp >= MD ? tmp - MD : tmp;
   }

   MOD operator - (const MOD& m2) const{
      T tmp = v - m2.v;
      return tmp < 0 ? tmp + MD : tmp;
   }
};

struct Function{
   MOD<int> a = 1, b = 0;
   Function operator +(const Function& f2) const{
      return Function{a * f2.a, a * f2.b + b};
   }

   int operator ()(int x){
      return (a * x + b).v; 
   }
};

template <class T>
struct SegmentTree{
   int n;
   vector<T> t;
   T(*op)(const T& v1, const T& v2);

   SegmentTree(int n, T(*op)(const T& v1, const T& v2)): n(n), t(n << 1), op(op){}
 
   void Set(int i, T vl){
      t[i + n] = vl;
   }
 
   void Build() {  // Build the tree
      for (int i = n - 1; i > 0; --i) 
         t[i] = op(t[i << 1], t[i << 1 | 1]);
   }
 
   void Update(int p, T value) { // Set value at position p
      for (t[p += n] = value; p >>= 1; ) 
         t[p] = op(t[p << 1], t[p << 1 | 1]);
   }
 
   T Query(int l, int r) { // Composite of the interval [l, r)
      T resl, resr;
      for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
         if (l & 1) resl = op(resl, t[l++]);
         if (r & 1) resr = op(t[--r], resr);
      }
      return op(resl, resr);
   }
};
 
template <class T>
struct HeavyLight {
   int n;
   vi dad, heavy, depth, root, treePos;
   vvi adj;
   SegmentTree<T> tree[2];
 
   static T suf(const T& t1, const T& t2){ return t1 + t2; }
   static T pref(const T& t1, const T& t2){ return t2 + t1; }

   HeavyLight(int n): n(n), dad(n),  heavy(n, -1), depth(n),
      root(n), treePos(n), adj(n), 
      tree({SegmentTree<T>(n, suf), SegmentTree<T>(n, pref)}){}
   
   void AddEdge(int u, int v){
      adj[u].push_back(v);
      adj[v].push_back(u);
   }

   void Build(vector<T>& vals, int r = 0){
      dad[r] = -1;
      depth[r] = 0;
      dfs(r);
 
      for (int i = 0, currentPos = 0; i < n; ++i)
         if (dad[i] == -1 || heavy[dad[i]] != i)
            for (int j = i; j != -1; j = heavy[j]) {
               root[j] = i;
               treePos[j] = currentPos++;
            }
      FOR(k, 2){
         FOR(i, n) tree[k].Set(treePos[i], vals[i]);
         tree[k].Build();
      }
   }
 
   int dfs(int v) {
      int size = 1, maxSubtree = 0;
      for (int u : adj[v]) 
         if (u != dad[v]) {
            dad[u] = v;
            depth[u] = depth[v] + 1;
 
            int subtree = dfs(u);
 
            if (subtree > maxSubtree) 
               heavy[v] = u, maxSubtree = subtree;
            size += subtree;
         }
      
      return size;
   }
 
   void UpdateVertex(int v, const T& value) {
      tree[0].Update(treePos[v], value);
      tree[1].Update(treePos[v], value);
   }
   
   // return the function f_u(f_...(f_v(x)))
   T QueryPath(int u, int v) {
      T res[2]; // res[0] is the result suffix, res[1] is the result prefix
      bool p = false;
      vector<T(*)(const T& v1, const T& v2)> op = {suf, pref};

      for (; root[u] != root[v]; v = dad[root[v]]) {
         if (depth[root[u]] > depth[root[v]]) 
            swap(u, v), p ^= 1;
         res[p] = op[p](tree[p].Query(treePos[root[v]], treePos[v] + 1), res[p]);
      }

      if (depth[u] > depth[v]) 
         swap(u, v), p ^= 1;
      res[p] = op[p](tree[p].Query(treePos[u], treePos[v] + 1), res[p]);
      return suf(res[1], res[0]);
   }
};
 
int main(){
   cin.tie(0)->sync_with_stdio(0);
   int n, q, u, v, p, tp, a, b, x;
   cin >> n >> q;
   vector<Function> vals(n);
   HeavyLight<Function> HLD(n);
   
   FOR(i, n){
      cin >> a >> b;
      vals[i] = {a, b};
   }
      
   FOR(i, n - 1){
      cin >> u >> v;
      HLD.AddEdge(u, v);
   }
   
   HLD.Build(vals);

   FOR(i, q){
      cin >> tp;
      switch(tp){
         case 0:
            cin >> p >> a >> b;
            HLD.UpdateVertex(p, {a, b});
            break;
         case 1:
            cin >> u >> v >> x;
            cout << HLD.QueryPath(v, u)(x) << endl;
      }
   }
} 
