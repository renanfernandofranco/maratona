/**
 * Minimum Spanning Tree
 *
 * Complexity:
 *  - Time: O(m lg m)
 *  - Space: O(m)
 *
 *  Problem: https://www.spoj.com/problems/MST/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
#define FOR(i, b) for(int i = 0; i < b; i++)
#define all(a) a.begin(), a.end()

struct Edge{
  int u, v;
  ll w;
  bool operator < (const Edge& e2) const { return w < e2.w; }
};

struct MST{
  struct UnionFind{
    vector<int> dad, sizes;

    UnionFind(int n): dad(n), sizes(n, 1){
      FOR(i, n) dad[i] = i;
    }

    int FindSet(int u){
      return u == dad[u] ? u : (dad[u] = FindSet(dad[u]));
    }

    int UnionSet(int u, int v){
      u = FindSet(u);
      v = FindSet(v);
      if (u != v){
        if (sizes[u] < sizes[v])
          swap(u, v);
        sizes[u] += sizes[v];
        dad[v] = dad[u];
      }
      return u;
    }

    bool Connected(int u, int v){
      return FindSet(u) == FindSet(v);
    }
  };

  UnionFind uf;
  vector<Edge> edges;

  MST(int n): uf(n){}

  void AddEdge(int u, int v, ll w){
    edges.push_back({u, v, w});
  }

  ll Solve(){
    sort(all(edges));
    ll ans = 0;
    for(auto e : edges){
      if(!uf.Connected(e.u, e.v)){
        ans += e.w;
        uf.UnionSet(e.u, e.v);
      }
    }
    return ans;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  cin >> n >> m;

  MST mst(n);

  FOR(i, m){
    int u, v;
    ll w;

    cin >> u >> v >> w;
    mst.AddEdge(u - 1, v - 1, w);
  }

  cout << mst.Solve() << endl;
}
