/**
 * Minimum Spanning Tree with upper bound in the degree of the root 
 * 
 * Complexity:
 *  - Time: O(m lg m)
 *  - Space: O(m)
 * 
 *  Problem: https://vjudge.net/problem/UVA-1537
 * 
 *  Status : weakly tested
 * 
 */

#include <bits/stdc++.h>
#include "../../DataStructures/randomizedHeap.hpp"

using namespace std;
using ll = long long;
using ii = pair<int, int>;
#define FOR(i, b) for(int i = 0; i < b; i++)

const ll INF = 1e18;

struct Edge{
    int u, v;
    ll w;
    bool operator <(const Edge& e2)const{ return w < e2.w; }
    Edge(int u = -1, int v = -1, ll w = INF): u(u), v(v), w(w){}
};

struct MST{
    struct UnionFind{
        vector<int> dad, sizes;
        vector<Heap<Edge>> toOut; // edges that are going out
        vector<Edge> toRoot;
        bool type;

        UnionFind(int n):dad(n), sizes(n, 1){
            type = false;
            FOR(i, n) dad[i] = i;
        }

        UnionFind(int n, vector<Edge>& edges, int root):
            dad(n), sizes(n, 1), toOut(n), toRoot(n){
            type = true;
            FOR(i, n) dad[i] = i;

            for(Edge e: edges)
                if(e.u != root && e.v != root){
                    toOut[e.u].insert(e);
                    toOut[e.v].insert(e);
                }
        }

        int findSet(int u){
            return u == dad[u] ? u : (dad[u] = findSet(dad[u]));
        }

        int unionSet(int u, int v){
            u = findSet(u);
            v = findSet(v);
            if (u != v){
                if (sizes[u] < sizes[v])
                    swap(u, v);
                sizes[u] += sizes[v];
                dad[v] = dad[u];

                if(type)
                    merge(u, v);
            }
            return u;
        }

        bool connected(int u, int v){
            return findSet(u) == findSet(v);
        }

        // merge edges that are going out
        void merge(int u, int v){
            toRoot[u] = min(toRoot[u], toRoot[v]);
            toOut[u].merge(toOut[v]);
        }

        void setToRoot(int u, Edge e){
            toRoot[findSet(u)] = e;
        }

        Edge getMin(int u){
            auto& heap = toOut[u];
            while(!heap.empty()){
                Edge e = heap.top();
                if(!connected(e.u, e.v))
                    return e;
                heap.pop();
            }
            return Edge();
        }

        ll getCost(int u){
            return getMin(u).w - toRoot[u].w;
        }
    };

    /**
     * u1 - union find of mst(classic)
     * u2 - auxiliary union-find that exclude edges to root, and maintain the
     *      cheapest edge to another component and the edge from component to root
     */
    UnionFind u1, u2;
    set<pair<ll, int>> comp; // {cost-benefit, component representative}
    vector<Edge>& edges;
    int k, n, u, v, root, degRoot = 0;
    ll ans = 0;

    MST(vector<Edge>& edges, int n, int k, int root = 0):
        u1(n), u2(n, edges, root), edges(edges), k(k), n(n), root(root){}

    void simpleMst(){
        sort(edges.begin(), edges.end());
        for (Edge e: edges){
            u = e.u;
            v = e.v;
            if (!u1.connected(u, v)){
                if (u == root){
                    degRoot++;
                    u2.setToRoot(v, e);
                }else
                    u2.unionSet(u, v);
                u1.unionSet(u, v);
                ans += e.w;
            }
        }
    }
    
    void insert(int v){
        Edge minEdg = u2.getMin(v);
        if(minEdg.w != INF)
            comp.insert({u2.getCost(v), v});
    }

    void changeEdge(int x){
        ans += u2.getCost(x);
        Edge minEdg = u2.getMin(x);
        u = u2.findSet(minEdg.u);
        v = u2.findSet(minEdg.v);

        comp.erase({u2.getCost(v), v});
        comp.erase({u2.getCost(u), u});

        u2.toRoot[x].w = INF;
        insert(u2.unionSet(v, u));
    }

    ll solve(){
        simpleMst();
        if(u1.sizes[u1.findSet(root)] != n)
            return -1;

        if(degRoot > k){
            vector<char> seen(n, false);
            FOR(i, n){
                v = u2.findSet(i);
                if(!seen[v] && i != root){
                    seen[v] = true;
                    insert(v);
                }
            }

            // decrease root's degree using the edge with the best cost-benefit
            while(!comp.empty() && degRoot > k){
                degRoot--;
                changeEdge(comp.begin()->second);
            }
        }
        return degRoot > k ? -1 : ans;
    }
};

int main(){
    int n, m, t, k, u, v;
    ll w;
    string s1, s2;
    cin >> t;

    while(t--){
        unordered_map<string, int> ids;
        ids["Park"] = 0;

        cin >> m;
        vector<Edge> edges(m);

        FOR(i, m){
            cin >> s1 >> s2 >> w;
            u = ids.count(s1) ? ids[s1] : (ids[s1] = ids.size());
            v = ids.count(s2) ? ids[s2] : (ids[s2] = ids.size());
            if (u > v)
                swap(u, v);
            edges[i] = Edge(u, v, w);
        }

        n = ids.size();
        cin >> k;
        MST mst(edges, n, k);
        ll ans = mst.solve();

        if(ans == -1)
            cout << "-1" << endl;
        else
            cout << "Total miles driven: " << ans << endl;

        if (t)
            cout << endl;
    }
}
