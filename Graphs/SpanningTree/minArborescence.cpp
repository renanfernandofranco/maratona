/**
 * Minimum Spanning Arborescence (Directed MST)
 * 
 * Complexity:
 *    - Time: O(|E| lg |E|)
 *    - Space: O(|V| + |E|)
 * 
 * The response edges can be obtained through the out_edges array
 * 
 * Attention: The Heap without releasing memory
 * 
 * Problem: https://codeforces.com/gym/102483/problem/F
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using ll = long long;
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T, class less = std::less<T>>
struct Heap{
   struct Node {
      T value;
      ll lazy = 0;
      Node* left = nullptr;
      Node* right = nullptr;
      Node(T value): value(value){}
   };

   Node* root = nullptr;
   less op;

   void check(){ assert(root); }
   bool empty(){ return !root; }
   
   Node* merge(Node* t1, Node* t2) {
      if (!t1 || !t2)
         return t1 ? t1 : t2;
      push(t1); push(t2);
      if (op(t2->value,  t1->value))
         swap(t1, t2);
      if (rand() & 1)
         swap(t1->left, t1->right);
      t1->left = merge(t1->left, t2);
      return t1;
   }

   void merge(Heap<T, less>& other){
      if(root == other.root) return;
      root = merge(root, other.root);
      other.root = nullptr;
   }

   T top(){
      check();
      push(root);
      return root->value;
   }

   void pop(){
      check();
      push(root);
      root = merge(root->left, root->right);
   }

   void insert(T value){
      root = merge(root, new Node(value));
   }

   void update(ll lazy){
      if(!root) return;
      root->lazy += lazy;
   }

   void push(Node* tree){
      if(!tree || !tree->lazy) return;
      if(tree->left)
         tree->left->lazy += tree->lazy;
      if(tree->right)
         tree->right->lazy += tree->lazy;
      tree->value = tree->value + tree->lazy;
      tree->lazy = 0;
   }
};

struct Edge {
   int id, from, to;
   ll cost;
   bool operator < (const Edge& o) const {
      return cost < o.cost;
   }
   Edge operator + (const ll value) const {
      return { id, from, to, cost + value };
   }
};

struct UnionFind{
   vi dad, sizes;

   UnionFind(int n): dad(n), sizes(n, 1){
      FOR(i, n) dad[i] = i;
   }

   int findSet(int v){
      return dad[v] == v ? v : (dad[v] = findSet(dad[v]));
   }

   int unionSet(int u, int v){
      u = findSet(u);
      v = findSet(v);
      if (u != v){
         if (sizes[u] < sizes[v])
            swap(u, v);
         dad[v] = u;
         sizes[u] += sizes[v];
      }
      return u;
   }
};

struct MinArborescence {
   int N;
   vector<Edge> edges;
   static const ll INF = numeric_limits<ll>::max();

   MinArborescence(int n = 0): N(n) {}

   void addEdge(int from, int to, ll cost, int id = 0) {
      check(from);
      check(to);
      assert(cost >= 0);
      edges.push_back({ id, from, to, cost });
   }

   ll solve(int root, vi& out_edges) {
      check(root);

      enum Processing { BEFORE = 0, PROCESSING = 1, DONE = 2 };
      UnionFind uf(N);
      vector<Processing> processing(N, BEFORE);
      vector<Heap<Edge>> come(N);
      vi from(N);
      vector<ll> from_cost(N);

      out_edges.clear();
      processing[root] = DONE;
      for (auto& e : edges) 
         come[e.to].insert(e);

      ll ans = 0;
      for (int i = 0; i < N; ++i) {
         if (processing[i] != BEFORE) continue;
         int c = i;
         vi nodes;

         while (processing[c] != DONE) {
            processing[c] = PROCESSING;
            nodes.push_back(c);

            if (come[c].empty()) return INF;
            auto e = come[c].top(); come[c].pop();
            auto fc = uf.findSet(e.from);
            if (fc == c) continue;

            from[c] = fc;
            from_cost[c] = e.cost;
            ans += from_cost[c];
            out_edges.push_back(e.id);

            if (processing[from[c]] != PROCESSING) {
               c = from[c];
               continue;
            }
            // When we found a cycle, compress the cycle.
            int p = c;
            do {
               come[p].update(-from_cost[p]);
               if (p != c) {
                  int n = uf.unionSet(p, c);
                  come[c].merge(come[p]);
                  come[n].merge(come[c]);
                  c = n;
               }
               p = uf.findSet(from[p]);
            } while (p != c);
         }
         for (auto v : nodes) 
            processing[v] = DONE;
      }

      return ans;
   }

   void check(int id) {
      assert(id >= 0 && id < N);
   }
};

int main(){
   cin.sync_with_stdio(0);
   cin.tie(0);
   int i, j, x, s, w, n;

   cin >> n;
   int n2 = 2 * n + 1;
   
   MinArborescence mst(n2);

   for(i = 1; i <= n; i++){
      cin >> x >> s;
      mst.addEdge(x, i + n, s);
      mst.addEdge(i + n, i, 0);

      for(j = 0; j <= n; j++){
         cin >> w;
         mst.addEdge(j, i + n, w);
      }
   }

   vi v;

   cout << mst.solve(0, v) << endl;
}
