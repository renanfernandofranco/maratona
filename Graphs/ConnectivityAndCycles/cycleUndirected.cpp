/**
 * Cycle Finder - Undirected Graph
 *
 * Complexity:
 *  - Time: O(n + m)
 *  - Space: O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1669/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using vvi = vector<vi>;
#define endl '\n'
#define all(a) a.begin(), a.end()
#define FOR(i, b) for(int i = 0; i < b; i++)

struct Graph{
  int n;
  vvi adj;
  vector<char> seen;
  vector<int> ans;

  Graph(int n): n(n), adj(n), seen(n, false){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  bool dfs(int v, int p){
    seen[v] = true;
    for(int u : adj[v]){
      if(!seen[u]){
        if(dfs(u, v)){
          ans.push_back(u);
          return true;
        }
      }else if(u != p){
        ans.push_back(u);
        return true;
      }
    }
    return false;
  }

  vi Solve(){
    ans.clear();
    FOR(u, n)
      if(!seen[u] && dfs(u, -1)){
        // the cycle begin in u
        if(count(all(ans), u) == 1)
          ans.push_back(u);
        // the cycle does not begin in u
        while(ans.back() != ans[0])
          ans.pop_back();
        return ans;
      }
    return ans;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, m;
  cin >> n >> m;

  Graph G(n);

  FOR(i, m){
    int u, v;
    cin >> u >> v; v--; u--;
    G.AddEdge(u, v);
  }

  vi ans = G.Solve();
  if(ans.empty()){
    cout << "IMPOSSIBLE" << endl;
  }else{
    cout << ans.size() << endl;
    FOR(i, ans.size())
      cout << ans[i] + 1 << " \n"[i == ans.size() - 1];
  }
}
