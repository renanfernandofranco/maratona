/**
 * Topologic sort
 *
 * Complexity:
 *  - Time : O(n + m)
 *  - Space : O(n + m)
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define all(a) a.begin(), a.end()

struct TopSort{
  int n;
  vvi adj;
  vi order;
  vector<char> seen;

  TopSort(int n): n(n), adj(n){}

  void AddEdge(int v, int u){
    adj[v].push_back(u);
  }

  void dfs (int v) {
    seen[v] = true;
    for(int u : adj[v])
      if (!seen[u])
        dfs(u);
    order.push_back(v);
  }

  bool hasCycle(){
    seen.assign(n, false);
    for(int v : order){
      seen[v] = true;
      for(int u : adj[v])
        if(!seen[u])
          return true;
    }
    return false;
  }

  bool Solve(){
    seen.assign(n, false);
    FOR(v, n)
      if (!seen[v])
        dfs (v);

    if(hasCycle())
      return false;
    reverse(all(order));
    return true;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int  n, m, u, v;

  cin >> n >> m;

  TopSort G(n);

  FOR(i, m){
    cin >> u >> v;
    u--; v--;
    G.AddEdge(u, v);
  }

  if(G.Solve()){
    auto ans = G.order;
    FOR(i, ans.size())
      cout << ans[i]  + 1 << " \n" [i  + 1UL == ans.size()];
  }else{
    cout << "IMPOSSIBLE\n";
  }

  return 0;
}
