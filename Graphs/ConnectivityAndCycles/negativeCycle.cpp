/**
 * Detect if the graph has a negative cycle using BellmanFord
 *
 * This algorithm can also recover the cycle, but I have no idea
 * if the algorithm is correct.
 *
 * Complexity:
 *  - Time: O(|V| * |E|)
 *  - Space: O(|V| + |E|)
 *
 * Problem: https://cses.fi/problemset/task/1197/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using vi = vector<int>;
using vvi = vector<vi>;
using vll = vector<ll>;

#define FOR(i, u) for(int i = 0; i < (u); i++)
#define all(a) a.begin(), a.end()

template<class T>
struct NegativeCycle{
  struct Edge{
    int v, u;
    T cost;
  };
  T INF = numeric_limits<T>::max() / 2 - 1;

  int n;
  vector<T> d;
  vector<Edge> edges;
  NegativeCycle(int n): n(n), d(n, 0){}

  void AddEdge(int v, int u, T cost){
    edges.push_back({v, u, cost});
  }

  void recover(int v, vi& p, vi& cycle){
    FOR(i, p.size())
      v = p[v];
    for (int cur = v; ; cur = p[cur]) {
      cycle.push_back (cur);
      if (cur == v && cycle.size() > 1)
        break;
    }
    reverse(all(cycle));
  }

  bool Solve(vi& cycle){
    vi p(n, -1);
    FOR(i, n)
      for(auto e : edges){
        int v = e.v, u = e.u;
        if(d[v] + e.cost < d[u]){
          p[u] = v;
          d[u] = max(d[v] + e.cost, -INF);
          if(i == n - 1){ // find cycle
            recover(u, p, cycle);
            return true;
          }
        }
      }
    return false;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, v, u, w;
  cin >> n >> m;

  NegativeCycle<ll> G(n);
  FOR(i, m){
    cin >> v >> u >> w;
    v--; u--;
    G.AddEdge(v, u, w);
  }

  vi ans;
  if(G.Solve(ans)){
    cout << "YES\n";
    FOR(i, ans.size())
      cout << ans[i]  + 1 << " \n"[i == ans.size() - 1];
  }else{
    cout << "NO\n";
  }
}
