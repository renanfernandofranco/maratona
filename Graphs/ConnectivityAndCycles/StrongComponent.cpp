/**
 * Partitions the vertices into Strong Components and
 * build the graph with the compressed components
 *
 * Variables:
 *  - comps : stores the vertices of each strong component
 *  - comp : stores the component index of each vertex
 *
 * Complexity:
 *  - Time : O(n)
 *  - Space : O(n)
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()

struct StrongComp{
  int n, nComps;
  vvi g, gr, comps;
  vector<bool> used;
  vi order, comp;

  StrongComp(int n): n(n), nComps(0), g(n), gr(n), comp(n){}

  void AddEdge(int v, int u){
    g[v].push_back(u);
    gr[u].push_back(v);
  }

  void dfs1 (int v) {
    used[v] = true;
    for(int u : g[v])
      if (!used[u])
        dfs1 (u);
    order.push_back (v);
  }

  void dfs2 (int v) {
    used[v] = true;
    comps[nComps].push_back(v);
    comp[v] = nComps;
    for(int u : gr[v])
      if (!used[u])
        dfs2 (u);
  }

  void Solve(){
    used.assign (n, false);
    FOR(v, n)
      if (!used[v])
        dfs1 (v);

    reverse(all(order));
    used.assign (n, false);

    for(int v : order){
      if (!used[v]) {
        comps.push_back(vi());
        dfs2 (v);
        nComps++;
      }
    }
  }

  vvi scGraph(vvi& G){
    vvi adj(nComps, vi());
    FOR(u, n)
      for(int v : G[u])
        if(comp[u] != comp[v])
          adj[comp[u]].push_back(comp[v]);

    for(auto& a : adj){
      sort(all(a));
      a.resize(unique(all(a)) - a.begin());
    }
    return adj;
  }
};
