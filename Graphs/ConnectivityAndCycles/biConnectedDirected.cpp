#include <bits/stdc++.h>

using namespace std;
using ii = pair<int, int>;

vector<vector<int>> adj;
vector<int> parent, color;
vector<ii> cycle;
int cycle_start, cycle_end;

bool dfs(int v){
  color[v] = 1;
  for(int w : adj[v]){
    if(color[w] == 1){
      cycle_start = w;
      cycle_end = v;
      return true;
    }
    if(color[w] == 0){
      parent[w] = v;
      if(dfs(w))
        return true;
    }
  }
  color[v] = 2;
  return false;
}

bool findCycle(int n, bool buildCycle){
  color.assign(n, 0);
  for(int i = 0; i < n; i++){
    if(color[i] == 0){
      parent[i] = i;
      if(dfs(i))
        goto found;
    }
  }
  return false;

  found:
  if(buildCycle){
    for(int u = cycle_start, v = cycle_end ; u != v; v = parent[v])
      cycle.push_back({parent[v], v});
    cycle.push_back({cycle_end, cycle_start});
  }
  return true;
}

void removeEdge(int v, int u){
  swap(*find(adj[v].begin(), adj[v].end(), u), adj[v].back());
  adj[v].pop_back();
}

bool hasCompBiConnected(int n){
  if(!findCycle(n, true))
    return false;

  for(ii p : cycle){
    removeEdge(p.first, p.second);
    if(!findCycle(n, false))
      return false;
    adj[p.first].push_back(p.second);
  }
  return true;
}

int main(){
  cin.sync_with_stdio(0);
  cin.tie(0);
  int i, j, n, m, u, v;

  cin >> n >> m;

  adj.resize(n);
  parent.resize(n);

  for(i = 0; i < m; i++){
    cin >> u >> v;
    adj[--u].push_back(--v);
  }

  if(hasCompBiConnected(n))
    cout << "NO" << endl;
  else
    cout << "YES" << endl;

  return 0;
}
