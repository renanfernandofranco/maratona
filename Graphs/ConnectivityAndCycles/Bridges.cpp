/**
 * Finding bridges - Undirected Graph
 *
 * Complexity:
 *  - Time : O(n + m)
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/2076
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
#define endl '\n'

struct Edge{
  int u, to;
  Edge(int u, int to): u(u), to(to) {}
};

struct Graph{
  int n;
  vvi adj;
  vi tin, low, marked;
  Graph(int n): n(n), adj(n), tin(n, -1), low(n, -1), marked(n, false) {}

  void addEdge(int u, int v){
    adj[v].push_back(u);
    adj[u].push_back(v);
  }

  void dfs(int u, int p, int &timer, vector<Edge> &bridges) {
    marked[u] = true;
    tin[u] = low[u] = timer++;

    for(int v : adj[u]) {
      if(v == p)
        continue;

      if(marked[v])
        low[u] = min(low[u], tin[v]);
      else {
        dfs(v, u, timer, bridges);
        low[u] = min(low[u], low[v]);
        if(low[v] > tin[u])
          bridges.push_back({u+1,v+1});
      }
    }
  }

  vector<Edge> find_bridges() {
    int timer = 0;
    vector<Edge> bridges;

    for(int i = 0; i < n; i++)
      if(!marked[i])
        dfs(i, -1, timer, bridges);

    return bridges;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  cin >> n >> m;

  Graph G(n);

  for(int i = 0; i < m; i++){
    int u, v;
    cin >> u >> v;
    G.addEdge(u-1,v-1);
  }

  vector<Edge> ans = G.find_bridges();
  int tam = ans.size();

  cout << tam << endl;
  for(int i = 0; i < tam; i++)
    cout << ans[i].u << ' ' << ans[i].to << endl;
}
