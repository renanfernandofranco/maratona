/**
 * Find the edges that make a graph strongly connected, using a
 * minimum amount
 *
 * Compress the strong components, building a DAG. Then connect
 * the weak components, connecting a sink of one component to a
 * source of another. Next, find a greedy correspondence between
 * sources and sinks (s[i], f[i]), so that s[i] reaches f[i].
 * Add edges f[i] -> s[i + 1], building a big cycle. Then connect
 * the remaining sources and sinks in any way.
 *
 * Complexity:
 *  - Time : O(n + m), excluding the useless ordernations
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1685/
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using ii = pair<int, int>;
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define REV(i, b) for(int i = (int)(b) - 1; i >= 0; i--)
#define all(a) a.begin(), a.end()

struct MakeStrongly{
  int n, nComps;
  vvi g, gr, comps, scG, scGr;
  vector<bool> used;
  vi order, comp, invC;

  MakeStrongly(int n): n(n), g(n), gr(n), comp(n){}

  void AddEdge(int v, int u){
    g[v].push_back(u);
    gr[u].push_back(v);
  }

  void dfs1 (int v) {
    used[v] = true;
    for(int u : g[v])
      if (!used[u])
        dfs1 (u);
    order.push_back (v);
  }

  void dfs2 (int v) {
    used[v] = true;
    comps[nComps].push_back(v);
    comp[v] = nComps;
    for(int u : gr[v])
      if (!used[u])
        dfs2 (u);
  }

  vvi scGraph(vvi& G){
    vvi adj(nComps, vi());
    FOR(u, n)
      for(int v : G[u])
        if(comp[u] != comp[v])
          adj[comp[u]].push_back(comp[v]);

    for(auto& a : adj){
      sort(all(a));
      a.resize(unique(all(a)) - a.begin());
    }
    return adj;
  }

  void buildStrong(){
    nComps = 0;
    order.clear();
    comps.clear();
    used.assign (n, false);
    FOR(v, n)
      if (!used[v])
        dfs1 (v);

    reverse(all(order));
    used.assign (n, false);

    for(int v : order){
      if (!used[v]) {
        comps.push_back(vi());
        dfs2 (v);
        nComps++;
      }
    }
    scG = scGraph(g);
    scGr = scGraph(gr);
    invC.resize(nComps);
    FOR(i, n)
      invC[comp[i]] = i;
  }

  int matching(int v, vvi& adj){
    used[v] = true;
    if(adj[v].empty())
      return v;
    for(int u : adj[v])
      if(!used[u]){
        int ret = matching(u, adj);
        if(ret != -1)
          return ret;
      }
    return -1;
  }

  vvi weaklyComps(vvi adj){
    MakeStrongly weak(adj.size());
    FOR(u, adj.size())
      for(int v : adj[u]){
        weak.AddEdge(u, v);
        weak.AddEdge(v, u);
      }
    weak.buildStrong();
    return weak.comps;
  }

  vector<ii> connectWeakly(){
    vvi weakComps = weaklyComps(scG);

    vector<ii> match;
    used.assign(nComps, false);
    for(vi& wc : weakComps)
      for(int v : wc)
        if(scGr[v].empty()){
          match.push_back({v, matching(v, scG)});
          break;
        }

    vector<ii> edges;
    FOR(i, weakComps.size() - 1)
      edges.push_back({invC[match[i].second], invC[match[i + 1].first]});
    return edges;
  }

  vector<ii> Solve(){
    buildStrong();
    if(nComps == 1)
      return {};

    vector<ii> edges = connectWeakly();
    for(auto pr : edges)
      AddEdge(pr.first, pr.second);
    buildStrong();

    vector<ii> match;
    vi H, T;
    used.assign(nComps, false);
    FOR(v, nComps)
      if(scGr[v].empty()){ // source
        int snk = matching(v, scG);
        if(snk != -1)
          match.push_back({v, snk});
        else
          H.push_back(v);
      }
    FOR(v, nComps)
      if(!used[v] && scG[v].empty()) // sink
        T.push_back(v);

    FOR(i, match.size()) // Build a big cycle with sources and sinks from matching
      edges.push_back({invC[match[i].second], invC[match[(i + 1) % match.size()].first]});

    if(!H.empty() || !T.empty()){ // Connect the remaining sources and sinks
      if(H.empty())
        H.push_back(match[0].first);
      if(T.empty())
        T.push_back(match[0].second);
      int sh = H.size(), st = T.size();
      FOR(i, max(sh, st))
        edges.push_back({invC[T[i % st]], invC[H[i % sh]]});
    }

    sort(all(edges));
    return edges;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  cin >> n >> m;

  MakeStrongly G(n);
  FOR(i, m){
    int u, v;
    cin >> u >> v;
    G.AddEdge(u - 1, v - 1);
  }

  auto edges = G.Solve();
  cout << edges.size() << endl;
  for(ii pr : edges){
    cout << pr.first + 1 << ' ' << pr.second + 1 << endl;
  }
}
