/**
 * Transitive Closure of a directed graph
 *
 * Can answer in Reachability Queries
 *
 * Check if in problem definition, vertex u can reach u with distance 0
 *
 * Time Complexity:
 *  - Build Time: O(|V| * |E| / 32)
 *  - Query Time: O(1)
 *  - Space: O(|V| ^ 2 / 32)
 *
 */

#include <bits/stdc++.h>

#define MAX 50'001
using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using bs = bitset<MAX>;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)
#define all(a) a.begin(), a.end()

struct TransClosure {
  int n, nComps;
  vvi g, gr, comps;
  vector<bool> used;
  vi order, comp;

  TransClosure(int n): n(n), nComps(0), g(n), gr(n), comp(n){}

  void AddEdge(int v, int u){
    g[v].push_back(u);
    gr[u].push_back(v);
  }

  void dfs1 (int v) {
    used[v] = true;
    for(int u : g[v])
      if (!used[u])
        dfs1 (u);
    order.push_back (v);
  }

  void dfs2 (int v) {
    used[v] = true;
    comps[nComps].push_back(v);
    comp[v] = nComps;
    for(int u : gr[v])
      if (!used[u])
        dfs2 (u);
  }

  void strongComponent(){
    used.assign (n, false);
    FOR(i, n)
      if (!used[i])
        dfs1 (i);

    reverse(all(order));
    used.assign (n, false);

    for(int v : order){
      if (!used[v]) {
        comps.push_back(vi());
        dfs2 (v);
        nComps++;
      }
    }
  }

  vector<bs> Solve(){
    strongComponent();
    vector<bs> tc(n, bs(0));
    vector<char> usedComp(nComps, false);

    REV(u, nComps){
      vi& C = comps[u];
      int p = C[0];
      vi to;

      for(int v : C){
        tc[p][v] = true;
        for(int u : g[v])
          to.push_back(u);
      }

      for(int u : to)
        if(!tc[p][u]){
          tc[p] |= tc[u];
          tc[p][u] = true;
        }

      for(int v : C)
        tc[v] = tc[p];

      // Enable if vertex u cannot reach u with distance 0
      // if(C.size() == 1 && !count(all(g[p]), p))
      //   tc[p][p] = false;
    }

    return tc;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, q;

  cin >> n >> m >> q;
  TransClosure ts(n);

  FOR(i, m){
    int u, v;
    cin >> u >> v;
    ts.AddEdge(u - 1, v - 1);
  }

  auto ret = ts.Solve();

  FOR(i, q){
    int u, v;
    cin >> u >> v; u--; v--;
    cout << (ret[u][v] ? "YES" : "NO") << '\n';
  }
}
