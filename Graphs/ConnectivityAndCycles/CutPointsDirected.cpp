/**
 * Articulation points between s and t - Directed Graph
 *
 * Find the cut vertices between the source and the sink
 *
 * Complexity:
 *  - Time : O((n + m) lg n)
 *  - Space : O((n + m) lg n)
 *
 * Problem: https://cses.fi/problemset/task/1703/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ii = pair<int, int>;
using vi = vector<int>;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()

template<class T, class U>
struct MCMF{
  const T INFT = numeric_limits<T>::max();
  const U INFU = numeric_limits<U>::max();

  struct FlowEdge {
    int from, to;
    T cap, flow;
    U cost;
    FlowEdge(int from, int to, T cap, U cost): from(from),
      to(to), cap(cap), flow(0), cost(cost){}
  };

  int n, s, t, m;
  vector<vi> adj;
  vi p;
  vector<U> d;
  vector<FlowEdge> edges;
  T push;
  ll flow;
  U cost;

  MCMF(int n) : n(n), m(0), adj(n), p(n), d(n){}

  void AddEdge(int from, int to, T dirCap, U cost, bool isDir = true){
    edges.emplace_back(FlowEdge(from, to, dirCap, cost));
    edges.emplace_back(FlowEdge(to, from, isDir ? 0 : dirCap, -cost));
    adj[from].push_back(m);
    adj[to].push_back(m + 1);
    m += 2;
  }

  void Dijkstra() {
    fill(all(d), INFU);
    fill(all(p), -1);
    d[s] = 0;
    priority_queue<ii, vector<ii>, greater<ii>> q;
    q.push({0, s});

    while (!q.empty()) {
      int u = q.top().second, dist = q.top().first; q.pop();

      if(d[u] != dist)
        continue;

      for (int id : adj[u]) {
        auto e = edges[id];
        auto v = e.to;
        if (e.cap - e.flow > 0 && d[v] > d[u] + e.cost) {
          d[v] = d[u] + e.cost;
          p[v] = id;
          q.push({d[v], v});
        }
      }
    }
  }

  void augument(int cur){
    if(cur != s){
      int id = p[cur];
      push = min(edges[id].cap - edges[id].flow, push);
      augument(edges[id].from);
      edges[id].flow += push;
      edges[id ^ 1].flow -= push;
    }
  }

  pair<ll, U> Solve(int ss, int tt){
    s = ss; t = tt;
    cost = flow = 0;
    while(Dijkstra(), d[t] != INFU){
      push = INFT;
      augument(t);
      flow += push;
      cost += d[t] * push;
    }
    return {flow, cost};
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m, v, u;
  cin >> n >> m;

  int s = 0, t = 2 * n - 1;
  MCMF<int, int> G(n + n);
  FOR(i, m){
    cin >> v >> u;
    v--; u--;
    G.AddEdge(v + n, u, 2, 0);
  }

  FOR(i, n){
    G.AddEdge(i, i + n, 1, 0);
    G.AddEdge(i, i + n, 1, 1); // penalizes the use of the same vertex twice
  }

  auto pr = G.Solve(0, 2 * n - 1);
  assert(pr.first == 2); // flow is always equal to 2

  vi art;
  FOR(i, 2){
    int v = s;
    while(v != t){ // dfs
      for(int id : G.adj[v]){
        if(G.edges[id].flow > 0){
          G.edges[id].flow--;
          if(G.edges[id].cost == 1)
            art.push_back(v);
          v = G.edges[id].to;
          break;
        }
      }
    }
  }

  int na = art.size();
  sort(all(art));
  cout << na << endl;
  FOR(i, na)
    cout << art[i] + 1 << " \n"[i + 1 == na];
}
