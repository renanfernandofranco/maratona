/**
 * Functional Graph
 * ================
 *  Gathers information about cycles and subtrees on a directed functional graph.
 *
 *  Strategy: Whiles (for directed graphs only)
 *
 *  Construction:
 *    - Time: O(n)
 *    - Memory: O(n)
 *
 *  Usage:
 *    Initialize struct with graph parents nodes vector.
 *    All information gathered will be available on the struct public fields.
 *
 *  Notes:
 *    Supports adaptation for using LCA on each subtree.
 */

#include <bits/stdc++.h>
using namespace std;
using vi = vector<int>;

struct fnGraph {
  vi &fun;
  int n, cycles;
  enum Status { UNSEEN = 0, PROCESSING = 1, CYCLE = 2, TREE = 3 };
  vi mark, cSize, cDescent, cycle, depth, idInCycle;

  fnGraph(vi &fun):
    fun(fun), n(fun.size()),
    cycles(0),        // number of cycles in total
    mark(n, UNSEEN),  // processing status of a vertex
    cSize(n, 0),      // size of the i-th cycle (cycle size)
    cDescent(n),      // each vertex's first descent that is part of a cycle
    cycle(n),         // which cycle a vertex belongs to
    depth(n),         // depth of a vertex on its tree (0 if in a cycle)
    idInCycle(n){     // new id for a vertex on their cycle (i.e. for shortest path)

    for (int i = 0; i < n; i++)
      if (mark[i] == UNSEEN)
        process(i);
  }

  void process(int v) {
    int s = v;      // store starting vertex for later processing

    while (mark[v] == UNSEEN) {
      mark[v] = PROCESSING;    // during process phase
      v = fun[v];
    }

    if (mark[v] == PROCESSING)
      processCycle(v); // a new cycle has been found

    // check whether starting vertex is not part of a cycle
    if (mark[s] == PROCESSING)
      processTree(s);
  }

  void processCycle(int v) {
    int cId = cycles++; // create new cycle id
    int vId = 0;        // vertex id (in the cycle)

    while (mark[v] != CYCLE) {
      mark[v] = CYCLE;
      /**
       * Here is the place to gather all information needed about each cycle,
       *      and about each subtree root vertices.
       */
      cSize[cId]++;
      cDescent[v] = v;
      cycle[v] = cId;
      idInCycle[v] = vId;
      depth[v] = 0;

      v = fun[v];
      vId++;
    }
  }

  void processTree(int v) {
    stack<int> s;
    while (mark[v] == PROCESSING) {
      mark[v] = TREE;
      s.push(v);      // store in reverse order to build parents' info first

      v = fun[v];
    }

    while (!s.empty()) {
      int u = s.top(); s.pop();
      int du = fun[u];
      /**
       * Here is the place to compute information about each subtree, based
       *      on the available information about the direct fun.
       * Note the root vertex of all subtrees belongs to a cycle. Thus, initial
       *      values must be set in `processCycle` method.
       */

      depth[u] = depth[du] + 1;
      cycle[u] = cycle[du];
      cDescent[u] = cDescent[du];
    }
  }
};

int main() {
  vector<int> fun = {2, 1, 4, 5, 3, 5, 6};

  fnGraph G(fun);

  cout << G.cycles << endl;

  return 0;
}
