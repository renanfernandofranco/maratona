/**
 * Strong Orientation
 *
 * Given an undirected graph, find an edge orientation that
 * minimizes the number of Strongly Connected Components
 *
 * Complexity:
 *  - Time : O(n + m)
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/2177
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using ii = pair<int, int>;
using vii = vector<ii>;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'

struct Graph{
  int n, m, nComp, nBrid, timer;
  vector<vii> adj; // (to, id)
  vi tin, low, orient;
  vii edges;
  Graph(int n): n(n), m(0), nComp(0), nBrid(0),
   timer(0), adj(n), tin(n, -1), low(n, -1){}

  void AddEdge(int u, int v){
    adj[v].push_back({u, m});
    adj[u].push_back({v, m});
    edges.push_back({u, v});
    m++;
  }

  void dfs(int u) {
    low[u] = tin[u] = timer++;

    for(auto p : adj[u]) {
      int v = p.first, id = p.second;
      if(orient[id] != -1)
        continue;

      orient[id] = u == edges[id].first;
      if(tin[v] == -1) {
        dfs(v);
        low[u] = min(low[u], low[v]);
        if(low[v] > tin[u])
          nBrid++;
      }else
        low[u] = min(low[u], low[v]);
    }
  }

  vii Solve(int& strongComps) {
    orient.assign(m, -1);

    FOR(v, n)
      if(tin[v] == -1)
        dfs(v), nComp++;

    vii ret;
    strongComps = nComp + nBrid;
    FOR(id, m)
      ret.push_back(orient[id] ? edges[id] : ii(edges[id].second, edges[id].first));
    return ret;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  cin >> n >> m;

  Graph G(n);

  FOR(i, m){
    int u, v;
    cin >> u >> v; u--; v--;
    G.AddEdge(u, v);
  }

  int nComp;
  vector<ii> edges = G.Solve(nComp);

  if(nComp != 1){
    cout << "IMPOSSIBLE\n";
    return 0;
  }

  for(ii e : edges)
    cout << e.first + 1 << ' ' << e.second + 1 << endl;
}
