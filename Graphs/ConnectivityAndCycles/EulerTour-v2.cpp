/**
 * Euler Tour / Euler Path - Directed Graph
 *
 * Complexity:
 *  - Time : O(n + m)
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1693/
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define all(a) a.begin(), a.end()
#define SZ(v) ((int)v.size())

struct Euler{
  int n;
  vvi g;
  vi dIn;

  Euler(int n): n(n), g(n), dIn(n, 0){}

  void AddEdge(int v, int u){
    g[v].push_back(u);
    dIn[u]++;
  }

  bool Solve(vi& res, bool path){
    vi odd;
    FOR(u, n){
      int diff = abs(SZ(g[u]) - dIn[u]);
      if(diff > 1)
        return false;
      if(diff)
        odd.push_back(u);
    }
    if(!(SZ(odd) <= 2 * path))
      return false;

    int v1 = 0;
    if(odd.empty()){
      FOR(u, n)
        if(!g[u].empty() > 0)
          v1 = u;
    }else{
      if(SZ(g[odd[0]]) > dIn[odd[0]])
        swap(odd[0], odd[1]);
      v1 = odd[0];
      AddEdge(v1, odd[1]);
    }

    stack<int> st;
    st.push(v1);
    while (!st.empty()) {
      int v = st.top();
      if(g[v].empty()){
        res.push_back(v);
        st.pop();
      }else{
        st.push(g[v].back());
        g[v].pop_back();
      }
    }
    FOR(u, n)
      if(!g[u].empty())
        return false;

    reverse(all(res));
    if(!odd.empty())
      res.erase(res.begin());
    return true;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);
  int n, m, u, v;

  cin >> n >> m;

  Euler G(n);

  FOR(i, m){
    cin >> u >> v;
    u--; v--;
    G.AddEdge(u, v);
  }

  vi ans;
  if(!G.g[0].empty() && G.Solve(ans, true) && ans.front() == 0 && ans.back() == n - 1){
    FOR(i, ans.size())
      cout << ans[i] + 1 << " \n"[i + 1UL == ans.size()];
  }else{
    cout << "IMPOSSIBLE\n";
  }
}
