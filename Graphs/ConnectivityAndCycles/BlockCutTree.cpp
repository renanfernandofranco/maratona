/**
 * Block Cut Tree
 *
 * Given x, y and m, check if m separate x and y
 *
 * Complexity:
 *  - Build Time: O(n * lg n + m)
 *  - Query Time: O(lg n)
 *  - Space: O(n * lg n + m)
 *
 * Problem: https://cses.fi/problemset/task/1705/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ii = pair<int, int>;
using vi = vector<int>;
using vvi = vector<vi>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define REV(i, b) for(int i = b - 1; i >= 0; i--)
#define FORI(i, b)  for(int i = 1; i <= (int)(b); i++)
#define all(a) a.begin(), a.end()

struct Tree{
  int n, timer, maxLg;
  vvi adj, up;
  vi tin, tout;

  Tree(int n): n(n), timer(0), maxLg(ceil(log2(n))),
   adj(n), up(maxLg + 1, vi(n, 0)), tin(n), tout(n){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void dfs(int v, int p){
    tin[v] = timer++;
    for (int u : adj[v])
      if (u != p)
        dfs(u, up[0][u] = v);
    tout[v] = timer++;
  }

  bool isAncestor(int u, int v) {
    return tin[u] <= tin[v] && tout[u] >= tout[v];
  }

  void Build(int root = 0) {
    up[0][root] = root;
    dfs(root, root);

    FORI(i, maxLg)
      FOR(v, n)
        up[i][v] = up[i - 1][up[i - 1][v]];
  }

  int lca(int u, int v){
    if (isAncestor(u, v))
      return u;
    if (isAncestor(v, u))
      return v;
    REV(i, maxLg + 1)
      if (!isAncestor(up[i][u], v))
        u = up[i][u];
    return up[0][u];
  }

  // check if m is between x and y
  bool IsBetween(int x, int m, int y){
    int v[] = {lca(x, m), lca(y, m), lca(x, y)};
    sort(v, v + 3);
    return v[v[0] == v[1] ? 2 : 0] == m;
  }
};

struct BlockCutTree{
  int n, timer;
  vvi adj, blocks;
  vi num, low, art, stk, id;
  Tree tree;

  BlockCutTree(int n) : n(n), timer(0), adj(n),
    num(n), low(n), art(n), id(n), tree(2){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void dfs(int u, int p){
    num[u] = low[u] = ++timer;
    stk.push_back(u);

    for (int v : adj[u]){
      if(v == p)
        continue;
      if (!num[v]){
        dfs(v, u);
        low[u] = min(low[u], low[v]);

        if (low[v] >= num[u]){
          art[u] = (num[u] > 1 || num[v] > 2);

          blocks.push_back({u});
          while (blocks.back().back() != v)
            blocks.back().push_back(stk.back()), stk.pop_back();
        }
      }
      else
        low[u] = min(low[u], num[v]);
    }
  }

  void Build(){
    FOR(u, n)
      if (!num[u])
        dfs(u, -1);

    int last = 0, nTree = blocks.size() + accumulate(all(art), 0);
    tree = Tree(nTree);

    FOR(u, n)
      if (art[u])
        id[u] = last++;

    int numEdges = 0;
    for (auto &block : blocks){
      int node = last++;
      for (int u : block)
        if (!art[u])
          id[u] = node;
        else
          tree.AddEdge(node, id[u]), numEdges++;
    }
    assert(numEdges == last - 1);
    tree.Build();
  }

  // there is a path between x and y when m is removed?
  bool HasPath(int u, int m, int v){
    if(u == m || v == m)
      return false;
    if(!art[m])
      return true;
    return !tree.IsBetween(id[u], id[m], id[v]);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, m, q;
  cin >> n >> m >> q;

  BlockCutTree G(n);

  FOR(i, m){
    int u, v;
    cin >> u >> v; u--; v--;
    G.AddEdge(u, v);
  }

  G.Build();

  FOR(i, q){
    int a, b, c;
    cin >> a >> b >> c;
    a--; b--; c--;
    cout << (G.HasPath(a, c, b) ? "YES" : "NO") << endl;
  }
}
