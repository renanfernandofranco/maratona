/**
 * Functional Graph - Two Dimensional
 * ==================================
 *  Gathers information about cycles and subtrees on a directed functional graph.
 *
 *  Strategy: Whiles (for directed graphs only)
 *
 *  Construction:
 *    - Time: O(n)
 *    - Memory: O(n)
 *
 *  Usage:
 *    Initialize struct with graph parents nodes vector.
 *    All information gathered will be available on the struct public fields.
 *
 *  Notes:
 *    Supports adaptation for using LCA on each subtree.
 *
 *  Problem: https://www.spoj.com/problems/HERDING
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
using vi = vector<int>;
using ii = pair<int, int>;

template<class T>
struct arr2D{
   int x, y;
   vector<vector<T>> v;

   arr2D(int x, int y, T val = T()): x(x), y(y), v(x, vector<T>(y, val)){}
   T& operator [] (ii p){ return v[p.first][p.second];}
   void set(T val){ FOR(i, x) FOR(j, y) v[i][j] = val; }
};

template<class T>
struct fnGraph {
  enum Status { UNSEEN = 0, PROCESSING = 1, CYCLE = 2, TREE = 3 };
  arr2D<T> &fun;
  int n, m, cycles;
  vi cSize;
  arr2D<T> cDescent;
  arr2D<int> mark, cycle, depth, idInCycle;

  fnGraph(arr2D<T> &fun, int n, int m):
    fun(fun), n(n), m(m),
    cycles(0),            // number of cycles in total
    cSize(n * m),         // size of the i-th cycle (cycle size)
    cDescent(n, m),       // each vertex's first descent that is part of a cycle
    mark(n, m, UNSEEN),   // processing status of a vertex
    cycle(n, m),          // which cycle a vertex belongs to
    depth(n, m),          // depth of a vertex on its tree (0 if in a cycle)
    idInCycle(n, m){      // new id for a vertex on their cycle (i.e. for shortest path)

    FOR(i, n)
      FOR(j, m)
      if (mark[{i, j}] == UNSEEN)
        process({i, j});
  }

  void process(T v) {
    T s = v;      // store starting vertex for later processing

    while (mark[v] == UNSEEN) {
      mark[v] = PROCESSING;    // during process phase
      v = fun[v];
    }

    if (mark[v] == PROCESSING)
      processCycle(v); // a new cycle has been found

    // check whether starting vertex is not part of a cycle
    if (mark[s] == PROCESSING)
      processTree(s);
  }

  void processCycle(T v) {
    int cId = cycles++; // create new cycle id
    int vId = 0;        // vertex id (in the cycle)

    while (mark[v] != CYCLE) {
      mark[v] = CYCLE;
      /**
       * Here is the place to gather all information needed about each cycle,
       *      and about each subtree root vertices.
       */
      cSize[cId]++;
      cDescent[v] = v;
      cycle[v] = cId;
      idInCycle[v] = vId;
      depth[v] = 0;

      v = fun[v];
      vId++;
    }
  }

  void processTree(T v) {
    stack<T> s;
    while (mark[v] == PROCESSING) {
      mark[v] = TREE;
      s.push(v);      // store in reverse order to build parents' info first

      v = fun[v];
    }

    while (!s.empty()) {
      T u = s.top(); s.pop();
      T du = fun[u];
      /**
       * Here is the place to compute information about each subtree, based
       *      on the available information about the direct fun.
       * Note the root vertex of all subtrees belongs to a cycle. Thus, initial
       *      values must be set in `processCycle` method.
       */

      depth[u] = depth[du] + 1;
      cycle[u] = cycle[du];
      cDescent[u] = cDescent[du];
    }
  }
};

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n, m;
  cin >> n >> m;

  vector<string> mat(n);
  FOR(i, n)
    cin >> mat[i];


  arr2D<ii> fun(n, m);
  FOR(i, n)
    FOR(j, m){
      int x = i, y = j;
      if(mat[x][y] == 'N')
        x--;
      else if(mat[x][y] == 'S')
        x++;
      else if(mat[x][y] == 'E')
        y++;
      else
        y--;
      fun[{i, j}] = {x,y};
    }

  fnGraph<ii> G(fun, n, m);

  cout << G.cycles << endl;

  return 0;
}
