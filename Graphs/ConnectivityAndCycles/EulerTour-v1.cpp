/**
 * Euler Tour / Euler Path - Undirected Graph
 *
 * Complexity:
 *  - Time : O(n + m)
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1691
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define all(a) a.begin(), a.end()
#define SZ(v) ((int)v.size())

struct Euler{
  int n, m;
  vvi g;
  vi edges;

  Euler(int n): n(n), m(0), g(n){}

  void AddEdge(int v, int u){
    edges.push_back(u);
    edges.push_back(v);
    g[v].push_back(m);
    g[u].push_back(m + 1);
    m += 2;
  }

  bool Solve(vi& res, bool path){
    vi odd;
    vector<char> used(m >> 1, false);
    FOR(u, n)
      if(SZ(g[u]) & 1)
        odd.push_back(u);
    if(!(SZ(odd) <= 2 * path))
      return false;

    int v1 = 0;
    if(odd.empty()){
      FOR(u, n)
        if(!g[u].empty() > 0)
          v1 = u;
    }else{
      v1 = odd[0];
      AddEdge(v1, odd[1]);
      used.resize(m >> 1, false);
    }

    stack<int> st;
    st.push(v1);
    while (!st.empty()) {
      int v = st.top();
      while(!g[v].empty() && used[g[v].back() >> 1])
        g[v].pop_back();

      if(g[v].empty()){
        res.push_back(v);
        st.pop();
      }else{
        int id = g[v].back();
        used[id >> 1] =  true;
        st.push(edges[id]);
      }
    }
    FOR(u, n)
      for(int id : g[u])
        if(!used[id >> 1])
          return false;

    if(!odd.empty())
      res.erase(res.begin());
    return true;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);
  int n, m, u, v;

  cin >> n >> m;

  Euler G(n);

  FOR(i, m){
    cin >> u >> v;
    u--; v--;
    G.AddEdge(u, v);
  }

  vi ans;
  if(!G.g[0].empty() && G.Solve(ans, false)){
    ans.pop_back();
    ans.insert(ans.end(), ans.begin(), next(find(all(ans), 0)));
    ans.erase(ans.begin(), find(all(ans), 0));
    FOR(i, ans.size())
      cout << ans[i] + 1 << " \n"[i + 1UL == ans.size()];
  }else{
    cout << "IMPOSSIBLE\n";
  }
}
