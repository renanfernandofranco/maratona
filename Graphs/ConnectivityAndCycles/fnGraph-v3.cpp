/**
 * Functional Graph (FnGraph)
 *
 * Given a vertex v and an integer k,
 * find the kth vertex starting v
 *
 * Complexity:
 *  - Time: O((n + Q)*lg n + m)
 *  - Space: O(n lg n)
 *
 * Problem : https://cses.fi/problemset/task/1160/
 *
 */

#include <bits/stdc++.h>
using namespace std;

using vi = vector<int>;
using vvi = vector<vi>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)
#define FORI(i, b) for(int i = 1; i <= b; i++)

struct fnGraph {
  vi &fun;
  int n, cycles;
  enum Status { UNSEEN = 0, PROCESSING = 1, CYCLE = 2, TREE = 3 };
  vi mark, cSize, cycle;
  vvi cycleComps, treeComps;

  fnGraph(vi &fun):
    fun(fun), n(fun.size()),
    cycles(0),        // number of cycles in total
    mark(n, UNSEEN),  // processing status of a vertex
    cSize(n, 0),      // size of the i-th cycle (cycle size)
    cycle(n){         // which cycle a vertex belongs to

    FOR(i, n)
      if (mark[i] == UNSEEN)
        process(i);

    treeComps.resize(cycles);
    cycleComps.resize(cycles);
    FOR(v, n){
      if(mark[v] == CYCLE)
        cycleComps[cycle[v]].push_back(v);
      else
        treeComps[cycle[v]].push_back(v);
    }
  }

  void process(int v) {
    int s = v;      // store starting vertex for later processing

    while (mark[v] == UNSEEN) {
      mark[v] = PROCESSING;    // during process phase
      v = fun[v];
    }

    if (mark[v] == PROCESSING)
      processCycle(v); // a new cycle has been found

    // check whether starting vertex is not part of a cycle
    if (mark[s] == PROCESSING)
      processTree(s);
  }

  void processCycle(int v) {
    int cId = cycles++; // create new cycle id
    while (mark[v] != CYCLE) {
      mark[v] = CYCLE;
      cSize[cId]++;
      cycle[v] = cId;
      v = fun[v];
    }
  }

  void processTree(int v) {
    stack<int> s;
    while (mark[v] == PROCESSING) {
      mark[v] = TREE;
      s.push(v);      // store in reverse order to build parents' info first
      v = fun[v];
    }

    while (!s.empty()) {
      int u = s.top(); s.pop();
      int du = fun[u];
      cycle[u] = cycle[du];
    }
  }
};

struct Forest{
  int n, maxLg;
  vvi adj, up;
  vi depth;

  Forest(int n): n(n), adj(n), depth(n, -1){
    maxLg = ceil(log2(n));
    up.assign(maxLg + 1, vi(n, 0));
  }

  void addEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  void dfs(int v, int p){
    for (int u : adj[v])
      if (u != p){
        up[0][u] = v;
        depth[u] = depth[v] + 1;
        dfs(u, v);
      }
  }

  void Build(vi& roots) {
    for(int root : roots){
      depth[root] = 0;
      up[0][root] = root;
      dfs(root, root);
    }

    FORI(i, maxLg)
      FOR(v, n)
        up[i][v] = up[i - 1][up[i - 1][v]];
  }

  int Query(int u, int& k){
    for(int i = maxLg; i >= 0; i--){
      int d = 1 << i;
      if(d <= k && d <= depth[u]){
        k -= d;
        u = up[i][u];
      }
    }
    return u;
  }
};

struct Solver{
  int n;
  vi fun;
  fnGraph G;
  Forest t;
  Solver (vi fun) : n(fun.size()), G(fun), t(1){
    t = Forest(2 * n);
    vi roots;
    FOR(c, G.cycles){
      vi& trees = G.treeComps[c], &cycle = G.cycleComps[c];
      for(int v : trees)
        t.addEdge(v, fun[v]);

      int u = cycle.back();
      roots.push_back(u + n);
      for(int v : cycle){
        if(v != u)
          t.addEdge(v + n, fun[v] + n);
        t.addEdge(v, fun[v] + (v == u ? n : 0));
      }
    }
    t.Build(roots);
  }

  int Query(int u, int k){
    int v = t.Query(u, k);
    v -= v >= n ? n : 0;
    if(k > 0){
      k %= G.cSize[G.cycle[u]];
      v = t.Query(v, k);
      assert(k == 0);
      v -= v >= n ? n : 0;
    }
    return v;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q;
  cin >> n >> q;

  vi fun(n);
  FOR(i, n){
    cin >> fun[i];
    fun[i]--;
  }

  Solver solver(fun);
  FOR(i, q){
    int u, k;
    cin >> u >> k; u--;
    cout << solver.Query(u, k) + 1 << endl;
  }

  return 0;
}
