/**
 * Graph Girth in Undirected Graph
 *
 * Find the legth of the shortest cycle
 *
 * Complexity:
 *  - Time : O(nm)
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1707/
 *
 */

#include "bits/stdc++.h"

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using ii = pair<int, int>;

#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define INF INT_MAX

struct Graph{
  int n;
  vvi adj;

  Graph (int n) : n(n), adj(n){}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  int findCycle(int s){
    queue<ii> q;
    vi p(n, -1), d(n, INF);

    q.push({s, 0});
    p[s] = s;

    while(!q.empty()){
      int v = q.front().first, dist = d[v] = q.front().second;
      q.pop();

      for(int u : adj[v]){
        if(p[u] == -1){
          p[u] = v;
          q.push({u, dist + 1});
        }else{
          if(p[v] != u && d[u] != INF)
            return 2 * dist + (d[u] == d[v]);
        }
      }
    }
    return INT_MAX;
  }

  int Solve(){
    int ans = INT_MAX;
    FOR(s, n)
      ans = min(ans, findCycle(s));
    return ans != INT_MAX ? ans : -1;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  cin >> n >> m;

  Graph G(n);

  FOR(i, m){
    int u, v;
    cin >> u >> v; u--; v--;
    G.AddEdge(u, v);
  }

  cout << G.Solve() << "\n";
}
