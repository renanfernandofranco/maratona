/**
 * Transform Tree to Biconnected Graph
 *
 * Given an undirected tree, find the smallest number of
 * edges to transform it into a biconnected graph.
 *
 * Complexity:
 *  - Time : O(n + m)
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/result/3401771/
 *
 */

#include "bits/stdc++.h"

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
using ii = pair<int, int>;

#define FOR(i, b) for(int i = 0; i < (int)(b); i++)

struct Graph{
  int n;
  vvi adj;
  vi seen;

  Graph (int n) : n(n), adj(n), seen(n, false) {}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
    adj[v].push_back(u);
  }

  int matching(int v, bool first = false){
    seen[v] = true;

    if(adj[v].size() == 1 && !first)
      return v;

    for(int u : adj[v])
      if(!seen[u]){
        int ret = matching(u);
        if(ret != -1)
          return ret;
     }

    return -1;
  }

  vector<ii> Solve(){
    vector<ii> pairs, edges;
    vi rem;
    FOR(v, n){
      if(adj[v].size() == 1 && !seen[v]){
        seen[v] = true;
        int u = matching(v, true);
        if(u == -1)
          rem.push_back(v);
        else
          pairs.push_back({v, u});
      }
    }

    FOR(i, pairs.size())
      edges.push_back(minmax(pairs[i].second, pairs[(i + 1) % pairs.size()].first));

    if(rem.size() & 1)
      rem.push_back(pairs.back().first);

    FOR(i, rem.size() / 2)
      edges.push_back(minmax(rem[i << 1], rem[i << 1 | 1]));

    return edges;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n;
  cin >> n;

  Graph G(n);

  FOR(i, n - 1){
    int u, v;
    cin >> u >> v; u--; v--;
    G.AddEdge(u, v);
  }

  auto ans = G.Solve();

  cout << ans.size() << "\n";
  for(auto e : ans)
    cout << e.first + 1 << ' ' << e.second + 1 << "\n";
}
