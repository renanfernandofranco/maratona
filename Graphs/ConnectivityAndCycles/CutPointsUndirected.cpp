/**
 * Finding articulation points - Undirected Graph
 *
 * Complexity:
 *  - Time : O(n + m)
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/2077
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
#define endl '\n'

struct Graph{
  int n;
  vvi adj;
  vi tin, low, marked;
  Graph(int n): n(n), adj(n), tin(n, -1), low(n, -1), marked(n, false) {}

  void addEdge(int u, int v){
    adj[v].push_back(u);
    adj[u].push_back(v);
  }

  void dfs(int u, int p, int &timer, vector <bool> &cutPoint) {
    marked[u] = true;
    tin[u] = low[u] = timer++;
    int children = 0;

    for(int v : adj[u]) {
      if(v == p)
        continue;

      if(marked[v])
        low[u] = min(low[u], tin[v]);
      else {
        dfs(v, u, timer, cutPoint);
        low[u] = min(low[u], low[v]);
        if(low[v] >= tin[u] and p != -1)
          cutPoint[u] = true;
        ++children;
      }
    }

    if(p == -1 and children > 1)
      cutPoint[u] = true;
  }

  vi find_cutPoints() {
    int timer = 0;
    vector<bool> is_cutPoint(n, false);
    vi cutPoints;

    for(int i = 0; i < n; i++)
      if(!marked[i])
        dfs(i, -1, timer, is_cutPoint);

    for(int i = 0; i < n; i++)
      if(is_cutPoint[i])
        cutPoints.push_back(i+1);

    return cutPoints;
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  cin >> n >> m;

  Graph G(n);
  for(int i = 0; i < m; i++){
    int u, v;
    cin >> u >> v;
    G.addEdge(u-1,v-1);
  }

  vi ans = G.find_cutPoints();
  int tam = ans.size();

  cout << tam << endl;
  for(int i = 0; i < tam; i++)
    cout << ans[i] << " \n"[i + 1 == tam];
}
