/**
 * Transitive Closure of a directed graph
 *
 * Time Complexity:
 *  - Time: O(|V| * |E|)
 *  - Space: O(|V| ^ 2)
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
#define FOR(i, u) for(int i = 0; i < (u); i++)

struct TransClosure{
  int n;
  vvi adj;
  vector<vector<bool>> tc;

  TransClosure(int n): n(n), adj(n), tc(n, vector<bool> (n, false)){};

  void DFSUtil(int s, int v){
    tc[s][v] = s != v || count(adj[v].begin(), adj[v].end(), v);
    for(int u : adj[v])
      if(!tc[s][u])
        DFSUtil(s, u);
  }

  void AddEdge(int v, int u) {
    adj[v].push_back(u);
  }

  void Solve(){
    FOR(i, n)
      DFSUtil(i, i);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  
  int t, n, m;
  cin >> t;

  while(t--){
    cin >> n >> m;
    TransClosure ts(n);

    FOR(i, m){
      int u, v;
      cin >> u >> v;
      ts.AddEdge(u - 1, v - 1);
    }

    ts.Solve();
    auto ret = ts.tc;

    int ans = 0;
    FOR(i, n)
      FOR(j, n)
        ans += ret[i][j] && i != j;

    cout << ans << endl;
  }
}
