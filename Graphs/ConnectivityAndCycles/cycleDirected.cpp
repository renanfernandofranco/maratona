/**
 * Cycle Fidner - Directed graph
 *
 * Complexity:
 *  - Time: O(|V| + |E|)
 *  - Space: O(|V| + |E|)
 *
 * Problem: https://cses.fi/problemset/task/1678/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using vvi = vector<vi>;

#define FOR(i, u) for(int i = 0; i < (u); i++)
#define all(a) a.begin(), a.end()

struct Graph{
  int n;
  vvi adj;
  vi p, c, cycle;
  int bg, ed;

  Graph(int n) : n(n), adj(n), p(n, -1), c(n, 0) {}

  void AddEdge(int u, int v){
    adj[u].push_back(v);
  }

  bool dfs(int v){
    c[v] = 1;
    for(int w : adj[v]){
      if(c[w] == 1){
        bg = w;
        ed = v;
        return true;
      }
      if(c[w] == 0){
        p[w] = v;
        if(dfs(w))
          return true;
      }
    }
    c[v] = 2;
    return false;
  }

  void buildCycle(){
    for(int v = ed; v != bg; v = p[v])
      cycle.push_back(v);
    cycle.push_back(bg);
    cycle.push_back(ed);
    reverse(all(cycle));
  }

  bool Solve(){
    FOR(i, n)
      if(c[i] == 0){
        p[i] = i;
        if(dfs(i)){
          buildCycle();
          return true;
        }
      }
    return false;
  }
};

int main(){
  cin.sync_with_stdio(0);
  cin.tie(0);
  int i, j, n, m, u, v;

  cin >> n >> m;

  Graph G(n);

  FOR(i, m){
    cin >> u >> v;
    u--; v--;
    G.AddEdge(u, v);
  }

  if(G.Solve()){
    auto ans = G.cycle;
    cout << ans.size() << endl;
    FOR(i, ans.size())
      cout << ans[i]  + 1 << " \n" [i == ans.size() - 1];
  }else{
    cout << "IMPOSSIBLE\n";
  }

  return 0;
}
