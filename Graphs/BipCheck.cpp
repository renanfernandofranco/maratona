/**
 * Bipartite Checker
 *
 * Separates each connected component into bipartitions
 *
 * Complexity:
 *  - Time : O(n + m)
 *  - Space : O(n + m)
 *
 * Problem: https://cses.fi/problemset/task/1668/
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using vvi = vector<vi>;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()

struct BipChecker{
  int n, nComps;
  vvi adj;
  vector<vvi> comps;
  vector<int> side;

  BipChecker(int n): n(n), nComps(0), adj(n), comps(2), side(n, -1){}

  void AddEdge(int v, int u){
    adj[v].push_back(u);
    adj[u].push_back(v);
  }

  bool Solve(){
    queue<int> q;
    bool is_bipartite = true;
    FOR(st, n){
      if (side[st] == -1) {
        q.push(st);
        side[st] = 0;
        nComps++;
        comps[0].push_back({st});
        comps[1].push_back({});
        while (!q.empty()) {
          int v = q.front();
          q.pop();
          for (int u : adj[v]) {
            if (side[u] == -1) {
              side[u] = side[v] ^ 1;
              q.push(u);
              comps[side[u]].back().push_back(u);
            } else {
              is_bipartite &= side[u] != side[v];
            }
          }
        }
      }
    }
    return is_bipartite;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, m;

  cin >> n >> m;
  BipChecker bip(n);

  FOR(i, m){
    int u, v;
    cin >> u >> v; u--; v--;
    bip.AddEdge(u, v);
  }

  if(bip.Solve()){
    FOR(i, n)
      cout << bip.side[i] + 1 << " \n"[i == n - 1];
  }else{
    cout << "IMPOSSIBLE" << endl;
  }
}