# Competitive Programming Notebook

This repository houses some useful algorithms aimed at solving a range of different competitive programing problems.

Some of them were inspired from the [CP Algorithms][20] website, where explanation and proof of correctness might be found. In order make it easier to identify when these are available, such information will be given either in the category description or in the source code file. 

## Repository strucure

Currently, the repository is organized by problem categories, which are described next.

- [DataStructures][1]: Algorithms which uses a special implementation or type of data structure;
- [Geometry][2]: Algorithms from the geometry field, or that uses its concepts;
- [Graphs][3]: Algorithms which work on the graph representation of a problem;
- [Inversions][4]: Solutions to problems involving inversions;
- [LIS][5]: Algorithms for solving the Longest Increasing Subsequence (LIS) problem;
- [ModularArithmetics][6]: Number theory algorithms to work with modular arithmetics.

[1]: ./DataStructures
[2]: ./Geometry
[3]: ./Graphs
[4]: ./Inversions
[5]: ./LIS
[6]: ./ModularArithmetics

[20]: https://cp-algorithms.com/
