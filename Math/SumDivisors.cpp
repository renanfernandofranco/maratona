/**
 * Let f(n) denote the sum of divisors of an integer n.
 * 
 * Given a integer 1 <= n <= 10^12, compute sum_{i=1}^n f(i).
 * 
 * A integer i is included in the answer floor(n / i) * i times.
 * 
 * For i  >= sqrt(n), the division floor(n / i) assume only a value x
 * from 1 to sqrt(n). So, for each x, we compute how many numbers i have
 * floor(n / i) = x
 * 
 * Complexity:
 *      - Time: O(sqrt(n))
 *      - Space: O(1)
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;

const ll MOD = 1e9 + 7;

ll inverse(ll i){
    if(i == 1) return 1;
    return (MOD - ((MOD / i) * inverse(MOD % i)) % MOD + MOD) % MOD;
}

const ll div2 = inverse(2);

int main(){
    ll n;

    cin >> n;

    ll sq = sqrt(n);

    ll ans = 0;
    for(ll i = 1; i < n/sq; i++)
        ans = (ans + (n / i) * i) % MOD;
    
    for(ll val = sq; val >= 1; val--){
        ll l = n / (val + 1) + 1;
        ll r = n / val;
        ll range = (r - l + 1) % MOD;
        ll mid = (((l  + r) % MOD) * div2) % MOD;
        ll sum = (range * mid) % MOD; // arithmetic sum from l to r
        ans = (ans + sum * val) % MOD;
    }

    cout << ans << '\n';
};
