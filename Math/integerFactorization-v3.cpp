/**
 * Integer Factorization - Pollard's rho algorithm
 *
 * Input: a integer number n <= 2^63 - 1 (~= 9*10^18)
 *
 * Complexity:
 *  - Time: O(n^(1/4)*lg n)) by factor until reach n < 10^9
 *          O(sqrt(n / ln n)) to factor out the rest
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'
#define all(a) a.begin(), a.end()

using ll = long long;
using ull = unsigned long long;
using vll = vector<ll>;

#define MAXL (50000 >> 5)+1
#define GET(x) (mark[x >> 5] >> (x & 31) & 1)
#define SET(x) (mark[x >> 5] |= 1 << (x & 31))
int mark[MAXL];
int P[50000], Psz = 0;

struct Fatorizer{
  vector<ll> fats;

  Fatorizer(){
    register int i, j, k;
    SET(1);
    int n = 46340;
    for (i = 2; i <= n; i++) {
      if (!GET(i)) {
        for (k = n / i, j = i * k; k >= i; k--, j -= i)
          SET(j);
        P[Psz++] = i;
      }
    }
  }

  ll mul(ull a, ull b, ull mod) {
    ll ret = 0;
    for (a %= mod, b %= mod; b != 0; b >>= 1, a <<= 1, a = a >= mod ? a - mod : a) {
      if (b & 1) {
        ret += a;
        if (ret >= mod)
          ret -= mod;
      }
    }
    return ret;
  }

  void exgcd(ll x, ll y, ll &g, ll &a, ll &b) {
    if (y == 0)
      g = x, a = 1, b = 0;
    else
      exgcd(y, x % y, g, b, a), b -= (x/y) * a;
  }

  ll llgcd(ll x, ll y) {
    if (x < 0)
      x = -x;
    if (y < 0)
      y = -y;
    if (!x || !y)
      return x + y;
    ll t;
    while (x % y)
      t = x, x = y, y = t%y;
    return y;
  }

  ll inverse(ll x, ll p) {
    ll g, b, r;
    exgcd(x, p, g, r, b);
    if (g < 0)
      r = -r;
    return (r % p + p) % p;
  }

  ll modPow(ll x, ll y, ll mod) {
    ll ret = 1;
    while (y) {
      if (y & 1)
        ret = mul(ret, x, mod);
      y >>= 1, x = mul(x, x, mod);
    }
    return ret % mod;
  }

  int isPrime(ll p) { // implements by miller-babin
    if (p < 2 || !(p & 1))
      return 0;
    if (p == 2)
      return 1;

    ll q = p - 1, a, t;
    int k = 0, b = 0;

    while (!(q & 1))
      q >>= 1, k++;

    for (int it = 0; it < 2; it++) {
      a = rand() % (p - 4) + 2;
      t = modPow(a, q, p);
      b = (t == 1) || (t == p-1);
      for (int i = 1; i < k && !b; i++) {
        t = mul(t, t, p);
        if (t == p-1)
          b = 1;
      }
      if (b == 0)
        return 0;
    }
    return 1;
  }

  ll pollard_rho(ll n, ll c) {
    ll x = 2, y = 2, i = 1, k = 2, d;
    while (true) {
      x = (mul(x, x, n) + c);
      if (x >= n)
        x -= n;
      d = llgcd(x - y, n);
      if (d > 1)
        return d;
      if (++i == k)
        y = x, k <<= 1;
    }
    return n;
  }

  void factorize(int n) {
    for (int i = 0; i < Psz && P[i] * P[i] <= n; i++) {
      if (n % P[i] == 0) {
        while (n % P[i] == 0)
          fats.push_back(P[i]), n /= P[i];
      }
    }
    if (n != 1)
      fats.push_back(n);
  }

  void llfactorize(ll n) {
    if (n == 1)
      return;

    if (n < 1e+9) {
      factorize(n);
      return;
    }

    if (isPrime(n)) {
      fats.push_back(n);
      return;
    }

    ll d = n;
    for (int i = 2; d == n; i++)
      d = pollard_rho(n, i);

    llfactorize(d);
    llfactorize(n / d);
  }

  vll Solve(ll n){
    fats.clear();
    llfactorize(n);
    sort(all(fats));
    return fats;
  }
};

int main(){
  cin.tie()->sync_with_stdio(0);
  ll n;
  Fatorizer fatorizer;
  while(cin >> n, n){
    auto fats = fatorizer.Solve(n);
    int sz = fats.size();
    FOR(i, sz){
      int j = i;
      while(j + 1  < sz && fats[i] == fats[j + 1])
        j++;
      cout << fats[i] << '^' << j - i + 1 << " \n"[j + 1 == sz];
      i = j;
    }
  }
}
