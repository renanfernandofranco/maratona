/**
 * Given an integer, your task is to find the number, sum and product of its divisors. 
 * 
 * As an example, let us consider the number 12:
 * 
 * the number of divisors is 6 (they are 1, 2, 3, 4, 6, 12)
 * the sum of divisors is 1+2+3+4+6+12=28
 * the product of divisors is 1⋅2⋅3⋅4⋅6⋅12=1728
 * 
 * Since the input number may be large, it is given as a prime factorization.
 *
 * Remember that a^(b^c) mod m = a^(b^c mod n) mod m, where n = φ(m) Euler's totient function.
 * If m is prime, then n = m-1.
 * 
 * Complexity:
 *      - Time: O(n * lg n), whatever it is n
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
using pll = pair<ll, ll>;
#define FOR(i, b) for(int i = 0; i < (b); i++)

const ll MOD = 1e9 + 7;
const ll MOD2 = 1e9 + 6;

ll inv(ll i, ll m){
    if(i == 1) return 1;
    return (m - ((m / i) * inv(m % i, m)) % m + m) % m;
}

ll pow(ll a, ll b, ll m){
    ll ans = 1;
    while(b > 0){
        if(b & 1) ans = (ans * a) % m;
        b >>= 1;
        a = (a * a) % m;
    }
    return ans;
}

ll numDivisors(vector<pll> fat){
    ll ans = 1;
    for(auto pr : fat)
        ans = (ans * (pr.second + 1)) % MOD;

    return ans;
}

ll sumDivisors(vector<pll> fat){
    ll ans = 1;
    for(auto pr : fat){
        ll a = (pow(pr.first, pr.second + 1, MOD) - 1 + MOD) % MOD;
        ll b = inv((pr.first - 1 + MOD) % MOD, MOD);
        ans = (ans * ((a * b) % MOD)) % MOD;
    }
    return ans;
}

ll prodDivisors(vector<pll> fat){
    bool find = false;
    ll exp = 1;
    for(auto pr : fat){
        if((pr.second & 1) && !find){
            // divide exponent by 2
            exp = (exp * ((pr.second + 1) / 2)) % MOD2;
            find = true;
        }else
            exp = (exp * (pr.second + 1)) % MOD2;
    }

    // if it was not possible to divide the exponent by 2
    // then apply the square root to the number
    if(!find)
        for(auto& pr : fat)
            pr.second /= 2;
    
    ll num = 1;
    for(auto pr : fat)
        num = (num * pow(pr.first, pr.second, MOD)) % MOD;

    return pow(num, exp, MOD);
}

int main(){
    int n;
    cin >> n;

    vector<pll> fat(n);

    for(auto& pr : fat)
        cin >> pr.first >> pr.second;

    cout << numDivisors(fat) << ' ' << sumDivisors(fat) << ' ' << prodDivisors(fat) << endl;

};
