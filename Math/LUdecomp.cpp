/**
 * LU Decomposition of the Square Linear System
 * by modulo 2
 * 
 * We would like to solve the system Ax = b
 * 
 * We can find P, L, and U such that PA = LU. So, solve the
 * system AX = b is equivalent to solve LUx = P^-1 * b
 * 
 * 
 * Operations:
 *    - Build: O(n ^ 3)
 *    - Solve: O(n ^ 2) by each vector bInv
 * 
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define REV(i, b) for(int i = (b - 1); i >= 0; i--)

using vi = vector<int>;
#define N 500
using bs = bitset<N>;
using vbs = vector<bs>;

// PAx = b -> PLUx = b -> LUx = P^-1 * b
bool LUdecomp (vbs A, vbs& L, vbs& U, vi& P, int n) {
   FOR(j, n){
      for(int i = j; i < n; ++i)
         if (A[i][j]) {
            swap (A[i], A[j]);
            swap(L[i], L[j]);
            swap(P[i], P[j]);
            break;
         }
      if (!A[j][j]) return false;

      for (int i = j + 1; i < n; i++)
         if (A[i][j]){
            A[i] ^= A[j];
            L[i][j] = true;
         }
   }
   FOR(i, n) L[i][i] = true;
   U = A;
   return true;
}

bs solve (vbs& L, vbs& U, bs& bInv, int n) {
   bs x, y; // LUx = b^-1
   FOR(i, n) // Solve Ly = b^-1, with y = Ux
      y[i] = bInv[i] ^ ((L[i] & y).count() & 1);
   REV(i, n) // Solve Ux = y
      x[i] = y[i] ^ ((U[i] & x).count() & 1);
   return x;
}

int main(){
   cin.tie(0)->sync_with_stdio(0);
   int n, x;
   
   cin >> n;
   vbs A(n);
   FOR(i, n)
      FOR(j, n)
         cin >> x, A[j][i] = x;
   
   vbs L(n), U(n);
   vi P(n);
   FOR(i, n) P[i] = i;

   if(!LUdecomp(A, L, U, P, n))
      return (cout << "-1" << endl, 0);

   vi PInv(n);
   FOR(i, n) PInv[P[i]] = i;

   FOR(i, n){
      bs bInv;
      bInv[PInv[i]] = true; // b^-1 = P^-1 * b
      bs x = solve(L, U, bInv, n);
      FOR(j, n)
         if(x[j])
            cout << j + 1 << ' ';
      cout << endl; 
   }
}
