/**
 * Integer Factorization
 *
 * Given a number n, with n < M
 *
 * Complexity:
 *  - Build Time : O(M log log M)
 *  - Factorization: O(amount of factors of the n)
 *  - Space: O(M)
 *
 */

#include <bits/stdc++.h>
using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
using vi = vector<int>;
using ii = pair<int, int>;

struct Sieve{
  vi primes, first;

  Sieve(int M = 1'000'006) : first(M, 0){
    vector<char> is_prime(M, true);
    is_prime[0] = is_prime[1] = false;
    for (int i = 2; i * i < M; i++)
      if (is_prime[i])
        for (int j = i * i; j < M; j += i)
          if(is_prime[j]){
            is_prime[j] = false;
            first[j] = i;
          }
    FOR(i, M)
      if(is_prime[i])
        primes.push_back(first[i] = i);
  }

  vi decomp(int n) {
    vi fats;
    while(first[n]){
      fats.push_back(first[n]);
      n /= first[n];
    }
    return fats;
  }

  vector<ii> decomp2(int num){
    vi v = decomp(num);
    vector<ii> fats;
    int sz = v.size();
    FOR(i, sz){
      int j = i;
      while(j + 1 < sz && v[i] == v[j + 1])
        j++;
      fats.push_back({v[i], j - i + 1});
      i = j;
    }
    return fats;
  }
};
