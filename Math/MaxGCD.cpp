/**
 * Given a array of n integers, find the maximum GCD between two elements in the array
 * 
 * Complexity:
 *  - Time: O(M lg M), where M is the maximum element in the array
 *  - Space: O(M)
 * 
 */

#include <bits/stdc++.h>

using namespace std;

int main(){
    cin.tie(0)->sync_with_stdio(0);
    int n;

    cin >> n;

    vector<int> v(n);
    for(int& e : v)
        cin >> e;

    int mx = *max_element(v.begin(), v.end());

    vector<int> freq(mx + 1);

    for(int e : v)
        freq[e]++;

    for(int i = mx; i >= 1; i--){
        int cnt = 0;
        for(int j = i; j <= mx; j += i)
            cnt += freq[j];
        if(cnt >= 2){
            cout << i << '\n';
            return 0;
        }
    }
}
