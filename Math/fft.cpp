/**
 * Fast Fourier Transform
 * ======================
 *
 * Calculates  the  discrete  convolution  of  two  vectors.
 *
 * Complexity:
 *  - Time: O(n lg n)
 *  - Space: O(n)
 *
 * Note: increasing the precision of the complex number may
 * increase the overall runtime by a factor of ~2.
 *
 * Problem: https://cses.fi/problemset/task/2111/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ld = double;
using cd = complex<ld>;
using ll = long long;
using vll = vector<ll>;
const ld PI_2 = acos(-1.0L) * 2;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()

void fft(vector<cd> &a, bool invert) {
  int n = a.size(); // power of 2

  for (int i = 1, j = 0; i < n; i++) {
    int bit = n >> 1;
    for (; j & bit; bit >>= 1)
      j ^= bit;
    j ^= bit;

    if (i < j)
      swap(a[i], a[j]);
  }

  for (int len = 2; len <= n; len <<= 1) {
    ld ang = PI_2 / len * (invert ? -1 : 1);
    cd wlen(cos(ang), sin(ang));
    for (int i = 0; i < n; i += len) {
      cd w(1);
      FOR(j, len / 2){
        cd u = a[i + j], v = a[i + j + len/2] * w;
        a[i + j] = u + v;
        a[i + j + len/2] = u - v;
        w *= wlen;
      }
    }
  }

  if (invert)
    for (cd& x : a)
      x /= n;
}

vll multiply(vll &a, vll &b) {
  vector<cd> fa(all(a)), fb(all(b));
  int n = 1 << (int)ceil(log2(a.size() + b.size()));
  fa.resize(n);
  fb.resize(n);

  fft(fa, false);
  fft(fb, false);
  FOR(i, n)
    fa[i] *= fb[i];
  fft(fa, true);

  vll result(n);
  FOR(i, n)
    result[i] = round(fa[i].real());
  return result;
}

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int k, n, m;
  cin >> k >> n >> m;
  vll a(k + 1), b(k + 1);

  FOR(i, n){
    int vl;
    cin >> vl;
    a[vl]++;
  }

  FOR(i, m){
    int vl;
    cin >> vl;
    b[vl]++;
  }

  vll ans = multiply(a, b);
  for(int i = 2; i <= 2 * k; i++)
    cout << ans[i] << " \n"[i == 2 * k];
}
