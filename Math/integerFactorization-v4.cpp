/**
 * Integer Factorization - Pollard's rho algorithm
 *
 * Input: a integer number n <= 2^127 - 1 (~= 10^37)
 *
 * Complexity:
 *  - Time: O(n^(1/4)*lg n)) by factor until reach n < 10^9
 *          O(sqrt(n / ln n)) to factor out the rest
 *
 * Obs: If you have faith maybe it runs faster than that
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'
#define all(a) a.begin(), a.end()

using u128 = __uint128_t;
using s128 = __int128_t;
using u64 = uint64_t;

#define MAXL (50000 >> 5)+1
#define GET(x) (mark[x >> 5] >> (x & 31) & 1)
#define SET(x) (mark[x >> 5] |= 1 << (x & 31))
int mark[MAXL];
int P[50000], Psz = 0;

struct Fatorizer{
  const u128 num_tries = 3;
  u128 n, niv_n;
  mt19937_64 rd;
  vector<s128> fats;

  Fatorizer(){
    register int i, j, k;
    SET(1);
    int n = 46340;
    for (i = 2; i <= n; i++) {
      if (!GET(i)) {
        for (k = n / i, j = i * k; k >= i; k--, j -= i)
          SET(j);
        P[Psz++] = i;
      }
    }
  }

  s128 binpower(s128 base, s128 e, s128 mod) {
    s128 result = 1;
    base %= mod;
    while (e) {
      if (e & 1)
        result = (s128)result * base % mod;
      base = (s128)base * base % mod;
      e >>= 1;
    }
    return result;
  }

  inline u128 mul(u128 x, u128 y, u128 c) {
    u128 ans = 0;
    static const u64 shift = 25;
    while (y) {
      ans += x * (y & ((1 << shift) - 1));
      x = (x << shift) % c;
      y >>= shift;
    }
    return ans % c;
  }

  void setn(u128 n_) {
    n = n_;
    niv_n = 1;
    u128 x = n;
    for (int i = 1; i <= 126; i++) {
      niv_n *= x;
      x *= x;
    }
  }

  struct u256 {
    u128 hi, lo;

    static u128 HI(u128 x) {
      return x >> 64;
    };
    static u128 LO(u128 x) {
      return u64(x);
    };
    static u256 mul128(u128 x, u128 y) {
      u128 t1 = LO(x) * LO(y);
      u128 t2 = HI(x) * LO(y) + HI(y) * LO(x) + HI(t1);
      return { HI(x) * HI(y) + HI(t2), (t2 << 64) + LO(t1) };
    }
  };

  u128 mmul(u128 x, u128 y) {
    u256 m = u256::mul128(x, y);
    u128 ans = m.hi - u256::mul128(m.lo * niv_n, n).hi;
    if (s128(ans) < 0)
      ans += n;
    return ans;
  }

  inline u128 f(u128 x, u128 inc) {
    return mmul(x, x) + inc;
  }

  inline u128 gcd(u128 a, u128 b) {
    int shift = __builtin_ctzll(a | b);
    b >>= __builtin_ctzll(b);

    while (a) {
      a >>= __builtin_ctzll(a);
      if (a < b)
        swap(a, b);
      a -= b;
    }
    return b << shift;
  }

  inline u128 rho(u128 seed, u128 n, u64 inc) {
    static const int step = 1 << 9;
    setn(n);
    auto sub = [&](u128 x, u128 y) {
      return x > y ? x - y : y - x;
    };

    u128 y = f(seed, inc);

    for (int l = 1;; l <<= 1) {
      u128 x = y;

      for (int i = 1; i <= l; i++)
        y = f(y, inc);

      for (int k = 0; k < l; k += step) {
        int d = min(step, l - k);
        u128 g = seed, y0 = y;

        for (int i = 1; i <= d; i++) {
          y = f(y, inc);
          g = mmul(g, sub(x, y));
        }

        g = gcd(g, n);
        if (g == 1)
          continue;
        if (g != n)
          return g;

        u128 y = y0;
        while (gcd(sub(x, y), n) == 1)
          y = f(y, inc);

        return gcd(sub(x, y), n) % n;
      }
    }
    return 0;
  }

  u128 rho(u128 x) {
    if (x % 2 == 0)
      return 2;
    if (x % 3 == 0)
      return 3;
    uniform_int_distribution<u64> rng(2, u64(x) - 1);
    for (u128 i = 2; i < num_tries; i++) {
      u128 ans = rho(rng(rd), x, i);
      if (ans != 0 and ans != x)
        return ans;
    }
    return 0;
  }

  u128 factor(u128 x) {
    return rho(x);
  }

  bool check_composite(s128 n, s128 a, s128 d, s128 s) {
    s128 x = binpower(a, d, n);
    if (x == 1 || x == n - 1)
      return false;
    for (int r = 1; r < s; r++) {
      x = (s128)x * x % n;
      if (x == n - 1)
        return false;
    }
    return true;
  }

  bool isPrime(s128 n) { // MillerRabin - returns true if n is prime, else returns false.
    if (n < 2)
      return false;
    s128 r = 0;
    s128 d = n - 1;
    while ((d & 1) == 0) {
      d >>= 1;
      r++;
    }
    for (int a : {
          2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37
        }) {
      if (n == a)
        return true;

      if (check_composite(n, a, d, r))
        return false;
    }
    return true;
  }

  void factorize(int n) {
    for (int i = 0; i < Psz && P[i] * P[i] <= n; i++) {
      if (n % P[i] == 0) {
        while (n % P[i] == 0)
          fats.push_back(P[i]), n /= P[i];
      }
    }
    if (n != 1)
      fats.push_back(n);
  }

  void llfactorize(s128 n) {
    if (n == 1)
      return;

    if (n < 1e+9) {
      factorize(n);
      return;
    }

    if (isPrime(n)) {
      fats.push_back(n);
      return;
    }

    s128 d = rho(n);

    llfactorize(d);
    llfactorize(n / d);
  }

  vector<s128> Solve(s128 n){
    fats.clear();
    llfactorize(n);
    sort(all(fats));
    return fats;
  }
};

s128 read() {
  s128 x = 0, f = 1;
  char ch = getchar();
  while (ch < '0' || ch > '9') {
    if (ch == '-')
      f = -1;
    ch = getchar();
  }
  while (ch >= '0' && ch <= '9') {
    x = x * 10 + ch - '0';
    ch = getchar();
  }
  return x * f;
}

void print(s128 x) {
  if (x < 0) {
    putchar('-');
    x = -x;
  }
  if (x > 9)
    print(x / 10);
  putchar(x % 10 + '0');
}

int main() {
  s128 n;
  Fatorizer fatorizer;
  while(n = read(), n){
    auto fats = fatorizer.Solve(n);
    int sz = fats.size();
    FOR(i, sz){
      int j = i;
      while(j + 1  < sz && fats[i] == fats[j + 1])
        j++;
      print(fats[i]);
      printf("^%d%c", j - i + 1, " \n"[j + 1 == sz]);
      i = j;
    }
  }
}
