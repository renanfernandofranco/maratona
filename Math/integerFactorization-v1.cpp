/**
 * Integer Factorization
 *
 * Given a number n, with n < M * M
 *
 * Complexity:
 *  - Build Time : O(M log log M)
 *  - Factorization: O(sqrt(n / ln n))
 *  - Space: O(M)
 *
 */

#include <bits/stdc++.h>
using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
using vi = vector<int>;

template<class T>
struct Sieve{
  vi primes;

  Sieve(int M = 1'000'006){
    vector<char> is_prime(M, true);
    is_prime[0] = is_prime[1] = false;
    for (int i = 2; i * i < M; i++)
      if (is_prime[i])
        for (int j = i * i; j < M; j += i)
          is_prime[j] = false;

    FOR(i, M)
      if(is_prime[i])
        primes.push_back(i);
  }

  vi decomp(T n) {
    vi fats;
    for (int d : primes) {
      if (d * 1LL * d > n)
        break;
      while (n % d == 0) {
        fats.push_back(d);
        n /= d;
      }
    }
    if (n > 1)
      fats.push_back(n);
    return fats;
  }

  using pti = pair<T, int>;
  vector<pti> decomp2(T n){
    vi v = decomp(n);
    vector<pti> fats;
    int sz = v.size();
    FOR(i, sz){
      int j = i;
      while(j + 1 < sz && v[i] == v[j + 1])
        j++;
      fats.push_back({v[i], j - i + 1});
      i = j;
    }
    return fats;
  }
};
