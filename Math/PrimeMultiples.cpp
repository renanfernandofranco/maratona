/**
 * Given k distinct prime numbers a1,a2,…,ak and an integer n
 * 
 * Calculate how many of the first n positive integers are 
 * divisible by at least one of the given prime numbers.
 * 
 * Complexity:
 *      - Time: O(2^k)
 * 
 */
#include <bits/stdc++.h>

using namespace std;
using ll = long long;
using pll = pair<ll, ll>;
#define FOR(i, b) for(int i = 0; i < (b); i++)

int main(){
    ll n, k;

    cin >> n >> k;

    vector<ll> p(k);

    for(ll& e : p)
        cin >> e;

    ll ans = 0;
    FOR(mask, (1 << k)){
        if(mask == 0)
            continue;
        ll prod = 1;
        FOR(j, k)
            if((1 << j) & mask){
                if(p[j] > n / prod)
                    prod = n + 1;
                else
                    prod *= p[j];
            }
        ans += (__builtin_popcount(mask) & 1 ? 1 : -1) * n / prod;
    }
    cout << ans << endl;
};
