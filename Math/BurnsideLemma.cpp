/**
 * Your task is to count the number of different necklaces that consist of n
 * pearls and each pearl has m possible colors.
 * 
 * Two necklaces are considered to be different if it is not possible to rotate
 * one of them so that they look the same.
 * 
 * Complexity:
 *      - Time: O(n * lg n)
 *      - Space: O(1) or O(n)
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
using vi = vector<int>;
#define FORI(i, b) for(int i = 1; i <= (b); i++)

const ll MOD = 1e9 + 7;

ll inv(ll i){
    if(i == 1) return 1;
    return (MOD - ((MOD / i) * inv(MOD % i)) % MOD + MOD) % MOD;
}

ll pw(ll a, ll b){
    ll ans = 1;
    while(b > 0){
        if(b & 1) ans = (ans * a) % MOD;
        b >>= 1;
        a = (a * a) % MOD;
    }
    return ans;
}

vi phi_1_to_n(int n) {
    vi phi(n + 1);
    for (int i = 0; i <= n; i++)
        phi[i] = i;

    for (int i = 2; i <= n; i++) 
        if (phi[i] == i)
            for (int j = i; j <= n; j += i)
                phi[j] -= phi[j] / i;
    return phi;
}

ll solve1(int n, int k){
    ll ans = 0;
    FORI(i, n)
        ans = (ans + pw(k, __gcd(i, n))) % MOD;
    ans = (ans *  inv(n)) % MOD;

    return ans;
}

ll solve2(int n, int k){
    vi phi = phi_1_to_n(n);
    ll ans = 0;
    FORI(i, n)
        if(n % i == 0)
            ans = (ans + phi[n / i] *  pw(k, i)) % MOD;
    ans = (ans * inv(n)) % MOD;

    return ans;
}

// Get accumulated possibilities for n from 1 to N
ll solve_sum(int N, int k){
    vi phi = phi_1_to_n(N);
    vector<ll> ans(N + 1);
    for(int d = 1; d <= N; d++){
        ll p = pw(k, d);
        for(int n = d; n <= N; n += d)
            ans[n] = (ans[n] + ((phi[n / d] *  p) % MOD)) % MOD;
    }
    
    ll ret = 0;
    FORI(i, N)
        ret = (ret + ans[i] * inv(i)) % MOD;

    return ret;
}

int main(){
    cin.tie(0)->sync_with_stdio(0);
    int n, k;
    cin >> n >> k;

    cout << solve2(n, k) << endl;
};
