/**
 * Simplex Method - Linear Programming Solver
 * 
 * Works faster than classic implementation.
 * But, I have no idea if this code is correct
 * 
 * Using Standard Form:
 *  max {cx : Ax <= b, x >= 0}
 * 
 * Given a LP with n variables and m constraints
 * 
 * Complexity:
 *  - Time: Exponential, but in practice frequently works in O(nm^2)
 *  - Space : O(nm)
 * 
 * Note: reducing the point floating precision can reduce
 * the overall running time by a factor of ~2,5.
 * 
 */

#include <bits/stdc++.h>
using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
#define all(a) a.begin(), a.end()
#define SZ(a) ((int)a.size())
using ld = long double;
using vd = vector<ld>;
using vvd = vector<vd>;
using vi = vector<int>;
using vvii = vector<vector<pair<int, int>>>;
 
const ld EPS = 1e-9;
#define diffZero(x) ((x > EPS) || (x < -EPS))

inline bool lessPair(ld d1, int i1, ld d2, int i2){
  return abs(d1 - d2) > EPS ? d1 < d2 : i1 < i2;
}

struct LPSolver {
  int m, n;
  vi B, N;
  vvd D;
 
  LPSolver(const vvd &A, const vd &b, const vd &c) :
   m(SZ(b)), n(SZ(c)), B(m + 1), N(n + 1), D(m + 1, vd(n + 1, 0)) {
    FOR(i, m){
      FOR(j, n)
        D[i + 1][j] = A[i][j];
      D[i + 1][n] = b[i];
    }
    FOR(i, n)
      D[0][i] = -c[i];
    FOR(i, m + 1)
      B[i] = -i;
    FOR(j, n + 1)
      N[j] = j;
  }
 
  void Pivot(int r, int s) {
    FOR(i, m + 1) 
      if (i != r && diffZero(D[i][s]))
        FOR(j, n + 1) 
          if (j != s)
            D[i][j] -= D[r][j] * D[i][s] / D[r][s];
    FOR(j, n + 1)
      if (j != s) 
        D[r][j] /= D[r][s];
    FOR(i, m + 1)
      if (i != r) 
        D[i][s] /= -D[r][s];
    D[r][s] = 1.0 / D[r][s];
    swap(B[r], N[s]);
  }

  ld Solve(vector<ld>& x){
    while(true){
      int ii = 1, jj = 0;
      FORI(i, m)
        if (lessPair(D[i][n], B[i], D[ii][n], B[ii])) 
          ii = i;
      if (D[ii][n] > -EPS)
        break;
      FOR(j, n)
        if (D[ii][j] < D[ii][jj])
          jj = j;
      if (D[ii][jj] > -EPS)
        return -numeric_limits<ld>::infinity(); //Infeasible
      Pivot(ii, jj);
    }

    while(true){
      int ii = 1, jj = 0;
      FOR(j, n)
        if (lessPair(D[0][j], N[j], D[0][jj], N[jj]))
          jj = j;
      if (D[0][jj] > -EPS)
        break;
      FORI(i, m)
        if (D[i][jj] > EPS && (D[ii][jj] < EPS ||
          lessPair(D[i][n] / D[i][jj], B[i], D[ii][n] / D[ii][jj], B[ii])))
            ii = i;
      if (D[ii][jj] < EPS)
        return numeric_limits<ld>::infinity(); //Unbounded
      Pivot(ii, jj);
    }

    x.assign(n, 0);
    FORI(i, m)
      if (B[i] >= 0) 
        x[B[i]] = D[i][n];
    return D[0][n];
  }
};

vvii adj;
void dfs(vector<int>& d, int v, int p, int dist, int x){
  d[v] = dist;
  for(auto pr : adj[v])
    if(pr.first != p && dist + pr.second <= x)
      dfs(d, pr.first, v, dist + pr.second, x);
}

int main() {
  cin.tie(0)->sync_with_stdio(0);

  int n, x, color, u, v, w;
  cin >> n >> x;

  vvd A(n + 2, vd(n));
  vd B(n + 2, -1), C(n);
  B[n] = 0;
  FOR(i, n){
    cin >> color;
    if(color == 0) 
      C[i] = -1; // the cost of change the color of a vertex white is -1
    else {
      C[i] = 0;
      B[n]++;
    }
  }
  
  adj.resize(n);
  vector<vector<int>> tr(n, vector<int>(n, x + 1));
  FOR(i, n - 1){
    cin >> u >> v >> w;
    u--; v--;
    adj[u].push_back({v, w});
    adj[v].push_back({u, w});
  }

  FOR(i, n)
    dfs(tr[i], i, -1, 0, x);

  /** constraint to force each vertex to be at most x away from at least
   * one black vertex. Remember that the standard form does not allow
   * inequalities of a greater or equal type.
   */
  FOR(i, n)
    FOR(j, n)
      A[i][j] = tr[i][j] > x ? 0 : -1;
  
  // constraints to force the amount of black vertices to equal B[n]
  B[n + 1] = -B[n];
  fill(all(A[n]), 1);
  fill(all(A[n + 1]), -1);
  
  LPSolver LPS(A, B, C);
  vd X;
  int ans = round(-LPS.Solve(X));

  cout << (SZ(X) < n ? -1 : ans) << endl;
 
  return 0;
}
