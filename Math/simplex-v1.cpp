/**
 * Simplex Method - Linear Programming Solver
 * 
 * Using Standard Form:
 *  max {cx : Ax <= b, x >= 0}
 * 
 * Given a LP with n variables and m constraints
 * 
 * Complexity:
 *  - Time: Exponential, but in practice frequently works in O(nm^2)
 *  - Space : O(nm)
 * 
 * Note: reducing the point floating precision can reduce
 * the overall running time by a factor of ~2,5.
 * 
 */

#include <bits/stdc++.h>
using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()
#define SZ(a) ((int)a.size())
using ld = long double;
using vd = vector<ld>;
using vvd = vector<vd>;
using vi = vector<int>;
using vvii = vector<vector<pair<int, int>>>;
 
const ld EPS = 1e-9;
#define diffZero(x) ((x > EPS) || (x < -EPS))

inline bool lessPair(ld d1, int i1, ld d2, int i2){
  return abs(d1 - d2) > EPS ? d1 < d2 : i1 < i2;
}
 
struct LPSolver {
  int m, n;
  vi B, N;
  vvd D;
 
  LPSolver(const vvd &A, const vd &b, const vd &c) :
   m(SZ(b)), n(SZ(c)), B(m), N(n + 1), D(m + 2, vd(n + 2)) {
    FOR(i, m)
      FOR(j, n)
        D[i][j] = A[i][j];
    FOR(i, m){
      B[i] = n + i;
      D[i][n] = -1;
      D[i][n + 1] = b[i];
    }
    FOR(j, n){
      N[j] = j;
      D[m][j] = -c[j];
    }
    N[n] = -1;
    D[m + 1][n] = 1;
  }
 
  void Pivot(int ii, int jj) {
    FOR(i, m + 2) 
      if (i != ii && diffZero(D[i][jj]))
        FOR(j, n + 2) 
          if (j != jj)
            D[i][j] -= D[ii][j] * D[i][jj] / D[ii][jj];
    FOR(j, n + 2)
      if (j != jj) 
        D[ii][j] /= D[ii][jj];
    FOR(i, m + 2)
      if (i != ii) 
        D[i][jj] /= -D[ii][jj];
    D[ii][jj] = 1.0 / D[ii][jj];
    swap(B[ii], N[jj]);
  }
 
  bool Simplex(int phase) {
    int x = phase == 1 ? m + 1 : m;
    while (true) {
      int jj = -1;
      FOR(j, n + 1){
        if (phase == 2 && N[j] == -1)
          continue;
        // I believe this ensures that the x0 will be removed from the base
        if (jj == -1 || lessPair(D[x][j], N[j], D[x][jj], N[jj]))
          jj = j;
      }
      if (D[x][jj] > -EPS)
        return true;
      int ii = -1;
      FOR(i, m){
        if (D[i][jj] < EPS)
          continue;
        if (ii == -1 || lessPair(D[i][n + 1] / D[i][jj], B[i], D[ii][n + 1] / D[ii][jj], B[ii]))
          ii = i;
      }
      if (ii == -1)
        return false;
      Pivot(ii, jj);
    }
  }
 
  ld Solve(vd &x) {
    int ii = 0;
    FOR(i, m)
      if (D[i][n + 1] < D[ii][n + 1])
        ii = i;
    if (D[ii][n + 1] < -EPS) {
      Pivot(ii, n);
      if (!Simplex(1) || D[m + 1][n + 1] < -EPS)
        return -numeric_limits<ld>::infinity(); //Infeasible
    }
    if (!Simplex(2))
      return numeric_limits<ld>::infinity(); // Unbounded
    x = vd(n);
    FOR(i, m)
      if (B[i] < n) 
        x[B[i]] = D[i][n + 1];
    return D[m][n + 1];
  }
};

vvii adj;
void dfs(vector<int>& d, int v, int p, int dist, int x){
  d[v] = dist;
  for(auto pr : adj[v])
    if(pr.first != p && dist + pr.second <= x)
      dfs(d, pr.first, v, dist + pr.second, x);
}

int main() {
  cin.tie(0)->sync_with_stdio(0);

  int n, x, color, u, v, w;
  cin >> n >> x;

  vvd A(n + 2, vd(n));
  vd B(n + 2, -1), C(n);
  B[n] = 0;
  FOR(i, n){
    cin >> color;
    if(color == 0) 
      C[i] = -1; // the cost of change the color of a vertex white is -1
    else {
      C[i] = 0;
      B[n]++;
    }
  }
  
  adj.resize(n);
  vector<vector<int>> tr(n, vector<int>(n, x + 1));
  FOR(i, n - 1){
    cin >> u >> v >> w;
    u--; v--;
    adj[u].push_back({v, w});
    adj[v].push_back({u, w});
  }

  FOR(i, n)
    dfs(tr[i], i, -1, 0, x);

  /** constraint to force each vertex to be at most x away from at least
   * one black vertex. Remember that the standard form does not allow
   * inequalities of a greater or equal type.
   */
  FOR(i, n)
    FOR(j, n)
      A[i][j] = tr[i][j] > x ? 0 : -1;
  
  // constraints to force the amount of black vertices to equal B[n]
  B[n + 1] = -B[n];
  fill(all(A[n]), 1);
  fill(all(A[n + 1]), -1);
  
  LPSolver LPS(A, B, C);
  vd X;
  int ans = round(-LPS.Solve(X));

  cout << (SZ(X) < n ? -1 : ans) << endl;
 
  return 0;
}
