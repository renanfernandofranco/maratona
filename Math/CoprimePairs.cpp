/**
 * Coprime Pairs
 * =============
 *
 * Given a list of n positive integers, count the number of pairs that are coprime
 * 
 * Mobius function:
 * 
 *  μ(n) = +1 if n is a square-free positive integer with an even number of prime factors.
 *  μ(n) = −1 if n is a square-free positive integer with an odd number of prime factors.
 *  μ(n) = 0 if n has a squared prime factor.
 *
 * Complexity:
 *  - Time: O(M lg M), where M is the greater number in the input
 *  - Space: O(M)
 *
 * Problem: https://cses.fi/problemset/task/2417
 *
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
const int MX = 1e6 + 1;

struct Mobius{
    vector<ll> lpf, mobius;

    Mobius(): lpf(MX), mobius(MX){
        // least prime factor
        for(int i = 2; i < MX; i++)
            if(!lpf[i])
                for(int j = i; j < MX; j += i)
                    if(!lpf[j])
                        lpf[j]=i;

        mobius[1] = 1;
        for(int i = 2; i < MX; i++)
            if(lpf[i / lpf[i]] == lpf[i])
                mobius[i] = 0;
            else
                mobius[i] = -1 * mobius[i / lpf[i]];
    }

    ll Solve(vector<ll> freq){
        ll ans = 0;
        for(int i = 1; i < MX; i++){
            if(mobius[i] == 0)
                continue;
            ll d = 0;
            for(int j = i; j < MX; j += i)
                d += freq[j];
            ans += (d * (d - 1)) / 2 * mobius[i];
        }
        return ans;
    }
};

int main(){
    cin.tie(0)->sync_with_stdio(0);
    ll n;
    cin >> n;

    vector<ll> freq(MX);
    for(int i = 0; i < n; i++){
        int x;
        cin >> x;
        freq[x]++;
    }
    Mobius mobius;

    cout<< mobius.Solve(freq) << '\n';
}