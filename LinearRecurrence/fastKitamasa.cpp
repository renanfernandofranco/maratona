/**
 * Fast Kitamasa Method - Linear Recurrence Solver
 *
 * Input:
 *  - The recurrence terms: A_0, A_1, ..., A_{k-1}
 *  - Constant terms: C_0, C_1, ..., C_{k-1}
 *  - An integer n
 *
 * Output:
 *  - Find term A_n = C_0 * A_{n - 1} + ... C_{k - 1} * A_{n - k}
 *
 * Attention!!!
 * Only works with MOD = c * 2 ^ x + 1 for arbitrary integers c and x.
 * Because NFT is being used to accelerate polynomial multiplication
 *
 * Complexity:
 *  - Time: O(k lg k lg n) with very high constant
 *  - Space: I have no idea, maybe O(k log k)
 *
 * With k = 3*10^4 and n = 10^18 the runtime ~= 6 seconds
 *
 * Problem: https://www.codechef.com/problems/RNG
 * Tutorial: https://codeforces.com/blog/entry/88760
 *
 */

#include <bits/stdc++.h>

#define all(v) v.begin(), v.end()
#define FOR(i, b) for(int i = 0; i < (int)b; i++)
using namespace std;

using ll = long long;

template <int MOD>
struct MINT{
  int v;

  MINT(ll val = 0){
    v = (-MOD <= val && val < MOD) ? val : val % MOD;
    if(v < 0) v += MOD;
  }

  MINT operator - () const { return MINT(-v); }
  MINT operator * (const MINT& m2) const { return v * 1LL * m2.v % MOD; }
  MINT operator / (const MINT& m2) const { return (*this) * inv(m2); }
  MINT& operator *= (const MINT& m2){ return (*this) = (*this) * m2; }
  MINT& operator /= (const MINT& m2){ return (*this) = (*this) / m2; }
  MINT& operator += (const MINT& m2){ return (*this) = (*this) + m2; }
  MINT& operator -= (const MINT& m2){ return (*this) = (*this) - m2; }
  bool operator == (const MINT& m2) const { return v == m2.v; }
  bool operator != (const MINT& m2) const { return v != m2.v; }
  bool operator < (const MINT& m2) const { return v < m2.v; }
  friend istream& operator >> (istream &is, MINT &a) { ll t; is >> t; a = MINT(t); return is; }
  friend ostream& operator << (ostream &os, const MINT &a) { return os << a.v; }
  friend MINT inv(const MINT& a) { return pw(a, MOD - 2); } // only if MOD is prime

  MINT operator + (const MINT& m2) const{
    int tmp = v + m2.v;
    return tmp >= MOD ? tmp - MOD : tmp;
  }

  MINT operator - (const MINT& m2) const{
    int tmp = v - m2.v;
    return tmp < 0 ? tmp + MOD : tmp;
  }

  friend MINT pw(MINT a, ll b){
    MINT ret = 1;
    while(b){
      if(b & 1) ret *= a;
      b >>= 1; a *= a;
    }
    return ret;
  }
};

namespace fft{
  template<int W, int M>
  static void NTT(vector<MINT<M>> &f, bool inv_fft = false){
    using T = MINT<M>;
    int N = f.size();
    vector<T> root(N >> 1);
    for(int i = 1, j=0; i<N; i++){
      int bit = N >> 1;
      while(j >= bit)
        j -= bit, bit >>= 1;
      j += bit;
      if(i < j)
      swap(f[i], f[j]);
    }

    T ang = pw(T(W), (M - 1) / N);
    if(inv_fft)
      ang = inv(ang);

    root[0] = 1;
    for(int i = 1; i < (N >> 1); i++)
      root[i] = root[i - 1] * ang;

    for(int i = 2; i <= N; i <<= 1){
      int step = N / i;
      for(int j = 0; j < N; j += i){
        FOR(k, i/2){
          T u = f[j + k], v = f[j + k + (i >> 1)] * root[k * step];
          f[j + k] = u + v;
          f[j + k + (i >> 1)] = u - v;
        }
      }
    }
    if(inv_fft){
      T rev = inv(T(N));
      FOR(i, N)
        f[i] *= rev;
    }
  }

  template<int W, int M>
  vector<MINT<M>> multiply_ntt(vector<MINT<M>> a, vector<MINT<M>> b){
    int N = 2;
    while((unsigned int) N < a.size() + b.size())
      N <<= 1;
    a.resize(N);
    b.resize(N);

    NTT<W, M>(a);
    NTT<W, M>(b);
    FOR(i, N)
      a[i] *= b[i];
    NTT<W, M>(a, true);

    return a;
  }
}

template<int W, int M>
struct PolyMod{
  using T = MINT<M>;
  vector<T> a;

  // constructor
  PolyMod(){}
  PolyMod(T a0) : a(1, a0) { normalize(); }
  PolyMod(const vector<T> a) : a(a) { normalize(); }

  // method from vector<T>
  int size() const { return a.size(); }
  int deg() const { return a.size() - 1; }
  void normalize(){ while(a.size() && a.back() == T(0)) a.pop_back(); }
  T operator [] (int idx) const { return a[idx]; }
  typename vector<T>::const_iterator begin() const { return a.begin(); }
  typename vector<T>::const_iterator end() const { return a.end(); }
  void push_back(const T val) { a.push_back(val); }
  void pop_back() { a.pop_back(); }

  // basic manipulation
  PolyMod reversed() const {
    return vector<T>(a.rbegin(), a.rend());
  }
  PolyMod trim(int n) const {
    return vector<T>(a.begin(), a.begin() + min(n, size()));
  }
  PolyMod inv(int n){
    PolyMod q(T(1) / a[0]);
    for(int i = 1; i < n; i <<= 1){
      PolyMod p = PolyMod(2) - q * trim(i * 2);
      q = (p * q).trim(i * 2);
    }
    return q.trim(n);
  }

  // operation with scala value
  PolyMod operator *= (const T x){
    for(auto &i : a) i *= x;
    normalize();
    return *this;
  }
  PolyMod operator /= (const T x){
    return *this *= (T(1) / T(x));
  }

  // operation with poly
  PolyMod& operator += (const PolyMod &b){
    a.resize(max(size(), b.size()));
    FOR(i, b.size())
      a[i] += b[i];
    normalize();
    return *this;
  }
  PolyMod& operator -= (const PolyMod &b){
    a.resize(max(size(), b.size()));
    FOR(i, b.size())
      a[i] -= b[i];
    normalize();
    return *this;
  }
  PolyMod& operator *= (const PolyMod &b){
    *this = fft::multiply_ntt<W, M>(a, b.a);
    normalize();
    return *this;
  }
  PolyMod& operator /= (const PolyMod &b){
    if(deg() < b.deg())
      return *this = PolyMod();
    int sz = deg() - b.deg() + 1;
    PolyMod ra = reversed().trim(sz), rb = b.reversed().trim(sz).inv(sz);
    *this = (ra * rb).trim(sz);
    while(size() < sz)
      push_back(T(0));
    reverse(all(a));
    normalize();
    return *this;
  }
  PolyMod& operator %= (const PolyMod &b){
    if(deg() < b.deg())
      return *this;
    PolyMod tmp = *this; tmp /= b; tmp *= b;
    *this -= tmp;
    normalize();
    return *this;
  }

  // operator
  PolyMod operator * (const T x) const { return PolyMod(*this) *= x; }
  PolyMod operator / (const T x) const { return PolyMod(*this) /= x; }
  PolyMod operator + (const PolyMod &b) const { return PolyMod(*this) += b; }
  PolyMod operator - (const PolyMod &b) const { return PolyMod(*this) -= b; }
  PolyMod operator * (const PolyMod &b) const { return PolyMod(*this) *= b; }
  PolyMod operator / (const PolyMod &b) const { return PolyMod(*this) /= b; }
  PolyMod operator % (const PolyMod &b) const { return PolyMod(*this) %= b; }
};

constexpr int W = 3, MOD = 104857601;
using mint = MINT<MOD>;
using poly = PolyMod<W, MOD>;

mint kitamasa(poly c, poly a, ll n){
  poly d = vector<mint>{1};
  poly xn = vector<mint>{0, 1};
  poly f;
  FOR(i, c.size())
    f.push_back(-c[i]);
  f.push_back(1);
  while(n){
    if(n & 1) d = d * xn % f;
    n >>= 1; xn = xn * xn % f;
  }
  mint ret = 0;
  FOR(i, a.size())
    ret += a[i] * d[i];
  return ret;
}

int main(){
  cin.tie(0)->sync_with_stdio(0);
  ll K, N;
  cin >> K >> N;

  vector<mint> v_dp(K), v_rec(K);
  FOR(i, K){
    int t; cin >> t; v_dp[i] = mint(t);
  }

  FOR(i, K){
    int t; cin >> t; v_rec[i] = mint(t);
  }

  reverse(all(v_rec));
  poly dp(v_dp), rec(v_rec);
  cout << kitamasa(rec, dp, N-1) << endl;
}
