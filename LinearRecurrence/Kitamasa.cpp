/**
 * Kitamasa Method - Linear Recurrence Solver
 *
 * Input:
 *  - Recurrence terms: A_1, A_2, ..., A_k
 *  - Constant terms: C_1, C_2, ..., C_k
 *  - An integer n
 *
 * Output:
 *  - Find term A_n = C_1 * A_{n - 1} + ... C_k * A_{n - k}
 *
 * Complexity:
 *  - Time: O(k^2 lg n)
 *  - Space: O(k^2)
 *
 * Problem: https://cses.fi/problemset/task/1722/
 * Tutorial: https://codeforces.com/blog/entry/88760
 *
 */

#include <bits/stdc++.h>

#define all(v) v.begin(), v.end()
#define FOR(i, b) for(int i = 0; i < (int)b; i++)
using namespace std;

using uint = unsigned;
using ll = long long;
using ull = unsigned long long;

template <int MOD>
struct MINT{
  int v;

  MINT(ll val = 0){
    v = (-MOD <= val && val < MOD) ? val : val % MOD;
    if(v < 0) v += MOD;
  }

  MINT operator - () const { return MINT(-v); }
  MINT operator * (const MINT& m2) const { return v * 1LL * m2.v % MOD; }
  MINT& operator *= (const MINT& m2){ return (*this) = (*this) * m2; }
  MINT& operator += (const MINT& m2){ return (*this) = (*this) + m2; }
  MINT& operator -= (const MINT& m2){ return (*this) = (*this) - m2; }
  friend ostream& operator << (ostream &os, const MINT &a) { return os << a.v; }

  MINT operator + (const MINT& m2) const{
    int tmp = v + m2.v;
    return tmp >= MOD ? tmp - MOD : tmp;
  }

  MINT operator - (const MINT& m2) const{
    int tmp = v - m2.v;
    return tmp < 0 ? tmp + MOD : tmp;
  }
};

template<int M>
struct PolyMod{
  using T = MINT<M>;
  vector<T> a;

  // constructor
  PolyMod(const vector<T> a = vector<T>()) : a(a){}

  // method from vector<T>
  int size() const { return a.size(); }
  T operator [] (int idx) const { return a[idx]; }
  void push_back(const T val) { a.push_back(val); }

  // operation with poly
  PolyMod& operator *= (const PolyMod &pb){
    PolyMod pa = *this;
    a.assign(pa.size() + pb.size() - 1, 0);
    FOR(i, pa.size())
      FOR(j, pb.size())
        a[i + j] += pa[i] * pb[j];
    return *this;
  }
  PolyMod& operator %= (const PolyMod &b){
    for(int i = size() - 1; i >= b.size() - 1; i--)
      FOR(j, b.size())
        a[i + j - b.size() + 1] -= a[i] * b[j];
    a.resize(b.size() - 1);
    return *this;
  }

  // operator
  PolyMod operator * (const PolyMod &b) const { return PolyMod(*this) *= b; }
  PolyMod operator % (const PolyMod &b) const { return PolyMod(*this) %= b; }
};

const int MOD = 1'000'000'007;
using mint = MINT<MOD>;
using poly = PolyMod<MOD>;

mint kitamasa(poly c, poly a, ll n){
  poly d = vector<mint>{1};
  poly xn = vector<mint>{0, 1};
  poly f;
  FOR(i, c.size())
    f.push_back(-c[i]);
  f.push_back(1);
  while(n){
    if(n & 1) d = d * xn % f;
    n >>= 1; xn = xn * xn % f;
  }
  mint ret = 0;
  FOR(i, a.size())
    ret += a[i] * d[i];
  return ret;
}

int main(){
  cin.tie(0)->sync_with_stdio(0);
  ll N;
  cin >> N;

  // Fibonacci
  vector<mint> v_dp = {0, 1}, v_rec = {1, 1};
  poly dp(v_dp), rec(v_rec);
  cout << kitamasa(rec, dp, N) << endl;
}
