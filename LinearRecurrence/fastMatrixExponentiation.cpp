/**
 * Fast Matrix Exponentiation
 * ==========================
 *
 * Complexity:
 *  - Time: O(n^3 lg k)
 *  - Space: O(n^2)
 *
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define FOR(i, b) for(int i = 0; i < (b); i++)

template<typename T>
struct Matrix {
  vector<vector<T>> raw;
  int n, m;

  Matrix(int n, int m, T val = 0): n(n), m(m) {
    raw.assign(n, vector<T>(m, val));
  }

  vector<T>& operator[](int i){
    return raw[i];
  }

  Matrix operator*(Matrix &other) {
    #ifndef ONLINE_JUDGE
    assert(m == other.n);
    #endif
    Matrix ans(n, other.m);
    FOR(i, n)
      FOR(j, other.m)
        FOR(k, m)
          ans[i][j] = ans[i][j] + raw[i][k] * other[k][j];

    return ans;
  }

  Matrix pow(ll k) {
    #ifndef ONLINE_JUDGE
    assert(n == m);
    #endif
    Matrix ans(n, n);
    Matrix tmp = *this;

    FOR(i, n)
      ans[i][i] = 1;

    while (k > 0) {
      if (k & 1) ans = ans * tmp;
      tmp = tmp * tmp;
      k >>= 1;
    }
    return ans;
  }

  friend void print(Matrix a) {
    FOR(i, a.n)
      FOR(j, a.m)
        cout << a[i][j] << " \n"[j + 1 == a.m];
    cout.flush();
  }
};

int main() {
  int n;
  cin >> n;

  Matrix<int> mat(2, 2), fib(2, 1);

  mat[0][0] = 1, mat[0][1] = 1;
  mat[1][0] = 1, mat[1][1] = 0;

  fib[0][0] = 1;
  fib[1][0] = 1;

  Matrix<int> ans = mat.pow(n - 1) * fib;

  cout << ans[1][0] << endl;

  return 0;
}
