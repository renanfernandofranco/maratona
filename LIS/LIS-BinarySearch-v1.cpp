/**
 * Finding the size of longest increasing subsequence using 
 * dynamic programming and binary search
 * 
 * Complexity:
 *    - Time: O(n * lg n)
 *    - Space: O(n)
 */

#include <bits/stdc++.h>

using namespace std;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()

int LIS(vector<int>& v){
   int n = v.size();

   // Store the best element found to occupy the iº position in the sequence
   vector<int> dp(n + 1, INT_MAX);
   dp[0] = INT_MIN;
   
   FOR(i, n){
      // Finds the first position that is greater than v[i]
      int j = upper_bound(all(dp), v[i]) - dp.begin();

      // Check if v[i] can be placed in that position and if the same is best that the current
      if(dp[j - 1] < v[i] && v[i] < dp[j])
         dp[j] = v[i];
   }
   // Finds last index that not is INT_MAX, since the same is size of longest sequence
   return lower_bound(all(dp), INT_MAX) - dp.begin() - 1;
}

int main(){
   vector<int> v = {2, 5, 3, 7, 11, 3, 8, 10, 13, 6};
   cout << LIS(v) << endl;
}
