/**
 * Finding the longest increasing subsequence using 
 * dynamic programming and binary search
 * 
 * Intuitively, the sequence generated is lower lexicographically
 * sequence possible, but I don't know prove this!
 * 
 * Complexity:
 *    - Time: O(n * lg n)
 *    - Space: O(n)
 */

#include <bits/stdc++.h>

using namespace std;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)
#define all(a) a.begin(), a.end()

int LIS(vector<int>& v, vector<int>& res){
   int n = v.size();
   vector<int> pos(n + 1), dad(n + 1), dp(n + 1, INT_MAX);
   // dp store the best element found to occupy the iº position in the sequence

   dp[0] = INT_MIN;
   FOR(i, n){
      // Finds the first position that is greater than v[i]
      int j = upper_bound(all(dp), v[i]) - dp.begin();

      // Check if v[i] can be placed in that position and if the same is best that the current
      if(dp[j - 1] < v[i] && v[i] < dp[j]){
         dp[j] = v[i];
         pos[j] = i;	// Store index of element in dp[j] currently
         dad[i] = pos[j - 1];	// Store the dad of this element
      }
   }

   // Finds last index that not is INT_MAX, since the element at that index is the last element in longest sequence
   int sz = lower_bound(all(dp), INT_MAX) - dp.begin() - 1;
   res.resize(sz);

   // Scales the dad vector to recover the sequence
   int cur = pos[sz];
   REV(i, sz){
      res[i] = v[cur];
      cur = dad[cur];
   }
   return sz;
}

int main(){
   int n;
   cin >> n;
   vector<int> v(n), res;

   FOR(i, n)
      cin >> v[i];
      
   cout << LIS(v, res) << endl;
}
