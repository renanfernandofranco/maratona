/**
 * Finding the size of longest increasing subsequence using dynamic programming
 * 
 * Complexity:
 *    - Time: O(n ^ 2)
 *    - Space: O(n)
 */

#include <bits/stdc++.h>

using namespace std;
#define FOR(i, b) for(int i = 0; i < (b); i++)

int LIS(vector<int>& v){
   int n = v.size();
   vector<int> dp(n, 1);// dp[i] store the size of the longest sequence that v[i] belong
   
   FOR(i, n)
      FOR(j, i)   // Search the longest sequence that v[i] can be attached
         if(v[i] > v[j] && dp[i] < dp[j] + 1)
            dp[i] = dp[j] + 1;
      
   return *max_element(dp.begin(),dp.end());
}
