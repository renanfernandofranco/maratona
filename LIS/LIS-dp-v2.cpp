/**
 * Finding the longest increasing subsequence using dynamic programming
 * 
 * Complexity:
 *    - Time: O(n ^ 2)
 *    - Space: O(n)
 */

#include <bits/stdc++.h>

using namespace std;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)
#define all(a) a.begin(), a.end()

int LIS(vector<int>& v, vector<int>& res){
   int n = v.size();
   vector<int> dp(n, 1);// dp[i] store the size of the longest sequence that v[i] belong
   vector<int> dad(n);// parent[i] store who precedes v[i] in the sequence given by dp[i]

   FOR(i, n)
      FOR(j, i)   // Search the longest sequence that v[i] can be attached
         if(v[i] > v[j] && dp[i] < dp[j] + 1){
            dad[i] = j;
            dp[i] = dp[j] + 1;
         }
   
   // Finds index with greater value, since the same is end of longest sequence
   int last = max_element(all(dp)) - dp.begin();
   int sz = dp[last];
   res.resize(sz);

   // Recover the longest sequence
   REV(i, sz){
      res[i] = v[last];
      last = dad[last];
   }
   
   return sz;
}

int main(){
   vector<int> res, v = {1, 4, 2, 10, 7 , 11};
   LIS(v, res);
}
