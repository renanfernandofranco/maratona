#include <bits/stdc++.h>

/**
 * This algorithm generates all combinations of pairs (the internal order of the is not important)
 * 
 * Complexity:
 *    - Time : apparently O(1) amortized by permutation with constant around of 30
 * 
 * Obs:
 *    - The test performed only for array with distinct elements
 *    - The number of permutation existing is n!/(2^(n/2) * (n/2)!), that is equal to number of
 * perfect matching on a complete graph with n vertices
 *    - Perhaps it is possible to improve the constant by exchanging the sort for something more efficient
 */ 

using namespace std;

vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

template<class BidirIt>
bool next_pair_permutation(BidirIt first, BidirIt last){
   if (first == last) return false;
   BidirIt cur = last;
   if (first == --cur) return false;
   if(distance(first, last) % 2 != 0) return false;

   BidirIt nxt, i, j;
   while (true) {
      nxt = cur;
      cur = prev(cur, 2);
      if (*cur < *nxt) {
         for(i = next(cur); i != last; i = next(i, 2))
            if(*cur < *i)
               break;
         if(i == last)
            for(i = prev(last); i != cur; i = prev(i, 2))
               if(*cur < *i)
                  break;
         std::iter_swap(cur, i);

         sort(next(cur), last);
            
         return true;
      }
      if (cur == next(first)) {
         sort(first, last);
         return false;
      }
   }
}

int main(){
   int cnt = 0;
   do{
      /*for(int u : v)
         cout << u << " ";
      cout << endl;*/
      cnt++;
   }while(next_pair_permutation<vector<int>::iterator>(v.begin(), v.end()));
   cout << cnt << endl;
}
