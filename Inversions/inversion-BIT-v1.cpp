/**
 * Counting inversions using Fenwick Tree
 * 
 * This algorithm need that all elements are non-negatives integers
 * 
 * Complexity: 
 *   - Time: O(n * lg maxElement)
 *   - Space: O(maxElement)
 * 
 */

#include<bits/stdc++.h> 
using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)
using vi = vector<int>;
using ll = long long;

struct BIT{
   int n;
   vi bit;

   BIT(int n): n(n), bit(n + 1){}

   int Sum(int r){
      int ret = 0;
      for (; r >= 0; r = (r & (r + 1)) - 1)
         ret += bit[r];
      return ret;
   }

   void Add(int idx, int value){
      for (; idx < n; idx |= idx + 1)
         bit[idx] += value;
   }
};

ll getInvCount(vector<int>& v){
   ll ret = 0;
   BIT bit(*max_element(v.begin(), v.end()));

   for (int i = v.size() - 1; i >= 0; i--){
      // Count all elements smaller than current element and that are after the same
      ret += bit.Sum(v[i] - 1);
      // Update the count of the elements with value v[i] in BIT
      bit.Add(v[i], 1); 
   }

   return ret;
}
  
int main() {
   cin.tie(0)->sync_with_stdio(0);

   int t, n;
   cin >> t;

   while(t--){
      cin >> n;
      vector<int> v(n);
      FOR(i, n)
         cin >> v[i];
      cout << getInvCount(v) << endl; 
   }

   return 0; 
}
