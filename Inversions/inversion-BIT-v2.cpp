/**
 * Counting inversions using Fenwick Tree
 * 
 * This algorithm maps the array of input in the Natural Numbers
 * 
 * Complexity: 
 *   - Time: O(n * lg n)
 *   - Space: O(n)
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)
#define all(a) a.begin(), a.end()
using vi = vector<int>;
using ll = long long;

struct BIT{
   int n;
   vi bit;

   BIT(int n): n(n), bit(n + 1){}

   int Sum(int r){
      int ret = 0;
      for (; r >= 0; r = (r & (r + 1)) - 1)
         ret += bit[r];
      return ret;
   }

   void Add(int idx, int value){
      for (; idx < n; idx |= idx + 1)
         bit[idx] += value;
   }
};

// Transforms K different elements in elements in the range [0, K) without change the precedence among them
template<typename T>
int mapToNatural(vector<T>& v){
   vector<T> aux = v;
   sort(all(aux));
   int n = unique(all(aux)) - aux.begin(); 
   aux.resize(n);

   for(int& e : v)
      e = lower_bound(all(aux), e) - aux.begin();

   return n;
}

template<typename T>
ll getInvCount(vector<T>& v){
   ll ret = 0;
   BIT bit(mapToNatural(v));

   for (int i = v.size() - 1; i >= 0; i--){
      // Count all elements smaller than current element and that are after the same
      ret += bit.Sum(v[i] - 1);
      // Update the count of the elements with value v[i] in BIT
      bit.Add(v[i], 1);
   }

   return ret;
} 
  
int main() {
   cin.tie(0)->sync_with_stdio(0);

   int t, n;
   cin >> t;

   while(t--){
      cin >> n;
      vector<int> v(n);
      FOR(i, n)
         cin >> v[i];
      cout << getInvCount(v) << endl;
   }

   return 0;
}
