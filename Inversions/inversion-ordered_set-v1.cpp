/**
 * Inversion Count using Ordered Set - Without Duplicates
 * 
 * Complexity:
 *    - Time: O(n lg n)
 *    - Space: O(n)
 * 
 */

#include <bits/stdc++.h> 
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;
using namespace __gnu_pbds;

template<typename T>
using ordered_set = tree<T, null_type, less<T>,
   rb_tree_tag, tree_order_statistics_node_update>;

#define endl '\n'
using ll = long long;

template<typename T>
ll invCount(vector<T> v){
   ordered_set<T> mp;
   ll ans = 0;
   for(T e : v){
      ans += mp.size() - mp.order_of_key(e);
      mp.insert(e);
   }
   return ans; 
}
  
int main(){
   cin.tie(0)->sync_with_stdio(0);
   int n, t;
   cin >> t;
   
   while(t--){
      cin >> n;
      vector<int> v(n);

      for(int i = 0; i < n; i++)
         cin >> v[i];
      cout << invCount(v) << endl;
   }

   return 0; 
} 
