/**
 * Couting Inversions using Merge Sort
 * 
 * Complexity:
 *    - Time: O(n * lg n)
 *    - Space: O(n)
 */

#include <bits/stdc++.h> 
using namespace std; 

#define MAXN 1000
int temp[MAXN];
int arr[MAXN];

int _merge(int left, int mid, int right);

int mergeSort(int left, int right){ 
   int mid, inv_count = 0; 
   if (right > left) { 
      mid = (right + left) / 2; 

      inv_count += mergeSort(left, mid);
      inv_count += mergeSort(mid + 1, right);
      inv_count += _merge(left, mid + 1, right);
   }

   return inv_count; 
}

int _merge(int left, int mid, int right){ 
   int i, j, k; 
   int inv_count = 0; 
  
   i = left; /* i is index for left subarray*/
   j = mid; /* j is index for right subarray*/
   k = left; /* k is index for resultant merged subarray*/
   while ((i <= mid - 1) && (j <= right)) { 
      if (arr[i] <= arr[j])
         temp[k++] = arr[i++]; 
      else {
          // The elements in the range [i: mid) are greater than arr[j]
         inv_count += mid - i;
         temp[k++] = arr[j++];
      }
   }
   // Copy remaining of elements using STL merge
   merge(arr + i, arr + mid, arr + j, arr + right + 1, temp + k);

   copy(temp + left, temp + right + 1, arr + left);
  
   return inv_count; 
}

int main(){
   int n, t;
   cin >> t;
   while(t--){
      cin >> n;
      for(int i = 0; i < n; i++)
         cin >> arr[i];
      int ans = mergeSort(0, n - 1); 
      cout << "Optimal train swapping takes " << ans << " swaps.\n";
   }
   return 0; 
}
