/**
 * Segment Tree Beats
 *
 * Operations:
 *  - a[i] = min(a[i], x), for l <= i <= r
 *  - a[i] = max(a[i], x), for l <= i <= r
 *  - a[i] = a[i] + x, for l <= i <= r
 * 	- sum_{i = l}^r a[i]
 *
 * Complexity: O((n + Q) lg^2 n)
 *
 * https://judge.yosupo.jp/problem/range_chmin_chmax_add_range_sum
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using vll = vector<ll>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

struct SegTree{
  struct Node {
    ll sum = 0;   // Sum tag
    ll max1 = 0;  // Max value
    ll max2 = 0;  // Second Max value
    ll maxc = 0;  // Max value count
    ll min1 = 0;  // Min value
    ll min2 = 0;  // Second Min value
    ll minc = 0;  // Min value count
    ll lazy = 0;  // Lazy tag
  };

  #define CL(v) (v << 1)
  #define CR(v) ((v << 1) | 1)
  const ll INF = LLONG_MAX / 2;
  int n;
  vector<Node> T;
  vector<ll> A;

  SegTree(int n2, vll B = vll()): n(1 << (int)ceil(log2(n2))), T(2 * n), A(B){
    A.resize(n);
    build(1, 0, n - 1);
  }

  private:
  void merge(int t) {
    // sum
    T[t].sum = T[CL(t)].sum + T[CR(t)].sum;

    // max
    if (T[CL(t)].max1 == T[CR(t)].max1) {
      T[t].max1 = T[CL(t)].max1;
      T[t].max2 = max(T[CL(t)].max2, T[CR(t)].max2);
      T[t].maxc = T[CL(t)].maxc + T[CR(t)].maxc;
    } else {
      if (T[CL(t)].max1 > T[CR(t)].max1) {
        T[t].max1 = T[CL(t)].max1;
        T[t].max2 = max(T[CL(t)].max2, T[CR(t)].max1);
        T[t].maxc = T[CL(t)].maxc;
      } else {
        T[t].max1 = T[CR(t)].max1;
        T[t].max2 = max(T[CL(t)].max1, T[CR(t)].max2);
        T[t].maxc = T[CR(t)].maxc;
      }
    }

    // min
    if (T[CL(t)].min1 == T[CR(t)].min1) {
      T[t].min1 = T[CL(t)].min1;
      T[t].min2 = min(T[CL(t)].min2, T[CR(t)].min2);
      T[t].minc = T[CL(t)].minc + T[CR(t)].minc;
    } else {
      if (T[CL(t)].min1 < T[CR(t)].min1) {
        T[t].min1 = T[CL(t)].min1;
        T[t].min2 = min(T[CL(t)].min2, T[CR(t)].min1);
        T[t].minc = T[CL(t)].minc;
      } else {
        T[t].min1 = T[CR(t)].min1;
        T[t].min2 = min(T[CL(t)].min1, T[CR(t)].min2);
        T[t].minc = T[CR(t)].minc;
      }
    }
  }

  void push_add(int t, int tl, int tr, ll v) {
    if (v == 0) { return; }
    T[t].sum += (tr - tl + 1) * v;
    T[t].max1 += v;
    if (T[t].max2 != -INF) { T[t].max2 += v; }
    T[t].min1 += v;
    if (T[t].min2 != INF) { T[t].min2 += v; }
    T[t].lazy += v;
  }

  // corresponds to a chmin update
  void push_max(int t, ll v, bool l) {
    if (v >= T[t].max1) { return; }
    T[t].sum -= T[t].max1 * T[t].maxc;
    T[t].max1 = v;
    T[t].sum += T[t].max1 * T[t].maxc;
    if (l) {
      T[t].min1 = T[t].max1;
    } else {
      if (v <= T[t].min1) {
        T[t].min1 = v;
      } else if (v < T[t].min2) {
        T[t].min2 = v;
      }
    }
  }

  // corresponds to a chmax update
  void push_min(int t, ll v, bool l) {
    if (v <= T[t].min1) { return; }
    T[t].sum -= T[t].min1 * T[t].minc;
    T[t].min1 = v;
    T[t].sum += T[t].min1 * T[t].minc;
    if (l) {
      T[t].max1 = T[t].min1;
    } else {
      if (v >= T[t].max1) {
        T[t].max1 = v;
      } else if (v > T[t].max2) {
        T[t].max2 = v;
      }
    }
  }

  void pushdown(int t, int tl, int tr) {
    if (tl == tr) return;
    // sum
    int tm = (tl + tr) >> 1;
    push_add(CL(t), tl, tm, T[t].lazy);
    push_add(CR(t), tm + 1, tr, T[t].lazy);
    T[t].lazy = 0;

    // max
    push_max(CL(t), T[t].max1, tl == tm);
    push_max(CR(t), T[t].max1, tm + 1 == tr);

    // min
    push_min(CL(t), T[t].min1, tl == tm);
    push_min(CR(t), T[t].min1, tm + 1 == tr);
  }

  void build(int t, int tl, int tr) {
    T[t].lazy = 0;
    if (tl == tr) {
      T[t].sum = T[t].max1 = T[t].min1 = A[tl];
      T[t].maxc = T[t].minc = 1;
      T[t].max2 = -INF;
      T[t].min2 = INF;
      return;
    }

    int tm = (tl + tr) >> 1;
    build(CL(t), tl, tm);
    build(CR(t), tm + 1, tr);
    merge(t);
  }

  void update_add(int l, int r, ll v, int t, int tl, int tr) {
    if (r < tl || tr < l) { return; }
    if (l <= tl && tr <= r) {
      push_add(t, tl, tr, v);
      return;
    }
    pushdown(t, tl, tr);

    int tm = (tl + tr) >> 1;
    update_add(l, r, v, CL(t), tl, tm);
    update_add(l, r, v, CR(t), tm + 1, tr);
    merge(t);
  }

  void update_chmin(int l, int r, ll v, int t, int tl, int tr) {
    if (r < tl || tr < l || v >= T[t].max1) { return; }
    if (l <= tl && tr <= r && v > T[t].max2) {
      push_max(t, v, tl == tr);
      return;
    }
    pushdown(t, tl, tr);

    int tm = (tl + tr) >> 1;
    update_chmin(l, r, v, CL(t), tl, tm);
    update_chmin(l, r, v, CR(t), tm + 1, tr);
    merge(t);
  }

  void update_chmax(int l, int r, ll v, int t, int tl, int tr) {
    if (r < tl || tr < l || v <= T[t].min1) { return; }
    if (l <= tl && tr <= r && v < T[t].min2) {
      push_min(t, v, tl == tr);
      return;
    }
    pushdown(t, tl, tr);

    int tm = (tl + tr) >> 1;
    update_chmax(l, r, v, CL(t), tl, tm);
    update_chmax(l, r, v, CR(t), tm + 1, tr);
    merge(t);
  }

  ll query_sum(int l, int r, int t, int tl, int tr) {
    if (r < tl || tr < l) { return 0; }
    if (l <= tl && tr <= r) { return T[t].sum; }
    pushdown(t, tl, tr);

    int tm = (tl + tr) >> 1;
    return query_sum(l, r, CL(t), tl, tm) +
          query_sum(l, r, CR(t), tm + 1, tr);
  }

  public:
  void UpdateAdd(int l, int r, ll v){
    update_add(l, r, v, 1, 0, n - 1);
  }

  void UpdateChmin(int l, int r, ll v){
    update_chmin(l, r, v, 1, 0, n - 1);
  }

  void UpdateChmax(int l, int r, ll v){
    update_chmax(l, r, v, 1, 0, n - 1);
  }

  ll QuerySum(int l, int r){
    return query_sum(l, r, 1, 0, n - 1);
  }
};

int main() {
  cin.tie(0)->sync_with_stdio(0);
	int n, Q;

	cin >> n >> Q;
  vll A(n);
	for (int i = 0; i < n; i++)
    cin >> A[i];
  SegTree seg(n, A);

	for (int q = 0; q < Q; q++) {
		int t;
		cin >> t;
		if (t == 0) {
			int l, r;
			ll x;
			cin >> l >> r >> x;
			seg.UpdateChmin(l, r - 1, x);
		} else if (t == 1) {
			int l, r;
			ll x;
			cin >> l >> r >> x;
			seg.UpdateChmax(l, r - 1, x);
		} else if (t == 2) {
			int l, r;
			ll x;
			cin >> l >> r >> x;
			seg.UpdateAdd(l, r - 1, x);
		} else if (t == 3) {
			int l, r;
			cin >> l >> r;
			cout << seg.QuerySum(l, r - 1) << '\n';
		}
	}
}
