/**
 * Segment Tree - Find the k-th one
 *
 * Time Complexity:
 *  - Build: O(nn)
 *  - Query/Flip: O(log n)
 *
 * Space Complexity : O(n)
 *
 * https://codeforces.com/edu/course/2/lesson/4/2/practice/contest/273278/problem/B
 *
 * Status: weakly tested
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

struct KthOne{
  #define CL(v) (v << 1)
  #define CR(v) ((v << 1) | 1)
  int n;
  vector<int> t;

  KthOne(int n2): n(1 << (int)ceil(log2(n2))),
   t(n << 1){}

  void Build(vector<int> a){
    a.resize(n, 0);
    copy_n(a.begin(), n, t.begin() + n);
    for (int i = n - 1; i > 0; --i)
      t[i] = t[CL(i)] + t[CR(i)];
  }

  void Flip(int p) {  // Flip value at position p
    for (t[p += n] ^= 1; p > 1; p >>= 1)
      t[p >> 1] = t[p] + t[p ^ 1];
  }

  int Query(int k){
    return query(1, 0, n - 1, k);
  }

  int query(int v, int tl, int tr, int k){
    if(tl == tr)
      return k == 1 ? tl : -1;
    int tm = (tl + tr) >> 1;
    if(t[CL(v)] >= k)
      return query(CL(v), tl, tm, k);
    return query(CR(v), tm + 1, tr, k - t[CL(v)]);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, tp, k, pos;
  cin >> n >> q;

  KthOne seg(n);

  vector<int> v(n);
  FOR(i, n)
    cin >> v[i];

  seg.Build(v);

  FOR(i, q){
    cin >> tp;
    switch(tp){
      case 1:
        cin >> pos;
        seg.Flip(pos);
        break;
      case 2:
        cin >> k;
        cout << seg.Query(k + 1) << endl;
    }
  }
}
