/**
 * Segment Tree - Point Set, Range Delete
 *
 * - RangeDel(l, r, p): deletes all elements in the range [l, r] with value
 * less equal to p, and returns the number of deleted elements
 * - SetPos(i, p): sets the element value at position i to p
 *
 * Time Complexity:
 *  - Build: O(n)
 *  - Update: O(log n) amortized
 *
 * Space Complexity : O(n)
 *
 * https://codeforces.com/edu/course/2/lesson/4/4/practice/contest/274684/problem/E
 *
 */

#include <bits/stdc++.h>

using namespace std;

template <class T>
struct SegmentTree{
  int n;
  vector<int> t;
  #define CL(v) (v << 1)
  #define CR(v) (v << 1 | 1)
  #define op min
  const T NIL = numeric_limits<T>::max();

  SegmentTree(int n2): n(1 << (int)ceil(log2(n2))), t(n << 1, NIL){}

  void Build(vector<T> a){
    a.resize(n, NIL);
    copy_n(a.begin(), n, t.begin() + n);
    for (int v = n - 1; v > 0; --v)
      t[v] = op(t[CL(v)], t[CR(v)]);
  }

  void SetPos(int p, T value) {  // Set value at position p
    for (t[p += n] = value; p > 1; p >>= 1)
      t[p >> 1] = op(t[p], t[p ^ 1]);
  }

  int RangeDel(int l, int r, int val){
    return rangeDel(1, 0, n - 1, l, r, val);
  }

  private:
  int rangeDel(int v, int tl, int tr, int l, int r, T val){
    if(tl > r || tr < l || t[v] > val)
      return 0;
    if(tl == tr)
      return t[v] = NIL, 1;
    int tm = (tl + tr) >> 1;
    int ans = rangeDel(CL(v), tl, tm, l, r, val) + rangeDel(CR(v), tm + 1, tr, l, r, val);
    t[v] = op(t[CL(v)], t[CR(v)]);
    return ans;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q;
  cin >> n >> q;

  SegmentTree<int> seg(n);
  seg.Build(vector<int>());

  while(q--){
    int tp, i, h, l, r, p;
    cin >> tp;
    if(tp == 1){
      cin >> i >> h;
      seg.SetPos(i, h);
    }else{
      cin >> l >> r >> p; r--;
      cout << seg.RangeDel(l, r, p) << '\n';
    }
  }
  return 0;
}
