/**
 * Iterative Segment Tree - Point Add, Range Sum
 *
 * Operations:
 *  - Range Query - O(log n)
 *  - Point Update - O(log n)
 *
 * Space Complexity : O(n)
 *
 * https://judge.yosupo.jp/problem/point_add_range_sum
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T, class OP>
struct SegmentTree{
  int n;
  vector<T> t;
  OP op;

  SegmentTree(int n): n(n), t(n << 1){}

  void Set(int i, T vl){
    t[i + n] = vl;
  }

  void Build() {  // Build the tree
    for (int i = n - 1; i > 0; --i)
      t[i] = op(t[i << 1], t[i << 1 | 1]);
  }

  void Update(int p, T value) {  // Increment value at position p
    for (t[p += n] += value; p > 1; p >>= 1)
      t[p >> 1] = op(t[p], t[p ^ 1]);
  }

  T Query(int l, int r) {  // Sum on interval [l, r)
    T res = 0;
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
      if (l & 1) res = op(res, t[l++]);
      if (r & 1) res = op(res, t[--r]);
    }
    return res;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, val, tp, p, x, l, r;
  cin >> n >> q;

  SegmentTree<ll, plus<ll>> seg(n);

  FOR(i, n){
    cin >> val;
    seg.Set(i, val);
  }
  seg.Build();

  FOR(i, q){
    cin >> tp;
    switch(tp){
      case 0:
        cin >> p >> x;
        seg.Update(p, x);
        break;
      case 1:
        cin >> l >> r;
        cout << seg.Query(l, r) << endl;
    }
  }
}
