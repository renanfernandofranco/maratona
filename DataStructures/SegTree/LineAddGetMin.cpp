/**
 * Li Chao Tree using Dynamic Segment Tree - Line Add, Get Min
 *
 * Complexity:
 *  - Time: O(log |Max Range|) per operation
 *  - Space: O((N + Q) * log |Max Range|)
 *
 * https://judge.yosupo.jp/problem/line_add_get_min
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T, class U>
struct LiChao{
  const static T INF = numeric_limits<T>::max();
  const static U MAXN = numeric_limits<U>::max() / 2 + 1;

  struct Line{
    T a, b;
    Line(T a = 0, T b = INF) : a(a), b(b){}
    T operator() (T x) { return a * x + b; }
  };

  struct Vertex{
    Line d;
    int c[2] = {-1, -1};
    Vertex(Line d = Line()): d(d){}
  };

  vector<Vertex> t;

  LiChao(int n = 1): t(1) {t.reserve(2 * n * ceil(log2(MAXN) + 1));}

  void extend(int v){
    if(t[v].c[0] == -1)
      FOR(i, 2)
        t[v].c[i] = t.size(), t.push_back(Vertex(t[v].d));
  }

  void AddLine(Line nw){
    U tl = -MAXN, tr = MAXN;
    for(int v = 0; v != -1; ){
      U tm = (tl + tr) / 2;
      bool lef = nw(tl) < t[v].d(tl);
      bool mid = nw(tm) < t[v].d(tm);

      if(tr - tl != 1) extend(v);
      if(mid) swap(t[v].d, nw);

      v = t[v].c[lef == mid];
      (lef == mid ? tl : tr) = tm;
    }
  }

  T GetMin(T x) {
    T ret = INF;
    U tl = -MAXN, tr = MAXN;
    for(int v = 0; v != -1; ){
      ret = min(ret, t[v].d(x));

      U tm = (tl + tr) / 2;
      v = t[v].c[x >= tm];
      (x >= tm ? tl : tr) = tm;
    }
    return ret;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, tp;
  ll a, b, x;
  cin >> n >> q;

  LiChao<ll, int> liChao(n + q);

  FOR(i, n){
    cin >> a >> b;
    liChao.AddLine({a, b});
  }

  FOR(i, q){
    cin >> tp;
    switch(tp){
      case 0:
        cin >> a >> b;
        liChao.AddLine({a, b});
        break;
      case 1:
        cin >> x;
        cout << liChao.GetMin(x) << endl;
    }
  }
}
