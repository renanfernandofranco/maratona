/**
 * Segment Tree - First element greater equal than a given amount
 *
 * Operations:
 *  - Range GTE - O(log n)
 *  - Point Add - O(log n)
 *
 * Space Complexity : O(n)
 *
 * https://cses.fi/problemset/task/1143/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T, class OP>
struct SegmentTree{
  #define CL(v) (v << 1)
  #define CR(v) ((v << 1) | 1)
  int n;
  vector<T> t;
  OP op;

  SegmentTree(int n2): n(1 << (int)ceil(log2(n2))), t(n << 1){}

  void Set(int i, T vl){
    t[i + n] = vl;
  }

  void Build() {  // Build the tree
    for (int i = n - 1; i > 0; --i)
      t[i] = op(t[i << 1], t[i << 1 | 1]);
  }

  void Update(int p, T value) {  // Increment value at position p
    for (t[p += n] += value; p > 1; p >>= 1)
      t[p >> 1] = op(t[p], t[p ^ 1]);
  }

  int gte(int v, int tl, int tr, int l, int r, T val){
    if(tl == tr)
      return t[v] >= val ? tl : -1;

    int ret = -1;
    int tm = (tl + tr) >> 1;
    if(l <= tm && t[CL(v)] >= val)
      ret = gte(CL(v), tl, tm, l, r, val);

    if(ret == -1 && r >= tm + 1 && t[CR(v)] >= val)
      ret = gte(CR(v), tm + 1, tr, l, r, val);
    return ret;
  }

  // First element greater than a given amount in range [l, r]
  int GTE(int l, int r, T val) {
    return gte(1, 0, n - 1, l, r, val);
  }

};

struct OP{
  int operator ()(const int a, const int b){
    return max(a, b);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, val;
  cin >> n >> q;

  SegmentTree<int, OP> seg(n);

  FOR(i, n){
    cin >> val;
    seg.Set(i, val);
  }
  seg.Build();

  FOR(i, q){
    cin >> val;
    int ans = seg.GTE(0, n - 1, val) + 1;
    if(ans)
      seg.Update(ans - 1, -val);
    cout << ans << " \n"[i == q - 1];
  }
}
