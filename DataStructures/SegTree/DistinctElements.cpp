/**
 * Find the number of distinct elements in range [l, r]
 *
 * Using Persistent Segment Tree
 *
 * Complexity:
 *  - Build: O(n lg n)
 *  - Query: O(lg n)
 *  - Space: O(n lg n)
 *
 * Problem: https://cses.fi/problemset/task/1734/
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'
#define all(a) a.begin(), a.end()

using vi = vector<int>;

struct Distincts{
  struct Vertex {
    int l, r, sum;
  };

  int n;
  int cnt = -1;
  vector<Vertex> t;
  vector<int> vers;

  template<class T>
  Distincts (vector<T> v): n(v.size()){
    // Compression of coordinate
    auto mp = v;
    sort(all(mp));
    mp.resize(unique(all(mp)) - mp.begin());
    FOR(i, n)
      v[i] = lower_bound(all(mp), v[i]) - mp.begin();

    // create SegTree
    t.resize(2 * n + n * ceil(log2(n << 1)));
    vers.reserve(n + 1);
    vers.push_back(build(0, n - 1));

    vi prev(n, 0);
    FOR(i, n){
      inc(prev[v[i]]);
      prev[v[i]] = i + 1; // 1-based indexing
    }
  }

  int merge(int v, int l, int r){
    t[v].l = l;
    t[v].r = r;
    t[v].sum = t[l].sum + t[r].sum;
    return v;
  }

  void inc(int pos){
    vers.push_back(inc(vers.back(), 0, n - 1, pos));
  }

  int build(int tl, int tr) {
    if (tl == tr)
      return t[++cnt].sum = 0, cnt;

    int tm = (tl + tr) >> 1;
    return merge(++cnt, build(tl, tm), build(tm + 1, tr));
  }

  int inc(int v, int tl, int tr, int pos) {
    if (tl == tr)
      return t[++cnt].sum = t[v].sum + 1, cnt;

    int tm = (tl + tr) >> 1;

    if (pos <= tm)
      return merge(++cnt, inc(t[v].l, tl, tm, pos), t[v].r);
    else
      return merge(++cnt, t[v].l, inc(t[v].r, tm + 1, tr, pos));
  }

  int prefSum(int VL, int VR, int tl, int tr, int last) {
    if (last >= tr)
      return t[VR].sum - t[VL].sum;

    int tm = (tl + tr) >> 1;

    int ret = prefSum(t[VL].l, t[VR].l, tl, tm, last);
    if (last >= tm + 1)
      ret += prefSum(t[VL].r, t[VR].r, tm + 1, tr, last);
    return ret;
  }

  // Find the number of distinct elements in range [l, r]
  int Query(int l, int r){ // Indexed by 1
    if (l > r)
      return 0;
    return prefSum(vers[l - 1], vers[r], 0, n - 1, l - 1);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q, l, r;

  cin >> n >> q;

  vi v(n);
  FOR(i, n)
    cin >> v[i];

  Distincts ed(v);

  FOR(i, q){
    cin >> l >> r;
    cout << ed.Query(l, r) << endl;
  }
}
