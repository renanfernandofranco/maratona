/**
 * Iterative Segment Tree - Point Set, Range Composite
 *
 * Each position stores a linear function and given two
 * positions l and r, this algorithm can answer queries of
 * type f_r (f _... (f_l (x))) mod M. See that this operation
 * is non-commutative.
 *
 * Operations:
 *  - Range Query - O(log n)
 *  - Point Update - O(log n)
 *
 * Space Complexity : O(n)
 *
 * https://judge.yosupo.jp/problem/point_set_range_composite
 *
 */

#include<bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

const int MD = 998'244'353;

template <class T = int>
struct MOD{
  T v;

  MOD(T vl = 0){ this->operator=(vl); }

  void operator *= (const MOD& m2){ (*this) = this->operator*(m2); }

  void operator += (const MOD& m2){ (*this) = this->operator+(m2); }

  void operator -= (const MOD& m2){ (*this) = this->operator-(m2); }

  void operator = (T vl){ v = vl; }

  bool operator == (const MOD& m2){ return v == m2.v; }

  MOD operator * (const MOD& m2) const{
    return v * 1LL * m2.v % MD;
  }

  MOD operator + (const MOD& m2) const{
    T tmp = v + m2.v;
    return tmp >= MD ? tmp - MD : tmp;
  }

  MOD operator - (const MOD& m2) const{
    T tmp = v - m2.v;
    return tmp < 0 ? tmp + MD : tmp;
  }
};

struct Function{
  MOD<int> a = 1, b = 0;
  Function operator +(const Function& f2) const{
    return Function{a * f2.a, f2.a * b + f2.b};
  }

  int operator ()(int x){
    return (a * x + b).v;
  }
};

template <class T, class OP>
struct SegmentTree{
  int n;
  vector<T> t;
  OP op;

  SegmentTree(int n): n(n), t(n << 1){}

  void Set(int i, T vl){
    t[i + n] = vl;
  }

  void Build() {  // Build the tree
    for (int i = n - 1; i > 0; --i)
      t[i] = op(t[i << 1], t[i << 1 | 1]);
  }

  void Update(int p, T value) { // Set value at position p
    for (t[p += n] = value; p >>= 1; )
      t[p] = op(t[p << 1], t[p << 1 | 1]);
  }

  T Query(int l, int r) { // Composite of the interval [l, r)
    T resl, resr;
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
      if (l & 1) resl = op(resl, t[l++]);
      if (r & 1) resr = op(t[--r], resr);
    }
    return op(resl, resr);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, tp, p, x, l, r, a, b;
  cin >> n >> q;

  SegmentTree<Function, plus<Function>> seg(n);

  FOR(i, n){
    cin >> a >> b;
    seg.Set(i, {a, b});
  }
  seg.Build();

  FOR(i, q){
    cin >> tp;
    switch(tp){
      case 0:
        cin >> p >> a  >> b;
        seg.Update(p, {a, b});
        break;
      case 1:
        cin >> l >> r >> x;
        cout << seg.Query(l, r)(x) << endl;
    }
  }
}
