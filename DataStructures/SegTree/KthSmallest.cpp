/**
 * Find K-th element in range [l, r] without
 * excluding repetitions - Indexed by 1
 *
 * Using Persistent Segment Tree
 *
 * Complexity:
 *  - Build: O(n lg n)
 *  - Query: O(lg n)
 *  - Space: O(n lg(num of distinct elements))
 *
 * Problem: https://judge.yosupo.jp/problem/range_kth_smallest
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define endl '\n'
#define all(a) a.begin(), a.end()

using vi = vector<int>;

template<class T>
struct KthSmallest{
  struct Vertex {
    int l, r, sum;
  };

  int n;
  int cnt = -1;
  vector<Vertex> t;
  vector<int> vers;
  vector<T> mp;

  KthSmallest (vector<T> v): mp(v){
    // Compression of coordinate
    sort(all(mp));
    n = unique(all(mp)) - mp.begin();
    mp.resize(n);
    int numUpdates = v.size();
    FOR(i, numUpdates)
      v[i] = lower_bound(all(mp), v[i]) - mp.begin();

    // create SegTree
    t.resize(2 * n + numUpdates * ceil(log2(n << 1)));
    vers.reserve(numUpdates + 1);
    vers.push_back(build(0, n - 1));

    FOR(i, numUpdates)
      extend(v[i]);
  }

  int merge(int v, int l, int r){
    t[v].l = l;
    t[v].r = r;
    t[v].sum = t[l].sum + t[r].sum;
    return v;
  }

  void extend(int pos){
    vers.push_back(extend(vers.back(), 0, n - 1, pos));
  }

  int build(int tl, int tr) {
    if (tl == tr)
      return t[++cnt].sum = 0, cnt;

    int tm = (tl + tr) >> 1;
    return merge(++cnt, build(tl, tm), build(tm + 1, tr));
  }

  int extend(int v, int tl, int tr, int pos) {
    if (tl == tr)
      return t[++cnt].sum = t[v].sum + 1, cnt;

    int tm = (tl + tr) >> 1;

    if (pos <= tm)
      return merge(++cnt, extend(t[v].l, tl, tm, pos), t[v].r);
    else
      return merge(++cnt, t[v].l, extend(t[v].r, tm + 1, tr, pos));
  }

  int findKth(int VL, int VR, int tl, int tr, int k) {
    if (tl == tr)
      return tl;

    int tm = (tl + tr) >> 1;
    int left_count = t[t[VR].l].sum - t[t[VL].l].sum;

    if (left_count >= k)
      return findKth(t[VL].l, t[VR].l, tl, tm, k);

    return findKth(t[VL].r, t[VR].r, tm + 1, tr, k - left_count);
  }

  // Find K-th in range [versL, versR] - Indexed by 1
  T Query(int versL, int versR, int k){ // k = 1 is the smallest
    return mp[findKth(vers[versL - 1], vers[versR], 0, n - 1, k)];
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q, l, r, k;

  cin >> n >> q;

  vi v(n);

  FOR(i, n)
    cin >> v[i];

  KthSmallest<int> kth(v);

  FOR(i, q){
    cin >> l >> r >> k;
    cout << kth.Query(l + 1, r, k + 1) << endl;
  }
}
