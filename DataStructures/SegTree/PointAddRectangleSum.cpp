/**
 * Iterative Segment Tree 2D - Point Add, Rectangle Sum
 *
 * Operations:
 *  - Range Query - O(lg n * lg m)
 *  - Point Update - O(lg n * lg m)
 *
 * Space Complexity : O(4nm)
 *
 * Problem: https://cses.fi/problemset/task/1739/
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T, class OP>
struct SegmentTree{
  int n;
  vector<T> t;
  OP op;

  SegmentTree(int n): n(n), t(n << 1){}

  void Set(int i, T vl){
    t[i + n] = vl;
  }

  T operator [] (int i){
    return t[i + n];
  }

  void Build() {  // Build the tree
    for (int i = n - 1; i > 0; --i)
      t[i] = op(t[i << 1], t[i << 1 | 1]);
  }

  void Update(int p, T value) {  // Increment value at position p
    for (t[p += n] += value; p > 1; p >>= 1)
      t[p >> 1] = op(t[p], t[p ^ 1]);
  }

  T Query(int l, int r) {  // Sum on interval [l, r)
    T res = 0;
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
      if (l & 1) res = op(res, t[l++]);
      if (r & 1) res = op(res, t[--r]);
    }
    return res;
  }
};

template <class T, class OP>
struct SegmentTree2D{

  int n, m;
  vector<SegmentTree<T, OP>> t;
  OP op;

  SegmentTree2D(int n, int m): n(n), m(m), t(n << 1, SegmentTree<T, OP>(m)){}

  void Set(int xp, int yp, T val){
    t[xp + n].Set(yp, val);
  }

  void Build() {  // Build the tree
    for(int x = n; x < (n << 1); x++)
      t[x].Build();
    for (int x = n - 1; x > 0; x--){
      FOR(y, m)
        t[x].Set(y, op(t[x << 1][y], t[x << 1 | 1][y]));
      t[x].Build();
    }
  }

  void Update(int xp, int yp, T value) {
    for (xp += n; xp > 0; xp >>= 1)
      t[xp].Update(yp, value);
  }

  // Count elements in square [xl, xr) * [yl, yr)
  T Query(int xl, int yl, int xr, int yr) {
    T resl = T(), resr = T();
    for (xl += n, xr += n; xl < xr; xl >>= 1, xr >>= 1) {
      if (xl & 1)
        resl = op(resl, t[xl++].Query(yl, yr));
      if (xr & 1)
        resr = op(t[--xr].Query(yl, yr), resr);
    }
    return op(resl, resr);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q;
  cin >> n >> q;

  SegmentTree2D<int, plus<int>> seg(n, n);
  vector<vector<char>> has(n, vector<char>(n, false));

  FOR(y, n){
    string line;
    cin >> line;
    FOR(x, n)
      if(line[x] == '*')
        seg.Set(x, y, has[x][y] = true);
  }

  seg.Build();

  FOR(i, q){
    int tp, xl, xr, yl, yr;
    cin >> tp;
    switch(tp){
      case 1:
        cin >> yl >> xl; xl--; yl--;
        has[xl][yl] ^= 1;
        seg.Update(xl, yl, has[xl][yl] ? 1 : -1);
        break;
      case 2:
        cin >> yl >> xl >> yr >> xr;
        cout << seg.Query(xl - 1, yl - 1, xr, yr) << endl;
    }
  }
}
