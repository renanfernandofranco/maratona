/**
 * Count how many elements larger than K are in the range [tl, tr]
 * of an array using Merge-sort Tree
 *
 * This algorithm does not support update
 *
 * Complexity:
 *  - Time Build: O(n * log n)
 *  - Time Query: O(log² n)
 *  - Space: O(n * log n)
 *
 * KQUERY Spoj
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using vvi = vector<vi>;
#define all(a) a.begin(), a.end()

struct MergeSortTree{
  int n;
  vvi tree;
  #define CL(v) (v << 1)
  #define CR(v) (v << 1 | 1)

  MergeSortTree(vi a): n(a.size()), tree(n << 2){
    build(a, 1, 0, n - 1);
  }

  void build(vi& a, int v, int tl, int tr){
    if(tl == tr)
      return void(tree[v].push_back(a[tl]));
    int tm = tl + tr >> 1;
    build(a, CL(v), tl, tm);
    build(a, CR(v), tm + 1, tr);
    merge(all(tree[CL(v)]), all(tree[CR(v)]), back_inserter(tree[v]));
  }

  int query(int v, int tl, int tr, int l, int r, int k){
    if(l > tr || r < tl) return 0;
    if(l <= tl && tr <= r)
      return tree[v].end() - upper_bound(all(tree[v]), k);
    int tm = tl + tr >> 1;
    return query(CL(v), tl, tm, l, r, k) + query(CR(v), tm + 1, tr, l, r, k);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, l, r, k;
  cin >> n;

  vi v(n);

  for(int i = 0; i < n; i++)
    cin >> v[i];

  MergeSortTree mergeTree(v);
  cin >> q;

  while(q--){
    cin >> l >> r >> k;
    cout << mergeTree.query(1, 0, n - 1, l - 1, r - 1, k) << '\n';
  }
  return 0;
}
