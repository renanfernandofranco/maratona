/**
 * Segment Tree with modular update
 *
 * Operations:
 *  - a[i] %= x, for l <= i <= r
 *  - a[i] = x, for l <= i <= r
 * 	- sum_{i = l}^r a[i]
 *
 * Complexity: O(n lg^2 n)
 *
 * https://codeforces.com/contest/438/problem/D
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T>
struct SegmentTree{
  #define CL(v) (v << 1)
  #define CR(v) ((v << 1) | 1)
  int n;
  const T NIL = 0;
  const T NONE = -1;
  vector<T> t, mx, mn, lazySet;

  SegmentTree(int n2): n(1 << (int)ceil(log2(n2))), t(n << 1),
    mx(n << 1, 0),  mn(n << 1, NONE), lazySet(n << 1, NONE){}

  void calc(int i){
    mx[i] = max(mx[CL(i)], mx[CR(i)]);
    mn[i] = min(mn[CL(i)], mn[CR(i)]);
    t[i] = t[CL(i)] + t[CR(i)];
  }

  void Build(vector<T> a){
    a.resize(n, NIL);
    copy_n(a.begin(), n, t.begin() + n);
    copy_n(a.begin(), n, mx.begin() + n);
    copy_n(a.begin(), n, mn.begin() + n);
    for (int i = n - 1; i > 0; --i)
      calc(i);
  }

  void set_node(int v, int tl, int tr, T val){
    lazySet[v] = val;
    t[v] = (tr - tl + 1) * val;
    mn[v] = mx[v] = val;
  }

  void push(int v, int tl, int tr){
    int tm = (tl + tr) >> 1;
    if(lazySet[v] != NONE){
      set_node(CL(v), tl, tm, lazySet[v]);
      set_node(CR(v), tm + 1, tr, lazySet[v]);
      lazySet[v] = NONE;
    }
  }

  void SetVal(int l, int r, T val){
    set_val(1, 0, n - 1, l, r, val);
  }

  void ModVal(int l, int r, T val){
    mod_val(1, 0, n - 1, l, r, val);
  }

  T QuerySum(int l, int r){
    return query_sum(1, 0, n - 1, l, r);
  }

  private:
  void set_val(int v, int tl, int tr, int l, int r, T val){
    if(l > tr || r < tl)
        return;
    if(tl >= l && tr <= r){
      set_node(v, tl, tr, val);
      return;
    }
    push(v, tl, tr);
    int tm = (tl + tr) >> 1;
    set_val(CL(v), tl, tm, l, min(r, tm), val);
    set_val(CR(v), tm + 1, tr, max(l, tm + 1), r, val);

    calc(v);
  }

  void mod_val(int v, int tl, int tr, int l, int r, T mod){
    if(l > tr || r < tl || mx[v] < mod)
        return;
    if(tl >= l && tr <= r && mx[v] == mn[v]){
      set_node(v, tl, tr, mx[v] % mod);
      return;
    }
    push(v, tl, tr);
    int tm = (tl + tr) >> 1;
    mod_val(CL(v), tl, tm, l, min(r, tm), mod);
    mod_val(CR(v), tm + 1, tr, max(l, tm + 1), r, mod);

    calc(v);
  }

  T query_sum(int v, int tl, int tr, int l, int r){
    if(l > tr || r < tl)
        return 0;
    if(tl >= l && tr <= r)
      return t[v];

    int tm = (tl + tr) >> 1;
    push(v, tl, tr);
    T ret = query_sum(CL(v), tl, tm, l, min(r, tm));
    ret += query_sum(CR(v), tm + 1, tr, max(l, tm + 1), r);
    return ret;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, tp, l, r, x, mod, pos;
  cin >> n >> q;

  SegmentTree<ll> seg(n);

  vector<ll> v(n);
  FOR(i, n)
    cin >> v[i];

  seg.Build(v);

  FOR(i, q){
    cin >> tp;
    switch(tp){
      case 1:
        cin >> l >> r;
        cout << seg.QuerySum(l - 1, r - 1) << endl;
        break;
      case 2:
        cin >> l >> r >> mod;
        seg.ModVal(l - 1, r - 1, mod);
        break;
      case 3:
        cin >> pos >> x;
        seg.SetVal(pos - 1, pos - 1, x);
    }
  }
}
