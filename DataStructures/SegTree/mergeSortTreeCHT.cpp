/**
 * MergeSortTree of Convex Hulls from a set of lines
 *
 * Given a set of lines, answer queries of type:
 * Which is the first line where a point (X, Y) is strictly below?
 *
 * As CHT is of the minimum type, we need to multiples lines coefficient
 * and Y's coordinate by -1.
 *
 * Complexity:
 *  - Time : O((N + Q) * lg N), where N is the number of lines
 *  - Space : O(N lg N)
 *
 * Problem: https://codeforces.com/gym/471914/problem/B
 *
 * */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ii = pair<int, int>;
using vi = vector<int>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

struct line{
  ll m, b;
  line(ll m = 0, ll b = 0): m(m), b(b){}
  friend bool cross(line bb, line lb, line cur){
    return (cur.m - bb.m) * (bb.b - lb.b) >= (bb.b - cur.b) * (lb.m - bb.m);
  }
  ll operator ()(ll x){ return x * m + b; }
};

struct CHT{
  vector<line> v, all;

  CHT(){}
  CHT(line l): v({l}), all({l}){}

  void add(ll m, ll b){
    all.push_back(line(m, b));

    if(!v.empty() && v.back().m == m){
      if(v.back().b > b)
        v.pop_back();
      else
        return;
    }

    line cur(m, b);
    while(v.size() >= 2 && cross(v[v.size() - 2], v.back(), cur))
      v.pop_back();

    v.push_back(cur);
  }

  ll query(ll x){
    int l = 0;
    int r = v.size() - 1;
    while (l < r){
      int m = (l + r) >> 1;
      if (v[m](x) <= v[m + 1](x))
        r = m;
      else
        l = m + 1;
    }
    return v[l](x);
  }
};

struct SegmentTree{
  #define CL(v) (v << 1)
  #define CR(v) ((v << 1) | 1)
  int n;
  vector<CHT> t;

  SegmentTree(int n2): n(1 << (int)ceil(log2(n2))), t(n << 1){}

  void Set(int i, line vl){
    t[i + n] = CHT(vl);
  }

  CHT merge(CHT& cht1, CHT& cht2){
    CHT ret;
    vector<line> lines = cht1.all;
    lines.insert(lines.end(), cht2.all.begin(), cht2.all.end());

    // CHT only works if the lines are ordered by linear coefficents.
    sort(lines.begin(), lines.end(), [](auto l1, auto l2){return l1.m > l2.m;});
    for(auto l : lines)
      ret.add(l.m, l.b);

    return ret;
  }

  void Build() {  // Build the tree
    for (int i = n - 1; i > 0; --i)
      t[i] = merge(t[i << 1], t[i << 1 | 1]);
  }

  int lte(int v, int tl, int tr, int l, int r, int X, int Y){
    if(tl == tr)
      return t[v].query(X) < Y ? tl : -1;

    int ret = -1;
    int tm = (tl + tr) >> 1;
    if(l <= tm && t[CL(v)].query(X) < Y)
      ret = lte(CL(v), tl, tm, l, r, X, Y);

    if(ret == -1 && r >= tm + 1 && t[CR(v)].query(X) < Y)
      ret = lte(CR(v), tm + 1, tr, l, r, X, Y);
    return ret;
  }

  // First line in range [l, r] that point (X, Y) is above
  int LTE(int l, int r, int X, int Y) {
    return lte(1, 0, n - 1, l, r, X, Y);
  }

};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int t;
  cin >> t;

  vector<ii> points(t);
  FOR(i, t)
    cin >> points[i].first >> points[i].second;

  int n;
  cin >> n;

  SegmentTree seg(n);
  
  FOR(i, n){
    int a, b;
    cin >> a >> b;
    seg.Set(i, line(-a, -b));
  }

  seg.Build();

  vector<vi> ans(n);

  FOR(i, t){
    int ret = seg.LTE(0, n - 1, points[i].first, -points[i].second);
    if(ret != -1)
      ans[ret].push_back(i + 1);
  }

  FOR(i, n){
    cout << ans[i].size();
    for(auto e : ans[i])
      cout << ' ' << e;
    cout << '\n';
  }
}
