/**
 * Dynamic Segment Tree - Point Update, Range Sum
 *
 * Assuming that all elements belong to range [L, R]
 * and n >= W = R - L
 *
 * Operations:
 *  - Range Query - O(log W)
 *  - Point Update - O(log W)
 *
 * Space Complexity : O(2n * (1 + log2(W / n)))
 *
 * https://judge.yosupo.jp/problem/point_add_range_sum
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class R, class T, class OP>
struct DynamicSegTree{
  static const R INF = numeric_limits<R>::max() / 2;
  static const T NIL = 0;

  struct Vertex{
    T val = NIL;
    int c[2] = {0, 0};
  };

  vector<Vertex> t;
  R rootL, rootR, l, r;
  OP op;

  DynamicSegTree(R rootL = 0, R rootR = INF) :
    t(2), rootL(rootL), rootR(rootR) {}

  int get(int v, bool ch){
    if(!t[v].c[ch])
      t[v].c[ch] = t.size(), t.push_back(Vertex());
    return t[v].c[ch];
  }

  T val(int v){
    return v ? t[v].val : NIL;
  }

  void Update(R p, T x){
    Update(1, rootL, rootR, p, x);
  }

  void Update(int v, R tl, R tr, R p, T x) {
    if(tl == tr)
      return void(t[v].val += x);
    R tm = (tl + tr) >> 1;
    if(p <= tm)
      Update(get(v, 0), tl, tm, p, x);
    else
      Update(get(v, 1), tm + 1, tr, p, x);
    t[v].val = op(val(t[v].c[0]), val(t[v].c[1]));
  }

  T Query(R l, R r){
    this->l = l, this->r = r;
    return Query(1, rootL, rootR);
  }

  T Query(int v, R tl, R tr) {
    if(!v) return NIL;
    if(l <= tl && tr <= r) return t[v].val;

    R tm = (tl + tr) >> 1;
    T ret = NIL;
    if(l <= tm)
      ret = op(ret, Query(t[v].c[0], tl, tm));
    if(r >= tm + 1)
      ret = op(ret, Query(t[v].c[1], tm + 1, tr));
    return ret;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, val, tp, p, x, l, r;
  cin >> n >> q;

  DynamicSegTree<int, ll, plus<ll>> seg(0, n - 1);

  FOR(i, n){
    cin >> val;
    seg.Update(i, val);
  }

  FOR(i, q){
    cin >> tp;
    switch(tp){
      case 0:
        cin >> p >> x;
        seg.Update(p, x);
        break;
      case 1:
        cin >> l >> r;
        cout << seg.Query(l, r - 1) << endl;
    }
  }
}
