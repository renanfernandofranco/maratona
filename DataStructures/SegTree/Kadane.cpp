/**
 * Iterative Segment Tree - Kanade's Algorithm
 *
 * Given an interval [l, r), find the subarray
 * with the maximum sum contained in [l, r)
 *
 * Operations:
 *  - Range Query - O(log n)
 *  - Point Update - O(log n)
 *
 * Space Complexity : O(n)
 *
 * Problem: https://cses.fi/problemset/result/2607557/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T>
struct Data{
  T sum, pref, suff, ans;
  Data(T val = 0){
    sum = val;
    pref = suff = ans = max<T>(0, val); // allow empty subarray
  }
};

template <class T>
struct Combine{
  Data<T> operator() (Data<T> l, Data<T> r){
    Data<T> res;
    res.sum = l.sum + r.sum;
    res.suff = max(l.suff + r.sum, r.suff);
    res.pref = max(r.pref + l.sum, l.pref);
    res.ans = max(max(l.ans, r.ans), l.suff + r.pref);
    return res;
  }
};

template <class T, class OP>
struct SegmentTree{
  int n;
  vector<T> t;
  OP op;

  SegmentTree(int n): n(n), t(n << 1){}

  void Set(int i, T val){
    t[i + n] = val;
  }

  void Build() {  // Build the tree
    for (int i = n - 1; i > 0; --i)
      t[i] = op(t[i << 1], t[i << 1 | 1]);
  }

  void Update(int p, T value) { // Set value at position p
    for (t[p += n] = value; p >>= 1; )
      t[p] = op(t[p << 1], t[p << 1 | 1]);
  }

  T Query(int l, int r) { // Composite of the interval [l, r)
    T resl, resr;
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
      if (l & 1) resl = op(resl, t[l++]);
      if (r & 1) resr = op(t[--r], resr);
    }
    return op(resl, resr);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q;
  cin >> n >> q;
  SegmentTree<Data<ll>, Combine<ll>> kadane(n);

  FOR(i, n){
    int val;
    cin >> val;
    kadane.Set(i, val);
  }

  kadane.Build();

  FOR(i, q){
    int tp, l, r, p, val;
    cin >> tp;
    if(tp == 1){
      cin >> p >> val;
      kadane.Update(p - 1, val);
    }else{
      int l, r;
      cin >> l >> r;
      cout << kadane.Query(l - 1, r).pref << endl;
    }
  }
}
