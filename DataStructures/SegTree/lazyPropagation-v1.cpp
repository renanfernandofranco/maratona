/**
 * Segment Tree with Lazy Propagration
 *
 * Operations:
 *  - Range Sum Update - O(log n)
 *  - Range Sum Query - O(log n)
 *
 * Space Complexity : O(n)
 *
 * https://cses.fi/problemset/task/1651
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T>
struct SegmentTree{
  #define CL(v) (v << 1)
  #define CR(v) ((v << 1) | 1)
  int n;
  vector<T> t, lazySum;
  const T NIL = 0;

  SegmentTree(int n2): n(1 << (int)ceil(log2(n2))),
   t(n << 1), lazySum(n << 1, 0){}

  void Build(vector<T> a){
    a.resize(n, NIL);
    copy_n(a.begin(), n, t.begin() + n);
    for (int i = n - 1; i > 0; --i)
      t[i] = t[CL(i)] + t[CR(i)];
  }

  void push(int v, int tl, int tr){
    if(tl == tr)  return;

    int tm = (tl + tr) >> 1;
    if(lazySum[v] != 0){
      T val = lazySum[v];
      lazySum[v] = 0;
      lazySum[CL(v)] += val;
      lazySum[CR(v)] += val;

      t[CL(v)] += (tm - tl + 1) * val;
      t[CR(v)] += (tr - tm) * val;
    }
  }

  void Update(int l, int r, T val){
    update(1, 0, n - 1, l, r, val);
  }

  T Query(int l, int r){
    return query(1, 0, n - 1, l, r);
  }

  private:
  void update(int v, int tl, int tr, int l, int r, T val){
    push(v, tl, tr);
    if(l == tl && tr == r){
      lazySum[v] += val;
      t[v] += (tr - tl + 1) * val;
      return;
    }
    int tm = (tl + tr) >> 1;
    if(l <= tm)
      update(CL(v), tl, tm, l, min(r, tm), val);
    if(r >= tm + 1)
      update(CR(v), tm + 1, tr, max(l, tm + 1), r, val);

    t[v] = t[CL(v)] + t[CR(v)];
  }

  T query(int v, int tl, int tr, int l, int r){
    if(l == tl && tr == r)
      return t[v];
    int tm = (tl + tr) >> 1;
    push(v, tl, tr);
    T ret = 0;
    if(l <= tm)
      ret = query(CL(v), tl, tm, l, min(r, tm));
    if(r >= tm + 1)
      ret += query(CR(v), tm + 1, tr, max(l, tm + 1), r);
    return ret;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, tp, l, r, k, pos;
  cin >> n >> q;

  SegmentTree<ll> seg(n);

  vector<ll> v(n);
  FOR(i, n)
    cin >> v[i];

  seg.Build(v);

  FOR(i, q){
    cin >> tp;
    switch(tp){
      case 1:
        cin >> l >> r >> k;
        seg.Update(l - 1, r - 1, k);
        break;
      case 2:
        cin >> pos;
        cout << seg.Query(pos - 1, pos - 1) << endl;
    }
  }
}
