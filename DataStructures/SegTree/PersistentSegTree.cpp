/**
 * Fully Persistent Segment Tree - Point Add Range Query
 *
 * Complexity:
 *  - Build Time: O(n)
 *  - Query/Update Time: O(lg n)
 *  - Space: O(n + numUpdates * lg n)
 *
 * Problem: https://cses.fi/problemset/task/1737
 *
 */

#include <bits/stdc++.h>

using namespace std;
#define FOR(i, b) for(int i = 0; i < (b); i++)
using ll = long long;

template <class T, class OP>
struct SegTree{
  struct Vertex {
    int l, r;
    T val;
  };

  const T NIL = 0;
  int n, cnt = -1;
  vector<Vertex> t;
  vector<int> vers;
  OP op;

  SegTree (int n, int numUpdates, vector<T> v = vector<T>()): n(n){
    t.resize(2 * n + numUpdates * ceil(log2(n << 1)));
    v.resize(n, NIL);
    vers.reserve(numUpdates + 1);
    vers.push_back(build(0, n - 1, v));
  }

  int merge(int v, int l, int r){
    t[v].l = l;
    t[v].r = r;
    t[v].val = op(t[l].val, t[r].val);
    return v;
  }

  int build(int tl, int tr, vector<T>& v) {
    if (tl == tr)
      return t[++cnt].val = v[tl], cnt;

    int tm = (tl + tr) >> 1;
    return merge(++cnt, build(tl, tm, v), build(tm + 1, tr, v));
  }

  int update(int v, int tl, int tr, int pos, T val) {
    if (tl == tr)
      return t[++cnt].val = val, cnt;

    int tm = (tl + tr) >> 1;
    if (pos <= tm)
      return merge(++cnt, update(t[v].l, tl, tm, pos, val), t[v].r);
    else
      return merge(++cnt, t[v].l, update(t[v].r, tm + 1, tr, pos, val));
  }

  T query(int v, int tl, int tr, int l, int r) {
    if(tl > r || tr < l)
      return NIL;
    if(l <= tl && tr <= r)
      return t[v].val;

    int tm = (tl + tr) >> 1;
    return op(query(t[v].l, tl, tm, l, r), query(t[v].r, tm + 1, tr, l, r));
  }

  void Update(int ver, int pos, T val){
    vers.push_back(update(vers[ver], 0, n - 1, pos, val));
  }

  T Query(int ver, int l, int r){
    return query(vers[ver], 0, n - 1, l, r);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q;
  cin >> n >> q;

  vector<ll> v(n);
  FOR(i, n)
    cin >> v[i];

  SegTree<ll, plus<ll>> seg(n, q, v);

  vector<int> vers = { 0 };
  FOR(i, q){
    int tp, k, a, x, l, r;
    cin >> tp;
    switch (tp){
    case 1:
      cin >> k >> a >> x; k--;
      seg.Update(vers[k], a - 1, x);
      vers[k] = seg.vers.size() - 1;
      break;
    case 2:
      cin >> k >> l >> r; k--;
      cout << seg.Query(vers[k], l - 1, r - 1) << endl;
      break;
    case 3:
      cin >> k; k--;
      vers.push_back(vers[k]);
    }
  }
}