#include "bits/stdc++.h"

using namespace std;
using vi = vector<int>;

#define FOR(i, b) for(int i = 0; i < (b); i++)

struct UnionFind{
  int n;
  vi dad, sz;
  UnionFind(int n): n(n), dad(n), sz(n, 1){
    FOR(i, n)
      dad[i] = i;
  }

  int Find(int v){
    return v == dad[v] ? v : (dad[v] = Find(dad[v]));
  }

  void Union(int u, int v){
    u = Find(u);
    v = Find(v);
    if(u != v){
      if(sz[u] < sz[v]) swap(u, v);
      sz[u] += sz[v];
      dad[v] = u;
    }
  }

  int Connected(int u, int v){
    return Find(u) == Find(v);
  }
};


int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, m;
  cin >> n >> m;

  UnionFind uf(n);
  FOR(i, m){
    string s;
    int u, v;
    cin >> s >> u >> v; u--; v--;
    if (s[0] == 'u'){
      uf.Union(u, v);
    }
    else
      cout << (uf.Connected(u, v) ? "YES" : "NO") << "\n";
  }
}
