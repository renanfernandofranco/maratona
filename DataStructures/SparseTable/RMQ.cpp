/**
 * Sparse Table - Static Range Minimum Query
 *
 * Time Complexity:
 *  - Build - O(n lg n)
 *  - Query - O(1)
 *
 * Space Complexity : O(n lg n)
 *
 * https://judge.yosupo.jp/problem/staticrmq
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T>
struct SparseTable{
  int n;
  vector<T> st;

  SparseTable(vector<T> v): n(v.size()), st(n * (int)log2(n << 1)){
    copy_n(v.begin(), n, st.begin());
    for (int l = 0, sz = 2; sz <= n; l += n, sz <<= 1)
      FOR(i, n - sz + 1)
        st[i + l + n] = min(st[i + l], st[i + l + (sz >> 1)]);
  }

  T Query(int L, int R){
    int j = 31 - __builtin_clz(R - L + 1);
    return min(st[n * j + L], st[n * j + R - (1 << j) + 1]);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, l, r;
  cin >> n >> q;
  vi v(n);

  FOR(i, n)
    cin >> v[i];

  SparseTable<int> st(v);

  FOR(i, q){
    cin >> l >> r;
    cout << st.Query(l, r - 1) << endl;
  }
}
