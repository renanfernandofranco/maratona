/**
 * Disjoint Sparse Table - Static Range Query
 *
 * This Data Structure works with any associative function
 *
 * OBS: If the function is non-commutative, then
 * provide the reverse operator for the second partial sum.
 *
 * Time Complexity:
 *  - Build - O(n lg n)
 *  - Query - O(1)
 *
 * Space Complexity : O(n lg n)
 *
 * https://judge.yosupo.jp/problem/static_range_sum
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

template <class T, class OP>
struct DisjointST{
  int H, n;
  vector<vector<T>> st;
  OP op;

  DisjointST(vector<T> A): H(ceil(log2(A.size()))), n(1 << H), st(H + 1, vector<T>(n)){
    A.resize(n);
    FOR(h, H + 1)
      for(int sz = 1 << (H - h), l = 0; l < n; l += sz){
        int m = l + (sz >> 1);
        partial_sum(A.begin() + m, A.begin() + (l + sz), st[h].begin() + m, op);
        partial_sum(A.rend() - m, A.rend() - l, st[h].rend() - m, op);
      }
  }

  T Query(int L, int R){
    if(L == R)
      return st.back()[L];
    int h = H + __builtin_clz(L ^ R) - 32;
    return op(st[h][L], st[h][R]);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, l, r;
  cin >> n >> q;
  vector<ll> A(n);

  FOR(i, n)
    cin >> A[i];

  DisjointST<ll, plus<ll>> st(A);

  FOR(i, q){
    cin >> l >> r;
    cout << st.Query(l, r - 1) << endl;
  }
}
