/**
 * Set Xor Min
 * 
 * Given x, finds the number i in set such that (i ^ x) is minimum 
 * 
 * Complexity:
 *    - Time: O(log |Max Range|) per operation
 *    - Space: O(N * log |Max Range|)
 * 
 * https://judge.yosupo.jp/problem/line_add_get_min
 * 
 */

#include <bits/stdc++.h>
 
using namespace std;

using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)
 
template <class T>
struct Tree{
   #define isOn(n, i) ((n & ((T)1 << i)) != 0)
   const static int HIGH = __LONG_LONG_WIDTH__ -
      __builtin_clzll(numeric_limits<T>::max()) - 1;

   struct Vertex{
      int sz = 0, c[2] = {0, 0};
   };

   vector<Vertex> t;

   Tree(int n = 2): t(2) {t.reserve(n * (HIGH + 2));}

   int get(int v, bool ch){
      if(!t[v].c[ch])
         t[v].c[ch] = t.size(), t.push_back(Vertex());
      return t[v].c[ch];
   }

   void Add(T x, bool on, int v = 1, int b = HIGH){
      if(b == -1)
         return void(t[v].sz = on);

      int ch = get(v, isOn(x, b));
      Add(x, on, ch, b - 1);
      t[v].sz = t[t[v].c[0]].sz + t[t[v].c[1]].sz;
   }

   T GetMin(T x, int v = 1, int b = HIGH) {
      if(b == -1)
         return 0;

      int ch = t[v].c[isOn(x, b)];
      if(t[ch].sz)
         return GetMin(x, ch, b - 1);
      return GetMin(x, t[v].c[!isOn(x, b)], b - 1) + ((T)1 << b); 
   }
};

int main(){
   cin.tie(0)->sync_with_stdio(0);

   int n, tp, x;
   cin >> n;

   Tree<int> tree(n);
   
   FOR(i, n){
      cin >> tp;
      switch(tp){
         case 0:
         case 1:
            cin >> x;
            tree.Add(x, !tp);
            break;
         case 2:
            cin >> x;
            cout << tree.GetMin(x) << endl;
      }
   }
}
