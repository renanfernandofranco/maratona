/**
 * Using Rope as Implicit Treap - Swap between Ranges
 * 
 * Complexity:
 *    - Time: O(lg n) per operation with high constant
 *    - Space: O(n)
 * 
 * https://codeforces.com/gym/102787/problem/A
 * 
 */

#include <bits/stdc++.h>
#include <ext/rope>

using namespace std;
using namespace __gnu_cxx;

#define FOR(i, b) for(int i = 0; i < (b); i++)
using Treap = rope<int>;

// Assume the intervals are not intersected
void swapRange(Treap& t, int l1, int r1, int l2, int r2){
   if(r1 > l2)
      swap(l1, l2), swap(r1, r2);

   Treap tail = t.substr(r2 + 1, t.size() - r2);
   t.erase(r2 + 1, t.size() - r2);

   Treap range2 = t.substr(l2, r2 - l2 + 1);
   t.erase(l2, r2 - l2 + 1);

   Treap between = t.substr(r1 + 1, l2 - r1 - 1);
   t.erase(r1 + 1, t.size() - r1);

   Treap range1 = t.substr(l1, r1 - l1 + 1);
   t.erase(l1, r1 - l1 + 1);

   t += range2;
   t += between;
   t += range1;
   t += tail;
   // t.insert(t.mutable_end(), tail);
}

int main(){
   cin.tie(0)->sync_with_stdio(0);

   int n, a, b;
   cin >> n;
   
   // Treap t(n, 0);
   // FOR(i, n)
   //    t.mutable_reference_at(i) = i + 1;
   
   Treap t;
   FOR(i, n)
      t.push_back(i + 1);
   
   FOR(i, n){
      cin >> a >> b;
      if(a < b){
         a--; b--;
         int len = min(n - b, b - a);
         swapRange(t, a, a + len - 1, b, b + len - 1);
      }
   }
   
   // FOR(i, n)
   //    cout << t[i] << ' ';
   for(auto e : t)
      cout << e << ' ';
   cout << endl;
}
