/**
 * Fenwick Tree 2D - Point Add, Rectangle Sum
 *
 * Given a grid n x m
 *
 * Operations:
 *  - Query/Update Time: O(lg n lg m)
 *  - Space: O(nm)
 *
 * Problem: https://cses.fi/problemset/task/1739/
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using ll = long long;
#define endl '\n'
#define all(a) a.begin(), a.end()
#define FOR(i, b) for(int i = 0; i < b; i++)
#define FORI(i, b) for(int i = 1; i <= b; i++)

template<class T>
struct BIT2D{ // Indexed by 1
  int n, m;
  vector<vector<T>> bit;

  BIT2D(int n, int m): n(n), m(m), bit(n + 1, vector<T>(m + 1)){}

  void Build(vector<vector<T>>& a){ // Build in O(nm)
    FORI(i, n)
      FORI(j, m){
        bit[i][j] += a[i][j];
        int ii = i + (i & -i), jj = j + (j & -j);
        if(jj <= m)
          bit[i][jj] += bit[i][j];
        if(ii <= n)
          bit[ii][j] += bit[i][j];
        if(ii <= n && jj <= m)
          bit[ii][jj] -= bit[i][j];
      }
  }

  T query(int x, int y) {
    T ret = 0;
    for (int i = x; i > 0; i -= i & -i)
      for (int j = y; j > 0; j -= j & -j)
        ret += bit[i][j];
    return ret;
  }

  void Add(int x, int y, T delta) {
    for (int i = x; i <= n; i += i & -i)
      for (int j = y; j <= m; j += j & -j)
        bit[i][j] += delta;
  }

  // Sum of rectangle [xl, xr] * [yl, yr]
  T Query(int xl, int yl, int xr, int yr) {
    T ret = query(xr, yr);
    ret -= query(xl - 1, yr);
    ret -= query(xr, yl - 1);
    ret += query(xl - 1, yl - 1);
    return ret;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q;
  cin >> n >> q;

  BIT2D<int> bit(n, n);
  vector<vector<int>> has(n + 1, vector<int>(n + 1, false));

  FOR(y, n){
    string line;
    cin >> line;
    FOR(x, n)
      if(line[x] == '*')
        has[x + 1][y + 1] = true;
  }
  bit.Build(has);

  FOR(i, q){
    int tp, xl, xr, yl, yr;
    cin >> tp;
    switch(tp){
      case 1:
        cin >> yl >> xl;
        has[xl][yl] ^= 1;
        bit.Add(xl, yl, has[xl][yl] ? 1 : -1);
        break;
      case 2:
        cin >> yl >> xl >> yr >> xr;
        cout << bit.Query(xl, yl, xr, yr) << endl;
    }
  }
}
