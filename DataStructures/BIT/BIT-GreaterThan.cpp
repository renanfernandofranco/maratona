/**
 * BIT - Count Element Greater Than X
 *
 * Complexity:
 *  - Time : O(log n)
 *  - Space : O(n)
 *
 * Where n = Max Element
 *
 */

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;

struct FT{ // Indexed by 0
  #define MAX_ELEMENT 100005
  vi BIT;
  int sizeBIT;

  // Support elements in the range [0, n]
  FT(int n = MAX_ELEMENT):BIT(n + 1, 0), sizeBIT(n + 1){}

  int sumFT(int r){
    int ret = 0;
    for (; r >= 0; r = (r & (r + 1)) - 1)
      ret += BIT[r];
    return ret;
  }

  void addFT(int idx, int value){
    for (; idx < sizeBIT; idx = idx | (idx + 1))
      BIT[idx] += value;
  }

  void add(int idx, int value){
    addFT(sizeBIT - 1, value);
    addFT(idx - 1, -value);
  }

  // Count elements greater than x
  int greaterThan(int x){
    return sumFT(sizeBIT - 1) - sumFT(x);
  }
};
