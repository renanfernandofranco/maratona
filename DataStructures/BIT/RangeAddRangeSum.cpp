/**
 * Fenwick Tree - Range Add, Range Sum
 *
 * Operations:
 *  - Range Add - O(log n)
 *  - Range Sum - O(log n)
 *
 * Space Complexity : O(n)
 *
 * https://cses.fi/problemset/task/1651
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)
using ll = long long;

template <class T>
struct FT { // Indexed by 1
  int n;
  vector<T> B1, B2;

  FT(int n): n(n), B1(n + 1, 0), B2(n + 1, 0){}

  T sum(vector<T>& bit, int r) {
    T ret = 0;
    for (; r > 0; r -= r & -r)
      ret += bit[r];
    return ret;
  }

  void add(vector<T>& bit, int i, T val) {
    for (; i <= n; i += i & -i)
      bit[i] += val;
  }

  T prefixSum(int i){
    return sum(B1, i) * i -  sum(B2, i);
  }

  void Add(int l, int r, T val){
    add(B1, l, val);
    add(B1, r + 1, -val);
    add(B2, l, val * (l - 1));
    add(B2, r + 1, -val * r);
  }

  T Sum(int l, int r) {
    return prefixSum(r) - prefixSum(l - 1);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, tp, l, r, k, pos;
  cin >> n >> q;

  FT<ll> ft(n);

  vector<ll> v(n);

  FOR(i, n){
   cin >> v[i];
   ft.Add(i + 1, i + 1, v[i]);
  }

  FOR(i, q){
   cin >> tp;
   switch(tp){
    case 1:
      cin >> l >> r >> k;
      ft.Add(l, r, k);
      break;
    case 2:
      cin >> pos;
      cout << ft.Sum(pos, pos) << endl;
   }
  }
}
