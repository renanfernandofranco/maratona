/**
 * Fenwick Tree - Range Add, Point Query
 *
 * Operations:
 *    - Range Add - O(log n)
 *    - Point Query - O(log n)
 *
 * Space Complexity : O(n)
 *
 * https://cses.fi/problemset/task/1651
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)
using ll = long long;

template <class T>
struct FT { // Indexed by 0
  int n;
  vector<T> bit;

  FT(int n): n(n), bit(n, 0){}

  T sum(int r) {
    T ret = 0;
    for (; r >= 0; r = (r & (r + 1)) - 1)
      ret += bit[r];
    return ret;
  }

  void add(int i, T val) {
    for (; i < n; i |= i + 1)
      bit[i] += val;
  }

  T Query(int pos) {
    return sum(pos);
  }

  void Add(int l, int r, T val){
    add(l, val);
    add(r + 1, -val);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, tp, l, r, k, pos;
  cin >> n >> q;

  FT<ll> ft(n);

  vector<ll> v(n);

  FOR(i, n){
   cin >> v[i];
   ft.Add(i, i, v[i]);
  }

  FOR(i, q){
   cin >> tp;
   switch(tp){
    case 1:
      cin >> l >> r >> k;
      ft.Add(l - 1, r - 1, k);
      break;
    case 2:
      cin >> pos;
      cout << ft.Query(pos - 1) << endl;
   }
  }
}
