/**
 * Fenwick Tree - Point Add, Range Sum
 *
 * Time Complexity:
 *  - Build: O(n)
 *  - Range Query: O(log n)
 *  - Point Update: O(log n)
 *
 * Space Complexity : O(n)
 *
 * https://cses.fi/problemset/task/1648/
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)
#define FORI(i, b)  for(int i = 1; i <= b; i++)
using ll = long long;

template <class T>
struct FT { // Indexed by 1
  int n;
  vector<T> bit;

  FT(int n): n(n), bit(n + 1, 0){}

  void Build(vector<T>& a){ // Build in O(n)
    FORI(i, n){
      bit[i] += a[i];
      int ii = i + (i & -i);
      if(ii <= n)
        bit[ii] += bit[i];
    }
  }

  T sum(int r) {
    T ret = 0;
    for (; r > 0; r -= r & -r)
      ret += bit[r];
    return ret;
  }

  void Add(int i, T delta) {
    for (; i <= n; i += i & -i)
      bit[i] += delta;
  }

  T Sum(int l, int r) {
    return sum(r) - sum(l - 1);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q;
  cin >> n >> q;

  vector<ll> v(n + 1);
  FT<ll> bit(n);

  FORI(i, n)
    cin >> v[i];

  bit.Build(v);

  FOR(i, q){
    int tp, p, x, l, r;
    cin >> tp;
    switch(tp){
      case 1:
        cin >> p >> x;
        bit.Add(p, x - v[p]);
        v[p] = x;
        break;
      case 2:
        cin >> l >> r;
        cout << bit.Sum(l, r) << endl;
    }
  }
}
