/**
 * Fenwick Tree 2D for grid of binary numbers
 *
 * Operations:
 *  - Query/Update Time: O(lg^ n)
 *  - Space: O(n + numUpdates * lg n)
 *
 * Problem: https://cses.fi/problemset/task/1739/
 *
 */

#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;
using namespace __gnu_pbds;

typedef pair<int, int> pii;
typedef tree<pii, null_type, less<pii>, rb_tree_tag, tree_order_statistics_node_update> OST;

using vi = vector<int>;
using ll = long long;
#define endl '\n'
#define all(a) a.begin(), a.end()
#define FOR(i, b) for(int i = 0; i < b; i++)

struct BIT2D{ // Indexed by 1
  int n;
  vector<OST> bit;

  BIT2D(int n): n(n), bit(n + 1){}

  void Add(int x, int y)  {
    for (int i = x; i <= n; i += i & -i)
      bit[i].insert({y, x});
  }

  void Rem(int x, int y){
    for (int i = x; i <= n; i += i & -i)
      bit[i].erase({y, x});
  }

  int query(int x, int y) {
    int ans = 0;
    for (int i = x; i > 0; i -= i & -i)
      ans += bit[i].order_of_key({y + 1, 0});
    return ans;
  }

  int Query(int xl, int yl, int xr, int yr) {
    int ret = query(xr, yr);
    ret -= query(xl - 1, yr);
    ret -= query(xr, yl - 1);
    ret += query(xl - 1, yl - 1);
    return ret;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q;
  cin >> n >> q;

  BIT2D bit(n);
  vector<vector<char>> has(n + 1, vector<char>(n + 1, false));

  FOR(y, n){
    string line;
    cin >> line;
    FOR(x, n)
      if(line[x] == '*'){
        has[x + 1][y + 1] = true;
        bit.Add(x + 1, y + 1);
      }
  }

  FOR(i, q){
    int tp, xl, xr, yl, yr;
    cin >> tp;
    switch(tp){
      case 1:
        cin >> yl >> xl;
        has[xl][yl] ^= 1;
        if(has[xl][yl])
          bit.Add(xl, yl);
        else
          bit.Rem(xl, yl);
        break;
      case 2:
        cin >> yl >> xl >> yr >> xr;
        cout << bit.Query(xl, yl, xr, yr) << endl;
    }
  }
}
