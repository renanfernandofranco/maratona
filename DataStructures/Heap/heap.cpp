/**
 * Heap with function of change priority through of the ID 
 * 
 * Using a pair as a priority, but is possible change simply replace pair for another type
 * 
 * The keys used should be in range [0, maxSize)
 */

#include <bits/stdc++.h>

using namespace std;

#define DAD(i)  ((i) >> 1)
#define CL(i)  ((i) << 1) //child left
#define CR(i)  ((i) << 1 | 1) //child right

template <class heapPrior>
class Heap{
    private:
    vector<heapPrior> heap; // Vector of priorities
    vector<int> keys;    // keys[i] contain the id of the element stored in the heap[i]
    vector<int> posKeys; // posKeys[key] store in what index the key are localized in keys/heap

    // Function that says when first element has priority greater than second
    bool (*compare)(heapPrior const& p1, heapPrior const& p2);
    int size;
    int maxSize;

    public:
    Heap(bool (*func_comp)(heapPrior const& p1, heapPrior const& p2), int _maxSize = 0){
        compare = func_comp;
        maxSize = _maxSize + 1;
        size = 1;
        heap.resize(maxSize);
        keys.resize(maxSize);
        posKeys.assign(maxSize, -1);
    }

    void push_back(int key, heapPrior prior){
        if(key >= maxSize - 1){
            maxSize = (key + 1) << 1;
            heap.resize(maxSize);
            keys.resize(maxSize);
            posKeys.resize(maxSize, -1);
        }
        if(posKeys[key] != -1)
            return;
        keys[size] = key;
        heap[size] = prior;
        posKeys[key] = size++;
    }

    void makeHeap(){
        for(int i = (size >> 1) - 1; i >= 1; i--)
            bubbleDown(i);
    }
    
    void makeHeap(vector<heapPrior>& v, int vSize){
        size = maxSize = vSize + 1;
        heap.resize(maxSize);
        keys.resize(maxSize);
        posKeys.resize(maxSize);
        for(int i = 1 ; i < maxSize; i++){
            heap[i] = v[i - 1];
            keys[i] = i - 1;
            posKeys[i - 1] = i;
        }
        posKeys[vSize] = -1;
        for(int i = (size >> 1) - 1; i >= 1; i--)
            bubbleDown(i);
    }

    const int* getIds(){
        return &keys[1];
    }

    const heapPrior* getPrios(){
        return &heap[1];
    }

    int sizes(){
        return size - 1;
    }

    bool empty(){
        return size == 1;
    }

    void clear(){
        heap.clear();
        keys.clear();
        posKeys.clear();
        maxSize = size = 1;
    }

    int topKey(){
        return keys[1];
    }

    heapPrior topPrior(){
        return heap[1];
    }

    heapPrior keyPrior(int id){
        return heap[posKeys[id]];
    }

    void swapHeap(int i, int j){
        swap(heap[i], heap[j]);
        swap(keys[i], keys[j]);
        swap(posKeys[keys[i]], posKeys[keys[j]]);
    }

    void bubbleDown(int n){
        int bestChild;
        if (CL(n) < size){
            bestChild = CL(n);
            if (CR(n) < size)
                if (compare(heap[CL(n)], heap[CR(n)]))
                    bestChild = CR(n);    
            if (compare(heap[n], heap[bestChild])){
                swapHeap(n, bestChild);
                bubbleDown(bestChild);
            }
        }
    }

    void bubbleUp(int n){
        if (DAD(n) >= 1 && compare(heap[DAD(n)], heap[n])){
            swapHeap(DAD(n), n);
            bubbleUp(DAD(n));
        }
    }

    // Besides removing the top element, also returns the key of the same
    int pop(){
        size--;
        swapHeap(size, 1);
        bubbleDown(1);
        return keys[size];
    }

    bool remove(int key){
        if(posKeys[key] == -1)
            return false;
        size--;
        if(posKeys[key] != size){
            int key2 = keys[size];
            swapHeap(size, posKeys[key]);
            bubbleUp(posKeys[key2]);
            bubbleDown(posKeys[key2]);
        }
        posKeys[key] = -1;
        return true;
    }

    void insert(int key, heapPrior prior){
        if(key >= maxSize - 1){
            maxSize = (key + 1) << 1;
            heap.resize(maxSize);
            keys.resize(maxSize);
            posKeys.resize(maxSize, -1);
        }
        if(posKeys[key] != -1)
            return;
        keys[size] = key;
        heap[size] = prior;
        posKeys[key] = size++;
        bubbleUp(size - 1);
    }

    void changePrior(int key, heapPrior newPrior){
        if(key >= maxSize -  1 || posKeys[key] == -1){
            insert(key, newPrior);
            return;
        }
        heap[posKeys[key]] = newPrior;
        bubbleUp(posKeys[key]);
        bubbleDown(posKeys[key]);
    }
};

int main(){
    int maxSize = 10;
    // Sample with function for Heap of Minimum
    Heap<double> heapMin([](double const& p1, double const& p2){return p1 > p2;});
    vector<int> v;
    for(int i = 0; i < 1000; i++)
        v.push_back(i);
    //shuffle(v.begin(), v.end(),default_random_engine(0));
    for(int a : v)
        heapMin.push_back(a, a);
    heapMin.makeHeap();
    for(int a : v)
        cout << heapMin.pop() << endl;

    // Sample with function for Heap of Maximum
    Heap<double> heapMax([](double const& p1, double const& p2){return p1 < p2;}, maxSize);
}