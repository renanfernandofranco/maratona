#include <bits/stdc++.h>

/**
 * Randomized Heap without releasing memory
 * 
 * Complexity:
 *    - Time: O(lg n) for all operations
 *    - Space: O(n)
 * 
 * Use srand(time(NULL)) in main function for use random seed;
 * 
 */

using namespace std;

template <class T, class less = std::less<T>>
struct Heap{
   struct Node {
      T value;
      Node* left = nullptr;
      Node* right = nullptr;
      Node(T value): value(value){}
   };

   Node* root = nullptr;
   less op;

   void check(){ assert(root); }
   bool empty(){ return !root; }
   
   Node* merge(Node* t1, Node* t2) {
      if (!t1 || !t2)
         return t1 ? t1 : t2;
      if (op(t2->value,  t1->value))
         swap(t1, t2);
      if (rand() & 1)
         swap(t1->left, t1->right);
      t1->left = merge(t1->left, t2);
      return t1;
   }

   void merge(Heap<T, less>& other){
      if(root == other.root) return;
      root = merge(root, other.root);
      other.root = nullptr;
   }

   T top(){
      check();
      return root->value;
   }

   void pop(){
      check();
      root = merge(root->left, root->right);
   }

   void insert(T value){
      root = merge(root, new Node(value));
   }
};