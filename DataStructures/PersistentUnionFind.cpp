/**
 * Persistent Union Find - Online
 *
 * Complexity:
 *  - Time: O(lg^2 n) by operation
 *  - Space: O(n + Updates * lg n)
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define SZ(v) ((int) v.size())
#define endl '\n'
#define all(a) a.begin(), a.end()

template<class T>
struct PersistentArray{
  struct Vertex {
    int l, r;
    T data;
  };

  int n, leaf, ver;
  int cnt = -1;
  vector<Vertex> t;
  vector<int> vers;

  PersistentArray (int n, int numUpdates, vector<T> a = vector<T>()):
   n(n), ver(0), t(2 * n + (ceil(log2(n)) + 1) * numUpdates){
    a.resize(n);
    vers.reserve(numUpdates + 1);
    vers.push_back(build(0, n - 1, a));
  }

  int merge(int v, int l, int r){
    t[v].l = l;
    t[v].r = r;
    return v;
  }

  void update(int pos){
    vers.push_back(update(vers.back(), 0, n - 1, pos));
  }

  int build(int tl, int tr, vector<T>& a) {
    if (tl == tr)
      return t[++cnt].data = a[tl], cnt;

    int tm = tl + tr >> 1;
    return merge(++cnt, build(tl, tm, a), build(tm + 1, tr, a));
  }

  T& get(int pos){
    return t[get(vers[ver], 0, n - 1, pos)].data;
  }

  int get(int v, int tl, int tr, int pos) {
    while(tl != tr){
      int tm = tl + tr >> 1;
      v = pos <= tm ? t[v].l : t[v].r;
      (pos <= tm ? tr : tl) = tm + (pos > tm);
    }
    return v;
  }

  int getMutable(int v, int tl, int tr, int pos) {
    if (tl == tr)
      return t[leaf = ++cnt] = t[v], leaf;

    int tm = tl + tr >> 1;
    if (pos <= tm)
      return merge(++cnt, getMutable(t[v].l, tl, tm, pos), t[v].r);
    else
      return merge(++cnt, t[v].l, getMutable(t[v].r, tm + 1, tr, pos));
  }

  T& GetMutable(int pos){
    vers.push_back(getMutable(vers[ver], 0, n - 1, pos));
    ver = SZ(vers) - 1;
    return t[leaf].data;
  }

  const T& operator[] (int pos){
    return t[get(vers[ver], 0, n - 1, pos)].data;
  }

  void SetVersion(int ver){	this->ver = ver; }
  int GetVersion(){	return ver;	}
};

struct PersistentUnionFind{
  struct Data{ int dad, size; };
  PersistentArray<Data> arr;
  vector<int> vers;

  PersistentUnionFind(int n, int q): arr(1, 1), vers(q + 1){
    vector<Data> v(n);
    FOR(i, n)
      v[i] = {i, 1};
    arr = PersistentArray<Data>(n, 2 * q, v);
    vers[0] = arr.GetVersion();
  }

  int FindSet(int u){
    return u == arr[u].dad ? u : FindSet(arr[u].dad);
  }

  void UnionSet(int u, int v, int oldV, int newV){
    arr.SetVersion(vers[oldV]);
    u = FindSet(u);
    v = FindSet(v);

    if (u != v){
      if (arr[u].size < arr[v].size)
        swap(u, v);
      Data& dataU = arr.GetMutable(u);
      Data& dataV = arr.GetMutable(v);
      dataU.size += dataV.size;
      dataV.dad = u;
    }
    vers[newV] = arr.GetVersion();
  }

  bool Connected(int u, int v, int ver){
    arr.SetVersion(vers[ver]);
    return FindSet(u) == FindSet(v);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q;
  cin >> n >> q;

  PersistentUnionFind uf(n, q);

  FOR(i, q){
    int tp, ver, u, v;
    cin >> tp >> ver >> u >> v;
    if(tp == 0){
      uf.UnionSet(u, v, ver + 1, i + 1);
    }else{
      cout << uf.Connected(u, v, ver + 1) << endl;
    }
  }
}
