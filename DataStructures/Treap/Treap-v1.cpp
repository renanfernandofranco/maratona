/**
 * Implicit Treaps - Swap between Ranges
 *
 * Complexity:
 *  - Time: O(lg n) per operation (probabilistic)
 *  - Space: O(n)
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
using vi = vector<int>;

struct Node{
  int data, prior, sz;
  Node *l, *r;
  Node (int data) : data(data), prior(rand()), l(NULL), r(NULL) { }
};

using Treap = Node*;

int sz (Treap t) {
  return t ? t->sz : 0;
}

void update (Treap t) {
  if (t)
    t->sz = 1 + sz(t->l) + sz (t->r);
}

void merge (Treap & t, Treap l, Treap r) {
  if (!l || !r)
    t = l ? l : r;
  else if (l->prior > r->prior)
    merge (l->r, l->r, r),  t = l;
  else
    merge (r->l, l, r->l),  t = r;
  update (t);
}

void split (Treap t, Treap & l, Treap & r, int key, int add = 0) {
  if (!t)
    return void( l = r = NULL );
  int cur_key = add + sz(t->l); //implicit key
  if (key <= cur_key)
    split (t->l, l, t->l, key, add),  r = t;
  else
    split (t->r, t->r, r, key, add + 1 + sz(t->l)),  l = t;
  update (t);
}

Treap build(vi& a){
  Treap t = NULL;
  FOR(i, (int)a.size())
    merge(t, t, new Node(a[i]));
  return t;
}

// Assume the intervals are not intersected
void swapRange(Treap& t, int l1, int r1, int l2, int r2){
  Treap range1, range2, between, tail;

  if(r1 > l2)
    swap(l1, l2), swap(r1, r2);

  split(t, t, tail, r2 + 1);
  split(t, t, range2, l2);
  split(t, t, between, r1 + 1);
  split(t, t, range1, l1);

  merge(t, t, range2);
  merge(t, t, between);
  merge(t, t, range1);
  merge(t, t, tail);
}

void print(Treap t){
  if(t){
    print(t->l);
    cout << t->data << ' ';
    print(t->r);
  }
}
