/**
 * Treap - Conditional Range Update
 *
 * Given two integer x and vl, perform two operations:
 *  1) Add vl to all elements greater than x
 *  2) Add vl to all elements less than or equal to x
 *
 * Time Complexity (Probabilistic):
 *  - O(m lg(n / m)) for type 1 operations with vl < 0 and type 2 with vl > 0
 *  - O(lg n) for other operations
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define all(a) a.begin(), a.end()
#define SZ(v) ((int)v.size())
using vi = vector<int>;
using vii = vector<pair<int, int>>;
using ll = long long;
using vll = vector<ll>;

struct Node{
	ll key, lazy = 0;
	int pos, prior;
	Node *l, *r;
	Node (ll key, int pos): key(key), pos(pos), prior(rand()), l(NULL), r(NULL) { }
};

using Treap = Node*;

void update (Treap t) {
	if (t){
		ll& lazy = t->lazy;
		if(t->l)
			t->l->lazy += lazy;
		if(t->r)
			t->r->lazy += lazy;
		t->key += lazy;
		lazy = 0;
	}
}

void merge (Treap & t, Treap l, Treap r) {
	update(l);
	update(r);
	if (!l || !r)
		t = l ? l : r;
	else if (l->prior > r->prior)
		merge (l->r, l->r, r),  t = l;
	else
		merge (r->l, l, r->l),  t = r;
}

// l contains the elements smaller than or equal to the key
// r contains elements greater than the key
void split (Treap t, Treap & l, Treap & r, ll key) {
	if (!t)
		return void( l = r = NULL);
	update(t);
	if (key < t->key)
		split (t->l, l, t->l, key),  r = t;
	else
		split (t->r, t->r, r, key),  l = t;
}

Treap unite (Treap l, Treap r) {
	update(l);
	update(r);
	if (!l || !r)  return l ? l : r;
	if (l->prior < r->prior)  swap (l, r);
	Treap lt, rt;
	split (r, lt, rt, l->key);
	l->l = unite (l->l, lt);
	l->r = unite (l->r, rt);
	return l;
}

Treap build(vll& a){
	Treap t = NULL;
	vii v(SZ(a));

	FOR(i, SZ(a))
		v[i].first = a[i], v[i].second = i;
	sort(all(v));

	FOR(i, SZ(a))
		merge(t, t, new Node(v[i].first, v[i].second));
	return t;
}

void rangeUpdate(Treap& t, ll x, ll vl, bool greater){
	Treap after = NULL;
	split(t, t, after, x);

	if(greater){
		if(after)
			after->lazy += vl;
	}else{
		if(t)
			t->lazy += vl;
	}

	if(vl > 0 ^ greater)
		t = unite(after, t);
	else
		merge(t, t, after);
}

void recoverArray(Treap t, vector<ll>& v){
	if(t){
		update(t);
		v[t->pos] = t->key;
		recoverArray(t->l, v);
		recoverArray(t->r, v);
	}
}
