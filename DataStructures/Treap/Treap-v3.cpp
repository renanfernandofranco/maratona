/**
 * Implicit Treaps - Reversals and Sums
 *
 * Given an array of n integers, performs the following operations:
 *  - Reverse a subarray
 *  - Calculate the sum of values in a subarray
 *
 * Complexity:
 *  - Time: O(lg n) per operation (probabilistic)
 *  - Space: O(n)
 *
 */

#include <bits/stdc++.h>

using namespace std;

#define FOR(i, b) for(int i = 0; i < (int)(b); i++)
#define endl '\n'
#define all(a) a.begin(), a.end()

using vi = vector<int>;
using ll = long long;

struct Node{
  int data, prior, sz;
  bool rev;
  ll sum;
  Node *l, *r;
  Node (int data): data(data), prior(rand()), sz(1),
    rev(false), sum(data), l(NULL), r(NULL) {}
};

using Treap = Node*;

int sz (Treap t) {
  return t ? t->sz : 0;
}

ll sum(Treap t){
  return t ? t->sum : 0;
}

void push(Treap t){
  if(t){
    if(t->rev){
      if(t->l)
        t->l->rev ^= 1;
      if(t->r)
        t->r->rev ^= 1;
      swap(t->l, t->r);
      t->rev = false;
    }
  }
}

void update (Treap t) {
  if (t){
    t->sz = 1 + sz(t->l) + sz(t->r);
    t->sum = t->data + sum(t->l) + sum(t->r);
  }
}

void merge (Treap & t, Treap l, Treap r) {
  push(l);
  push(r);
  if (!l || !r)
    t = l ? l : r;
  else if (l->prior > r->prior)
    merge (l->r, l->r, r),  t = l;
  else
    merge (r->l, l, r->l),  t = r;
  update (t);
}

void split (Treap t, Treap & l, Treap & r, int key, int add = 0) {
  if (!t)
    return void( l = r = NULL );
  push (t);
  int cur_key = add + sz(t->l); //implicit key
  if (key <= cur_key)
    split (t->l, l, t->l, key, add),  r = t;
  else
    split (t->r, t->r, r, key, add + 1 + sz(t->l)),  l = t;
  update (t);
}

Treap build(vi& a){
  Treap t = NULL;
  FOR(i, a.size())
    merge(t, t, new Node(a[i]));
  return t;
}

ll rangeSum(Treap& t, int tl, int tr){
  assert(tl <= tr);
  Treap range, suff;
  split(t, t, suff, tr + 1);
  split(t, t, range, tl);

  ll ans = range->sum;

  merge(t, t, range);
  merge(t, t, suff);

  return ans;
}

void revRange(Treap& t, int tl, int tr){
  assert(tl <= tr);
  Treap range, suff;
  split(t, t, suff, tr + 1);
  split(t, t, range, tl);

  range->rev ^= 1;

  merge(t, t, range);
  merge(t, t, suff);
}

void freeT(Treap t){
  if(t){
    freeT(t->l);
    freeT(t->r);
    delete t;
  }
}

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, q;
  cin >> n >> q;

  vi v(n);
  FOR(i, n)
    cin >> v[i];

  Treap t = build(v);

  FOR(i, q){
    int tp, l, r;
    cin >> tp >> l >> r;
    switch (tp){
      case 1:
        revRange(t, l - 1, r - 1);
        break;
      case 2:
        cout << rangeSum(t, l - 1, r - 1) << endl;
      }
  }
  freeT(t);
}
