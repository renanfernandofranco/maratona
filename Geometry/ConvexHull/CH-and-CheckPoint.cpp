/**
 *  In this code have two algorithms, and in both is used integer coordinates
 * 
 *  The first them is to build a Convex Hull, the same have some important features:
 *    - The points are sorted by lower x to higher x
 *    - The array "ch" contain the points In the CH
 *    - The first point is repeated at the end
 *    - Complexity :
 *      - Time: O(n * lg n)
 *      - Space: O(n)
 * 
 *  The second is to check if a point is inside of a Polygon Convex, having as features:
 *    - Points on the boundary are considered included
 *    - To exclude points on the boundary, you need to remove the equality in the 
 *       InTriangule function if the edge of the triangle belongs to the convex hull
 *    - The points need are in counter clockwise, with first point repeated at the end
 *    - Complexity:
 *      - Time: O(lg n)
 *      - Space: O(1)
 * 
 * Problem : https://vjudge.net/problem/SPOJ-INOROUT
 * 
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define SZ(v) ((int) v.size())
#define all(a) a.begin(), a.end()
#define endl '\n'

template <class F>
struct Point{
  F x, y;
  Point() : x(0), y(0) {}
  Point(const F& x, const F& y) : x(x), y(y) {}

  Point<F> operator + (const Point<F>& ot) const{ return Point(x + ot.x, y + ot.y);}
  Point<F> operator - (const Point<F>& ot) const{ return Point(x - ot.x, y - ot.y);}
  bool operator == (const Point<F>& ot) const {return x == ot.x && y == ot.y;}
  
  // a * b = |a||b| cos c
  F operator * (const Point<F>& ot) const { return x * ot.x + y * ot.y; }
  // a ^ b = |a||b| sin c
  F operator ^ (const Point<F>& ot) const { return x * ot.y - y * ot.x; }

  // < 0 if rhs <- lhs counter-clockwise, 0 if collinear, > 0 if clockwise.
  friend F ccw(const Point<F>& lhs, const Point<F>& rhs) { return rhs ^ lhs;}
  friend F ccw(const Point<F>& lhs, const Point<F>& rhs, const Point<F>& origin){
    return ccw(lhs - origin, rhs - origin);
  }

  friend F abs(Point<F> v) { return v * v; }
};

template<class F>
struct CH{
  vector<Point<F>> v;
  const bool collinear = false;

  void Add(Point<F> t){
    v.push_back(t);
  }

  bool check(const Point<F>& p1, const Point<F>& p2, const Point<F>& p3) const{
    return (collinear ? ccw(p1, p2, p3) <= 0 : ccw(p1, p2, p3) < 0);
  }

  auto buildPart(){
    vector<Point<F>> hu = {v[0]};
    for(int i = 1; i < SZ(v); i++)
      if(i + 1 == SZ(v) || check(v[0], v[i], v.back())){
        while(SZ(hu) >= 2 && !check(hu[SZ(hu) - 2], hu.back(), v[i]))
          hu.pop_back();
        hu.push_back(v[i]);
      }
    return hu;
  }

  void Build() {
    auto opX = [](auto p1, auto p2){ return p1.x != p2.x ? p1.x < p2.x : p1.y < p2.y;};
    sort(all(v), opX);
    v.resize(unique(all(v)) - v.begin());

    if(SZ(v) == 1)
      return;

    auto up = buildPart(); // Builds the upper part of the Convex Hull
    if(collinear && SZ(up) == SZ(v)){
      reverse(all(up));
      rotate(up.begin(), min_element(all(up), opX), up.end());
      return void(v = up);
    }

    reverse(all(v));
    auto down = buildPart(); // Builds the bottom part of the Convex Hull
    up.insert(up.end(), next(down.begin()), prev(down.end()));
    v = up;
  }

  // v1, v2, v3 need are in clockwise
  bool InTriangle(Point<F> v1, Point<F> v2, Point<F> v3, Point<F> p){
    F f1 = ccw(v1, p, v3);
    F f2 = ccw(v2, p, v3);
    F f3 = ccw(v2, p, v1);
    return f1 <= 0 && f2 >= 0 && f3 <= 0;
  }

  #define ccwR(p1, p2) ccw(p1, p2, v[0])

  // Polygon should be ordered by smallest coordinate X
  // Cannot repeated last point in the end
  bool In(const Point<F>& t) {
    if (ccwR(v[1], t) > 0 || ccwR(v.back(), t) < 0)
      return 0;

    if (ccwR(v[1], t) == 0)
        return abs(v[1] - v[0]) >= abs(t - v[0]) ? 1 : 0;

    int r = lower_bound(v.begin(), v.end(), t, [this](auto p, auto t)
      {return ccwR(p, t) < 0;}) - v.begin();

    return InTriangle(v[r - 1], v[r], v[0], t);
  }
};

int main() {
  int n, q, x, y;
  cin >> n >> q;
  CH<ll> ch;

  FOR(i, n){
    cin >> x >> y;
    ch.Add({x, y});
  }

  ch.Build();
  FOR(i, q){
    cin >> x >> y;
    cout << (ch.In({x, y}) ? "D" : "F") << '\n';
  }
  return 0;
}