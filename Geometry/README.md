# Geometry

Here algorithms directly dealing with geometry problems and others using its techniques to solve problems only related to geometry are presented.

## Finding Convex Hull and checking whether a point belongs to it

This problem of finding the convex hull of a set points is quite common in programming competitions (see [CP Algorithms][4] to know more about it). As a result, there are some different tactics to solve it. In [CH-and-CheckPoint](./CH-and-CheckPoint.cpp), we present the simplest one we've found, which is from a [Chinese blog post][1]. Besides finding the hull, this way of building the hull allows fast checking of whether a point belongs to the convex polygon, by binary searching over the hull vertices.

The code solves the [Saint John Festival - 2015-2016 ACM-ICPC Southwestern Europe Regional Contest][5] problem.

## Sweep Line technique

When dealing with intervals, it is quite useful to process them in an organized way. Using the [Sweep Line technique][2], we are able to organize the segments by order of appearance of their starting and ending points. This makes it easier to check segment interceptions in 2D plane, for example.

In [Sweep-Line](./Sweep-Line.cpp), we're interested solving the [Circuits - 2018 ACM-ICPC Asia Regional Seoul Contest][3] problem, which includes finding information about how many segments are "opened" at each segment starting and ending points.

[1]: https://blog.csdn.net/pocket_lengend/article/details/79475204
[2]: https://cp-algorithms.com/geometry/intersecting_segments.html
[3]: https://codeforces.com/group/NTEJ3kWgR3/contest/267402/attachments/download/7921/20182019-acmicpc-asia-seoul-regional-contest-en.pdf
[4]: https://cp-algorithms.com/geometry/grahams-scan-convex-hull.html
[5]: https://vj.z180.cn/3699857ff0a17d77d1099699cdf4da13?v=1580549044
