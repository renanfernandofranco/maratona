/**
 * This algorithm finds the center and the radius of the circle formed by three points
 * 
 * Note: This algorithm doesn't check if three points are collinear
 */

#include <bits/stdc++.h>

using namespace std;

struct pt{
    double x, y;
    pt(){};
    pt(double _x, double _y): x(_x), y(_y){};

    pt operator +(pt const& p){
        return pt(x + p.x, y + p.y);
    }

    pt operator -(pt const& p){
        return pt(x - p.x, y - p.y);
    }

    pt operator *(pt const & p){
        return pt(x * p.x, y * p.y);
    }

    pt operator /(pt const & p){
        return pt(x / p.x, y / p.y);
    }

    pt operator ~(){
        return pt(y, x);
    }

    friend pt operator *(double val, pt const & p){
        return pt(val * p.x, val * p.y);
    }

    double dot(pt const& p){
        return x * p. x + y * p.y;
    }
};

void findCircle(pt p1, pt p2, pt p3, pt& center, double& radius){
    pt vt12 = p1 - p2;
    pt vt13 = p1 - p3;
    double d31 = p3.dot(p3) - p1.dot(p1);
    double d12 = p1.dot(p1) - p2.dot(p2);
    center = 0.5 * ~(d31 * vt12 + d12 * vt13) / (~vt13 * vt12 - ~vt12 * vt13);
    radius = hypot(center.x - p1.x, center.y - p1.y);
}

int main(){
    pt center;
    double raio;
    findCircle(pt(0, 1), pt(3, 7), pt(13, -10), center, raio);
}