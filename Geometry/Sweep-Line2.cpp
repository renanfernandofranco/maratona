#include <bits/stdc++.h>

/**
 * Sweep Line technique
 * ====================
 * 
 * Given a set of line segments without intersection and vertical segments,
 * do a sweep line keeping a BST(std::set) of the opens segments ordered by y-coordinate
 * 
 * Complexity:
 *      - Time: O(n lg n)
 *      - Space: O(n)
 * 
 */

using namespace std;
#define MAX 1'000'002
#define INF 1'000'000'000
#define INSERT 1
#define DELETE 2

struct Seg {
    static int xSweepLine;
    int xi, yi, xf, yf, id;
    double m;

    Seg(){}
    Seg(int xi, int yi, int xf, int yf, int id):xi(xi), yi(yi), xf(xf), yf(yf), id(id){
        m = (.0 + yf - yi) / (xf - xi);
    }

    double eval(int x) const{
        return m * (x - xi) + yi;
    }

    // Compare two segments, assuming that they including the point x
    bool operator < (const Seg& s2) const{ 
        return eval(xSweepLine) < s2.eval(xSweepLine);
    }
};

struct Event{
    Seg seg;
    int type, x;

    Event(){}

    Event(Seg seg, int x, int type): seg(seg), type(type), x(x){}

    // Attention!!! Check if this ordering is correct for the problem
    bool operator < (const Event& ev) const{
        return x != ev.x ? x < ev.x : 
            (type != ev.type ? type < ev.type : 
                (type == INSERT ? seg.eval(x) > ev.seg.eval(x) : seg.eval(x) < ev.seg.eval(x)));
    }
};

int Seg::xSweepLine = 0;

int main(){
    cin.sync_with_stdio(0);
    cin.tie(0);
    int i, n, xi, xf, yi, yf;
    int& x = Seg::xSweepLine;
    set<Seg> opens;
    vector<Seg> segs;
    vector<Event> events;

    cin >> n;

    segs.resize(n + 1);
    events.resize(n << 1);

    for(i = 0; i < n; i++){
        cin >> xi >> yi >> xf >> yf;

        if (xi > xf){
            swap(xi, xf);
            swap(yi, yf);
        }
        
        segs[i] = Seg(xi, yi, xf, yf, i);
        events[i << 1] = Event(segs[i], xi, INSERT);
        events[i << 1 | 1] = Event(segs[i], xf, DELETE);
    }

    // this line represents the sky
    segs[n] = Seg(-INF, INF, INF, INF, n);
    opens.insert(segs[n]);

    sort(events.begin(), events.end());

    for(Event event : events){
        Seg seg = event.seg;
        x = event.x;

        if (event.type == INSERT)
            opens.insert(seg);
        else
            opens.erase(opens.find(seg));
    }
}