/**
 * Sweep Line technique
 * ====================
 *  Processes several line segments or intervals dealing with their starting
 *  and ending points.
 * 
 *  Complexity:
 *      - Time: O(n * lg n)
 *      - Space: O(n)
 * 
 *  Problem origin:
 *    Circuits - 2018 ACM ICPC Asia Regional - Seoul
 * 
 *  Notes:
 *    A Lazy Segment Tree has also been used to solve this particular problem.
 */

#include <bits/stdc++.h>
using namespace std;

#define L(x) (x << 1)
#define R(x) ((x << 1) + 1)

using ii = pair<int, int>;

struct SegTree {
  vector<int> lazy, t, &arr;
  int n;

  SegTree(vector<int> &v, int n): arr(v), n(n) {
    t.resize(n << 2);
    lazy.assign(n << 2, 0);

    build(1, 0, n - 1);
  }

  void build(int v, int l, int r) {
    if (l == r)
      return void(t[v] = arr[l]);

    int m = (l + r) >> 1;
    build(L(v), l, m);
    build(R(v), m + 1, r);

    t[v] = max(t[R(v)], t[L(v)]);
  }

  void push(int v) {
    t[R(v)] += lazy[v];
    lazy[R(v)] += lazy[v];
    lazy[L(v)] += lazy[v];
    t[L(v)] += lazy[v];
    lazy[v] = 0;
  }

  void update(int l, int r, int add) {
    update(1, 0, n - 1, l, r, add);
  }

  void update(int v, int tl, int tr, int l, int r, int add) {
    if (l > r) return;

    if (tl == l && tr == r) {
      t[v] += add;
      lazy[v] += add;
      return;
    }

    push(v);

    int m = (tl + tr) >> 1;
    update(L(v), tl, m, l, min(r, m), add);
    update(R(v), m + 1, tr, max(l, m + 1), r, add);

    t[v] = max(t[R(v)], t[L(v)]);
  }

  int query(int l, int r) {
    return query(1, 0, n - 1, l, r);
  }

  int query(int v, int tl, int tr, int l, int r) {
    if (l > r) return INT_MIN;

    if (tl == l && tr == r) return t[v];

    push(v);

    int m = (tl + tr) >> 1;
    return max(query(L(v), tl, m, l, min(r, m)), query(R(v), m + 1, tr, max(l, m + 1), r));
  }
};

/**
 * Sweep line event object
 */
struct Event {
  int x;  // spatial point location
  int t;  // starting (+1) or ending (-1) point identification
  int id; // segment id
  Event() {}
  Event(int x, int t, int id): x(x), t(t), id(id) {}
  bool operator<(Event const &b) {
    return x == b.x ? t > b.t : x < b.x;
  }
};

int main() {
  cin.sync_with_stdio(false);
  cin.tie(NULL);

  int n;

  cin >> n;

  vector<Event> v(n << 1);

  int a, b, c, d;

  for (int i = 0; i < n; i++) {
    cin >> a >> b >> c >> d;

    v[i << 1] = Event(d, +1, i);
    v[(i << 1) + 1] = Event(b, -1, i);
  }

  sort(v.begin(), v.end());

  vector<int> arr(n << 1);
  vector<ii> sq(n);

  int cnt = 0;
  for (int i = 0; i < (n << 1); i++) {
    /**
     * Here's where sweep line technique logic might take place.
     * 
     * In this case, only a counting process is done, along with an
     * identification of the starting and ending point of each
     * segment (for later use in the SegTree).
     */
    arr[i] = (v[i].t == 1) ? ++cnt : cnt--;
    if (v[i].t == 1)
      sq[v[i].id].first = i;
    else
      sq[v[i].id].second = i;
  }

  SegTree tree(arr, n << 1);

  int ans = INT_MIN;
  for (int i = 0; i < tree.n; i++) {
    int now = arr[i];

    tree.update(sq[v[i].id].first, sq[v[i].id].second, -1);

    now += tree.query(i, tree.n - 1);

    ans = max(ans, now);
  }

  cout << ans << endl;

  return 0;
}
