/**
 * Minkowski Algorithm
 *
 * Given two convex polyhedron P and Q, find polyhedron R
 * such that R = {p + q, for all p \in P and q \in Q}
 *
 * Remember that you can multiply all points' coordinates
 * by a constant to produce another combination beyond p + q
 *
 * It is useful when we have to get symmetric points
 * between P and  Q
 *
 * Complexity:
 * - Time: O(|P| + |Q|)
 * - Space: O(|P| + |Q|)
 *
 * Problem: https://codeforces.com/gym/104252/problem/G
 *
 */

#include <bits/stdc++.h>

using namespace std;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
#define SZ(b) ((int) b.size())
#define all(a) a.begin(), a.end()

using ll = long long;

template <class F>
struct Point{
  F x, y;
  Point() : x(0), y(0) {}
  Point(const F& x, const F& y) : x(x), y(y) {}

  Point<F> operator + (const Point<F>& ot) const{ return Point(x + ot.x, y + ot.y);}
  Point<F> operator - (const Point<F>& ot) const{ return Point(x - ot.x, y - ot.y);}
  template <class G> Point<G> operator * (const G& factor) const{ return Point<G>(x * factor, y * factor);}
  bool operator < (const Point<F>& ot) const {return y < ot.y || (y == ot.y && x < ot.x); }

  // a * b = |a||b| cos c
  F operator * (const Point<F>& ot) const { return x * ot.x + y * ot.y; }
  // a ^ b = |a||b| sin c
  F operator ^ (const Point<F>& ot) const { return x * ot.y - y * ot.x; }

  // < 0 if rhs <- lhs counter-clockwise, 0 if collinear, > 0 if clockwise.
  friend F ccw(const Point<F>& lhs, const Point<F>& rhs) { return rhs ^ lhs;}
  friend F ccw(const Point<F>& lhs, const Point<F>& rhs, const Point<F>& origin){
    return ccw(lhs - origin, rhs - origin);
  }

  friend F abs(Point<F> v) { return v * v; }
};


template<class F>
struct Minkowski{
  vector<Point<F>> v;

  Minkowski(vector<Point<F>> P, vector<Point<F>> Q){
    // the first vertex must be the lowest
    // Using operator that sort by smallest coordinate Y
    rotate(P.begin(), min_element(all(P)), P.end());
    rotate(Q.begin(), min_element(all(Q)), Q.end());
    // we must ensure cyclic indexing
    P.push_back(P[0]);
    P.push_back(P[1]);
    Q.push_back(Q[0]);
    Q.push_back(Q[1]);
    // main part
    int i = 0, j = 0;
    while(i < SZ(P) - 2 || j < SZ(Q) - 2){
      v.push_back(P[i] + Q[j]);
      auto cross = (P[i + 1] - P[i]) ^ (Q[j + 1] - Q[j]);
      if(cross >= 0 && i < SZ(P) - 2)
        ++i;
      if(cross <= 0 && j < SZ(Q) - 2)
        ++j;
    }

    // Preparation to use function In
    auto opX = [](auto p1, auto p2){ return p1.x != p2.x ? p1.x < p2.x : p1.y < p2.y;};
    rotate(v.begin(), min_element(all(v), opX), v.end());
  }
  // v1, v2, v3 need are in clockwise
  bool InTriangle(Point<F> v1, Point<F> v2, Point<F> v3, Point<F> p){
    F f1 = ccw(v1, p, v3);
    F f2 = ccw(v2, p, v3);
    F f3 = ccw(v2, p, v1);
    return f1 <= 0 && f2 >= 0 && f3 <= 0;
  }

  #define ccwR(p1, p2) ccw(p1, p2, v[0])

  // Polygon should be ordered by smallest coordinate X
  // Cannot repeated last point in the end
  bool In(const Point<F>& t) {
    if (ccwR(v[1], t) > 0 || ccwR(v.back(), t) < 0)
      return false;

    if (ccwR(v[1], t) == 0)
        return abs(v[1] - v[0]) >= abs(t - v[0]);

    int r = lower_bound(v.begin(), v.end(), t, [this](auto p, auto t)
      {return ccwR(p, t) < 0;}) - v.begin();

    return InTriangle(v[r - 1], v[r], v[0], t);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n;
  cin >> n;

  vector<Point<ll>> P1(n), P2(n), P3(n);
  FOR(i, n){
    cin >> P1[i].x >> P1[i].y;
    P2[i] = P1[i] * 2LL;
    P3[i] = P1[i] * -1LL;
  }

  int m;
  cin >> m;
  vector<Point<ll>> Q1(m), Q2(m), Q3(m);
  FOR(i, m){
    cin >> Q1[i].x >> Q1[i].y;
    Q2[i] = Q1[i] * 2LL;
    Q3[i] = Q1[i] * -1LL;
  }

  Minkowski<ll> PolyMid(P1, Q1), PolyLeft(P3, Q2), PolyRigth(P2, Q3);

  int t;
  cin >> t;
  string ans;
  FOR(i, t){
    Point<ll> cur;
    cin >> cur.x >> cur.y;
    bool ret = PolyMid.In(cur * 2LL) || PolyLeft.In(cur)  || PolyRigth.In(cur);
    ans.push_back(ret ? 'Y' : 'N');
  }
  cout << ans << endl;
}
