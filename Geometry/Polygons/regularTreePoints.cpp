/**
 * Given three points not collinear, this algorithm finds the regular
 * polygon with smallest area that contain the points as vertices
 * 
 * Complexity:
 *    - Time: O(n), where n is the amount of sides of polygon of the answer
 *    - Space: O(1)
 */

#include <bits/stdc++.h>

using namespace std;
#define EPS 1e-6
#define toRange(i) min(max(i, -1.0), 1.0)
const double PI = acos(-1);

struct pt{
   double x, y;
   pt(){};
   pt(double _x, double _y): x(_x), y(_y){};

   pt operator +(pt const& p){
      return pt(x + p.x, y + p.y);
   }

   pt operator -(pt const& p){
      return pt(x - p.x, y - p.y);
   }

   pt operator *(pt const & p){
      return pt(x * p.x, y * p.y);
   }

   pt operator /(pt const & p){
      return pt(x / p.x, y / p.y);
   }

   pt operator ~(){
      return pt(y, x);
   }

   friend pt operator *(double val, pt const & p){
      return pt(val * p.x, val * p.y);
   }

   double dot(pt const& p){
      return x * p. x + y * p.y;
   }
};

void findCircle(pt p1, pt p2, pt p3, pt& center, double& radius){
   pt vt12 = p1 - p2;
   pt vt13 = p1 - p3;
   double d31 = p3.dot(p3) - p1.dot(p1);
   double d12 = p1.dot(p1) - p2.dot(p2);
   center = 0.5 * ~(d31 * vt12 + d12 * vt13) / (~vt13 * vt12 - ~vt12 * vt13);
   radius = hypot(center.x - p1.x, center.y - p1.y);
}

double angle(pt vt1, pt vt2){
   double norm1 = hypot(vt1.x, vt1.y);
   double norm2 = hypot(vt2.x, vt2.y);
   return acos(toRange(vt1.dot(vt2) / (norm1 * norm2)));
}

bool isEqual(double n1, double n2){
   return abs(n1 - n2) / n2 < EPS;
}

bool isDivisible(double ang1, double ang2, double deno){
   double ang3 = 2 * PI - ang1 - ang2;
   if(!isEqual(round(ang1 / deno), ang1 / deno))
      return false;
   if(!isEqual(round(ang2 / deno), ang2 / deno))
      return false;
   if(!isEqual(round(ang3 / deno), ang3 / deno))
      return false;
   return true;
}

int main(){
   pt p1, p2, p3, cent;
   int i, sides;
   double ang, ang1, ang2, ang3, deno, area, radius;
   cin >> p1.x >> p1.y;
   cin >> p2.x >> p2.y;
   cin >> p3.x >> p3.y;

   findCircle(p1, p2, p3, cent, radius);

   ang1 = angle(p1 - cent, p2 - cent);
   ang2 = angle(p3 - cent, p1 - cent);
   ang3 = angle(p2 - cent, p3 - cent);

   if(ang1 > ang2)
      swap(ang1, ang2);
   if(ang2 > ang3)
      swap(ang2, ang3);

   for(i = 3; i <= 100; i++){
      deno = PI - (i - 2) * PI / i;
      if(isDivisible(ang1, ang2, deno)){
         sides = i;
         break;
      }
   }

   ang = (sides - 2) * PI / sides;
   area = radius * radius * sin(ang) * sides / 2;
   cout << setprecision(8) << fixed << area << endl;
}
