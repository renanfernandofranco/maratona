/**
 * Divide and Conquer Optimization
 *
 * Given a vector v, find max {(v[j] + v[i]) * (j - i)},
 * with i < j.
 *
 * The problem can be reduced to find the rectangle with the
 * maximum area, with (i, -v[i]) being the left-bottom corner
 * and (j, v[j]) being the right-top corner.
 *
 * Build a vector lb with the left-bottom corners, where
 * lb[i].first < lb[j].first for i < j.
 *
 * Build a vector up with the right-top corners, where
 * up[i].first > up[j].first for i < j.
 *
 * The matching vector with the optimal answers for vector lb is
 * monotone, in other words, ans[lb[i]] <= ans[lb[j]] for i < j,
 * so we can use divide and conquer.
 *
 * Complexity:
 *  - Time: O(n lg n)
 *  - Space: O(n)
 *
 * https://codeforces.com/gym/102920/problem/L
 *
 * */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
using pll = pair<ll, ll>;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)
#define all(a) a.begin(), a.end()

ll best = LONG_LONG_MIN;
vector<pll> lb, up;

void divConq(int l, int r, int optl, int optr){
	if(l > r)
		return;

	int m = (l + r) >> 1;
	int opt = 0;
	ll curbest = LONG_LONG_MIN;
	for(int k = optl; k <= optr; k++){
		ll ans = (up[k].first - lb[m].first) * (up[k].second - lb[m].second);
		if(ans > curbest){
			curbest = ans;
			opt = k;
		}
	}

	best = max(best, curbest);
	divConq(l, m - 1, optl, opt);
	divConq(m + 1, r, opt, optr);
}

int main(){
	cin.tie(0)->sync_with_stdio(0);

	int n;
	cin >> n;
	vector<ll> v(n);

	FOR(i, n)
		cin >> v[i];

	FOR(i, n){
		while(!up.empty() && up.back().first <= v[i])
			up.pop_back();
		up.push_back({v[i], i + 1});
	}

	REV(i, n){
		while(!lb.empty() && lb.back().first >= -v[i])
			lb.pop_back();
		lb.push_back({-v[i], i + 1});
	}
	reverse(all(lb));

	divConq(0, lb.size() - 1, 0, up.size() - 1);
	cout << best << endl;
}
