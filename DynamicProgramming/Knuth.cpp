/**
 * Knuth's Optimization
 *
 * Original Recurrence:
 *  dp[i][j] = min_(i < k < j) {dp[i][k] + dp[k][j] + C[i][j]}, where dp[i][j]
 * is the answer to the range [i, j)
 *
 * Necessary Condition: mid[i][j-1] <= mid[i][j] <= mid[i+1][j], where mid[i][j] is the
 * smallest index i < k* < j that minimizes dp[i][k] + dp[k][j]
 *
 * A sufficient (but not necessary) condition is:
 *  C[a][c] + C[b][d] <= C[a][d] + C[b][c] and C[b][c] <= C[a][d], where a <= b <= c <= d
 *
 * Complexity:
 *  - Time : O(N^2), while the naive approach is O(n^3)
 *  - Space : O(N^2)
 *
 * This Optimization not is cache-friendly, then for n = 5000 the run time is 700ms
 *
 **/

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
using plli = pair<ll, int>;

#define FOR(i, b) for(int i = 0; i < (int)(b); i++)

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n;
  cin >> n;

  vector<int> v(n);
  FOR(i, n)
    cin >> v[i];

  vector<vector<plli>> dp(n + 1, vector<plli>(n + 1));
  vector<ll> pref(n + 1);

  FOR(i, n)
    pref[i + 1] = pref[i] + v[i];

  for (int l = 0; l <= n; l++) {
    for (int i = 0; i + l <= n; i++) {
      int j = i + l;
      if (l < 2) {
        dp[i][j] = {0, i};
        continue;
      }

      dp[i][j] = {LONG_LONG_MAX / 3, 0};
      for (int k = dp[i][j - 1].second; k <= dp[i + 1][j].second; k++)
        dp[i][j] = min(dp[i][j], {dp[i][k].first + dp[k][j].first + pref[j] - pref[i], k});
    }
  }
  cout << dp[0][n].first << '\n';
}
