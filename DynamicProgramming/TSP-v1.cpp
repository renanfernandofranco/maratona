/**
 * TSP with recovering of the path - Maximization Problem
 *
 * This implementation find the maximum path with m cities
 *
 * Complexity:
 *  - Time : O(n^2 * 2^n)
 *  - Space: O(n * 2^n)
 *
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define isOn(S, j) (S & (1 << j))
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define MAX 18

ll dp[1 << MAX][MAX + 1];
ll C[MAX + 1][MAX]; // C[i][j] is the cost of go from city i to city j
ll vl[MAX]; // Cost of include the city i

ll TSP(int n, int m){
  FOR(st, 1 << n)
    FOR(j, n)
      if(isOn(st, j))
        FOR(i, n + 1)
          dp[st][i] = max(dp[st][i], dp[st ^ (1 << j)][j] + C[i][j] + vl[j]);

  // Note that the minimum value is 0, you might want it to be -INF
  // In this case, remember to start the DP correctly
  ll best = 0;
  FOR(st, 1 << n)
    if(__builtin_popcount(st) == m) // Find the best answer with m cities
      best = max(best, dp[st][n]); // N is an extra city used as a starting point
  return best;
}
