/**
 * Count number of times that each digit appear in the range [ini, fim]
 * excluding leading zeros
 * 
 * Complexity:
 *  - Time: O(800 * |log10 (n)|)
 *  - Space: O(400 * |log10 (n)|)
 * 
 * It's safe to use it with numbers up to 10^18
 * 
 * Uri: 1138
 */

#include <bits/stdc++.h>

using namespace std;    
using ll = long long;

#define MAX 20
vector<int> num;
int N;
ll dp[MAX][2][2];
ll dpDig[MAX][2][2][10];

/**
 * dp - store the amount of numbers of a given state
 * dpDig - store the answer of a given state
 * lim - indicate if the current index is limited by the number of the input
 * has - indicate if there is some digit different of zero to the left
 */
ll solve(int idx, bool lim, bool has) {
    if(idx == N)
        return has;
    if (dp[idx][lim][has] != -1)
        return dp[idx][lim][has];

    ll& dp1 = dp[idx][lim][has] = 0;
    ll* dpDig1 = dpDig[idx][lim][has];
    
    int maxDig = lim ? num[idx] : 9;

    for(int dig = 0; dig <= maxDig; dig++){
        bool lim2 = num[idx] == dig ? lim : false;
        bool has2 = dig || has;

        ll dp2 = solve(idx + 1, lim2, has2);
        dpDig1[dig] += has2 ? dp2 : 0;
        dp1 += dp2;
        
        for(int i = 0; i < 10; i++)
            dpDig1[i] += dpDig[idx + 1][lim2][has2][i];
    }

    return dp1;
}

int sliceNum(ll n){
    num.clear();
    while(n > 0){
        num.push_back(n % 10);
        n /= 10;
    }
    reverse(num.begin(), num.end());
    return num.size();
}

vector<ll> run_dp(ll n){
    N = sliceNum(n);

    memset(dp, -1, sizeof(dp));
    memset(dpDig, 0 , sizeof(dpDig));
    solve(0, true, false);

    ll* v = dpDig[0][true][false];
    return vector<ll> (v, v + 10);
}

void query(ll low, ll high){
    low = max(0LL, low - 1);
    
    auto h = run_dp(high);
    auto l = run_dp(low);

    for(int i = 0; i < 9; i++)
        cout << h[i] - l[i] << ' ';
    cout << h[9] - l[9] << endl;
}

int main(){
    cin.sync_with_stdio(0);
    cin.tie(0);
    
    ll ini, fim;
    while(cin >> ini >> fim, ini != 0 || fim != 0){
        query(ini, fim);
    }
}