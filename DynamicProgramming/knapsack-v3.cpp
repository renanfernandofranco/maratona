/**
 * Knapsack Problem with demands
 *
 * In this version we have f[i] copies of the item i
 *
 * Let N = sum_{1 <= i <= n} (lg f[i])
 *
 * Complexity:
 *  - Time : O(NW)
 *  - Space: O(N + W)
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
using ll = long long;
#define FOR(i, b) for(int i = 0; i < (b); i++)

template<class T>
struct Knapsack{
  int n, W;
  vector<T> dp, vl;
  vi wg;

  Knapsack(int n_, int W): n(0), W(W),
    dp(W + 1, 0), vl(n_), wg(n_){}

  void Add(T v, int w){
    vl[n] = v;
    wg[n++] = w;
  }

  T Solve(){
    FOR(i, n)
      for(int sz = W; sz >= wg[i]; sz--)
        dp[sz] = max(dp[sz], dp[sz - wg[i]] + vl[i]);
    return dp[W];
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, W;
  cin >> n >> W;

  vector<pair<ll, ll>> items;
  vi wgs(n), vls(n), freqs(n);

  FOR(i, n)
    cin >> wgs[i];
  FOR(i, n)
    cin >> vls[i];
  FOR(i, n)
    cin >> freqs[i];

  FOR(i, n){
    ll vl = vls[i], wg = wgs[i], freq = freqs[i];

    for(int b = 0; (1 << b) <= freq; b++){
      items.push_back({vl << b, wg << b});
      freq -= (1 << b);
    }

    while(freq){
      if(freq & 1)
        items.push_back({vl, wg});
      freq >>=1;
      vl <<= 1;
      wg <<= 1;
    }
  }

  Knapsack<ll> ks(items.size(), W);
  for(auto p : items)
    ks.Add(p.first, p.second);

  cout << ks.Solve() << endl;
}
