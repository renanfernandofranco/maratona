/**
 * Nim with random piles
 *
 * Given three piles, with sizes chose uniformly in [Li, Ri], for 1 <= i <= 3,
 * find the probability of player 1 win a nim with these piles.
 *
 * Complexity:
 *  - Time/Space: O(log(max(Ri + MOD)))
 *
 */

#include <bits/stdc++.h>

using namespace std;
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
using vi = vector<int>;
using ll = long long;
using vll = vector<ll>;
using ii = pair<int, int>;

ll l1, r1, l2, r2, l3, r3;
ll dp[30][2][2][2][2][2][2][2];
#define isOn(a, i) ((a) & (1 << i))

const ll MOD = 1e9 + 7;

ll solve(int i, bool ga, bool la, bool gb, bool lb, bool gc, bool lc, bool none){
    if(i < 0)
        return !none;

    ll& p = dp[i][ga][la][gb][lb][gc][lc][none];
    if(p != -1)
        return p;

    p = 0;
    FOR(a, 2)
        FOR(b, 2)
            FOR(c, 2){
                if(ga && a == 0 && isOn(l1, i))
                    continue;
                if(gb && b == 0 && isOn(l2, i))
                    continue;
                if(gc && c == 0 && isOn(l3, i))
                    continue;

                if(la && a == 1 && !isOn(r1, i))
                    continue;
                if(lb && b == 1 && !isOn(r2, i))
                    continue;
                if(lc && c == 1 && !isOn(r3, i))
                    continue;

                p += solve(i - 1, !(!isOn(l1, i) && a == 1) && ga, !(isOn(r1, i) && a == 0) && la,
                                    !(!isOn(l2, i) && b == 1) && gb, !(isOn(r2, i) && b == 0) && lb,
                                    !(!isOn(l3, i) && c == 1) && gc, !(isOn(r3, i) && c == 0) && lc, none && ((a + b + c) % 2 == 0));
                p %= MOD;
            }
    return p;
}

ll inv(ll i){
    if(i == 1) return 1;
    return (MOD - ((MOD / i) * inv(MOD % i)) % MOD + MOD) % MOD;
}

int main(){
    cin.tie(0)->sync_with_stdio(0);

    cin >> l1 >> r1 >> l2 >> r2 >> l3 >> r3;

    memset(dp, -1, sizeof(dp));

    ll num = solve(29, true, true, true, true, true, true, true);

    ll dem = ((inv(r1 - l1 + 1) * inv(r2 - l2 + 1) % MOD) * inv(r3 - l3 + 1)) % MOD;

    cout << (num * dem) % MOD << endl;

    return 0;
}
