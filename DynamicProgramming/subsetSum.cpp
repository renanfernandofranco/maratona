/**
 * Given a set of n elements and value W, this algorithm
 * check if exists a subset with sum equal to W
 *
 * Complexity:
 *  - Time: O (n * W / 32)
 *  - Space: O(W / 32)
 *
 */

#include<bits/stdc++.h>

using namespace std;
#define MAXW 100'002
#define FOR(i, b) for(int i = 0; i < (b); i++)

bool subsetSum(vector<int>& items, int obj){
  bitset<MAXW> dp(1); // dp[0] = 1

  for(auto it: items)
    dp |= dp << it;

  return dp[obj];
}

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int obj, n;
  cin  >> obj >> n;
  vector<int> its(n);

  FOR(i, n)
    cin >> its[i];

  cout << (subsetSum(its, obj)? "S\n": "N\n");
}
