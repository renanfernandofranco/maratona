/**
 * Line Container - Line AddLine, Get Max
 *
 * For get the min, insert the line ax + b like -ax - b,
 * and change sign of the answer
 *
 * Complexity:
 *  - Time: O(log n) amortized
 *  - Space: O(n)
 *
 * https://judge.yosupo.jp/problem/line_AddLine_get_min
 *
 */

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
#define endl '\n'
#define FOR(i, b) for(int i = 0; i < b; i++)

struct Line {
  mutable ll k, m, p;
  bool operator<(const Line& o) const { return k < o.k; }
  bool operator<(ll x) const { return p < x; }
};

struct LineContainer : multiset<Line, less<>> {
  // (for doubles, use INF = 1/.0, div(a,b) = a/b)
  static const ll INF = LLONG_MAX;
  ll div(ll a, ll b) { // floored signed division
    return a / b - ((a ^ b) < 0 && a % b); }

  bool isect(iterator x, iterator y) {
    if (y == end()) return x->p = INF, 0;

    x->p = (x->k == y->k) ?
        (x->m > y->m ? INF : -INF):
        div(y->m - x->m, x->k - y->k);
    return x->p >= y->p;
  }

  void AddLine(ll k, ll m) {
    auto z = insert({k, m, 0}), y = z++, x = y;

    while (isect(y, z))
      z = erase(z);

    if (x != begin() && isect(--x, y))
      isect(x, y = erase(y));

    while ((y = x) != begin() && (--x)->p >= y->p)
      isect(x, erase(y));
  }

  ll GetMax(ll x) {
    assert(!empty());
    auto l = *lower_bound(x);
    return l.k * x + l.m;
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, q, tp;
  ll a, b, x;
  cin >> n >> q;

  LineContainer lc;

  FOR(i, n){
    cin >> a >> b;
    lc.AddLine(-a, -b);
  }

  FOR(i, q){
    cin >> tp;
    switch(tp){
      case 0:
        cin >> a >> b;
        lc.AddLine(-a, -b);
        break;
      case 1:
        cin >> x;
        cout << -lc.GetMax(x) << endl;
    }
  }
}
