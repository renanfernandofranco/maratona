/**
 * Convex Hull Optimization 2 - Minimum Query
 *
 * Original Recurrence:
 *  dp[i] = min_(i < j) {dp[j] + b[j] * a[i]}
 *
 * Neccesary and Sufficient Condition of Applicability:
 *  b[j] >= b[j + 1]
 *
 * Note that:
 *  dp[j] is the linear coeficient of the line
 *  b[j] is the angular coeficient of the line
 *  a[i] is the point where the line is being evaluated
 *
 * Complexity:
 *  - Time : O((N + Q) * lg N)
 *  - Space : O(N)
 *
 * Problem: https://cses.fi/problemset/task/2084
 *
 * ATTETION!!! Inputs with values greater than 10^6 can cause overflow
 *
 * */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
using pll = pair<ll, ll>;

#define all(a) a.begin(), a.end()

struct CHT{
  struct line{
    ll m, b;
    line(ll m = 0, ll b = 0): m(m), b(b){}
    friend bool cross(line bb, line lb, line cur){
      return (cur.m - bb.m) * (bb.b - lb.b) >= (bb.b - cur.b) * (lb.m - bb.m);
    }
    ll operator ()(ll x){ return x * m + b; }
  };

  vector<line> v;

  void add(ll m, ll b){
    if(!v.empty() && v.back().m == m){
      if(v.back().b > b)
        v.pop_back();
      else
        return;
    }

    line cur(m, b);
    while(v.size() >= 2 && cross(v[v.size() - 2], v.back(), cur))
      v.pop_back();

    v.push_back(cur);
  }

  ll query(ll x){
    int l = 0;
    int r = v.size() - 1;
    while (l < r){
      int m = (l + r) >> 1;
      if (v[m](x) <= v[m + 1](x))
        r = m;
      else
        l = m + 1;
    }
    return v[l](x);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, x;
  cin >> n >> x;
  n++;

  vector<ll> dp(n);
  vector<pll> v(n);

  v[0].first = x;
  for(int i = 1; i < n; i++)
    cin >> v[i].second;

  for(int i = 1; i < n; i++)
    cin >> v[i].first;

  CHT cht;
  dp[0] = 0;
  cht.add(v[0].first, 0);

  for(int i = 1; i < n; i++){
    dp[i] = cht.query(v[i].second);
    cht.add(v[i].first, dp[i]);
  }
  cout << dp[n - 1] << endl;
}
