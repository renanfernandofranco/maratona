/**
 * Knapsack Problem
 *
 * Complexity:
 *  - Time : O(nW)
 *  - Space: O(n + W)
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
#define FOR(i, b) for(int i = 0; i < (b); i++)

template<class T>
struct Knapsack{
  int n, W;
  vector<T> dp, vl;
  vi wg;

  Knapsack(int n_, int W): n(0), W(W),
    dp(W + 1, 0), vl(n_), wg(n_){}

  void Add(T v, int w){
    vl[n] = v;
    wg[n++] = w;
  }

  T Solve(){
    FOR(i, n)
      for(int sz = W; sz >= wg[i]; sz--)
        dp[sz] = max(dp[sz], dp[sz - wg[i]] + vl[i]);
    return dp[W];
  }
};


int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, W, vl, wg;
  cin >> n >> W;
  Knapsack<int> ks(n, W);

  FOR(i, n){
    cin >> vl >> wg;
    ks.Add(vl, wg);
  }

  cout << ks.Solve() << endl;
}
