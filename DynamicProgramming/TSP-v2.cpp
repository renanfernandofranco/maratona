/**
 * TSP with recovering of the path - Minimization Problem
 *
 * This implementation find the minimum path, not the minimum cycle
 *
 * Complexity:
 *	- Time : O(n^2 * 2^n)
 *	- Space: O(n * 2^n)
 *
 */

#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define isOn(S, j) (S & (1 << j))
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define MAX 18

ll dp[1 << MAX][MAX + 1];
char child[1 << MAX][MAX + 1];
int C[MAX + 1][MAX]; // C[i][j] is the cost of go from city i to city j

void TSP(int n){
  memset(dp, 0x3f, sizeof(dp)); // Set the dp with infinity
  memset(dp[0], 0, sizeof(dp[0])); // Set all "states 0" with 0
  FOR(st, 1 << n)
    FOR(j, n)
      if(isOn(st, j))
        FOR(i, n + 1){
          auto nxt = dp[st ^ (1 << j)][j] + C[i][j];
          if(nxt < dp[st][i]){
            dp[st][i] = nxt;
            child[st][i] = j;
          }
        }

  int st = (1 << n) - 1;
  int cur = n; // N is an extra city used as a starting point
  cout << dp[st][cur] << endl; // Print the solution value

  // Print the path
  FOR(i, n){
    int cd = child[st][cur];
    cout << cd + 1 << endl;
    st ^= 1 << cd;
    cur = cd;
  }
}
