/**
 * Divide and Conquer Optimization
 *
 * Given an array of n elements, your task is to divide into k subarrays.
 * The cost of each subarray is the square of the sum of the values in
 * the subarray. What is the minimum total cost if you act optimally?
 *
 * Recurrence:
 *  dp[k][r] = min_(l <= r) {dp[k - 1][l - 1] + C(l, r)}, where
 *  dp[k][r] is the answer to prefix [0, r] using k subarrays.
 *
 * Let opt(k, r) be the value l that minimize the above expression. As
 * opt(k, r) <= opt(k, r + 1), then we can apply the Divide-and-Conquer DP.
 *
 * Complexity:
 *  - Time: O(n*k lg n)
 *  - Space: O(n)
 *
 * Problem: https://cses.fi/problemset/task/2086
 *
 **/

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using vll = vector<ll>;
#define FOR(i, b) for(int i = 0; i < b; i++)

vll dpBef, dpCur, pref;

ll C(int l, int r){
  return (pref[r + 1] - pref[l]) * (pref[r + 1] - pref[l]);
}

void compute(int l, int r, int optl, int optr) {
  if (l > r)
    return;

  int mid = (l + r) >> 1;
  pair<ll, int> best = {LLONG_MAX, -1};

  for (int k = optl; k <= min(mid, optr); k++)
    best = min(best, {(k ? dpBef[k - 1] : 0) + C(k, mid), k});

  dpCur[mid] = best.first;
  int opt = best.second;

  compute(l, mid - 1, optl, opt);
  compute(mid + 1, r, opt, optr);
}

ll solve(int n, int k, vll v) {
  dpBef.resize(n);
  dpCur.resize(n);
  pref.resize(n + 1);

  FOR(i, n){
    pref[i + 1] = pref[i] + v[i];
    dpBef[i] = C(0, i);
  }

  for (int i = 1; i < k; i++) {
    compute(0, n - 1, 0, n - 1);
    dpBef.swap(dpCur);
  }

  return dpBef[n - 1];
}

int main(){
  cin.tie(0)->sync_with_stdio(0);
  int n, k;
  cin >> n >> k;

  vll v(n);
  FOR(i, n)
    cin >> v[i];

  cout << solve(n, k, v) << endl;
}
