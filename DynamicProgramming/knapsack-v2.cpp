/**
 * Knapsack Problem with recovery of the answer
 *
 * Complexity:
 *  - Time : O(nW)
 *  - Space: O(n + W)
 *
 * Technique : https://codeforces.com/blog/entry/47247?#comment-316200
 *
 */

#include <bits/stdc++.h>

using namespace std;
using vi = vector<int>;
#define FOR(i, b) for(int i = 0; i < (b); i++)

template<class T>
struct Knapsack{
  int n, W;
  vector<T> dp, vl;
  vi ans, opt, wg;

  Knapsack(int n_, int W): n(0), W(W),
    dp(W + 1), vl(n_), opt(W + 1), wg(n_){}

  void Add(T v, int w){
    vl[n] = v;
    wg[n++] = w;
  }

  T conquer(int l, int r, int W){
    if(l == r){
      if(W >= wg[l])
        return ans.push_back(l), vl[l];
      return 0;
    }

    FOR(i, W + 1)
      opt[i] = dp[i] = 0;
    int m = (l + r) >> 1;

    for(int i = l; i <= r; i++)
      for(int sz = W; sz >= wg[i]; sz--){
        T dpCur = dp[sz - wg[i]] + vl[i];
        if(dpCur > dp[sz]){
          dp[sz] = dpCur;
          opt[sz] = i <= m ? sz : opt[sz - wg[i]];
        }
      }

    T ret = dp[W];
    int K = opt[W];
    T ret2 = conquer(l, m, K) + conquer(m + 1, r, W - K);
    assert(ret2 == ret);
    return ret;
  }

  T Solve(){
    return conquer(0, n - 1, W);
  }
};

int main(){
  cin.tie(0)->sync_with_stdio(0);

  int n, W, vl, wg;
  cin >> n >> W;
  Knapsack<int> ks(n, W);

  FOR(i, n){
    cin >> vl >> wg;
    ks.Add(vl, wg);
  }

  cout << ks.Solve() << endl;
}
